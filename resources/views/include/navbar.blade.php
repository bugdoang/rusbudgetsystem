<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header">
                <a class="navbar-brand logo" href="/">
                    <div class="logo_img">
                        <img src ="/assets/img/logo.png">
                    </div>
                    <div style="float:left;margin-left:0;">
                        <div class="logo_text_header myfont">ระบบจัดสรรงบประมาณ</div>
                        <div class="logo_text_border"></div>
                        <div class="logo_text_sub myfont">คณะบริหารธุรกิจและเทคโนโลยีสารสนเทศ <span class="icon_i" style="margin: auto"> | </span> RUS</div>
                    </div>
                </a>
            </div>
        </div>



        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle myfont" style="font-size: 18px;" data-toggle="dropdown" role="button" aria-expanded="false"> แผนเสนอของบประมาณ <span class="caret"></span></a>
                            <ul class="dropdown-menu myfont head-text" role="menu">
                                <li><a href="/project-plan"> รายละเอียดประกอบโครงการ </a></li>
                                <li><a href="/durable-article-plan"> รายละเอียดประกอบค่าครุภัณฑ์ </a></li>
                                <!-- <li><a href="/build-plan"> รายละเอียดประกอบค่าที่ดินและสิ่งก่อสร้าง </a></li> -->
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle myfont" style="font-size: 18px;" data-toggle="dropdown" role="button" aria-expanded="false"> ขออนุมัติใช้งบประมาณ <span class="caret"></span></a>
                            <ul class="dropdown-menu myfont head-text" role="menu">
                                <li><a href="/expenditure-budget"> งบประมาณรายจ่าย (งบแผ่นดิน) </a></li>
                                <li><a href="/revenue-budget"> งบประมาณรายได้ (ผลประโยชน์) </a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle myfont" style="font-size: 18px;" data-toggle="dropdown" role="button" aria-expanded="false"> คุมยอดงบประมาณ <span class="caret"></span></a>
                            <ul class="dropdown-menu myfont head-text" role="menu">
                                <li><a href="/expenditure-index"> งบประมาณรายจ่าย (งบแผ่นดิน) </a></li>
                                <li><a href="/revenue-index"> งบประมาณรายได้ (ผลประโยชน์) </a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle myfont" style="font-size: 18px;" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> {{\App\Services\MyProfile::get()->position_abbreviation.' '.Auth::user()->first_name}} <span class="caret"></span></a>
                            <ul class="dropdown-menu myfont head-text" role="menu">
                                <li><a href="/profile"><i class="fa fa-user" aria-hidden="true"></i>
 ข้อมูลส่วนตัว</a></li>
                                <li><a href="/personnel"><i class="fa fa-users" aria-hidden="true"></i>
 ข้อมูลบุคลากร</a></li>
                                <li><a href="/password" class="edit-content"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> เปลี่ยนรหัสผ่าน</a></li>
                                <li><a href="/management"><i class="fa fa-wrench" aria-hidden="true"></i>
 จัดการข้อมูลพื้นฐาน</a></li>
                                <li class="divider"></li>
                                <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>
 ออกจากระบบ</a></li>
                            </ul>
                        </li>
                    </ul>
                @else
                <li class="head-text pull-right myfont">

                </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>