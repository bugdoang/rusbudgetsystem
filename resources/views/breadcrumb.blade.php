    <div class="row">
        <ol class="breadcrumb">
            <li><a class="link-breadcrumb" href="/"><i class="fa fa-home" aria-hidden="true"></i> หน้าแรก</a></li>
            @foreach($items as $index=>$item)
                @if($index==count($items)-1)
                    <li class="active">{{$item['label']}}</li>
                @else
                    <li><a href="{{$item['link']}}">{{$item['label']}}</a></li>
                @endif
            @endforeach
        </ol>
    </div>
