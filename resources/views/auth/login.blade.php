@extends('masterlayout')
@section('title','ล็อคอินเข้าสู่ระบบ ระบบจัดสรรงบประมาณ')

@section('content')
<div class="row">
    <div class="container-login" style="width:40%;margin:0 auto;margin-top: 60px;">
        <div class="col-md-12">
            <h2 class="header-text-login myfont"><i class="fa fa-lock" aria-hidden="true"></i> เข้าใช้งานระบบ / sign in</h2>
            <form class="form-horizontal no-ajax" style="border:1px solid #e0e0e0; padding: 15px; background-color: #e0e0e0; margin-top: -9px;" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                    <input class="form-control" type="text" placeholder="ชื่อเข้าใช้" id=login_id" maxlength="9" name="username">
                </div>
                <span class="help-block"></span>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-lock"></i>
                    </span>
                    <input id="pin_id" class="form-control" type="password" placeholder="รหัสผ่าน" maxlength="6" name="password">
                </div>
                <span class="help-block"></span>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button id="login-btn" class="form-control btn btn-login myfont" type="submit">
                                <i class="fa fa-lock"></i>
                                เข้าสู่ระบบ
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
