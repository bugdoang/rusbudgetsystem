<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 9/23/16
 * Time: 5:59 PM
 */

namespace App\Services;

class UI
{
    public static function breadcrumb($items)
    {
        $html = view('breadcrumb',['items'=>$items]);
        return $html->render();
    }
}