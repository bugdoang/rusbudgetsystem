<?php
/**
 * Created by PhpStorm.
 * User: mahadoang
 * Date: 3/24/16
 * Time: 12:35 PM
 */
namespace App\Services;
use mPDF;
class MahaPDF
{
    public static function html($html='',$preview='',$top=19,$orentation='P')
    {
        include_once dirname(__FILE__).'/mpdf/mpdf.php';
        date_default_timezone_set('Asia/Bangkok');
        if($orentation=='P') $pdf = new mPDF('th','A4',"","",24,15,$top,10,6,3,'P'); // margin left right top bottom
        else $pdf = new mPDF('th','A4-L',"","",10,10,10,10,6,3,'L');
        $css = 'body{font-family:thsarabun;font-size:15pt;letter-spacing:0px;}.fa{font-family:fa;font-size:15px;}span{body{font-family:thsarabun;font-size:15pt;}';
        $pdf->WriteHTML($css,1);
        $pdf->WriteHTML(trim($html));
        if(empty($preview))
        {
            $path_inner = './downloads/'.date('Ym');
            if(!is_dir($path_inner))
            {
                mkdir($path_inner);
                chmod($path_inner,0777);
            }
            $path = $path_inner.'/'.md5(microtime()).'.pdf';
            $pdf->Output($path);
            return $path;
        }
        else
        {
            $pdf->Output();
        }
    }
}