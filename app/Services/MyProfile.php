<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 9/23/16
 * Time: 5:59 PM
 */

namespace App\Services;
use DB;
class MyProfile
{
    public static function get()
    {
    	$personnel_id = \Auth::id();
        return DB::table('personnels')
        ->leftJoin('positions','positions.position_id','=','personnels.position_id')
        ->where('personnel_id',$personnel_id)
        ->first();
    }
}