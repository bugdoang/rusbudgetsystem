<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $a = 1;
        $modules   = [];
        $modules[] = '\App\Modules\Home\ModuleServiceProvider';
        $modules[] = '\App\Modules\Profile\ModuleServiceProvider';
        $modules[] = '\App\Modules\Plan\ModuleServiceProvider';
        $modules[] = '\App\Modules\Approval\ModuleServiceProvider';
        $modules[] = '\App\Modules\Control\ModuleServiceProvider';
        $modules[] = '\App\Modules\Personnel\ModuleServiceProvider';
        $modules[] = '\App\Modules\Management\ModuleServiceProvider';
        $modules[] = '\App\Modules\Password\ModuleServiceProvider';



        foreach ($modules as $module){
            $this->app->register($module);
        }

        if($this->app->environment() !== 'production'){
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
