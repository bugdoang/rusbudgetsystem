<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });

        $routes = [];
        $routes[] = 'Modules/Home/routes.php';
        $routes[] = 'Modules/Profile/routes.php';
        $routes[] = 'Modules/Plan/routes.php';
        $routes[] = 'Modules/Approval/routes.php';
        $routes[] = 'Modules/Control/routes.php';
        $routes[] = 'Modules/Personnel/routes.php';
        $routes[] = 'Modules/Management/routes.php';
        $routes[] = 'Modules/Password/routes.php';


        foreach ($routes as $route){
            $router->group([
                'namespace' => $this->namespace, 'middleware' => 'web',
            ], function ($router) use ($route){
                require app_path($route);
            });
        }
    }
}
