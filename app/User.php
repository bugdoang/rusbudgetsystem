<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        = 'personnels';
    protected $appends      = ['id'];
    protected $primaryKey   = 'personnel_id';
    protected $fillable     = [
        'personnel_picture','title_name', 'first_name','username','password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute(){
        return $this->attributes['title_name'].$this->attributes['first_name'];
    }
}