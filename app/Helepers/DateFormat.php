<?php
namespace App\Helepers;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/9/2559
 * Time: 17:08
 */
class DateFormat
{
    public static function thai_to_eng($date)
    {
        if (preg_match("#^(0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/([0-9]{4})$#",$date,$split))
        {
            if($split[3]>2000)
            {
                return ($split[3]-543).'-'.$split[2].'-'.$split[1];
            }
        }
        return '';
    }

    public static function eng_to_thai($date,$spe='-')
    {
        if (preg_match("#^([0-9]{4})$spe(0[1-9]|1[0-2])$spe(0[1-9]|[1-2][0-9]|3[0-1])$#",$date,$split))
        {
            if($split[1]<2500)
            {
                return ($split[3]).'/'.$split[2].'/'.($split[1]+543);
            }
        }
        return '';
    }
    public static function thaiDate($my_date){//ฟังก์ชั่น แปลงวันที่จาก ค.ศ. เป็น พ.ศ.
        if(empty($my_date))  return '';
        list($Y,$m,$d) = explode('-',$my_date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y+543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
        return $d."/".$m."/".$Y;
    }
    public static function engDate($my_date){//ฟังก์ชั่น แปลงวันที่จาก พ.ศ. เป็น ค.ศ.
        if(empty($my_date))  return '';
        list($d,$m,$Y) = explode('/',$my_date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y-543; // เปลี่ยน พ.ศ. เป็น ค.ศ.
        return $Y."-".$m."-".$d;
    }

    public static function get_time($start=0,$end=24)
    {
        $times = array();
        for($i=$start;$i<$end;$i++)
        {
            $times[] =$i.'.00';
            $times[] =$i.'.15';
            $times[] =$i.'.30';
            $times[] =$i.'.45';
        }

        return $times;
    }
    public static function Date_Clock($my_date){//ฟังก์ชั่น แปลงวันที่จาก ค.ศ. เป็น พ.ศ.
        if(empty($my_date))  return '';
        list($date)=explode(' ',$my_date);
        list($Y,$m,$d) = explode('-',$date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y+543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
//        list($H,$i) = explode(':',$time);
        return $d."/".$m."/".$Y;
    }

    public static function getMonthName($month)
    {
        $months = config('myconfig.months');
        if(empty($month)) return '';
        if(isset($months[$month-1]))
        {
            return $months[$month-1];
        }
        else
        {
            return '';
        }
    }

}