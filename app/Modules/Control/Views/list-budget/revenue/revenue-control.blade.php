@extends('masterlayout')
@section('title','การคุมยอดงบประมาณรายได้')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การคุมยอดงบประมาณรายได้','link'=>'/revenue-control']]) !!}
{{csrf_field()}}

<!-- Page Content -->
<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">
        <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา</h2>
        <div class="form-planbudget shadow">
            <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="get" action="/revenue-control" >
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ชื่อรายการ : </label>
                    <div class="col-md-12">
                        <input id="project_name" class="form-control input-md" type="text" name="project_name" value="{{$params['project_name'] or ''}}" placeholder="ชื่อรายการ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ประเภทแผนงบประมาณ : </label>
                    <div class="col-md-12">
                        <select class="" name="budget_plan_category" id="budget_plan_category">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($budget_plan_categories))
                                @foreach($budget_plan_categories as $budget_plan_category)
                            <option {{($budget_plan_category==$params['budget_plan_category'])?' selected ':''}} value="{{$budget_plan_category}}">{{$budget_plan_category}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">รายชื่อผู้จัดแผนเสนอของบประมาณ : </label>
                    <div class="col-md-12">
                        <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$params['first_name'] or ''}}" placeholder="รายชื่อผู้จัดแผนเสนอของบประมาณ">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            ค้นหา
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="height:10px;clear:both;"></div>
    </div>
    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการคุมยอดงบประมาณรายได้</h2>
        <div class="form-planbudget shadow">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div style="padding:12px 6px;" class="panel-heading">
                        <span class="myfont" style="font-size: 19px;">รายงานตัดยอดงบประมาณรายได้</span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr style="background-color: #E0FFFF;">
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่จัดแผนเสนอขอทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">
                                        {{number_format($sum_expenditure['sum_1'],2)}}
                                    </td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ได้รับจัดสรรทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">
                                        {{number_format($sum_expenditure['sum_2'],2)}}
                                    </td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr style="background-color: #E0FFFF;">
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ไม่ได้รับอนุมัติจัดแผนเสนอขอทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_3'],2)}}</td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ขออนุมัติใช้ทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_4'],2)}}</td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr style="background-color: #E0FFFF;">
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ได้รับอนุมัติขออนุมัติใช้ทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_5'],2)}}</td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ไม่ได้รับอนุมัติขออนุมัติใช้ทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_6'],2)}}</td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>
                                <tr style="background-color: #E0FFFF;">
                                    <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่รอการขออนุมัติใช้ทั้งหมด</td>
                                    <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_7'],2)}}</td>
                                    <td class="text-center text-middle" style="width: 30%">บาท</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(count($revenue_list)>0)
                <table class="table table-bordered">
                    <tbody align="center">
                        <tr style="background-color: #F5F5F5;">
                            <th class="text-center text-middle" style="width: 5%;" >#</th>
                            <th class="text-center text-middle" style="width: 40%;">ชื่อรายการทั้งหมด</th>
                            <th class="text-center text-middle" style="width: 20%;">งบประมาณที่ได้รับจัดสรร</th>
                            <th class="text-center text-middle" style="width: 15%;">ยอดเงินคงเหลือ</th>
                            <th class="text-center text-middle" style="width: 20%;">รายชื่ออาจารย์ผู้จัดแผน</th>
                            <th class="text-center text-middle" style="width: 8%;">#</th>
                        </tr>
                        @foreach($revenue_list as $index=>$item)
                            <tr>
                                <td class="text-middle text-center">
                                    {{$index+1}}
                                </td>
                                <td class="text-left text-middle">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="project_name" id="project_name">
                                        {{$item->project_name}}
                                    </span>
                                    @else
                                    <span name="durable_article_name" id="durable_article_name">
                                        {{$item->durable_article_name}}
                                    </span>
                                    @endif
                                </td>
                                <td class="text-right text-middle" style="width: 40px;">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="project_num_after" id="project_num_after">{{number_format($item->project_money_approve,2)}}</span>
                                    @else
                                    <span name="money_approve" id="money_approve">{{number_format($item->money_approve,2)}}</span>
                                    @endif
                                </td>
                                <td class="text-right text-middle">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="balance" id="balance">
                                    <?php
                                        $balance = $item->project_money_approve - ($item->money_received_first+$item->money_received_two)
                                    ?>
                                    {{number_format($balance,2)}}
                                    </span>
                                    @else
                                    <span name="balance" id="balance">
                                    <?php
                                        $balance = $item->money_approve - ($item->money_received)
                                    ?>
                                    {{number_format($balance,2)}}
                                    </span>
                                    @endif
                                </td>
                                <td class="text-middle text-left">
                                    {{$item->position_abbreviation}} {{$item->first_name}}
                                </td>
                                <td class="text-middle text-center">
                                    <a class="edit-content" href="/revenue-control/modal/{{$item->budget_plan_id}}" title="รายละเอียด" style="color:#3d3d3d;">
                                        <i class="fa fa-th-list" aria-hidden="true"></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> 
                <div class="text-center">
                    {{$revenue_list->render()}}
                </div>
            @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลคะ ::</span>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

