<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 70%; margin-top: 150px;">

    <!-- Modal content-->
    <form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="POST" action="/expenditure-control/modal/{{$budget_plan_id}}" >
      {{csrf_field()}} 
      <input type="hidden" name="use_prcen" id="use_prcen" value="{{$use_prcen}}">
      <input type="hidden" name="total_money" id="total_money" value="{{$total_money}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title myfont" style="font-size: 22px;">รายละเอียดข้อมูลคุมยอดงบประมาณ</h4>
        </div>
        @if($budgetPlanApproval->budget_plan_category=='โครงการ')
          <br>
          <br>
          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ชื่อโครงการ :</label>
              <div class="col-md-8">
                  <input id="project_name" class="form-control input-md" type="text" name="project_name" value="{{$budgetPlanApproval->project_name or ''}}" placeholder="ชื่อโครงการ">
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">งบประมาณที่ได้รับ :</label>
              <div class="col-md-2">
                  <input id="project_money_approve" class="form-control input-md" type="text" name="project_money_approve" value="{{number_format($budgetPlanApproval->project_money_approve,2)}}" placeholder="งบประมาณที่ได้รับการจัดสรร">
              </div>
              <label class="col-md-1 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ขออนุมัติใช้งบครั้งที่ 1 :</label>
              <div class="col-md-2">
                  <input id="money_received_first" class="form-control input-md" type="text" name="money_received_first" value="{{number_format($budgetPlanApproval->money_received_first,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 1">
              </div>
              <label class="col-md-1 control-label" style="text-align: left;" for="textinput">บาท</label>
              <label class="col-md-2 control-label" for="textinput">สถานะครั้งที่ 1 :</label>
              <div class="col-md-2">
                  <input id="approval_status" class="form-control input-md" type="text" name="approval_status" value="{{$budgetPlanApproval->approval_status}}" placeholder="สถานะครั้งที่ 1">
              </div>
              <div class="col-md-1" style="text-align: center; display: block; margin: top; margin-top: 5px;">
                  <a href="/download?path={{$budgetPlanApproval->file_received_first}}&name={{$budgetPlanApproval-> filename_received_first}}" title="ไฟล์แนบเอกสารครั้งที่ 1">
                      <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                  </a>
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ขออนุมัติใช้งบครั้งที่ 2 :</label>
              <div class="col-md-2">
                  <input id="money_received_two" class="form-control input-md" type="text" name="money_received_two" value="{{number_format($budgetPlanApproval->money_received_two,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 2">
              </div>
              <label class="col-md-1 control-label" style="text-align: left;" for="textinput">บาท</label>
              <label class="col-md-2 control-label" for="textinput">สถานะครั้งที่ 2 :</label>
              <div class="col-md-2">
                  <input id="approval_status_1" class="form-control input-md" type="text" name="approval_status_1" value="{{$budgetPlanApproval->approval_status_1}}" placeholder="สถานะครั้งที่ 2">
              </div>
              <div class="col-md-1" style="text-align: center; display: block; margin: top; margin-top: 5px;">
                  <a href="/download?path={{$budgetPlanApproval->file_received_two}}&name={{$budgetPlanApproval-> filename_received_two}}" title="ไฟล์แนบเอกสารครั้งที่ 2">
                      <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                  </a>
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ยอดเงินคงเหลือ :</label>
              <div class="col-md-2">
                  <input id="balance" readonly class="form-control input-md" type="text" name="balance" value="{{number_format($balance,2)}}" placeholder="ยอดเงินคงเหลือ">
              </div>
              <label class="col-md-1 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">อาจารย์ผู้ขออนุมัติใช้งบ :</label>
              <div class="col-md-2">
                  <input id="position_abbreviation" class="form-control" type="text" name="position_abbreviation" value="{{$budgetPlanApproval->position_abbreviation}}">
              </div>
              <div class="col-md-6">
                  <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$budgetPlanApproval->first_name}}" placeholder="รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ">
              </div>
          </div>
          <br>
          <br>
          <div style="height:10px;clear:both;"></div>
        @else
          <br>
          <br>
          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ชื่อครุภัณฑ์ :</label>
              <div class="col-md-8">
                  <input id="durable_article_name" class="form-control input-md" type="text" name="durable_article_name" value="{{$budgetPlanApproval->durable_article_name or ''}}" placeholder="ชื่อครุภัณฑ์">
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">งบประมาณที่ได้รับ :</label>
              <div class="col-md-7">
                  <input id="money_approve" class="form-control input-md" type="text" name="money_approve" value="{{number_format($budgetPlanApproval->money_approve,2)}}" placeholder="งบประมาณที่ได้รับการจัดสรร">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ขออนุมัติใช้งบ :</label>
              <div class="col-md-7">
                  <input id="money_approve" class="form-control input-md" type="text" name="money_approve" value="{{number_format($budgetPlanApproval->money_approve,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบ">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">สถานะ :</label>
              <div class="col-md-7">
                  <input id="approval_status" class="form-control input-md" type="text" name="approval_status" value="{{$budgetPlanApproval->approval_status}}" placeholder="สถานะ">
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ไฟล์แนบเอกสาร :</label>
              <div class="col-md-1" style="text-align: center; display: block; margin: top; margin-top: 5px;">
                  <a href="/download?path={{$budgetPlanApproval->file_received}}&name={{$budgetPlanApproval-> filename_received}}" title="ไฟล์แนบเอกสาร">
                      <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                  </a>
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">ยอดเงินคงเหลือ :</label>
              <div class="col-md-7">
                  <input id="balance" readonly class="form-control input-md" type="text" name="balance" value="{{number_format($balance,2)}}" placeholder="ยอดเงินคงเหลือ">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-3 control-label" for="textinput">อาจารย์ผู้ขออนุมัติใช้งบ :</label>
              <div class="col-md-2">
                  <input id="position_abbreviation" class="form-control" type="text" name="position_abbreviation" value="{{$budgetPlanApproval->position_abbreviation}}">
              </div>
              <div class="col-md-5">
                  <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$budgetPlanApproval->first_name}}" placeholder="รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ">
              </div>
          </div>
          <br>
          <br>
          <div style="height:10px;clear:both;"></div>

          <br>
          <br>
        @endif
       
      </div>
    </form>
  </div>
</div>
    
<script type="text/javascript">$('.uploadFileExpenditureFirst').each(function(i,e){$(e).uploadFileExpenditureFirst();});</script>
<script type="text/javascript">$('.uploadFileExpenditure').each(function(i,e){$(e).uploadFileExpenditure();});</script>