@extends('masterlayout')
@section('title','การคุมยอดงบประมาณรายจ่าย')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การคุมยอดงบประมาณรายจ่าย','link'=>'/expenditure-index']]) !!}
{{csrf_field()}}

<!-- Page Content -->
<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">
        <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา </h2>
        <div class="form-planbudget shadow">
            <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="get" action="/expenditure-index" >
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ปีงบประมาณ : </label>
                    <div class="col-md-12">
                        <select class="" name="fiscal_year" id="fiscal_year">
                            <option value="">กรุณาเลือก</option>
                             @if(!empty($startYear))
                                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                                    <option
                                        {{ (isset($params['fiscal_year']) && $params['fiscal_year']==$i) ? 'selected':''}} 
                                          value="{{$i}}">{{$i+543}}
                                    </option>
                                    @endfor
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            ค้นหา
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="height:10px;clear:both;"></div>
    </div>
    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการคุมยอดงบประมาณรายจ่าย</h2>
        <div class="form-planbudget shadow">
            
            @if(count($expenditure_list)>0)
            <!--แผนภูมิวงกลม-->
                <div style="margin-bottom: 20px;" class="row">
                    <div class="col-md-4">
                        <div id="chart"></div>
                        <div class="text-middle text-center" style="font-size: 12px;">
                            งบประมาณจัดแผนเสนอขอทั้งหมด :  {{number_format($sum_expenditure['sum_1'],2)}} บาท
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="chart1"></div>
                        <div class="text-middle text-center" style="font-size: 12px;">
                            งบประมาณขออนุมัติใช้ทั้งหมด :  {{number_format($sum_expenditure['sum_4'],2)}} บาท
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="chart2"></div>
                        <div class="text-middle text-center" style="font-size: 12px;">
                            งบประมาณได้รับการจัดสรรทั้งหมด :  {{number_format($sum_expenditure['sum_2'],2)}} บาท
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div style="padding:12px 6px;" class="panel-heading">
                            <span class="myfont" style="font-size: 19px;">รายงานตัดยอดงบประมาณรายจ่าย</span>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr style="background-color: #E0FFFF;">
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่จัดแผนเสนอขอทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">
                                            {{number_format($sum_expenditure['sum_1'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ได้รับจัดสรรทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">
                                            {{number_format($sum_expenditure['sum_2'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท
                                        </td>
                                    </tr>
                                    <tr style="background-color: #E0FFFF;">
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ไม่ได้รับอนุมัติจัดแผนเสนอขอทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_3'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ขออนุมัติใช้ทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_4'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท</td>
                                    </tr>
                                    <tr style="background-color: #E0FFFF;">
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ได้รับอนุมัติขออนุมัติใช้ทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_5'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่ไม่ได้รับอนุมัติขออนุมัติใช้ทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">{{number_format($sum_expenditure['sum_6'],2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท</td>
                                    </tr>
                                    <tr style="background-color: #E0FFFF;">
                                        <td class="text-right text-middle myfont" style="font-size: 18px; width: 40%;">งบประมาณที่รอการขออนุมัติใช้ทั้งหมด</td>
                                        <td class="text-right text-middle" style="width: 30%">
                                        <?php
                                            $total_sum = round($sum_expenditure['sum_7']);
                                        ?>
                                        {{number_format($total_sum,2)}} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp บาท</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered">
                    <tbody align="center">
                        <tr style="background-color: #F5F5F5;">
                            <th class="text-center text-middle" style="width: 5%;" >#</th>
                            <th class="text-center text-middle" style="width: 40%;">ชื่อรายการทั้งหมด</th>
                            <th class="text-center text-middle" style="width: 20%;">งบประมาณที่ได้รับจัดสรร</th>
                            <th class="text-center text-middle" style="width: 15%;">ยอดเงินคงเหลือ</th>
                            <th class="text-center text-middle" style="width: 20%;">รายชื่ออาจารย์ผู้จัดแผน</th>
                            <th class="text-center text-middle" style="width: 8%;">ปีงบประมาณ</th>
                            <th class="text-center text-middle" style="width: 8%;">#</th>
                        </tr>
                        @foreach($expenditure_list as $index=>$item)
                            <tr>
                                <td class="text-middle text-center">
                                    {{$index+1}}
                                </td>
                                <td class="text-left text-middle">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="project_name" id="project_name">
                                        {{$item->project_name}}
                                    </span>
                                    @else
                                    <span name="durable_article_name" id="durable_article_name">
                                        {{$item->durable_article_name}}
                                    </span>
                                    @endif
                                </td>
                                <td class="text-right text-middle" style="width: 40px;">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="project_num_after" id="project_num_after">{{number_format($item->project_money_approve,2)}}</span>
                                    @else
                                    <span name="money_approve" id="money_approve">{{number_format($item->money_approve,2)}}</span>
                                    @endif
                                </td>
                                <td class="text-right text-middle">
                                    @if($item->budget_plan_category=='โครงการ')
                                    <span name="balance" id="balance">
                                    <?php
                                        $sum = round(floatval($item->money_received_first)+floatval($item->money_received_two),2);
                                        $approve = round($item->project_money_approve,2) ;
                                        $balance = $approve - $sum;
                                    ?>
                                    {{number_format($balance,2)}}
                                    </span>
                                    @else
                                    <span name="balance" id="balance">
                                    <?php
                                        $balance = $item->money_approve - ($item->money_received)
                                    ?>
                                    {{number_format($balance,2)}}
                                    </span>
                                    @endif
                                </td>
                                <td class="text-middle text-left">
                                    {{$item->position_abbreviation}} {{$item->first_name}}
                                </td>
                                <td class="text-middle text-center">
                                    {{$item->fiscal_year+543}}
                                </td>
                                <td class="text-middle text-center">
                                    <a class="edit-content" href="/expenditure-control/modal/{{$item->budget_plan_id}}" title="รายละเอียด" style="color:#3d3d3d;">
                                        <i class="fa fa-th-list" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table> 
                <div class="text-center">
                    {{$expenditure_list->render()}}
                </div>
            @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลคะ ::</span>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
        var sum_2={{$sum_expenditure['sum_2']}};
        var sum_3={{$sum_expenditure['sum_3']}};
        var sum_4={{$sum_expenditure['sum_4']}};
        var sum_5={{$sum_expenditure['sum_5']}};
        var sum_6={{$sum_expenditure['sum_6']}};
        var sum_7={{$sum_expenditure['sum_7']}};
    $( document ).ready(function() {
        var chart = c3.generate({
            bindto: '#chart',
            data: {
                columns: [
                    ['งบประมาณได้รับจัดสรร',sum_2],
                    ['งบประมาณไม่ได้รับจัดสรร', sum_3],
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            donut: {
                title: "งบประมาณจัดแผนเสนอขอ"
            }
        });

        var chart = c3.generate({
            bindto: '#chart1',
            data: {
                columns: [
                    ['งบประมาณได้รับอนุมัติ', sum_5],
                    ['งบประมาณไม่ได้รับอนุมัติ', sum_6],
                ],
                type : 'donut',
                colors: {
                    งบประมาณได้รับอนุมัติ: '#2ca02c',
                    งบประมาณไม่ได้รับอนุมัติ: '#d62728',
                },
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            donut: {
                title: "งบประมาณขออนุมัติใช้"
            }
        });

        var chart = c3.generate({
            bindto: '#chart2',
            data: {
                columns: [
                    ['งบประมาณขออนุมัติใช้', sum_4],
                    ['งบประมาณรอการขออนุมัติใช้', sum_7],
                ],
                type : 'donut',
                colors: {
                    งบประมาณขออนุมัติใช้: '#fbb034',
                    งบประมาณรอการขออนุมัติใช้: '#1E90FF',
                },
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            donut: {
                title: "งบประมาณได้รับการจัดสรร"
            }
        });
    });

</script>
@stop

