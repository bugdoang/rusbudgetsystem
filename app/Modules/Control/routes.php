<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // **List Expenditure หน้ารายการงบประมาณรายได้**
    // List Page
    Route::get('expenditure-index', '\App\Modules\Control\Controllers\ExpenditureBudget@home');
    Route::get('revenue-index', '\App\Modules\Control\Controllers\RevenueBudget@home');
    Route::get('expenditure-control', '\App\Modules\Control\Controllers\ExpenditureBudget@index');
    Route::get('revenue-control', '\App\Modules\Control\Controllers\RevenueBudget@index');

    // // Modal Form Expenditure
    Route::get('expenditure-control/modal/{id}', '\App\Modules\Control\Controllers\ExpenditureBudget@detail');

    // // Modal Form Revenue
    Route::get('revenue-control/modal/{id}', '\App\Modules\Control\Controllers\RevenueBudget@detail');

});