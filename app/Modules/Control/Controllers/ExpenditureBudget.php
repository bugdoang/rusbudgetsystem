<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/28/16
 * Time: 9:48 AM
 */

namespace App\Modules\Control\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Control\Models\BudgetExpenditureModel;
use App\Modules\Control\Models\SumaryExpenditureModel;
use Illuminate\Http\Request;
use Auth;
use DB;

class ExpenditureBudget extends Controller
{
    public function home(Request $request)
    {
        $year = $request->get('fiscal_year');
        if(!is_numeric($year))
        {
            $year = date('Y');
        }
        $sum_expenditure = SumaryExpenditureModel::get($type='รายจ่าย (งบประมาณแผ่นดิน)',$year);
        $params=[];
        $params['fiscal_year']= $year;
        $personnel_id = 0;
        if(Auth::user()->group_id==4)
        {
            return view('control::permission-denied',[
            ]);
        }
        $startYear=config('myconfig.start_year');
        $expenditure_list = BudgetExpenditureModel::getBudgetPlan($personnel_id,$params);
        return view('control::list-budget/expenditure/expenditure-index',[
            'expenditure_list'=>$expenditure_list,
            'params'=>$params,
            'startYear'=>$startYear,
            'my_group_id'=>Auth::user()->group_id,
            'sum_expenditure'=>$sum_expenditure,
        ]);
    }

    // public function index(Request $request)
    // {
    //     $sum_expenditure = SumaryExpenditureModel::get($type='รายจ่าย (งบประมาณแผ่นดิน)');
    //     $params=[];
    //     $params['project_name']= $request->get('project_name');
    //     $params['durable_article_name']= $request->get('durable_article_name');
    //     $params['approval_status'] = $request->get('approval_status');
    //     $params['first_name'] = $request->get('first_name');
    //     $params['budget_plan_category'] = $request->get('budget_plan_category');
    //     $personnel_id = 0;
    //     if(Auth::user()->group_id==4)
    //     {
    //         return view('control::permission-denied',[
    //         ]);
    //     }
    //     $approval_statuses = config('myconfig.approval_statuses');
    //     $budget_plan_categories = config('myconfig.budget_plan_categories');
    //     $expenditure_list = BudgetExpenditureModel::getBudgetPlan($personnel_id,$params);
    //     return view('control::list-budget/expenditure/expenditure-control',[
    //         'expenditure_list'=>$expenditure_list,
    //         'params'=>$params,
    //         'approval_statuses'=>$approval_statuses,
    //         'budget_plan_categories'=>$budget_plan_categories,
    //         'my_group_id'=>Auth::user()->group_id,
    //         'sum_expenditure'=>$sum_expenditure
    //     ]);
    // }

    public function detail($budget_plan_id)
    {
        $budgetPlanApproval = BudgetExpenditureModel::budgetApprovalId($budget_plan_id);
        $use_prcen = 50;
        $total_money = ($budgetPlanApproval->project_money_approve-($budgetPlanApproval->project_money_approve*$use_prcen)/100);
        $balance = $total_money;
        if($budgetPlanApproval->budget_plan_category=='โครงการ')
        {
            if(is_numeric($budgetPlanApproval->pecen_received_first) && $budgetPlanApproval->pecen_received_first > 0)
            {
                $use_prcen = 100-$budgetPlanApproval->pecen_received_first;
                $total_money =$budgetPlanApproval->project_money_approve- ($budgetPlanApproval->project_money_approve-($budgetPlanApproval->project_money_approve*$use_prcen)/100);
                $balance = 0;
            }
        }
        $html=view('control::list-budget/expenditure/expenditure-modal',compact('budget_plan_id'),[
            'budgetPlanApproval'=>$budgetPlanApproval,
            'use_prcen'=>$use_prcen,
            'total_money'=>$total_money,
            'balance'=>$balance,
        ]);
        return ['title'=>'รายละเอียดข้อมูลคุมยอดงบประมาณ','body'=>$html->render()];
        
    }
}