<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:25 AM
 */

namespace App\Modules\Control\Models;
use App\Helepers\DateFormat;
use DB;

class SumaryExpenditureModel
{
    // project
    public static function get($type='รายจ่าย (งบประมาณแผ่นดิน)',$fiscal_year='')
    {       

    	$result_type = DB::table('plan_incomes')
    	->where('income_name',$type)
    	->first();

    	if(empty($result_type) || !is_numeric($fiscal_year))
    	{
    		return [
	        	'sum_1'=>0,
	        	'sum_2'=>0,
	        	'sum_3'=>0,
	        	'sum_4'=>0,
	        	'sum_5'=>0,
	        	'sum_6'=>0,
	        	'sum_7'=>0
	        ];
    	}


    	// 1 คำร้องขอเงิน งบประมาณที่จัดแผนเสนอของบประมาณทั้งหมด
        $sql=" SELECT SUM(durable_article_budget_net) as total FROM plan_durable_articles WHERE deleted_at is null AND EXISTS(SELECT 1 FROM budget_plans WHERE plan_durable_articles.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND budget_plans.fiscal_year={$fiscal_year} AND deleted_at is null) ";
        $result = DB::select($sql);
        $durable_money = $result[0]->total;

        $sql=" SELECT SUM(project_budget_price) as total FROM `plan_project_budgets` WHERE deleted_at is null AND EXISTS(SELECT 1 FROM plan_projects WHERE plan_project_budgets.project_id = plan_projects.project_id AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ))";
        $result = DB::select($sql);
        $project_money = $result[0]->total;

        $total_budget_for_request = $durable_money+$project_money;

        // 2 เงินที่ได้รับการอนุมัติจากการขอ
        $sql=" SELECT SUM(money_approve) as total FROM plan_durable_articles WHERE deleted_at is null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_durable_articles.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $durable_money_approved = $result[0]->total;

        $sql=" SELECT SUM(project_money_approve) as total FROM `plan_projects` WHERE deleted_at is null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $project_money_approved = $result[0]->total;

        $total_budget_for_request_approved = $durable_money_approved+$project_money_approved;


        // 3 เงินที่ไมได้รับการอนุมัติจากการขอ
        $total_budget_for_request_non_approved = $total_budget_for_request-$total_budget_for_request_approved;





        $sql=" SELECT SUM(money_received) as total FROM plan_durable_articles WHERE deleted_at is null and money_received is not null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_durable_articles.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $durable_money_request_use = $result[0]->total;


        $sql=" SELECT SUM(money_received_first) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_first is not null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $project_money_request_use1 = $result[0]->total;


        $sql=" SELECT SUM(money_received_two) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_two is not null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $project_money_request_use2 = $result[0]->total;

        $total_budget_for_request_use = $durable_money_request_use+$project_money_request_use1+$project_money_request_use2;




        $sql=" SELECT SUM(money_received) as total FROM plan_durable_articles WHERE deleted_at is null and money_received is not null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_durable_articles.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status='อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null )";
        $result = DB::select($sql);
        $durable_money_request_use_approved = $result[0]->total;


        $sql=" SELECT SUM(money_received_first) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_first is not null  AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status='อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null )";
        $result = DB::select($sql);
        $project_money_request_use1_approved = $result[0]->total;


        $sql=" SELECT SUM(money_received_two) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_two is not null  AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status_1='อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null )";
        $result = DB::select($sql);
        $project_money_request_use2_approved = $result[0]->total;

        $total_budget_for_request_use_approved = $durable_money_request_use_approved+$project_money_request_use1_approved+$project_money_request_use2_approved;





        $sql=" SELECT SUM(money_received) as total FROM plan_durable_articles WHERE deleted_at is null and money_received is not null AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_durable_articles.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status='ไม่อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null ) ";
        $result = DB::select($sql);
        $durable_money_request_use_non = $result[0]->total;


        $sql=" SELECT SUM(money_received_first) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_first is not null  AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status='ไม่อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null )";
        $result = DB::select($sql);
        $project_money_request_use1_non = $result[0]->total;


        $sql=" SELECT SUM(money_received_two) as total FROM `plan_projects` WHERE deleted_at is null AND money_received_two is not null  AND EXISTS(SELECT 1 FROM budget_plans WHERE budget_plans.fiscal_year={$fiscal_year} AND plan_projects.budget_plan_id = budget_plans.budget_plan_id AND budget_plans.approval_status_1='ไม่อนุมัติ' AND budget_plans.income_id={$result_type->income_id} AND deleted_at is null )";
        $result = DB::select($sql);
        $project_money_request_use2_non = $result[0]->total;

        $total_budget_for_request_use_non_approved = $durable_money_request_use_non+$project_money_request_use1_non+$project_money_request_use2_non;


        // ยอดงบประมาณที่รอการขออนุมัติใช้ทั้งหมด
        $total_budget_for_request_approved_non = $total_budget_for_request_approved-$total_budget_for_request_use;



        return [
        	'sum_1'=>$total_budget_for_request,
        	'sum_2'=>$total_budget_for_request_approved,
        	'sum_3'=>$total_budget_for_request_non_approved,
        	'sum_4'=>$total_budget_for_request_use,
        	'sum_5'=>$total_budget_for_request_use_approved,
        	'sum_6'=>$total_budget_for_request_use_non_approved,
        	'sum_7'=>$total_budget_for_request_approved_non
        ];
    }
}