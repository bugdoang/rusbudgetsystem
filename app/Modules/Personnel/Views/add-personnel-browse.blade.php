@extends('masterlayout')
@section('title','ข้อมูลบุคลากร')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดข้อมูลบุคลากร','link'=>'/personnel'],['label'=>'ข้อมูลบุคลากร','link'=>'personnel/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <!-- <input type="hidden" name="personnel_id" value="{{$budgetPlanDurable->durable_article_id or ''}}"> -->
    <!-- <div style="padding-right: 0;" class="col-lg-12 col-md-12 form-planbudget shadow"> -->
        <div class="tab-content disable-select">
            <h2 class="header-text myfont">ข้อมูลพื้นฐาน</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="col-md-4 col-lg-4" align="center">
                    <label class="col-md-6 control-label" for="filebutton">รูปบุคลากร :</label>
                    <img alt="User Pic" id="personnel_picture" src="{{(!empty($personnelid->personnel_picture) ? $personnelid->personnel_picture : '/uploads/profile/noimage.jpg')}}" class="img-circle img-responsive">
                    <!-- File Button -->
                    <input type="hidden" name="temp_picture" value="{{ (!empty($personnelid->personnel_picture) ? $personnelid->personnel_picture : ' ') }}">
                </div>
                <div class="col-md-8 col-lg-8">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> คำนำหน้าชื่อ : </label>
                        <div class="col-md-6">
                            <select class="form-control" name="title_name" id="title_name">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($title_names))
                                    @foreach($title_names as $title_name)
                                        <option {{(isset($personnelid->title_name) && $personnelid->title_name==$title_name)?' selected ':''}} value="{{$title_name}}">{{$title_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ชื่อ - นามสกุล :</label>
                        <div class="col-md-6">
                            <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$personnelid->first_name or ''}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ตำแหน่งทางวิชาการ : </label>
                        <div class="col-md-6">
                            <select class="form-control" name="position_id" id="position_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($positions))
                                    @foreach($positions as $position)
                                        <option {{(isset($personnelid->position_id) && $personnelid->position_id==$position->position_id)?' selected ':''}} value="{{$position->position_id}}">{{$position->position_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> เบอร์โทรศัพท์ :</label>
                        <div class="col-md-6">
                            <input id="mobile" class="form-control input-md" type="text" name="mobile" value="{{$personnelid->mobile or ''}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> สาขา : </label>
                        <div class="col-md-6">
                            <select class="form-control" name="branch_id" id="branch_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                        <option {{(isset($personnelid->branch_id) && $personnelid->branch_id==$branch->branch_id)?' selected ':''}} value="{{$branch->branch_id}}">{{$branch->branch_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ศูนย์พื้นที่ : </label>
                        <div class="col-md-6">
                            <select class="form-control" name="campus_id" id="campus_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($campuses))
                                    @foreach($campuses as $campus)
                                        <option {{(isset($personnelid->campus_id) && $personnelid->campus_id==$campus->campus_id?' selected ':'')}} value="{{$campus->campus_id}}">{{$campus->campus_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> กลุ่มการใช้งาน : </label>
                        <div class="col-md-6">
                            <select class="form-control" name="group_id" id="group_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($groups))
                                    @foreach($groups as $group)
                                        <option {{(isset($personnelid->group_id) && $personnelid->group_id==$group->group_id)?' selected ':''}} value="{{$group->group_id}}">{{$group->group_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush