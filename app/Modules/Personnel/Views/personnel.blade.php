@extends('masterlayout')
@section('title','ข้อมูลบุคลากร')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดข้อมูลบุคลากร','link'=>'/personnel']]) !!}
{{csrf_field()}}
<div class="site-breadcrumb">
    <a class="btn btn-default" href="/personnel/form"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลบุคลากร</a>
</div>

<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">
            <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา</h2>
            <div class="form-planbudget shadow">
                <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="" action="" >
                    <div class="form-group">
                        <label class="col-md-12" for="textinput">รายชื่อบุคลากร : </label>
                        <div class="col-md-12">
                            <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$params['first_name'] or ''}}" placeholder="ชื่อบุคลากร">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="textinput">สังกัด : </label>
                        <div class="col-md-12">
                            <select class="" name="branch_id" id="branch_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($branches))
                                    @foreach($branches as $branch)
                                <option {{(($branch->branch_id)==$params['branch_id'])?' selected ':''}} value="{{$branch->branch_id}}">{{$branch->branch_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="textinput">กลุ่มการใช้งาน : </label>
                        <div class="col-md-12">
                            <select class="" name="group_id" id="group_id">
                                <option value="">กรุณาเลือก</option>
                                @if(!empty($groups))
                                    @foreach($groups as $group)
                                <option {{(($group->group_id)==$params['group_id'])?' selected ':''}} value="{{$group->group_id}}">{{$group->group_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                ค้นหา
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div style="height:10px;clear:both;"></div>
    </div>

    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการรายละเอียดข้อมูลบุคลากร</h2>
        <div class="form-planbudget shadow">

            @if(count($personnel_list)>0)
            <table class="table table-bordered">
                <tbody align="center">
                    <tr>
                        <th class="text-center text-middle" style="width:30px;">#</th>
                        <th class="text-center text-middle" style="width: 200px;">รายชื่อบุคลากร</th>
                        <th class="text-center text-middle" style="width:440px;">สังกัด</th>
                        <th class="text-center text-middle">กลุ่มการใช้งาน</th>
                        <th class="text-center text-middle" style="width:70px;">#</th>
                    </tr>
                    @foreach($personnel_list as $index=>$item)
                    <tr>
                        <td>
                            <span name="personnel_id" id="personnel_id">{{$index+1}}</span>
                        </td>
                        <td class="text-left">
                            <span name="first_name" id="first_name">{{$item->position_abbreviation}} {{$item->first_name}}</span>
                        </td>
                        <td class="text-left">
                            <span name="branch_name" id="branch_name">สาขา{{$item->branch_name}} ศูนย์{{$item->campus_name}}</span>
                        </td>
                        <td class="text-left">
                            <span name="group_name" id="group_name">{{$item->group_name}}</span>
                        </td>
                        <td class="text-center text-middle">
                            <a href="/personnel/browse/{{$item->personnel_id}}">
                                <i class="fa fa-search" style="color:#3d3d3d;"></i>
                            </a>
                            <a href="/personnel/{{$item->personnel_id}}/">
                                <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                            </a>
                            <a class="delete-content" href="/personnel/{{$item->personnel_id}}">
                                <i class="fa fa-times" style="color:#b00;"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {{$personnel_list->render()}}
            </div>
            @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลบุคลากรคะ ::</span>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

