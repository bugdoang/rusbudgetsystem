<?php
namespace App\Modules\Personnel\Models;

/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/26/16
 * Time: 3:38 PM
 */
use \Illuminate\Database\Eloquent\Model;
use DB;

class PersonnelModel extends Model
{
    public static function myPersonnel()
    {
        return DB::table('personnels')
            ->whereNull('deleted_at')
            ->get();
    }

    public static function insert($item)
    {
        return DB::table('personnels')
            ->insertGetId($item);
    }

    public static function updatePersonnel($personnel_id,$item)
    {
        return DB::table('personnels')
            ->where('personnel_id',$personnel_id)
            ->update($item);
    }

    public static function getPersonnel($personnel_id=0,$params)
    {
        $q = DB::table('personnels')
            ->leftJoin('positions','positions.position_id','=','personnels.position_id')
            ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
            ->leftJoin('groups','groups.group_id','=','personnels.group_id')
            ->leftJoin('campuses','campuses.campus_id','=','personnels.campus_id')
            ->select([
                'personnels.*',
                'position_name',
                'position_abbreviation', 
                'group_name',
                'branch_name',
                'campus_name',
            ]);
        if(!empty($params['first_name']))
        {
            $q->where('personnels.first_name','LIKE','%'.trim($params['first_name']).'%');
        }
        if(!empty($params['branch_id']))
        {
            $q->where('personnels.branch_id','LIKE','%'.trim($params['branch_id']).'%');
        }
        if(!empty($params['group_id']))
        {
            $q->where('personnels.group_id','LIKE','%'.trim($params['group_id']).'%');
        }
        if($personnel_id!=0)
        {
            $q->where('personnels.personnel_id',$personnel_id);
        }
        $q->orderBy('personnel_id','DESC');
        return $q->whereNull('personnels.deleted_at')->paginate(10);
    }

    public static function PersonnelId($personnel_id)
    {
        $personnels = DB::table('personnels')
            ->leftJoin('positions','positions.position_id','=','personnels.position_id')
            ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
            ->leftJoin('groups','groups.group_id','=','personnels.group_id')
            ->leftJoin('campuses','campuses.campus_id','=','personnels.campus_id')            
            ->where('personnels.personnel_id',$personnel_id)
            ->whereNull('personnels.deleted_at')
            ->first();
        return $personnels;
    }

    public static function deletePersonnel($personnel_id)
    {
        return DB::table('personnels')
            ->where('personnel_id','=',$personnel_id)
            ->update(['personnels.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
}



