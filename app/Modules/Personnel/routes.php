<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // Create Form
	Route::get('personnel/form', '\App\Modules\Personnel\Controllers\Personnel@create');
    Route::post('personnel/form', '\App\Modules\Personnel\Controllers\Personnel@save');

    // Update Form
    Route::get('personnel/{id}', '\App\Modules\Personnel\Controllers\Personnel@edit');
    Route::put('personnel/{id}', '\App\Modules\Personnel\Controllers\Personnel@update');

    // List Page
    Route::get('personnel', '\App\Modules\Personnel\Controllers\Personnel@index');

    // Delete Form
    Route::delete('personnel/{id}', '\App\Modules\Personnel\Controllers\Personnel@delete');

    // Browse Form
    Route::get('personnel/browse/{id}', '\App\Modules\Personnel\Controllers\Personnel@browse');
});