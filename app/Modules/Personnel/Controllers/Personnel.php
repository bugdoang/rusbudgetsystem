<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/4/2016
 * Time: 5:08 PM
 */

namespace App\Modules\Personnel\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Personnel\Models\PersonnelModel;
use App\Modules\Branch\Models\BranchModel;
use App\Modules\Campus\Models\CampusModel;
use App\Modules\Position\Models\PositionModel;
use App\Modules\Profile\Models\ProfileModel;
use App\Modules\Group\Models\GroupModel;
use Auth;
use Illuminate\Http\Request;

class Personnel extends Controller
{
    public function index(Request $request)
    {
        $params=[];
        $params['first_name']= $request->get('first_name');
        $params['branch_id'] = $request->get('branch_id');
        $params['group_id'] = $request->get('group_id');

        $personnel_id = 0;
        if(Auth::user()->group_id!=1)
        {
            return view('personnel::permission-denied',[
        ]);
        }
        $branches = BranchModel::getAll();
        $groups = GroupModel::getAll();
        $personnel_list = PersonnelModel::getPersonnel($personnel_id,$params);
        return view('personnel::personnel',[
            'personnel_list'=>$personnel_list,
            'branches'=>$branches,
            'groups'=>$groups,
            'params'=>$params,
        ]);
    }

    public function create()
    {
        $method='POST';
        $action='/personnel/form';
        $personnel_id = Auth::id();//ตรวจสอบว่า login หรือยัง
        $personnels = PersonnelModel::myPersonnel();
        $title_names = config('myconfig.title_names');
        $positions = PositionModel::getAll();
        $branches = BranchModel::getAll();
        $campuses = CampusModel::getAll();
        $groups = GroupModel::getAll();

        return view('personnel::add-personnel', [
            'personnels' => $personnels,
            'title_names' => $title_names,
            'positions' => $positions,
            'branches' => $branches,
            'campuses' => $campuses,
            'groups' => $groups,
            'method'=>$method,
            'action'=>$action
        ]);
    }

     public function save(Request $request)
    {
        $personnel_picture = $request->get('image_picter');
        $title_name = $request->get('title_name');
        $first_name = $request->get('first_name');
        $mobile = $request->get('mobile');
        $username = $request->get('username');
        $password = $request->get('password');
        $position_id = $request->get('position_id');
        $branch_id = $request->get('branch_id');
        $group_id = $request->get('group_id');
        $campus_id = $request->get('campus_id');

        $checks = array('image_picter','title_name','first_name','mobile','username','password','branch_id','group_id','campus_id');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่วงด้วยคะ'],422);
            }
        }
        
        if(!is_numeric($branch_id))
        {
            return response(['กรุณาป้อนสาขาให้ถูกต้องด้วยคะ']);
        }
        if(!is_numeric($campus_id))
        {
            return response(['กรุณาป้อนศูนย์พื้นที่ให้ถูกต้องด้วยคะ']);
        }
        if(!is_numeric($group_id))
        {
            return response(['กรุณาป้อนกลุ่มการใช้งานให้ถูกต้องด้วยคะ']);
        }
        PersonnelModel::insert([
            'personnel_picture'=>$personnel_picture,
            'title_name'=>$title_name,
            'first_name'=>$first_name,
            'mobile'=>$mobile,
            'username'=>$username,
            'password'=>\Illuminate\Support\Facades\Hash::make($password),
            'position_id'=>$position_id,
            'branch_id'=>$branch_id,
            'group_id'=>$group_id,
            'campus_id'=>$campus_id
        ]);

        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/personnel']);
    }

    public function edit($personnel_id)
    {
        $method='PUT';
        $action='/personnel/'.$personnel_id;
        $title_names = config('myconfig.title_names');
        $positions = PositionModel::getAll();
        $branches = BranchModel::getAll();
        $campuses = CampusModel::getAll();
        $groups = GroupModel::getAll();
        $personnelid = PersonnelModel::PersonnelId($personnel_id);

        return view('personnel::add-personnel',[
            'personnelid'=>$personnelid,
            'title_names' => $title_names,
            'positions' => $positions,
            'branches' => $branches,
            'campuses' => $campuses,
            'groups' => $groups,
            'method'=>$method,
            'action'=>$action
        ]);
    }

    public function update($id,Request $request)
    {
        $personnel_picture = $request->get('image_picter');
        $temp_picture = $request->get('temp_picture');
        $title_name = $request->get('title_name');
        $first_name = $request->get('first_name');
        $mobile = $request->get('mobile');
        $username = $request->get('username');
        $password = $request->get('password');
        $confirmpassword = $request->get('confirmpassword');
        $position_id = $request->get('position_id');
        $branch_id = $request->get('branch_id');
        $group_id = $request->get('group_id');
        $campus_id = $request->get('campus_id');

        $checks = array('title_name','first_name','mobile','username','branch_id','group_id','campus_id');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องด้วยคะ'],422);
            }
        }
        if(!is_numeric($branch_id))
        {
            return response(['กรุณาป้อนสาขาให้ถูกต้องด้วยคะ'],422);
        }
        if(!is_numeric($campus_id))
        {
            return response(['กรุณาป้อนศูนย์พื้นที่ให้ถูกต้องด้วยคะ'],422);
        }
        if(!is_numeric($group_id))
        {
            return response(['กรุณาป้อนกลุ่มการใช้งานให้ถูกต้องด้วยคะ'],422);
        }
        if (empty($personnel_picture)) {
            
            $up_pic = $temp_picture;
        }
        else
        {
            $up_pic = $personnel_picture;
        }

        $update_data = [
            'personnel_picture'=>$up_pic,
            'title_name'=>$title_name,
            'first_name'=>$first_name,
            'mobile'=>$mobile,
            'username'=>$username,
            'position_id'=>$position_id,
            'branch_id'=>$branch_id,
            'group_id'=>$group_id,
            'campus_id'=>$campus_id
        ];

        if (!empty($password))
        {
            if($password!=$confirmpassword)
            {
                return response(['กรุณาป้อนยืนยันรหัสผ่านให้ถูกต้องด้วยคะ'],422);
            }
            $update_data['password'] = \Illuminate\Support\Facades\Hash::make($password);
        }
        PersonnelModel::updatePersonnel($id,$update_data);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/personnel/'.$id]);
    }

    public function delete($personnel_id)
    {
        PersonnelModel::deletePersonnel($personnel_id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

    public function browse($personnel_id)
    {
        $method='GET';
        $action='/personnel/'.$personnel_id;
        $title_names = config('myconfig.title_names');
        $positions = PositionModel::getAll();
        $branches = BranchModel::getAll();
        $campuses = CampusModel::getAll();
        $groups = GroupModel::getAll();
        $personnelid = PersonnelModel::PersonnelId($personnel_id);
        return view('personnel::add-personnel-browse',[
            'personnelid'=>$personnelid,
            'title_names' => $title_names,
            'positions' => $positions,
            'branches' => $branches,
            'campuses' => $campuses,
            'groups' => $groups,
            'method'=>$method,
            'action'=>$action
        ]);
    }
}
