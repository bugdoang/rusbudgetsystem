<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // **List Expenditure หน้ารายการงบประมาณรายจ่าย**
    // List Page
    Route::get('expenditure-budget', '\App\Modules\Approval\Controllers\ExpenditureBudget@index');
    Route::get('revenue-budget', '\App\Modules\Approval\Controllers\RevenueBudget@index');

    // Modal Form Expenditure
    Route::get('expenditure-budget/modal/{id}', '\App\Modules\Approval\Controllers\ExpenditureBudget@approval');
    Route::post('expenditure-budget/modal/{id}', '\App\Modules\Approval\Controllers\ExpenditureBudget@saveApproval');
    
    Route::get('expenditure-budget-status/modal/{id}', '\App\Modules\Approval\Controllers\ExpenditureBudget@approvalStatus');

    Route::post('expenditure-budget-status/modal/{id}/reject', '\App\Modules\Approval\Controllers\ExpenditureBudget@rejectApprovalStatus');


    Route::post('expenditure-budget-status/modal/{id}', '\App\Modules\Approval\Controllers\ExpenditureBudget@saveApprovalStatus');

    // Modal Form Revenue
    Route::get('revenue-budget/modal/{id}', '\App\Modules\Approval\Controllers\RevenueBudget@approval');
    Route::post('revenue-budget/modal/{id}', '\App\Modules\Approval\Controllers\RevenueBudget@saveApproval');
    
    Route::get('revenue-budget-status/modal/{id}', '\App\Modules\Approval\Controllers\RevenueBudget@approvalStatus');
    Route::post('revenue-budget-status/modal/{id}', '\App\Modules\Approval\Controllers\RevenueBudget@saveApprovalStatus');

    Route::post('revenue-budget-status/modal/{id}/reject', '\App\Modules\Approval\Controllers\RevenueBudget@rejectApprovalStatus');
});