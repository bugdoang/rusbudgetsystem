<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:25 AM
 */

namespace App\Modules\Approval\Models;
use App\Helepers\DateFormat;
use DB;

class BudgetExpenditureModel
{
    // project
    public static function getBudgetPlan($personnel_id=0,$params)
    {      
        //การคิวรี่จำนวนเงินที่เสนอขอ
        $temp = DB::select("SELECT project_id,sum(project_budget_price) as net_total_price FROM plan_project_budgets WHERE deleted_at IS NULL GROUP BY project_id");
        $total_price=[];
        foreach ($temp as $value) {
            $total_price[$value->project_id]=$value->net_total_price;
        }
        $q = DB::table('budget_plans')
            ->leftJoin('plan_projects','plan_projects.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('plan_durable_articles','plan_durable_articles.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('plan_incomes','plan_incomes.income_id','=','budget_plans.income_id')
            ->leftJoin('personnels','personnels.personnel_id','=','budget_plans.personnel_id')
            ->leftJoin('positions','positions.position_id','=','personnels.position_id')
            ->select([
                'budget_plans.*',
                'project_name',
                'project_id',
                'durable_article_id',
                'durable_article_name',
                'first_name',
                'position_abbreviation',
                'project_money_approve',
                'money_approve',
                'plan_projects.upload_file',
                'plan_projects.upload_file_name',
                'status',
                'income_name',
                'approval_status',
                'approval_status_1',
                'pecen_received',
                'date_received',
                'money_received',
                'pecen_received_two',
                'pecen_received_first',
                'date_received_two',
                'date_received_first',
                'money_received_two',
                'money_received_first',
                'budget_plan_category',
                'file_received',
                'filename_received',
                'file_received_first',
                'file_received_two',
                'filename_received_first',
                'filename_received_two',
                'fiscal_year'
            ]);
            if(!empty($params['project_name']))
            {
                $q->where('plan_projects.project_name','LIKE','%'.trim($params['project_name']).'%');
            }
            if(!empty($params['durable_article_name']))
            {
                $q->where('plan_durable_articles.durable_article_name','LIKE','%'.trim($params['durable_article_name']).'%');
            }
            if(!empty($params['approval_status']))
            {
                $q->where('budget_plans.approval_status','LIKE','%'.trim($params['approval_status']).'%');
            }
            if(!empty($params['approval_status_1']))
            {
                $q->where('budget_plans.approval_status_1','LIKE','%'.trim($params['approval_status_1']).'%');
            }
            if(!empty($params['budget_plan_category']))
            {
                $q->where('budget_plans.budget_plan_category','LIKE','%'.trim($params['budget_plan_category']).'%');
            }
            if(!empty($params['first_name']))
            {
                $q->where('personnels.first_name','LIKE','%'.trim($params['first_name']).'%');
            }
            
            if($personnel_id!=0)
            {
                $q->where('budget_plans.personnel_id',$personnel_id);
            }

            $q->where('status','อนุมัติแล้ว');
            $q->where('income_name','รายจ่าย (งบประมาณแผ่นดิน)');

        return $q->paginate(4);
    }

    public static function budgetApprovalId($budget_plan_id)
    {   

        $temp = DB::select("SELECT project_id,sum(project_budget_price) as net_total_price FROM plan_project_budgets WHERE deleted_at IS NULL GROUP BY project_id");
        $total_price=[];
        foreach ($temp as $value) {
            $total_price[$value->project_id]=$value->net_total_price;
        }

        $budget_plan = DB::table('budget_plans')
            ->leftJoin('plan_products','plan_products.product_id','=','budget_plans.product_id')
            ->leftJoin('plan_plans','plan_plans.plan_id','=','budget_plans.plan_id')
            ->leftJoin('plan_incomes','plan_incomes.income_id','=','budget_plans.income_id')
            ->leftJoin('plan_income_types','plan_income_types.income_type_id','=','budget_plans.income_type_id')
            ->leftJoin('plan_income_groups','plan_income_groups.income_group_id','=','budget_plans.income_group_id')
            ->leftJoin('masterplan_key_projects','masterplan_key_projects.key_project_id','=','budget_plans.key_project_id')
            ->leftJoin('masterplan_objective_strategies','masterplan_objective_strategies.objective_strategy_id','=','budget_plans.objective_strategy_id')
            ->leftJoin('masterplan_strategies','masterplan_strategies.strategy_id','=','budget_plans.strategy_id')
            ->leftJoin('masterplan_goals','masterplan_goals.goal_id','=','budget_plans.goal_id')
            ->leftJoin('masterplan_objective_indicators','masterplan_objective_indicators.objective_indicator_id','=','budget_plans.objective_indicator_id')
            ->leftJoin('masterplan_issues','masterplan_issues.issue_id','=','budget_plans.issue_id')
            ->leftJoin('masterplan_federal_circuits','masterplan_federal_circuits.federal_circuit_id','=','budget_plans.federal_circuit_id')
            ->leftJoin('masterplan_visions','masterplan_visions.vision_id','=','budget_plans.vision_id')
            ->leftJoin('plan_durable_articles','plan_durable_articles.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('plan_projects','plan_projects.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('plan_project_activities','plan_project_activities.project_id','=','plan_projects.project_id')
            ->leftJoin('plan_project_benefits','plan_project_benefits.project_id','=','plan_projects.project_id')
            ->leftJoin('plan_project_budgets','plan_project_budgets.project_id','=','plan_projects.project_id')
            ->leftJoin('plan_project_periods','plan_project_periods.project_id','=','plan_projects.project_id')
            ->leftJoin('plan_project_suggestions','plan_project_suggestions.project_id','=','plan_projects.project_id')
            ->leftJoin('personnels','personnels.personnel_id','=','budget_plans.personnel_id')
            ->leftJoin('positions','positions.position_id','=','personnels.position_id')
            ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
            ->leftJoin('faculties','faculties.faculty_id','=','branches.faculty_id')
            ->where('budget_plans.budget_plan_id',$budget_plan_id)
            ->whereNull('budget_plans.deleted_at')
            ->first();
        if(!empty($budget_plan))
        {
            $temp = DB::table('plan_project_activities')
                ->whereNull('deleted_at')
                ->where('project_id',$budget_plan->project_id)
                ->get();
            $budget_plan->plan_project_activities = $temp;

            $temp = DB::table('plan_project_benefits')
                ->whereNull('deleted_at')
                ->where('project_id',$budget_plan->project_id)
                ->get();
            $budget_plan->plan_project_benefits = $temp;

            $temp = DB::table('plan_project_budgets')
                ->whereNull('deleted_at')
                ->where('project_id',$budget_plan->project_id)
                ->get();
            $budget_plan->plan_project_budgets = $temp;

            $temp = DB::table('plan_project_periods')
                ->whereNull('deleted_at')
                ->where('project_id',$budget_plan->project_id)
                ->get();
            $budget_plan->plan_project_periods = $temp;

            $temp = DB::table('plan_project_suggestions')
                ->whereNull('deleted_at')
                ->where('project_id',$budget_plan->project_id)
                ->get();
            $budget_plan->plan_project_suggestions = $temp;

            $temp = DB::table('plan_objectives')
                ->whereNull('deleted_at')
                ->where('budget_plan_id',$budget_plan->budget_plan_id)
                ->get();
            $budget_plan->plan_objectives = $temp;

            $temp = DB::table('plan_committees')
                ->leftJoin('personnels','personnels.personnel_id','=','plan_committees.personnel_id')
                ->leftJoin('positions','positions.position_id','=','personnels.position_id')
                ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
                ->leftJoin('campuses','campuses.campus_id','=','personnels.campus_id')
                ->whereNull('plan_committees.deleted_at')
                ->where('budget_plan_id',$budget_plan->budget_plan_id)
                ->get();
            $budget_plan->plan_committees = $temp;
            $budget_plan->total_price = $total_price;
        }

        // if(in_array($type,['โครงการ','ครุภัณฑ์']))
        // {
        //     $q->where('budget_plan_category',$type);
        // }

        return $budget_plan;
    }

    public static function getIncome()
    {
        return DB::table('plan_incomes')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIncomeType()
    {
        return DB::table('plan_income_types')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIncomeGroup($income_type_id=0)
    {
        $q= DB::table('plan_income_groups')
            ->whereNull('deleted_at');
        if(is_numeric($income_type_id) && $income_type_id>0)
        {
            $q->where('income_type_id',$income_type_id);
        }
          return  $q->get();
    }
    public static function getFaculty()
    {
        return DB::table('faculties')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getCampus()
    {
        return DB::table('campuses')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getVision()
    {
        return DB::table('masterplan_visions')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getFederalCircuit()
    {
        return DB::table('masterplan_federal_circuits')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIssue($federal_circuit_id=0)
    {
        $q= DB::table('masterplan_issues')
            ->whereNull('deleted_at');
        if(is_numeric($federal_circuit_id) && $federal_circuit_id>0)
        {
            $q->where('federal_circuit_id',$federal_circuit_id);
        }
        return  $q->get();
    }
    public static function getGoal($issue_id=0)
    {
        $q= DB::table('masterplan_goals')
            ->whereNull('deleted_at');
        if(is_numeric($issue_id) && $issue_id>0)
        {
            $q->where('issue_id',$issue_id);
        }
        return  $q->get();
    }
    public static function getObjectiveIndicator($goal_id=0)
    {
        $q= DB::table('masterplan_objective_indicators')
            ->whereNull('deleted_at');
        if(is_numeric($goal_id) && $goal_id>0)
        {
            $q->where('goal_id',$goal_id);
        }
        return  $q->get();
    }
    public static function getStrategy($goal_id=0)
    {
        $q= DB::table('masterplan_strategies')
            ->whereNull('deleted_at');
        if(is_numeric($goal_id) && $goal_id>0)
        {
            $q->where('goal_id',$goal_id);
        }
        return  $q->get();
    }
    public static function getObjectiveStrategy($strategy_id=0)
    {
        $q= DB::table('masterplan_objective_strategies')
            ->whereNull('deleted_at');
        if(is_numeric($strategy_id) && $strategy_id>0)
        {
            $q->where('strategy_id',$strategy_id);
        }
        return  $q->get();
    }
    public static function getKeyProject($strategy_id=0)
    {
        $q= DB::table('masterplan_key_projects')
            ->whereNull('deleted_at');
        if(is_numeric($strategy_id) && $strategy_id>0)
        {
            $q->where('strategy_id',$strategy_id);
        }
        return  $q->get();
    }
    public static function getPlan()
    {
        return DB::table('plan_plans')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getProduct($plan_id=0)
    {
        $q= DB::table('plan_products')
            ->whereNull('deleted_at');
        if(is_numeric($plan_id) && $plan_id>0)
        {
            $q->where('plan_id',$plan_id);
        }
        return  $q->get();
    }
    public static function getPersonnel()
    {
        return DB::table('personnels')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getPosition()
    {
        return DB::table('positions')
            ->whereNull('deleted_at')
            ->get();
    }

    public static function update_budget_plan($budget_plan_id,$approval_status)
    {
        return DB::table('budget_plans')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'approval_status'=>$approval_status,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_budget_plan_1($budget_plan_id,$approval_status_1)
    {
        return DB::table('budget_plans')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'approval_status_1'=>$approval_status_1,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_project_first($budget_plan_id,$money_received_first,$pecen_received_first)
    {
        return DB::table('plan_projects')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'money_received_first'=>$money_received_first,
                'pecen_received_first'=>$pecen_received_first,
                'date_received_first'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_durable($budget_plan_id,$money_approve,$pecen_approve)
    {
        return DB::table('plan_durable_articles')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'money_received'=>$money_approve,
                'pecen_received'=>$pecen_approve,
                'date_received'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_project_two($budget_plan_id,$money_received_two,$pecen_received_two)
    {
        return DB::table('plan_projects')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'money_received_two'=>$money_received_two,
                'date_received_two'=>date('Y-m-d H:i:s'),
                'pecen_received_two'=>$pecen_received_two,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_upload_file_first($budget_plan_id,$file_received_first,$filename_received_first)
    {
        return DB::table('plan_projects')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'file_received_first'=>$file_received_first,
                'filename_received_first'=>$filename_received_first,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_upload_file_two($budget_plan_id,$file_received_two,$filename_received_two)
    {
        return DB::table('plan_projects')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'file_received_two'=>$file_received_two,
                'filename_received_two'=>$filename_received_two,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_upload_file($budget_plan_id,$file_received,$filename_received)
    {
        return DB::table('plan_durable_articles')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'file_received'=>$file_received,
                'filename_received'=>$filename_received,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }
}