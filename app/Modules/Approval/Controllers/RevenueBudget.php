<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/28/16
 * Time: 9:48 AM
 */

namespace App\Modules\Approval\Controllers;


use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Modules\Approval\Models\BudgetRevenueModel;
use Illuminate\Http\Request;

class RevenueBudget extends Controller
{
    public function index(Request $request)
    {
        $params=[];
        $params['project_name']= $request->get('project_name');
        $params['durable_article_name']= $request->get('durable_article_name');
        $params['approval_status'] = $request->get('approval_status');
        $params['approval_status_1'] = $request->get('approval_status_1');
        $params['first_name'] = $request->get('first_name');
        $params['budget_plan_category'] = $request->get('budget_plan_category');
        $personnel_id = 0;
        if(Auth::user()->group_id==4)
        {
            $personnel_id = Auth::id();
        }
        $approval_statuses = config('myconfig.approval_statuses');
        $budget_plan_categories = config('myconfig.budget_plan_categories');
        $revenue_list = BudgetRevenueModel::getBudgetPlan($personnel_id,$params);
        return view('approval::list-budget/revenue/revenue-budget',[
            'revenue_list'=>$revenue_list,
            'params'=>$params,
            'approval_statuses'=>$approval_statuses,
            'budget_plan_categories'=>$budget_plan_categories,
            'my_group_id'=>Auth::user()->group_id
        ]);
    }

    public function approval($budget_plan_id)
    {
        $budgetPlanApproval = BudgetRevenueModel::budgetApprovalId($budget_plan_id);
        $use_prcen = 50;
        $total_money = ($budgetPlanApproval->project_money_approve-($budgetPlanApproval->project_money_approve*$use_prcen)/100);
        $balance = $total_money;
        if($budgetPlanApproval->budget_plan_category=='โครงการ')
        {
            if(is_numeric($budgetPlanApproval->pecen_received_first) && $budgetPlanApproval->pecen_received_first > 0)
            {
                $use_prcen = 100-$budgetPlanApproval->pecen_received_first;
                $total_money =$budgetPlanApproval->project_money_approve- ($budgetPlanApproval->project_money_approve-($budgetPlanApproval->project_money_approve*$use_prcen)/100);
                $balance = 0;
            }
        }
        $html=view('approval::list-budget/revenue/revenue-modal',compact('budget_plan_id'),[
            'budgetPlanApproval'=>$budgetPlanApproval,
            'use_prcen'=>$use_prcen,
            'total_money'=>$total_money,
            'balance'=>$balance,
        ]);
        return ['title'=>'ข้อมูลการปรับขออนมัติใช้งบประมาณ','body'=>$html->render()];
    }

    public function saveApproval($budget_plan_id,Request $request)
    {
        // print_r($request->all());exit();
        $use_prcen = $request->get('use_prcen');
        $total_money = $request->get('total_money');
        $file_received = $request->get('file_received');
        $filename_received = $request->get('filename_received');
        $file_received_first = $request->get('file_received_first');
        $file_received_two = $request->get('file_received_two');
        $filename_received_first = $request->get('filename_received_first');
        $filename_received_two = $request->get('filename_received_two');

        $budgetPlanApproval = BudgetRevenueModel::budgetApprovalId($budget_plan_id);

        if($budgetPlanApproval->budget_plan_category=='โครงการ')
        {

            if(!is_numeric($use_prcen) || empty($use_prcen) || $budgetPlanApproval->project_money_approve == $total_money)
            {
                return response(['กรุณาเลือกเปอร์เซนต์การปรับขออนุมัติใช้งบประมาณด้วยค่ะ'],422);
            }
            
            $total_money = ($budgetPlanApproval->project_money_approve*$use_prcen)/100;

            if(!empty($budgetPlanApproval->money_received_first)) // 2
            {
                BudgetRevenueModel::update_budget_plan_1($budget_plan_id,'รอการตรวจสอบ');
                BudgetRevenueModel::update_project_two($budget_plan_id,$total_money,$use_prcen);
                BudgetRevenueModel::update_upload_file_two($budget_plan_id,$file_received_first,$filename_received_first);
            }
            else // 1
            {
                BudgetRevenueModel::update_budget_plan($budget_plan_id,'รอการตรวจสอบ');
                BudgetRevenueModel::update_project_first($budget_plan_id,$total_money,$use_prcen);
                BudgetRevenueModel::update_upload_file_first($budget_plan_id,$file_received_first,$filename_received_first);

            }

            return response(['ระบบได้ทำการบันทึกข้อมูลเรียบร้อยแล้วค่ะ'],200);
        }
        else
        {
            BudgetRevenueModel::update_budget_plan($budget_plan_id,'รอการตรวจสอบ');
            BudgetRevenueModel::update_durable($budget_plan_id,$budgetPlanApproval->money_approve,100);
            BudgetRevenueModel::update_upload_file($budget_plan_id,$file_received,$filename_received);
            return response(['ระบบได้ทำการบันทึกข้อมูลเรียบร้อยแล้วค่ะ'],200);
        }
        return response(['ขออภัยคะ ไม่สามารถบันทึกข้อมูลได้ค่ะ'],422);
    }

    public function approvalStatus($budget_plan_id)
    {
        $budgetPlanApproval = BudgetRevenueModel::budgetApprovalId($budget_plan_id);
        $percen = $budgetPlanApproval->pecen_received_first;
        $percen2 = $budgetPlanApproval->pecen_received_two;
        $total_money1 = $budgetPlanApproval->project_money_approve;
        $total_money2 = $budgetPlanApproval->project_money_approve;

        if(!empty($percen))
        {
            $total_money1 = ($budgetPlanApproval->project_money_approve-($budgetPlanApproval->project_money_approve*$percen)/100);
            $total_money2 = $budgetPlanApproval->project_money_approve-$total_money1;
        }

        $html=view('approval::list-budget/revenue/revenue-modal-status',[
            'budget_plan_id'=>$budget_plan_id,
            'budgetPlanApproval'=>$budgetPlanApproval,
            'balance'=> (!empty($percen2))?0:$total_money1,
        ]);       

        return ['title'=>'การปรับสถานะการขอใช้งบประมาณรายจ่าย','body'=>$html->render()];
    }

    public function saveApprovalStatus($budget_plan_id,Request $request)
    {
        $budgetPlanApproval = BudgetRevenueModel::budgetApprovalId($budget_plan_id);

        if($budgetPlanApproval->budget_plan_category=='โครงการ')
        {
            if($budgetPlanApproval->money_received_two) // 2
            {
                BudgetRevenueModel::update_budget_plan_1($budget_plan_id,'อนุมัติ');
            }
            else // 1
            {
                BudgetRevenueModel::update_budget_plan($budget_plan_id,'อนุมัติ');
            }
        }
        else
        {
            BudgetRevenueModel::update_budget_plan($budget_plan_id,'อนุมัติ');
        }
        return response(['ระบบได้ทำการ อนุมัติงบประมาณรายได้ เรียบร้อยแล้วค่ะ'],200);

    }

    public function rejectApprovalStatus($budget_plan_id)
    {
        $budgetPlanApproval = BudgetRevenueModel::budgetApprovalId($budget_plan_id);

        if($budgetPlanApproval->budget_plan_category=='โครงการ')
        {
            if($budgetPlanApproval->approval_status=='รอการตรวจสอบ') // 1
            {
                BudgetRevenueModel::update_budget_plan($budget_plan_id,'ไม่อนุมัติ');
            }
            else // 2
            {
                BudgetRevenueModel::update_budget_plan_1($budget_plan_id,'ไม่อนุมัติ');
            }
        }
        else
        {
            BudgetRevenueModel::update_budget_plan($budget_plan_id,'ไม่อนุมัติ');
        }

        return response(['ไม่ได้รับการอนุมัติคะ'],200);

    }
}