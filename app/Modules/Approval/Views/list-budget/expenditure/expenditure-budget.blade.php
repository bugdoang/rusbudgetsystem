@extends('masterlayout')
@section('title','ขออนุมัติใช้งบประมาณรายจ่าย')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'ขออนุมัติใช้งบประมาณรายจ่าย','link'=>'/revenue-budget']]) !!}
{{csrf_field()}}
<style type="text/css">
    .panel-special{
        background-image: none;
        background-color: #f07c4a!important;
        color: #ffffff;
    }
</style>
<!-- Page Content -->
<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">

        <h2 class="header-text myfont"><i class="fa fa-bullhorn" aria-hidden="true"></i> หมายเหตุ</h2>
        <div class="form-planbudget shadow">
            <table style="width: 100%">
                <tr>
                    <th class="text-center">
                        <p class="box_css3_raduis"></p>  
                    </th>
                    <th>
                    </th>
                    <th>
                        <p class="text-center">ครุภัณฑ์</p>
                    </th>
                </tr>
                <tr>
                    <th>
                        <p class="box_css3_raduis1"></p>  
                    </th>
                    <th>
                    </th>
                    <th>
                        <p class="text-center">โครงการ</p>
                    </th>
                </tr>
            </table>
        </div>
        
        <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา</h2>
        <div class="form-planbudget shadow">
            <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="get" action="/expenditure-budget" >
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ชื่อรายการที่ได้รับอนุมัติ : </label>
                    <div class="col-md-12">
                        <input id="project_name" class="form-control input-md" type="text" name="project_name" value="{{$params['project_name'] or ''}}" placeholder="ชื่อรายการที่ได้รับอนุมัติ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">สถานะงบประมาณรายจ่ายครั้งที่ 1 : </label>
                    <div class="col-md-12">
                        <select class="" name="approval_status" id="approval_status">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($approval_statuses))
                                @foreach($approval_statuses as $approval_status)
                            <option {{($approval_status==$params['approval_status'])?' selected ':''}} value="{{$approval_status}}">{{$approval_status}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="textinput">สถานะงบประมาณรายจ่ายครั้งที่ 2 : </label>
                    <div class="col-md-12">
                        <select class="" name="approval_status_1" id="approval_status_1">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($approval_statuses))
                                @foreach($approval_statuses as $approval_status)
                            <option {{($approval_status==$params['approval_status_1'])?' selected ':''}} value="{{$approval_status}}">{{$approval_status}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="textinput">ประเภทแผนงบประมาณ : </label>
                    <div class="col-md-12">
                        <select class="" name="budget_plan_category" id="budget_plan_category">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($budget_plan_categories))
                                @foreach($budget_plan_categories as $budget_plan_category)
                            <option {{($budget_plan_category==$params['budget_plan_category'])?' selected ':''}} value="{{$budget_plan_category}}">{{$budget_plan_category}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">รายชื่อผู้จัดแผนเสนอของบประมาณ : </label>
                    <div class="col-md-12">
                        <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$params['first_name'] or ''}}" placeholder="รายชื่อผู้จัดแผนเสนอของบประมาณ">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            ค้นหา
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="height:10px;clear:both;"></div>

    </div>
    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการงบประมาณรายจ่าย</h2>
        <div class="form-planbudget shadow">
         @if(count($expenditure_list)>0)
                <div class="row">
            @foreach($expenditure_list as $index=>$item)
                    <div class="col-md-12">
                        <div class="panel @if($item->budget_plan_category=='โครงการ') panel-primary @else  panel-special @endif">
                            <div style="padding:12px 6px;" class="panel-heading">
                                @if($item->budget_plan_category=='โครงการ')
                                {{$item->project_name}}
                                @else
                                {{$item->durable_article_name}}
                                @endif
                                </div>
                            <div class="panel-body">
                                <table class="table table-bordered" style="color: #000000;">

                                    @if($item->budget_plan_category=='โครงการ')

                                        <tr style="background-color:#696964; color: #ffffff;">
                                            <th colspan="3"  style="width:50px;" class="text-center">ขออนุมัติใช้งบประมาณโครงการครั้งที่ 1</th>
                                            <th colspan="3"  style="width:50px;" class="text-center">ขออนุมัติใช้งบประมาณโครงการครั้งที่ 2</th>
                                        </tr>
                                        <tr style="background-color: #F5F5F5;">
                                            <th class="text-center" style="width: 20%;">จำนวนเงิน</th>
                                            <th class="text-center" style="width: 20%;">สถานะ</th>
                                            <th class="text-center" style="width: 10%;">#</th>
                                            <th class="text-center" style="width: 20%;">จำนวนเงิน</th>
                                            <th class="text-center" style="width: 20%;">สถานะ</th>
                                            <th class="text-center" style="width: 10%;">#</th>
                                        </tr>

                                        @if($my_group_id==4)

                                            @if($item->approval_status=='รอการตรวจสอบ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #f07c4a;" class="text-center">
                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        
                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='รอการตรวจสอบ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #f07c4a;" class="text-center">
                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">

                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>

                                                    </th>
                                                    <th colspan="3" class="text-center">

                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>

                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status=='อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-middle text-center">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status=='ไม่อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #b00;" class="text-center">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        
                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-middle text-center">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='ไม่อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #b00;" class="text-center">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #f07c4a;" class="text-center">
                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        
                                                    </td>
                                                    <td class="text-right">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @endif

                                        @else

                                            @if($item->approval_status=='รอการตรวจสอบ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #f07c4a;" class="text-center">
                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        
                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget-status/modal/{{$item->budget_plan_id}}">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status=='อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='รอการตรวจสอบ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #f07c4a;" class="text-center">
                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-center text-middle">
                                                        <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget-status/modal/{{$item->budget_plan_id}}">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-center text-middle">
                                                        <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status=='ไม่อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two == 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #b00;" class="text-center">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @elseif($item->approval_status_1=='ไม่อนุมัติ' && $item->money_received_first > 0 && $item->money_received_two > 0)
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td style="color: #1CB94E;" class="text-center">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        <a href="/download?path={{$item->file_received_first}}&name={{$item-> filename_received_first}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_two,2)}} ( {{number_format($item->pecen_received_two,2)}}% )
                                                    </td>
                                                    <td style="color: #b00;" class="text-center">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                        {{$item->approval_status_1}}   
                                                    </td>
                                                    <td class="text-center text-middle">
                                                    <a href="/download?path={{$item->file_received_two}}&name={{$item-> filename_received_two}}" title="ไฟล์แนบเอกสาร">
                                                            <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="text-right">
                                                        {{number_format($item->money_received_first,2)}} ( {{number_format($item->pecen_received_first,2)}}% )
                                                    </td>
                                                    <td class="text-center">
                                                        {{$item->approval_status}}   
                                                    </td>
                                                    <td class="text-middle text-center">
                                                        
                                                    </td>
                                                    <td class="text-right">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center text-middle">
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                    <th colspan="3" class="text-center">
                                                        <a id="display-list-only" style="margin:0;" class="btn btn-default" disabled="disabled">ปรับสถานะ</a>
                                                    </th>
                                                </tr>
                                            @endif
                                        @endif
                                    @else
                                        <tr style="background-color:#696964; color: #ffffff;">
                                            <th colspan="6" style="width:100px;" class="text-center">ขออนุมัติใช้งบประมาณครุภัณฑ์ทั้งหมด</th>
                                        </tr>
                                        <tr style="background-color: #F5F5F5;">
                                            <th class="text-center" style="width: 20%;">งบประมาณที่ได้รับการจัดสรร</th>
                                            <th class="text-center" style="width: 20%;">จำนวนเงินที่ขออนุมัติใช้</th>
                                            <th class="text-center" style="width: 20%;">ยอดเงินคงเหลือ</th>
                                            <th class="text-center" style="width: 20%;">สถานะ</th>
                                            <th class="text-center" style="width: 10%;">ไฟล์แนบ</th>
                                            <th class="text-center" style="width: 10%;">#</th>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                {{number_format($item->money_approve,2)}}
                                            </td>
                                            <td class="text-right">
                                                {{number_format($item->money_received,2)}}
                                            </td>
                                            <td class="text-right">
                                                <?php
                                                    $balance = $item->money_approve - ($item->money_received)
                                                ?>
                                                {{number_format($balance,2)}}
                                            </td>
                                            <td class="text-center">
                                                @if($item->approval_status=='รอการตรวจสอบ')
                                                    <span style="color: #f07c4a;" name="approval_status" id="approval_status"><i class="fa fa-spinner" aria-hidden="true"></i>
                                                        {{$item->approval_status}}
                                                    </span>
                                                @elseif($item->approval_status=='อนุมัติ')
                                                    <span style="color: #1CB94E;" name="approval_status" id="approval_status"><i class="fa fa-check" aria-hidden="true"></i>
                                                        {{$item->approval_status}}
                                                    </span>
                                                @elseif($item->approval_status=='ไม่อนุมัติ')
                                                    <span style="color: #b00;" name="approval_status" id="approval_status"><i class="fa fa-times" aria-hidden="true"></i>
                                                        {{$item->approval_status}}
                                                    </span>
                                                @else
                                                    <span name="approval_status" id="approval_status">
                                                        {{$item->approval_status}}
                                                    </span>
                                                @endif
                                            </td>
                                            @if($my_group_id==4)

                                                @if($item->approval_status=='รอการตรวจสอบ' && $item->money_received > 0)
                                                    <td class="text-center">
                                                        <a href="/download?path={{$item->file_received}}&name={{$item->filename_received}}" title="ไฟล์แนบเอกสาร">
                                                        <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                @elseif($item->approval_status=='อนุมัติ' || $item->approval_status=='ไม่อนุมัติ')
                                                    <td class="text-center">
                                                        <a href="/download?path={{$item->file_received}}&name={{$item->filename_received}}" title="ไฟล์แนบเอกสาร">
                                                        <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                @else
                                                    <td class="text-center">
                                                        
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                @endif

                                            @else

                                                @if($item->approval_status=='รอการตรวจสอบ' && $item->money_received > 0)
                                                    <td class="text-center">
                                                        <a href="/download?path={{$item->file_received}}&name={{$item->filename_received}}" title="ไฟล์แนบเอกสาร">
                                                        <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget-status/modal/{{$item->budget_plan_id}}">ปรับสถานะ</a>
                                                    </th>
                                                @elseif($item->approval_status=='อนุมัติ' || $item->approval_status=='ไม่อนุมัติ')
                                                    <td class="text-center">
                                                        <a href="/download?path={{$item->file_received}}&name={{$item->filename_received}}" title="ไฟล์แนบเอกสาร">
                                                        <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                                                        </a>
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" disabled="disabled">ขออนุมัติใช้</a>
                                                    </th>
                                                @else
                                                    <td class="text-center">
                                                        
                                                    </td>
                                                    <th colspan="3" class="text-center">
                                                        <a class="btn btn-default edit-content" href="/expenditure-budget/modal/{{$item->budget_plan_id}}">ขออนุมัติใช้</a>
                                                    </th>
                                                @endif
                                            @endif
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
            @endforeach
                </div>
                <div class="text-center">
                    {{$expenditure_list->render()}}
                </div>
            @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลคะ ::</span>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

