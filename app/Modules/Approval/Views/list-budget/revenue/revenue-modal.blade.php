<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 60%; margin-top: 150px;">

    <!-- Modal content-->
    <form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="POST" action="/revenue-budget/modal/{{$budget_plan_id}}" >
      {{csrf_field()}} 
      <input type="hidden" name="use_prcen" id="use_prcen" value="{{$use_prcen}}">
      <input type="hidden" name="total_money" id="total_money" value="{{$total_money}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title myfont" style="font-size: 22px;">ข้อมูลการปรับขออนุมัติใช้งบประมาณ</h4>
        </div>
        @if($budgetPlanApproval->budget_plan_category=='โครงการ')
          <br>
          @if(!is_numeric($budgetPlanApproval->pecen_received_first))
          <table>
            <tr>
              <td style="width: 200px; text-align: right;">0%</td>
              <td style="width: 800px;"><input type="range" min="0" style="width:80%;margin:10px auto;display:block;" max="100" value="{{$use_prcen}}" step="10" onchange="showValue(this.value)" /></td>
              <td style="width: 200px; text-align: left;">100%</td>
            </tr>
          </table>
          <br>
          @endif
          <p style="text-align: center;">งบประมาณที่ได้รับการจัดสรรทั้งหมด {{number_format($budgetPlanApproval->project_money_approve,2)}} บาท</p>
          <p style="text-align: center;">
            ขออนุมัติใช้งบประมาณ 
            <span id="range">{{$use_prcen}}</span>%
            ได้เป็นจำนวนเงิน : <span id="total-price"> {{number_format($total_money,2)}} </span> บาท
          </p>
          <p style="text-align: center;">
            ยอดงบประมาณคงเหลือ : <span id="sum-price"> {{number_format($balance,2)}} </span> บาท
          </p>
    
          <script type="text/javascript">
          $(document ).ready(function() {
               $('#total-price').digits();
               $('#sum-price').digits();
          });
          function showValue(newValue)
          {
            $('#range').text(newValue);
            $('#total-price').text((({{$budgetPlanApproval->project_money_approve}}*newValue)/100).toFixed(2)).digits();
            var total_price = ({{$budgetPlanApproval->project_money_approve}}-parseFloat($('#total-price').text().replace(',',''))).toFixed(2);
            $('#sum-price').text(total_price).digits();
            $('#use_prcen').val(newValue);
            $('#total_money').val(total_price);
          }
          </script>

          <div class="form-group" style="margin-top: 18px;">
              <label class="col-md-4 control-label" for="filebutton"> ไฟล์แนบเอกสารขออนุมัติใช้งบประมาณ :</label>
              <div class="col-md-3" id="uploadFileExpenditureFirst-container">
                  <button style="font-size: 16px;" id="data-uploadFileExpenditureFirst" type="button" class="myfont uploadFileExpenditureFirst form-control">
                      <i class="fa fa-file" aria-hidden="true"></i> 
                      แนบไฟล์
                  </button>
              </div>
              <label class="off-col-md-4 control-label" for="textinput" style="color: #b00;"> .pdf* .xls* .xlsx* .doc* .docx*</label>
          </div>

          <br>
          <br>
          <p style="color: red; text-align: center;"><span class="font-color-red"> * </span> หมายเหตุ : โครงการสามารถขออนุมัติใช้งบประมาณได้ 2 ครั้งค่ะ </p>
          <div style="height:10px;clear:both;"></div>

        @else
          <br>
          <br>
          <div class="form-group" style="margin-top: 18px;">
            <label class="col-md-5 control-label" for="filebutton">ขอใช้งบประมาณเป็นจำนวนเงิน :</label>
            <label class="col-md-2 control-label" style="text-align: center;" id="total-price"> {{number_format($budgetPlanApproval->money_approve,2)}}</label>
            <label class="col-md-5 control-label" style="text-align: left;">บาท</label>
          </div>
          
          <div class="form-group" style="margin-top: 18px;">
              <label class="col-md-5 control-label" for="filebutton"> ไฟล์แนบเอกสารขออนุมัติใช้งบประมาณ :</label>
              <div class="col-md-3" id="uploadFileExpenditure-container">
                  <button style="font-size: 16px;" id="data-uploadFileExpenditure" type="button" class="myfont uploadFileExpenditure form-control">
                      <i class="fa fa-file" aria-hidden="true"></i> 
                      แนบไฟล์
                  </button>
              </div>
              <label class="off-col-md-4 control-label" for="textinput" style="color: #b00;"> .pdf* .xls* .xlsx* .doc* .docx*</label>
          </div>
          <div style="height:10px;clear:both;"></div>

          <br>
          <br>
        @endif
        <div class="modal-footer">
            <div class="form-group">
                <div class="row">
                    <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                    <button id="login-btn" type="submit" class="form-control btn btn-login btn-lg myfont" type="submit">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        บันทึก
                        </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </form>
  </div>
</div>
    
<script type="text/javascript">$('.uploadFileExpenditureFirst').each(function(i,e){$(e).uploadFileExpenditureFirst();});</script>
<script type="text/javascript">$('.uploadFileExpenditure').each(function(i,e){$(e).uploadFileExpenditure();});</script>