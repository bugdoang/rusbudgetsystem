<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 60%; margin-top: 150px;">

    <!-- Modal content-->
    <form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="POST" action="/revenue-budget-status/modal/{{$budget_plan_id}}" >
      {{csrf_field()}} 
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title myfont" style="font-size: 22px;">การปรับสถานะการขอใช้งบประมาณรายได้</h4>
        </div>
        @if($budgetPlanApproval->budget_plan_category=='โครงการ')
          <br>
          <br>
          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ชื่อโครงการ :</label>
              <div class="col-md-6">
                  <input id="project_name" class="form-control input-md" type="text" name="project_name" value="{{$budgetPlanApproval->project_name or ''}}" placeholder="ชื่อโครงการ">
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">งบประมาณที่ได้รับการจัดสรร :</label>
              <div class="col-md-6">
                  <input id="project_money_approve" class="form-control input-md" type="text" name="project_money_approve" value="{{number_format($budgetPlanApproval->project_money_approve,2)}}" placeholder="งบประมาณที่ได้รับการจัดสรร">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 1 :</label>
              <div class="col-md-6">
                  <input id="money_received_first" class="form-control input-md" type="text" name="money_received_first" value="{{number_format($budgetPlanApproval->money_received_first,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 1">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 2 :</label>
              <div class="col-md-6">
                  <input id="money_received_two" class="form-control input-md" type="text" name="money_received_first" value="{{number_format($budgetPlanApproval->money_received_two,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบครั้งที่ 2">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ยอดเงินคงเหลือ :</label>
              <div class="col-md-6">
                  <input id="balance" readonly class="form-control input-md" type="text" name="balance" value="{{number_format($balance,2)}}" placeholder="ยอดเงินคงเหลือ">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ :</label>
              <div class="col-md-2">
                  <input id="position_abbreviation" class="form-control" type="text" name="position_abbreviation" value="{{$budgetPlanApproval->position_abbreviation}}">
              </div>
              <div class="col-md-4">
                  <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$budgetPlanApproval->first_name}}" placeholder="รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ">
              </div>
          </div>
          <br>
          <br>

          <div class="modal-footer">
              <div class="form-group">
                  <div class="col-md-6">
                      <div class="" style="width:50%; display:block; text-align:right; margin: auto;">
                        <button data-click1="1" id="login-btn" class="apv-submit1 form-control btn btn-login btn-lg myfont" type="button">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            อนุมัติ
                        </button>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="" style="width:50%; display:block; text-align:left; margin: auto;">
                        <button data-click1="2" id="login-btn1" type="button" class="apv-submit1 form-control btn btn-login1 btn-lg myfont">
                            <i class="fa fa-times" aria-hidden="true"></i>
                            ไม่อนุมัติ
                        </button>
                      </div>
                  </div>
              </div>
          </div>
        
        @else
          <br>
          <br>
          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ชื่อรายการครุภัณฑ์ :</label>
              <div class="col-md-6">
                  <input id="durable_article_name" class="form-control input-md" type="text" name="durable_article_name" value="{{$budgetPlanApproval->durable_article_name or ''}}" placeholder="ชื่อรายการครุภัณฑ์">
              </div>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">งบประมาณที่ได้รับการจัดสรร :</label>
              <div class="col-md-6">
                  <input id="money_received" class="form-control input-md" type="text" name="money_received" value="{{number_format($budgetPlanApproval->money_received,2)}}" placeholder="งบประมาณที่ได้รับการจัดสรร">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">ยอดเงินที่ขออนุมัติใช้งบ :</label>
              <div class="col-md-6">
                  <input id="money_received" class="form-control input-md" type="text" name="money_received" value="{{number_format($budgetPlanApproval->money_received,2)}}" placeholder="ยอดเงินที่ขออนุมัติใช้งบ">
              </div>
              <label class="col-md-2 control-label" style="text-align: left;" for="textinput">บาท</label>
          </div>

          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput">รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ :</label>
              <div class="col-md-2">
                  <input id="position_abbreviation" class="form-control" type="text" name="position_abbreviation" value="{{$budgetPlanApproval->position_abbreviation}}">
              </div>
              <div class="col-md-4">
                  <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$budgetPlanApproval->first_name}}" placeholder="รายชื่ออาจารย์ผู้ขออนุมัติใช้งบ">
              </div>
          </div>
          <br>
          <br>

          <div class="modal-footer">
              <div class="form-group">
                  <div class="col-md-6">
                      <div class="" style="width:50%; display:block; text-align:right; margin: auto;">
                        <button data-click="1" id="login-btn" class="apv-submit form-control btn btn-login btn-lg myfont" type="button">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            อนุมัติ
                        </button>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="" style="width:50%; display:block; text-align:left; margin: auto;">
                        <button data-click="2" id="login-btn1" type="button" class="apv-submit form-control btn btn-login1 btn-lg myfont">
                            <i class="fa fa-times" aria-hidden="true"></i>
                            ไม่อนุมัติ
                        </button>
                      </div>
                  </div>
              </div>
          </div>
        @endif
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
    $('.apv-submit').on('click',function(){
        var data_click = $(this).attr('data-click');
        if(data_click == 1){
          $('#form-plan-durable').attr("action",'/revenue-budget-status/modal/{{$budget_plan_id}}');
        } else {
          $('#form-plan-durable').attr("action",'/revenue-budget-status/modal/{{$budget_plan_id}}/reject');
        }
        $('#form-plan-durable').submit();
    });
</script>

<script type="text/javascript">
    $('.apv-submit1').on('click',function(){
        var data_click1 = $(this).attr('data-click1');
        if(data_click1 == 1){
          $('#form-plan-durable').attr("action",'/expenditure-budget-status/modal/{{$budget_plan_id}}');
        } else{
          $('#form-plan-durable').attr("action",'/expenditure-budget-status/modal/{{$budget_plan_id}}/reject');
        }
        $('#form-plan-durable').submit();
    });
</script>
<script type="text/javascript">$('.uploadFileExpenditureFirst').each(function(i,e){$(e).uploadFileExpenditureFirst();});</script>
<script type="text/javascript">$('.uploadFileExpenditure').each(function(i,e){$(e).uploadFileExpenditure();});</script>