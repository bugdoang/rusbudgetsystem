<?php
namespace App\Modules\Management\Models;

/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/26/16
 * Time: 3:38 PM
 */
use \Illuminate\Database\Eloquent\Model;
use DB;

class ManagementModel extends Model
{
    public static function getIncome($income_id='')
    {
        $income_item = DB::table('plan_incomes')
            ->whereNull('deleted_at');
            if(is_numeric($income_id) && $income_id>0)
            {
                $income_item->where('income_id',$income_id);
                return $income_item->first();
            }
            return $income_item->paginate(10);
    }
    public static function getIncomeType($income_type_id='')
    {
        $income_type_item = DB::table('plan_income_types')
            ->whereNull('deleted_at');
            if(is_numeric($income_type_id) && $income_type_id>0)
            {
                $income_type_item->where('income_type_id',$income_type_id);
                return $income_type_item->first();
            }
            return $income_type_item->paginate(10);
    }
    public static function getIncomeGroup($income_type_id=0)
    {
        $q= DB::table('plan_income_groups')
            ->whereNull('deleted_at');
        if(is_numeric($income_type_id) && $income_type_id>0)
        {
            $q->where('income_type_id',$income_type_id);
        }   
          return  $q->paginate(10);
    }
    public static function getVision($vision_id='')
    {
        $vision_item = DB::table('masterplan_visions')
            ->whereNull('deleted_at');
            if(is_numeric($vision_id) && $vision_id>0)
            {
                $vision_item->where('vision_id',$vision_id);
                return $vision_item->first();
            }
            return $vision_item->paginate(10);
    }
    
    public static function getFederalCircuit($federal_circuit_id='',$all=false)
    {
        $federal_circuit_item = DB::table('masterplan_federal_circuits')
            ->whereNull('deleted_at');
            if(is_numeric($federal_circuit_id) && $federal_circuit_id>0)
            {
                $federal_circuit_item->where('federal_circuit_id',$federal_circuit_id);
                return $federal_circuit_item->first();
            }
            if($all==TRUE) return $federal_circuit_item->get();
            return $federal_circuit_item->paginate(10);
    }

    public static function getIssue($federal_circuit_id=0,$tpye='',$all=false)
    {
        $q= DB::table('masterplan_issues')
            ->whereNull('deleted_at');
        if (empty($tpye)) 
        {
            if(is_numeric($federal_circuit_id) && $federal_circuit_id>0)
            {   
                $q->where('federal_circuit_id',$federal_circuit_id);
            }
        }
        else
        {
            $q->where('issue_id',$federal_circuit_id);
            return $q->first();
        }
        if($all==TRUE) return $q->get();
        return  $q->paginate(10);
    }
    public static function getGoal($issue_id=0,$type='',$all=false)
    {
        $q= DB::table('masterplan_goals')
            ->whereNull('deleted_at');
        if (empty($type))  
        {
            if(is_numeric($issue_id) && $issue_id>0)
            {
                $q->where('issue_id',$issue_id);
            }
        }  
        else
        {
            $q->where('goal_id',$issue_id);
            return $q->first();
        }
        if($all==TRUE) return $q->get();
        return  $q->paginate(10);
    }

    public static function getObjectiveIndicator($goal_id=0,$type='',$all=false)
    {
        $q= DB::table('masterplan_objective_indicators')
            ->whereNull('deleted_at');
        if (empty($type)) 
        {  
            if(is_numeric($goal_id) && $goal_id>0)
            {
                $q->where('goal_id',$goal_id);
            }
        }
        else
        {
            $q->where('objective_indicator_id',$goal_id);
            return $q->first();
        }
        if($all==TRUE) $q->get();
        return  $q->paginate(10);
    }
    public static function getStrategy($goal_id=0,$type='',$all=false)
    {
        $q= DB::table('masterplan_strategies')
            ->whereNull('deleted_at');
        if (empty($type))
        {
            if(is_numeric($goal_id) && $goal_id>0)
            {
                $q->where('goal_id',$goal_id);
            }
        }
        else
        {
            $q->where('strategy_id',$goal_id);
            return $q->first();
        }
        if($all==TRUE) return $q->get();
        return  $q->paginate(10);
    }
    public static function getObjectiveStrategy($strategy_id=0,$type='',$all=false)
    {
        $q= DB::table('masterplan_objective_strategies')
            ->whereNull('deleted_at');
        if (empty($type))
        {
          if(is_numeric($strategy_id) && $strategy_id>0)
            {
                $q->where('strategy_id',$strategy_id);
            }  
        }
        else
        {
            $q->where('objective_strategy_id',$strategy_id);
            return $q->first();
        }
        if($all==TRUE) return $q->get();
        return  $q->paginate(10);
    }
    public static function getKeyProject($strategy_id=0,$type='',$all=false)
    {
        $q= DB::table('masterplan_key_projects')
            ->whereNull('deleted_at');
        if (empty($type))
        {
           if(is_numeric($strategy_id) && $strategy_id>0)
            {
                $q->where('strategy_id',$strategy_id);
            } 
        }
        else
        {
            $q->where('key_project_id',$strategy_id);
            return $q->first();
        } 
        if($all==TRUE) return $q->get();  
        return  $q->paginate(10);
    }
    public static function getPlan()
    {
        return DB::table('plan_plans')
            ->whereNull('deleted_at')
            ->paginate(10);
    }
    public static function getProduct($plan_id=0)
    {
        $q= DB::table('plan_products')
            ->whereNull('deleted_at');
        if(is_numeric($plan_id) && $plan_id>0)
        {
            $q->where('plan_id',$plan_id);
        }
        return  $q->paginate(10);
    }

    public static function insertIncome($item)
    {
        return DB::table('plan_incomes')
            ->insertGetId($item);
    }
    public static function insertIncomeType($item)
    {
        return DB::table('plan_income_types')
            ->insertGetId($item);
    }
    public static function insertIncomeGroup($item)
    {
        return DB::table('plan_income_groups')
            ->insert($item);
    }
    public static function insertPlan($item)
    {
        return DB::table('plan_plans')
            ->insertGetId($item);
    }
    public static function insertProduct($item)
    {
        return DB::table('plan_products')
            ->insert($item);
    }
    public static function insertVision($item)
    {
        return DB::table('masterplan_visions')
            ->insertGetId($item);
    }
    public static function insertFederalCircuit($item)
    {
        return DB::table('masterplan_federal_circuits')
            ->insertGetId($item);
    }
    public static function insertIssue($item)
    {
        return DB::table('masterplan_issues')
            ->insertGetId($item);
    }
    public static function insertGoal($item)
    {
        return DB::table('masterplan_goals')
            ->insertGetId($item);
    }
    public static function insertObjectiveIndicator($item)
    {
        return DB::table('masterplan_objective_indicators')
            ->insertGetId($item);
    }
    public static function insertStrategy($item)
    {
        return DB::table('masterplan_strategies')
            ->insertGetId($item);
    }
    public static function insertObjectiveStrategy($item)
    {
        return DB::table('masterplan_objective_strategies')
            ->insertGetId($item);
    }
    public static function insertKeyProject($item)
    {
        return DB::table('masterplan_key_projects')
            ->insertGetId($item);
    }
    

    public static function getIncomeGroupId($income_type_id=0)
    {
        $q = DB::table('plan_income_groups')
            ->select([
                DB::raw('GROUP_CONCAT(income_group_name) as list'),
                'income_type_id'
            ])
            ->groupby('income_type_id');
        $q->orderBy('income_group_id','DESC');
        if($income_type_id!=0)
        {
            $q->where('income_type_id',$income_type_id);
        }
        return $q->whereNull('plan_income_groups.deleted_at')->get();
    }
    public static function getProductId($plan_id=0)
    {
        $q = DB::table('plan_products')
            ->select([
                DB::raw('GROUP_CONCAT(product_name) as list'),
                'plan_id'
            ])
            ->groupby('plan_id');
        $q->orderBy('product_id','DESC');
        if($plan_id!=0)
        {
            $q->where('plan_id',$plan_id);
        }
        return $q->whereNull('plan_products.deleted_at')->get();
    }

    public static function updateIncome($income_id,$item)
    {
        return DB::table('plan_incomes')
            ->where('income_id',$income_id)
            ->update($item);
    }
    public static function updateIncomeType($income_type_id,$item)
    {
        return DB::table('plan_income_types')
            ->where('income_type_id',$income_type_id)
            ->update($item);
    }
    public static function updatePlan($plan_id,$item)
    {
        return DB::table('plan_plans')
            ->where('plan_id',$plan_id)
            ->update($item);
    }
    public static function updateVision($vision_id,$item)
    {
        return DB::table('masterplan_visions')
            ->where('vision_id',$vision_id)
            ->update($item);
    }
    public static function updateFederalCircuit($federal_circuit_id,$item)
    {
        return DB::table('masterplan_federal_circuits')
            ->where('federal_circuit_id',$federal_circuit_id)
            ->update($item);
    }
    public static function updateIssue($issue_id,$item)
    {
        return DB::table('masterplan_issues')
            ->where('issue_id',$issue_id)
            ->update($item);
    }
    public static function updateGoal($goal_id,$item)
    {
        return DB::table('masterplan_goals')
            ->where('goal_id',$goal_id)
            ->update($item);
    }
    public static function updateObjectiveIndicator($objective_indicator_id,$item)
    {
        return DB::table('masterplan_objective_indicators')
            ->where('objective_indicator_id',$objective_indicator_id)
            ->update($item);
    }
    public static function updateStrategy($strategy_id,$item)
    {
        return DB::table('masterplan_strategies')
            ->where('strategy_id',$strategy_id)
            ->update($item);
    }
    public static function updateObjectiveStrategy($objective_strategy_id,$item)
    {
        return DB::table('masterplan_objective_strategies')
            ->where('objective_strategy_id',$objective_strategy_id)
            ->update($item);
    }
    public static function updateKeyProject($key_project_id,$item)
    {
        return DB::table('masterplan_key_projects')
            ->where('key_project_id',$key_project_id)
            ->update($item);
    }

    public static function deleteIncome($income_id)
    {
        return DB::table('plan_incomes')
            ->where('income_id','=',$income_id)
            ->update(['plan_incomes.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteIncomeType($income_type_id)
    {
        return DB::table('plan_income_types')
            ->where('income_type_id','=',$income_type_id)
            ->update(['plan_income_types.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deletePlan($plan_id)
    {
        return DB::table('plan_plans')
            ->where('plan_id','=',$plan_id)
            ->update(['plan_plans.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteVision($vision_id)
    {
        return DB::table('masterplan_visions')
            ->where('vision_id','=',$vision_id)
            ->update(['masterplan_visions.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteFederalCircuit($federal_circuit_id)
    {
        return DB::table('masterplan_federal_circuits')
            ->where('federal_circuit_id','=',$federal_circuit_id)
            ->update(['masterplan_federal_circuits.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteIssue($issue_id)
    {
        return DB::table('masterplan_issues')
            ->where('issue_id','=',$issue_id)
            ->update(['masterplan_issues.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteGoal($goal_id)
    {
        return DB::table('masterplan_goals')
            ->where('goal_id','=',$goal_id)
            ->update(['masterplan_goals.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteObjectiveIndicator($objective_indicator_id)
    {
        return DB::table('masterplan_objective_indicators')
            ->where('objective_indicator_id','=',$objective_indicator_id)
            ->update(['masterplan_objective_indicators.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteStrategy($strategy_id)
    {
        return DB::table('masterplan_strategies')
            ->where('strategy_id','=',$strategy_id)
            ->update(['masterplan_strategies.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteObjectiveStrategy($objective_strategy_id)
    {
        return DB::table('masterplan_objective_strategies')
            ->where('objective_strategy_id','=',$objective_strategy_id)
            ->update(['masterplan_objective_strategies.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
    public static function deleteKeyProject($key_project_id)
    {
        return DB::table('masterplan_key_projects')
            ->where('key_project_id','=',$key_project_id)
            ->update(['masterplan_key_projects.deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }

    public static function incomeTypeId($income_type_id)
    {
        $plan_income_types = DB::table('plan_income_types')
            ->where('plan_income_types.income_type_id',$income_type_id)
            ->whereNull('plan_income_types.deleted_at')
            ->first();
        if(!empty($plan_income_types))
        {
            $plan_income_types->groups=[];
            $temp = DB::table('plan_income_groups')
                ->whereNull('deleted_at')
                ->where('income_type_id',$plan_income_types->income_type_id)
                ->get();
            if(!empty($temp))
            {
                foreach($temp as $item)
                {
                    $plan_income_types->groups[] = ['income_group_name'=>$item->income_group_name];
                }
            }
        }
        return $plan_income_types;
    }
    public static function planId($plan_id)
    {
        $plan_plans = DB::table('plan_plans')
            ->where('plan_plans.plan_id',$plan_id)
            ->whereNull('plan_plans.deleted_at')
            ->first();
        if(!empty($plan_plans))
        {
            $plan_plans->groups=[];
            $temp = DB::table('plan_products')
                ->whereNull('deleted_at')
                ->where('plan_id',$plan_plans->plan_id)
                ->get();
            if(!empty($temp))
            {
                foreach($temp as $item)
                {
                    $plan_plans->groups[] = ['product_name'=>$item->product_name];
                }
            }
        }
        return $plan_plans;
    }
}



