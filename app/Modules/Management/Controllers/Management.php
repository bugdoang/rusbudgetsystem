<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/4/2016
 * Time: 5:08 PM
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use Auth;
use DB;
use Illuminate\Http\Request;

class Management extends Controller
{
    public function index(Request $request)
    {
        $page_type = $request->get('page_type');
       	switch ($page_type) {
       		case 'income': $html_page = \App\Modules\Management\Services\Management1::get();break;
       		case 'income-type': $html_page = \App\Modules\Management\Services\Management2::get();break;
       		case 'vision': $html_page = \App\Modules\Management\Services\Management3::get();break;
       		case 'federal-circuit': $html_page = \App\Modules\Management\Services\Management4::get();break;
       		case 'issue': $html_page = \App\Modules\Management\Services\Management5::get();break;
       		case 'goal': $html_page = \App\Modules\Management\Services\Management6::get();break;
       		case 'objective-indicator': $html_page = \App\Modules\Management\Services\Management7::get();break;
       		case 'strategy': $html_page = \App\Modules\Management\Services\Management8::get();break;
       		case 'objective-strategy': $html_page = \App\Modules\Management\Services\Management9::get();break;
          case 'key-project': $html_page = \App\Modules\Management\Services\Management10::get();break;
       		case 'plan': $html_page = \App\Modules\Management\Services\Management11::get();break;
       		default: 
       			$html_page = \App\Modules\Management\Services\Management1::get();
       			$page_type = 'income';
       			break;
       	}
        if(Auth::user()->group_id!=1)
        {
            return view('management::permission-denine',[
            ]);
        }
        return view('management::management',[
        	'html_page'=>$html_page,
        	'page_type'=>$page_type
        ]);
    }

    public function createForm(Request $request)
    {
        $form = $request->get('form');
        
        switch ($form) {
          case 'form2-income-type': $params['group'] = \App\Modules\Management\Models\ManagementModel::getIncomeGroup();break;
          case 'form4-federal-circuit': $params['vision'] = \App\Modules\Management\Models\ManagementModel::getVision();break;
          case 'form5-issue': $params['federal_circuit'] = \App\Modules\Management\Models\ManagementModel::getFederalCircuit(0,'',TRUE);
          break;
          case 'form6-goal': $params['issue'] = \App\Modules\Management\Models\ManagementModel::getIssue(0,'',TRUE);break;
          case 'form7-objective-indicator': $params['goal'] = \App\Modules\Management\Models\ManagementModel::getGoal();break;
          case 'form8-strategy': $params['goal'] = \App\Modules\Management\Models\ManagementModel::getGoal(0,'',TRUE);break;
          case 'form9-objective-strategy': $params['strategy'] = \App\Modules\Management\Models\ManagementModel::getStrategy(0,'',TRUE);break;
          case 'form10-key-project': $params['strategy'] = \App\Modules\Management\Models\ManagementModel::getStrategy(0,'',TRUE);break;
          case 'form11-plan': $params['product'] = \App\Modules\Management\Models\ManagementModel::getProduct(0,'',TRUE);break;

          default: 
            $params = [];
            break;
        }
        $method='POST';
        $action='/management/form';
        return view('management::create/'.$form,[
            'params'=>$params,
            'method'=>$method,
            'action'=>$action,
        ]);
    }

    public function saveForm(Request $request)
    {
      $form = $request->get('form');
        
        switch ($form) {
          case 'form1-income': $response = \App\Modules\Management\Services\Management1::insert($request);break;
          case 'form2-income-type': $response = \App\Modules\Management\Services\Management2::insert($request);break;
          case 'form3-vision': $response = \App\Modules\Management\Services\Management3::insert($request);break;
          case 'form4-federal-circuit': $response = \App\Modules\Management\Services\Management4::insert($request);break;
          case 'form5-issue': $response = \App\Modules\Management\Services\Management5::insert($request);break;
          case 'form6-goal': $response = \App\Modules\Management\Services\Management6::insert($request);break;
          case 'form7-objective-indicator': $response = \App\Modules\Management\Services\Management7::insert($request);break;
          case 'form8-strategy': $response = \App\Modules\Management\Services\Management8::insert($request);break;
          case 'form9-objective-strategy': $response = \App\Modules\Management\Services\Management9::insert($request);break;
          case 'form10-key-project': $response = \App\Modules\Management\Services\Management10::insert($request);break;
          case 'form11-plan': $response = \App\Modules\Management\Services\Management11::insert($request);break;
          default: 
          return response(['ขออภัยคะ ไม่สามารถบันทึกข้อมูลได้คะ'],422);
            $response = [];
            break;
        }
        return $response;
    }

    public function editForm($id,Request $request)
    {
        $form = $request->get('form');
        $item = NULL;
        
        switch ($form) {
          case 'form1-income':
          {
            $item = \App\Modules\Management\Models\ManagementModel::getIncome($id);
            break;
          } 
          case 'form2-income-type':
          {
            $item = \App\Modules\Management\Models\ManagementModel::incomeTypeId($id);
            break;
          } 
          case 'form3-vision':
          {
            $item = \App\Modules\Management\Models\ManagementModel::getVision($id);
            break;
          }
          case 'form4-federal-circuit':
          {
            $item = \App\Modules\Management\Models\ManagementModel::getFederalCircuit($id);
            $params['vision'] = \App\Modules\Management\Models\ManagementModel::getVision(0,'',TRUE);
            break;
          } 
          case 'form5-issue':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getIssue($id,'issue');
            $params['federal_circuit'] = \App\Modules\Management\Models\ManagementModel::getFederalCircuit(0,'',TRUE);
            break;
          }
          case 'form6-goal':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getGoal($id,'goal');
            $params['issue'] = \App\Modules\Management\Models\ManagementModel::getIssue(0,'',TRUE);
            break;
          } 
          case 'form7-objective-indicator':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getObjectiveIndicator($id,'objective_indicator');
            $params['goal'] = \App\Modules\Management\Models\ManagementModel::getGoal(0,'',TRUE);
            break;
          } 
          case 'form8-strategy':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getStrategy($id,'strategy');
            $params['goal'] = \App\Modules\Management\Models\ManagementModel::getGoal(0,'',TRUE);
            break;
          }
          case 'form9-objective-strategy':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getObjectiveStrategy($id,'objective_strategy');
            $params['strategy'] = \App\Modules\Management\Models\ManagementModel::getStrategy(0,'',TRUE);
            break;
          } 
          case 'form10-key-project':
          { 
            $item = \App\Modules\Management\Models\ManagementModel::getKeyProject($id,'key_project');
            $params['strategy'] = \App\Modules\Management\Models\ManagementModel::getStrategy(0,'',TRUE);
            break;
          } 
          case 'form11-plan':
          {
            $item = \App\Modules\Management\Models\ManagementModel::planId($id);
            break;
          } 
        }
        $method='PUT';
        $action='/management/'.$id;
        return view('management::create/'.$form,[
            'method'=>$method,
            'action'=>$action,
            'item'=>$item,
            'params'=>$params
        ]);
    }

    public function updateForm($id,Request $request)
    {
        $form = $request->get('form');
        switch ($form) {
          case 'form1-income': $response = \App\Modules\Management\Services\Management1::update($id,$request);break;
          case 'form2-income-type': $response = \App\Modules\Management\Services\Management2::update($id,$request);break;
          case 'form3-vision': $response = \App\Modules\Management\Services\Management3::update($id,$request);break;
          case 'form4-federal-circuit': $response = \App\Modules\Management\Services\Management4::update($id,$request);break;
          case 'form5-issue': $response = \App\Modules\Management\Services\Management5::update($id,$request);break;
          case 'form6-goal': $response = \App\Modules\Management\Services\Management6::update($id,$request);break;
          case 'form7-objective-indicator': $response = \App\Modules\Management\Services\Management7::update($id,$request);break;
          case 'form8-strategy': $response = \App\Modules\Management\Services\Management8::update($id,$request);break;
          case 'form9-objective-strategy': $response = \App\Modules\Management\Services\Management9::update($id,$request);break;
          case 'form10-key-project': $response = \App\Modules\Management\Services\Management10::update($id,$request);break;
          case 'form11-plan': $response = \App\Modules\Management\Services\Management11::update($id,$request);break;
          default: 
          return response(['ขออภัยคะ ไม่สามารถบันทึกข้อมูลได้คะ'],422);
            $response = [];
            break;
        }
        return $response;
    }

    public function deleteForm($id,Request $request)
    {
      $form = $request->get('form');
        switch ($form) {
          case 'form1-income': $response = \App\Modules\Management\Services\Management1::delete($id,$request);break;
          case 'form2-income-type': $response = \App\Modules\Management\Services\Management2::delete($id,$request);break;
          case 'form3-vision': $response = \App\Modules\Management\Services\Management3::delete($id,$request);break;
          case 'form4-federal-circuit': $response = \App\Modules\Management\Services\Management4::delete($id,$request);break;
          case 'form5-issue': $response = \App\Modules\Management\Services\Management5::delete($id,$request);break;
          case 'form6-goal': $response = \App\Modules\Management\Services\Management6::delete($id,$request);break;
          case 'form7-objective-indicator': $response = \App\Modules\Management\Services\Management7::delete($id,$request);break;
          case 'form8-strategy': $response = \App\Modules\Management\Services\Management8::delete($id,$request);break;
          case 'form9-objective-strategy': $response = \App\Modules\Management\Services\Management9::delete($id,$request);break;
          case 'form10-key-project': $response = \App\Modules\Management\Services\Management10::delete($id,$request);break;
          case 'form11-plan': $response = \App\Modules\Management\Services\Management11::delete($id,$request);break;
          default: 
          return response(['ขออภัยคะ ไม่สามารถลบข้อมูลได้คะ'],422);
            $response = [];
            break;
        }
        return $response;
    }
}
