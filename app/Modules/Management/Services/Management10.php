<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management10
{
    public static function get()
    {
        $key_projects=ManagementModel::getKeyProject();
        $key_projects->appends(['page_type'=>'key-project']);
        $html= view('management::tab/management-tab10',[
            'key_projects' =>  $key_projects,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $key_project_name = $request->get('key_project_name');
        $strategy_id = $request->get('strategy_id');
        $checks = array('key_project_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลโครงการสำคัญด้วยคะ'],422);
            }
        }
        if(!is_numeric($strategy_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนกลยุทธ์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertKeyProject([
            'key_project_name'=>$key_project_name,
            'strategy_id'=>$strategy_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=key-project']);
    }

    public static function update($id, $request)
    {   
        $key_project_name = $request->get('key_project_name');
        $strategy_id = $request->get('strategy_id');
        $checks = array('key_project_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลโครงการสำคัญด้วยคะ'],422);
            }
        }
        if(!is_numeric($strategy_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนกลยุทธ์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'key_project_name'=>$key_project_name,
            'strategy_id'=>$strategy_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateKeyProject($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form10-key-project']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteKeyProject($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

}