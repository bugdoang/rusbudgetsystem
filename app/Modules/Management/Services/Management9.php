<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management9
{
    public static function get()
    {
        $objective_strategies=ManagementModel::getObjectiveStrategy();
        $objective_strategies->appends(['page_type'=>'objective-strategy']);
        $html= view('management::tab/management-tab9',[
            'objective_strategies' =>  $objective_strategies,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $objective_strategy_name = $request->get('objective_strategy_name');
        $strategy_id = $request->get('strategy_id');
        $checks = array('objective_strategy_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลตัวชี้วัดกลยุทธ์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($strategy_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนกลยุทธ์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertObjectiveStrategy([
            'objective_strategy_name'=>$objective_strategy_name,
            'strategy_id'=>$strategy_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=objective-strategy']);
    }

    public static function update($id, $request)
    {   
        $objective_strategy_name = $request->get('objective_strategy_name');
        $strategy_id = $request->get('strategy_id');
        $checks = array('objective_strategy_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลตัวชี้วัดกลยุทธ์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($strategy_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนกลยุทธ์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'objective_strategy_name'=>$objective_strategy_name,
            'strategy_id'=>$strategy_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateObjectiveStrategy($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form9-objective-strategy']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteObjectiveStrategy($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}