<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management8
{
    public static function get()
    {
        $strategies=ManagementModel::getStrategy();
        $strategies->appends(['page_type'=>'strategy']);
        $html= view('management::tab/management-tab8',[
            'strategies' =>  $strategies,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $strategy_name = $request->get('strategy_name');
        $goal_id = $request->get('goal_id');
        $checks = array('strategy_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลกลยุทธ์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($goal_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนเป้าประสงค์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertStrategy([
            'strategy_name'=>$strategy_name,
            'goal_id'=>$goal_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=strategy']);
    }

    public static function update($id, $request)
    {   
        $strategy_name = $request->get('strategy_name');
        $goal_id = $request->get('goal_id');
        $checks = array('strategy_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลกลยุทธ์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($goal_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนเป้าประสงค์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'strategy_name'=>$strategy_name,
            'goal_id'=>$goal_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateStrategy($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form8-strategy']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteStrategy($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }



}