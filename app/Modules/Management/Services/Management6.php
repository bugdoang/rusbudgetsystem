<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management6
{
    public static function get()
    {
        $goals=ManagementModel::getGoal();
        $goals->appends(['page_type'=>'goal']);
        $html= view('management::tab/management-tab6',[
            'goals' =>  $goals,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $goal_name = $request->get('goal_name');
        $issue_id = $request->get('issue_id');
        $checks = array('goal_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลเป้าประสงค์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($issue_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนประเด็นยุทธศาสตร์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertGoal([
            'goal_name'=>$goal_name,
            'issue_id'=>$issue_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=goal']);
    }

    public static function update($id, $request)
    {   
        $goal_name = $request->get('goal_name');
        $issue_id = $request->get('issue_id');
        $checks = array('goal_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลเป้าประสงค์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($issue_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนประเด็นยุทธศาสตร์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'goal_name'=>$goal_name,
            'issue_id'=>$issue_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateGoal($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form6-goal']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteGoal($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}