<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management3
{
    public static function get()
    {
        $visions=ManagementModel::getVision();

        $visions->appends(['page_type'=>'vision']);

        $html= view('management::tab/management-tab3',[
            'visions' =>  $visions,
        ]);
        
        return $html->render();
    }

    public static function insert($request)
    {
    	$vision_name = $request->get('vision_name');

        $checks = array('vision_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกวิสัยทัศน์ด้วยคะ'],422);
            }
        }
        ManagementModel::insertVision([
            'vision_name'=>$vision_name,
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=vision']);
    }

    public static function update($id, $request)
    {
        $vision_name = $request->vision_name;
        
        $messages = array();
        if($vision_name == '')
        {
            $messages[] = "ขออภัยคะ กรุณากรอกวิสัยทัศน์ด้วยคะ";
        }
        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        $item=[
            'vision_name'=>$vision_name,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateVision($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form3-vision']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteVision($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}