<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management4
{
    public static function get()
    {
        $federal_circuits=ManagementModel::getFederalCircuit();
        $federal_circuits->appends(['page_type'=>'federal-circuit']);
        $html= view('management::tab/management-tab4',[
            'federal_circuits' =>  $federal_circuits,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $federal_circuit_name = $request->get('federal_circuit_name');
        $vision_id = $request->get('vision_id');
        $checks = array('federal_circuit_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลพันธกิจด้วยคะ'],422);
            }
        }
        if(!is_numeric($vision_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนวิสัยทัศน์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertFederalCircuit([
            'federal_circuit_name'=>$federal_circuit_name,
            'vision_id'=>$vision_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=federal-circuit']);
    }

    public static function update($id, $request)
    {   
        $federal_circuit_name = $request->get('federal_circuit_name');
        $vision_id = $request->get('vision_id');
        $checks = array('federal_circuit_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลพันธกิจด้วยคะ'],422);
            }
        }
        if(!is_numeric($vision_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนวิสัยทัศน์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'federal_circuit_name'=>$federal_circuit_name,
            'vision_id'=>$vision_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateFederalCircuit($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form4-federal-circuit']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteFederalCircuit($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}