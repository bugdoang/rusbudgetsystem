<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
use DB;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management1
{
    public static function get()
    {
        $incomes=ManagementModel::getIncome();

        $incomes->appends(['page_type'=>'incomes']);

        $html= view('management::tab/management-tab1',[
            'incomes' =>  $incomes,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {
    	$income_name = $request->get('income_name');

        $checks = array('income_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลแหล่งเงินได้ด้วยคะ'],422);
            }
        }
        ManagementModel::insertIncome([
            'income_name'=>$income_name,
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=income']);
    }

    public static function update($id, $request)
    {
        $income_name = $request->income_name;
        
        $messages = array();
        if($income_name == '')
        {
            $messages[] = "ขออภัยคะ กรุณากรอกข้อมูลแหล่งเงินได้ด้วยคะ";
        }
        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        $item=[
            'income_name'=>$income_name,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateIncome($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form1-income']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteIncome($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }


}