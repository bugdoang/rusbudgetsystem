<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management5
{
    public static function get()
    {
        $issuese=ManagementModel::getIssue();
        $issuese->appends(['page_type'=>'issue']);
        $html= view('management::tab/management-tab5',[
            'issuese' =>  $issuese,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $issue_name = $request->get('issue_name');
        $federal_circuit_id = $request->get('federal_circuit_id');
        $checks = array('issue_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลประเด็นยุทธศาสตร์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($federal_circuit_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนพันธกิจให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertIssue([
            'issue_name'=>$issue_name,
            'federal_circuit_id'=>$federal_circuit_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=issue']);
    }

    public static function update($id, $request)
    {   
        $issue_name = $request->get('issue_name');
        $federal_circuit_id = $request->get('federal_circuit_id');
        $checks = array('issue_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลประเด็นยุทธศาสตร์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($federal_circuit_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนพันธกิจให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'issue_name'=>$issue_name,
            'federal_circuit_id'=>$federal_circuit_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateIssue($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form5-issue']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteIssue($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

}