<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management7
{
    public static function get()
    {
        $objective_indicators=ManagementModel::getObjectiveIndicator();
        $objective_indicators->appends(['page_type'=>'objective-indicator']);
        $html= view('management::tab/management-tab7',[
            'objective_indicators' =>  $objective_indicators,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {   
        $objective_indicator_name = $request->get('objective_indicator_name');
        $goal_id = $request->get('goal_id');
        $checks = array('objective_indicator_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลตัวชี้วัดเป้าประสงค์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($goal_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนเป้าประสงค์ให้ถูกต้องด้วยคะ'],422);
        }
        ManagementModel::insertObjectiveIndicator([
            'objective_indicator_name'=>$objective_indicator_name,
            'goal_id'=>$goal_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=objective-indicator']);
    }

    public static function update($id, $request)
    {   
        $objective_indicator_name = $request->get('objective_indicator_name');
        $goal_id = $request->get('goal_id');
        $checks = array('objective_indicator_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลตัวชี้วัดเป้าประสงค์ด้วยคะ'],422);
            }
        }
        if(!is_numeric($goal_id))
        {
            return response(['ขออภัยคะ กรุณาป้อนประเด็นยุทธศาสตร์ให้ถูกต้องด้วยคะ'],422);
        }
        $item=[
            'objective_indicator_name'=>$objective_indicator_name,
            'goal_id'=>$goal_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateObjectiveIndicator($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form7-objective-indicator']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteObjectiveIndicator($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}