<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
use DB;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management11
{
    public static function get($item=NULL)
    {
        $getProductId=ManagementModel::getProductId();
        $plans=ManagementModel::getPlan();

        $plans->appends(['page_type'=>'plan']);

        $html= view('management::tab/management-tab11',[
            'plans' =>  $plans,
            'getProductId' => $getProductId,
        ]);
        
        return $html->render();
    }
    public static function insert($request)
    {
        $plan_name = $request->get('plan_name');
        $product = $request->get('product_name');

        if (is_array($product))
        {
            $plan_products = [];
            foreach ($product as $index=>$item)
            {
                $plan_products[] = [
                    'product_name'=>trim($item),
                    'plan_id'=>0,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ];
            }
        }

        $checks = array('plan_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลแผนงานด้วยคะ'],422);
            }
        }
        
        if(!empty($plan_products))
        {
            $plan_id = ManagementModel::insertPlan([
                'plan_name'=>$plan_name,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);

            $products = array_map(function($item) use($plan_id) {
                $item['plan_id'] = $plan_id;
                return $item;
            },$plan_products);
             ManagementModel::insertProduct($products);
        }
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=plan']);
    }

    public static function update($id, $request)
    {
        $plan_name = $request->get('plan_name');
        $product_name = $request->get('product_name');

        $messages = array();
        if($plan_name == '')
        {
            $messages[] = "ขออภัยคะ กรุณากรอกข้อมูลแผนงานด้วยคะ";
        }
        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        DB::table('plan_products')
            ->where('plan_id',$id)
            ->delete();

        if(!empty($product_name))
        {
            if (!empty($product_name) && is_array($product_name))
            {
                $plan_products = [];
                foreach ($product_name as $index=>$item)
                {
                    $plan_products[] = [
                        'product_name'=>$item,
                        'plan_id'=>$id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }
                if(!empty($plan_products))
                {
                    DB::table('plan_products')->insert($plan_products);
                    
                }
            }
        }
        $item=[
            'plan_name'=>$plan_name,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updatePlan($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form11-plan']);
    }

    public static function delete($id)
    {
        ManagementModel::deletePlan($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

}