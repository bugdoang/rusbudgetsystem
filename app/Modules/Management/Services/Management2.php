<?php
namespace App\Modules\Management\Services;
use App\Modules\Management\Controllers\Management;
use App\Modules\Management\Models\ManagementModel;
use DB;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class Management2
{
    public static function get($item=NULL)
    {
        $getIncomeGroupId=ManagementModel::getIncomeGroupId();
        $income_types=ManagementModel::getIncomeType();
        
        $income_types->appends(['page_type'=>'income-type']);

        $html= view('management::tab/management-tab2',[
            'getIncomeGroupId' => $getIncomeGroupId,
            'income_types' =>  $income_types,
        ]);
        return $html->render();
    }

    public static function insert($request)
    {
        $income_type_name = $request->get('income_type_name');
        $income_group = $request->get('income_group_name');

        if (is_array($income_group))
        {
            $plan_income_groups = [];
            foreach ($income_group as $index=>$item)
            {
                $plan_income_groups[] = [
                    'income_group_name'=>trim($item),
                    'income_type_id'=>0,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ];
            }
        }

        $checks = array('income_type_name');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลให้ครบทุกช่องด้วยคะ'],422);
            }
        }
        
        if(!empty($plan_income_groups))
        {
            $type_id = ManagementModel::insertIncomeType([
                'income_type_name'=>$income_type_name,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);

            $groups = array_map(function($item) use($type_id) {
                $item['income_type_id'] = $type_id;
                return $item;
            },$plan_income_groups);
             ManagementModel::insertIncomeGroup($groups);
        }
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management?page_type=income-type']);
    }

    public static function update($id, $request)
    {
        $income_type_name = $request->income_type_name;
        $income_group_name = $request->income_group_name;

        $messages = array();
        if($income_type_name == '')
        {
            $messages[] = "ขออภัยคะ กรุณากรอกข้อมูลแหล่งเงินได้ด้วยคะ";
        }
        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        DB::table('plan_income_groups')
            ->where('income_type_id',$id)
            ->delete();

        if(!empty($income_group_name))
        {
            if (!empty($income_group_name) && is_array($income_group_name))
            {
                $plan_income_groups = [];
                foreach ($income_group_name as $index=>$item)
                {
                    $plan_income_groups[] = [
                        'income_group_name'=>$item,
                        'income_type_id'=>$id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }
                if(!empty($plan_income_groups))
                {
                    DB::table('plan_income_groups')->insert($plan_income_groups);
                    
                }
            }
        }
        $item=[
            'income_type_name'=>$income_type_name,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        ManagementModel::updateIncomeType($id,$item);
        return response(['message'=>'บันทึกข้อมูลเรียบร้อยแล้วคะ','callback'=>'callback_form','redirect'=>'/management/'.$id.'?form=form2-income-type']);
    }

    public static function delete($id)
    {
        ManagementModel::deleteIncomeType($id);
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}