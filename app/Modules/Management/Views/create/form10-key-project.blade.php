@extends('masterlayout')
@section('title','ข้อมูลโครงการสำคัญ')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=key-project'],['label'=>'ข้อมูลโครงการสำคัญ','link'=>'management-key-project/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form10-key-project">
        <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลโครงการสำคัญ</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> โครงการสำคัญ :</label>
                    <div class="col-md-7">
                        <input id="key_project_name" class="form-control input-md" type="text" name="key_project_name" value="{{isset($item)?$item->key_project_name:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> กลยุทธ์ : </label>
                    <div class="col-md-7">
                        <select class="form-control" name="strategy_id" id="strategy_id" data-clear="false">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($params['strategy']))
                                @foreach($params['strategy'] as $param)
                                    <option {{(isset($item->strategy_id) && $param->strategy_id==$item->strategy_id)?' selected ':''}} value="{{$param->strategy_id}}">
                                        {{$param->strategy_name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush