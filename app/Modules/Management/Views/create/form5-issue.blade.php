@extends('masterlayout')
@section('title','ข้อมูลประเด็นยุทธศาสตร์')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=issue'],['label'=>'ข้อมูลประเด็นยุทธศาสตร์','link'=>'management-issue/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form5-issue">
        <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลประเด็นยุทธศาสตร์</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> ประเด็นยุทธศาสตร์ :</label>
                    <div class="col-md-7">
                        <input id="issue_name" class="form-control input-md" type="text" name="issue_name" value="{{isset($item)?$item->issue_name:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> พันธกิจ : </label>
                    <div class="col-md-7">
                        <select class="form-control" name="federal_circuit_id" id="federal_circuit_id" data-clear="false">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($params['federal_circuit']))
                                @foreach($params['federal_circuit'] as $param)
                                    <option {{(isset($item->federal_circuit_id) && $param->federal_circuit_id==$item->federal_circuit_id)?' selected ':''}} value="{{$param->federal_circuit_id}}">
                                        {{$param->federal_circuit_name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush