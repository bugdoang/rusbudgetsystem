@extends('masterlayout')
@section('title','ข้อมูลตัวชี้วัดเป้าประสงค์')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=objective-indicator'],['label'=>'ข้อมูลตัวชี้วัดเป้าประสงค์','link'=>'management-objective-indicator/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form7-objective-indicator">
        <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลตัวชี้วัดเป้าประสงค์</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> ตัวชี้วัดเป้าประสงค์ :</label>
                    <div class="col-md-7">
                        <input id="objective_indicator_name" class="form-control input-md" type="text" name="objective_indicator_name" value="{{isset($item)?$item->objective_indicator_name:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> เป้าประสงค์ : </label>
                    <div class="col-md-7">
                        <select class="form-control" name="goal_id" id="goal_id" data-clear="false">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($params['goal']))
                                @foreach($params['goal'] as $param)
                                    <option {{(isset($item->goal_id) && $param->goal_id==$item->goal_id)?' selected ':''}} value="{{$param->goal_id}}">
                                        {{$param->goal_name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush