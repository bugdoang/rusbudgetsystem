@extends('masterlayout')
@section('title','ข้อมูลพันธกิจ')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=federal-circuit'],['label'=>'ข้อมูลพันธกิจ','link'=>'management-federal-circuit/form']]) !!}
<form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form4-federal-circuit">
    <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลพันธกิจ</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> พันธกิจ :</label>
                    <div class="col-md-7">
                        <input id="federal_circuit_name" class="form-control input-md" type="text" name="federal_circuit_name" value="{{isset($item)?$item->federal_circuit_name:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> วิสัยทัศน์ : </label>
                    <div class="col-md-7">
                        <select class="form-control" name="vision_id" id="vision_id" data-clear="false">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($params['vision']))
                                @foreach($params['vision'] as $param)
                                    <option {{(isset($item->vision_id) && $param->vision_id==$item->vision_id)?' selected ':''}} value="{{$param->vision_id}}">
                                        {{$param->vision_name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush