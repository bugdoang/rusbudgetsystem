@extends('masterlayout')
@section('title','ข้อมูลแหล่งเงินได้')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=income'],['label'=>'ข้อมูลแหล่งเงินได้','link'=>'management-income/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form1-income">
    <!-- <div style="padding-right: 0;" class="col-lg-12 col-md-12 form-planbudget shadow"> -->
        <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลแหล่งเงินได้</h2>
            <div class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> แหล่งเงินได้ :</label>
                    <div class="col-md-8">
                        <input id="income_name" class="form-control input-md" type="text" name="income_name" value="{{isset($item)?$item->income_name:''}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
@endpush