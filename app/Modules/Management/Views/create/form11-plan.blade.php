@extends('masterlayout')
@section('title','ข้อมูลแผนงาน')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'/management?page_type=plan'],['label'=>'ข้อมูลแผนงาน','link'=>'management-plan/form']]) !!}

<!-- Page Content -->
<form id="form-personnel" name="form-personnel" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="form" value="form11-plan">
        <div class="tab-content">
            <h2 class="header-text myfont">ข้อมูลแผนงาน</h2>
            <div id="product" class="col-lg-12 col-md-12 form-planbudget shadow">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> แผนงาน :</label>
                    <div class="col-md-7">
                        <input id="plan_name" class="form-control input-md" type="text" name="plan_name" value="{{isset($item)?$item->plan_name:''}}">
                    </div>
                </div>
                <div class="site-breadcrumb" style="margin: 3px 85px;">
                    <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> ผลผลิต</a>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> ผลผลิต :</label>
                    <div class="col-md-7">
                        <table class="table table-bordered">
                            <tbody align="center">
                                <tr>
                                    <th class="text-center text-middle" style="width: 60px;">#</th>
                                    <th class="text-center text-middle">ผลผลิต</th>
                                    <th class="text-center text-middle" style="width: 30px;">ลบ</th>
                                </tr>
                                <tr v-for="(item,index) in items">
                                    <td>
                                        <span>@{{index+1}}</span>
                                    </td>
                                    <td>
                                        <input name="product_name[]" class="form-control input-md" style="width: 100%;" type="text" v-model="item.product_name">
                                    </td>
                                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0"  v-on:click="remove_item(index)">ลบ</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:10px;clear:both;"></div>
        </div>
    </div>
</form>
@stop
@push('scripts')
<script type="text/javascript">
    function callback_form(data)
    {
        MessageBox.alert(data.message,function(){
            window.location.href=data.redirect;
        });
    }
</script>
<script type="text/javascript">
    var product = new Vue({
        el: '#product',
        data: {
            @if(isset($item))
            items:{!! json_encode($item->groups,JSON_NUMERIC_CHECK) !!}
            @else
            items:[]
            @endif
        },
        created:function() {

            @if(empty($item))
            this.items.push({
                product_name:'',
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    product_name:'',
                })
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush