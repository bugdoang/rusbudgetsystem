<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form8-strategy"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลกลยุทธ์</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลกลยุทธ์</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลกลยุทธ์</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($strategies as $index=>$item)
                <tr>
                    <td>
                        <span name="strategy_id" id="strategy_id">{{($strategies->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="strategy_name" id="strategy_name">
                        {{$item->strategy_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->strategy_id}}?form=form8-strategy" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->strategy_id}}?form=form8-strategy" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$strategies->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>