<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form3-vision"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลวิสัยทัศน์</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลวิสัยทัศน์</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลวิสัยทัศน์</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($visions as $index=>$item)
                <tr>
                    <td>
                        <span name="vision_id" id="vision_id">{{($visions->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="vision_name" id="vision_name">
                        {{isset($item)?$item->vision_name:''}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->vision_id}}?form=form3-vision" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->vision_id}}?form=form3-vision" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$visions->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>