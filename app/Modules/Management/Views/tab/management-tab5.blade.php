<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form5-issue"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลประเด็นยุทธศาสตร์</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลประเด็นยุทธศาสตร์</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลประเด็นยุทธศาสตร์</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($issuese as $index=>$item)
                <tr>
                    <td>
                        <span name="issue_id" id="issue_id">{{($issuese->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="issue_name" id="issue_name">
                        {{$item->issue_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->issue_id}}?form=form5-issue" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->issue_id}}?form=form5-issue" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$issuese->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>