<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form6-goal"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลเป้าประสงค์</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลเป้าประสงค์</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลเป้าประสงค์</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($goals as $index=>$item)
                <tr>
                    <td>
                        <span name="goal_id" id="goal_id">{{($goals->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="goal_name" id="goal_name">
                        {{$item->goal_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->goal_id}}?form=form6-goal" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->goal_id}}?form=form6-goal" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$goals->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>