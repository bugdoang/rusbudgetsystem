<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form1-income"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลแหล่งเงินได้</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลแหล่งเงินได้</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลแหล่งเงินได้</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($incomes as $index=>$item)
                <tr>
                    <td>
                        <span name="income_id" id="income_id">{{($incomes->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="income_name" id="income_name">
                        {{isset($item)?$item->income_name:''}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->income_id}}?form=form1-income" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->income_id}}?form=form1-income" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$incomes->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>