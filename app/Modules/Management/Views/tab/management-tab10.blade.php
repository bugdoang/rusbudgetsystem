<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form10-key-project"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลโครงการสำคัญ</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลโครงการสำคัญ</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลโครงการสำคัญ</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($key_projects as $index=>$item)
                <tr>
                    <td>
                        <span name="key_project_id" id="key_project_id">{{($key_projects->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="key_project_name" id="key_project_name">
                        {{$item->key_project_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->key_project_id}}?form=form10-key-project" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->key_project_id}}?form=form10-key-project" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$key_projects->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>