<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form4-federal-circuit"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลพันธกิจ</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลพันธกิจ</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลพันธกิจ</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($federal_circuits as $index=>$item)
                <tr>
                    <td>
                        <span name="federal_circuit_id" id="federal_circuit_id">{{($federal_circuits->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="federal_circuit_name" id="federal_circuit_name">
                        {{$item->federal_circuit_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->federal_circuit_id}}?form=form4-federal-circuit" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->federal_circuit_id}}?form=form4-federal-circuit" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$federal_circuits->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>