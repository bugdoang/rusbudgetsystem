<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form2-income-type"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลประเภทแหล่งเงินได้</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลประเภทแหล่งเงินได้</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" colspan="4">รายการข้อมูลประเภทแหล่งเงินได้</th>
                </tr>
                <tr>
                    <th class="text-center text-middle" rowspan="1" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" rowspan="1" style="width: 300px;">ประเภทแหล่งเงินได้</th>
                    <th class="text-center text-middle" rowspan="1" style="width: 300px;">หมวดแหล่งเงินได้</th>
                    <th class="text-center text-middle" rowspan="1" style="width: 50px;">#</th>
                </tr>
                @foreach($income_types as $index=>$item)
                <tr>
                    <td>
                        <span name="income_type_id" id="income_type_id">{{($income_types->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="income_type_name" id="income_type_name">
                            {{isset($item)?$item->income_type_name:''}}
                        </span>
                    </td>
                    <td class="text-left">
                        @foreach($getIncomeGroupId as $items)
                            @if($item->income_type_id==$items->income_type_id && $items->list!='')
                                {{isset($items)?$items->list:''}}
                            @endif
                        @endforeach
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->income_type_id}}?form=form2-income-type" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->income_type_id}}?form=form2-income-type" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$income_types->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>