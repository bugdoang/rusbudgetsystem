<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form11-plan"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลแผนงาน</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลแผนงาน</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลแผนงาน</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($plans as $index=>$item)
                <tr>
                    <td>
                        <span name="plan_id" id="plan_id">{{($plans->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="plan_name" id="plan_name">
                            {{isset($item)?$item->plan_name:''}}
                            @foreach($getProductId as $items)
                                @if($item->plan_id==$items->plan_id && $items->list!='')
                                    <br /><span class="font-color-red"> * </span> 
                                    ผลผลิต [ {{isset($items)?$items->list:''}} ]
                                @endif
                            @endforeach
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->plan_id}}?form=form11-plan" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->plan_id}}?form=form11-plan" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$plans->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>