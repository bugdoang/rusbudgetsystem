<div>
    {{csrf_field()}}
    <div class="site-breadcrumb-management">
        <a class="btn btn-default" href="/management/form?form=form7-objective-indicator"><i class="fa fa-plus" aria-hidden="true"></i> ข้อมูลตัวชี้วัดเป้าประสงค์</a>
    </div>
    <h2 class="header-text myfont">รายการรายละเอียดข้อมูลตัวชี้วัดเป้าประสงค์</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;" >#</th>
                    <th class="text-center text-middle" style="width: 400px;">รายการข้อมูลตัวชี้วัดเป้าประสงค์</th>
                    <th class="text-center text-middle" style="width: 50px;">#</th>
                </tr>
                @foreach($objective_indicators as $index=>$item)
                <tr>
                    <td>
                        <span name="objective_indicator_id" id="objective_indicator_id">{{($objective_indicators->firstItem()+$index)}}</span>
                    </td>
                    <td class="text-left">
                        <span name="objective_indicator_name" id="objective_indicator_name">
                        {{$item->objective_indicator_name}}
                        </span>
                    </td>
                    <td class="text-center">
                        <a href="/management/{{$item->objective_indicator_id}}?form=form7-objective-indicator" title="แก้ไข">
                            <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                        </a>
                        <a class="delete-content" href="/management/{{$item->objective_indicator_id}}?form=form7-objective-indicator" title="ลบ">
                            <i class="fa fa-times" style="color:#b00;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
        {{$objective_indicators->render()}}
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>