@extends('masterlayout')
@section('title','การจัดการข้อมูลพื้นฐาน')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'การจัดการข้อมูลพื้นฐาน','link'=>'project-form']]) !!}
<!-- Page Content -->
<form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="durable_article_id" value="{{$budgetPlanDurable->durable_article_id or ''}}">
    <div class="row">
        <!--start left-->
        <div class="col-lg-3 col-md-3">
            <div class="row">
                <h2 class="header-text myfont">ประเภทข้อมูล</h2>
                <div class="tabs-left shadow">
                    <ul class="nav nav-tabs">
                        <li class="{{$page_type=='income'?'active':''}}">
                            <a href="/management/?page_type=income">1. ข้อมูลแหล่งเงินได้</a>
                        </li>
                        <li class="{{$page_type=='income-type'?'active':''}}">
                            <a href="/management/?page_type=income-type">2. ข้อมูลประเภท / หมวดแหล่งเงินได้</a>
                        </li>
                        <li class="{{$page_type=='vision'?'active':''}}">
                            <a href="/management/?page_type=vision">3. ข้อมูลวิสัยทัศน์</a>
                        </li>
                        <li class="{{$page_type=='federal-circuit'?'active':''}}">
                            <a href="/management/?page_type=federal-circuit">4. ข้อมูลพันธกิจ</a>
                        </li>
                        <li class="{{$page_type=='issue'?'active':''}}">
                            <a href="/management/?page_type=issue">5. ข้อมูลประเด็นยุทธศาสตร์</a>
                        </li>
                        <li class="{{$page_type=='goal'?'active':''}}">
                            <a href="/management/?page_type=goal">6. ข้อมูลเป้าประสงค์</a>
                        </li>
                        <li class="{{$page_type=='objective-indicator'?'active':''}}">
                            <a href="/management/?page_type=objective-indicator">7. ข้อมูลตัวชี้วัดเป้าประสงค์</a>
                        </li>
                        <li class="{{$page_type=='strategy'?'active':''}}">
                            <a href="/management/?page_type=strategy">8. ข้อมูลกลยุทธ์</a>
                        </li>
                        <li class="{{$page_type=='objective-strategy'?'active':''}}">
                            <a href="/management/?page_type=objective-strategy">9. ข้อมูลตัวชี้วัดกลยุทธ์</a>
                        </li>
                        <li class="{{$page_type=='key-project'?'active':''}}">
                            <a href="/management/?page_type=key-project">10. ข้อมูลโครงการสำคัญ</a>
                        </li>
                        <li class="{{$page_type=='plan'?'active':''}}">
                            <a href="/management/?page_type=plan">11. ข้อมูลแผนงาน / ผลผลิต</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--start right-->
        <div style="padding-right: 0;" class="col-lg-9 col-md-9">
            <div class="tab-content">
            {!!$html_page!!}
            </div>
        </div>
    </div>
</form>
@stop

@section('scripts')

<script>
    function save_success(data)
    {
        MessageBox.alert('ระบบได้บันทึกข้อมูลเรียบร้อยแล้วคะ',function(){
            window.location.href = data.redirect_url;
        });
    }
</script>
@stop
