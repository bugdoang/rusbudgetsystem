<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // List Page
    Route::get('management', '\App\Modules\Management\Controllers\Management@index');

    // Create Data
    Route::get('management/form', '\App\Modules\Management\Controllers\Management@createForm');
    Route::post('management/form', '\App\Modules\Management\Controllers\Management@saveForm');

    // Update Data
    Route::get('management/{id}', '\App\Modules\Management\Controllers\Management@editForm');
    Route::put('management/{id}', '\App\Modules\Management\Controllers\Management@updateForm');

    // Delete Form
    Route::delete('management/{id}', '\App\Modules\Management\Controllers\Management@deleteForm');
});