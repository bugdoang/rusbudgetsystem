<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/29/16
 * Time: 4:51 PM
 */

namespace App\Modules\Campus\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class CampusModel extends Model
{
    public static function getAll()
    {
        return DB::table('campuses')
            ->whereNull('deleted_at')
            ->get();
    }
}