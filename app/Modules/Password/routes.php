<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // Modal Form
    Route::get('password', '\App\Modules\Password\Controllers\Password@index');

    // Modal Update
    Route::put('password/{id}', '\App\Modules\Password\Controllers\Password@update');

});