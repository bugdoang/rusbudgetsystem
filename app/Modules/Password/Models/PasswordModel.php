<?php
namespace App\Modules\Password\Models;

/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/26/16
 * Time: 3:38 PM
 */
use \Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class PasswordModel extends Model
{
    public static function updatePassword($personnel_id,$item)
    {
        return DB::table('personnels')
            ->where('personnel_id',$personnel_id)
            ->update($item);
    }

    public static function checkPassword($personnel_id,$password)
    {
        $pass=DB::table('personnels')
            ->where('personnel_id',$personnel_id)
            ->first();
        if(!empty($pass))
        {
            if (Auth::attempt(array('password' => $password))) {
                return true;
            }
        }
        return false;
    }



}



