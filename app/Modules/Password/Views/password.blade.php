<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 50%; margin-top: 140px;">

    <!-- Modal content-->
    <form id="form-password" name="form-password" class="form-horizontal" role="form" method="PUT" action="password/{{$personnel_id}}">
    {{csrf_field()}}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title myfont" style="font-size: 22px;">เปลี่ยนรหัสผ่าน</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label class="col-md-5 control-label" for="textinput"><span class="font-color-red"> * </span> รหัสผ่านเดิม :</label>
              <div class="col-md-5">
                  <input id="oldpassword" maxlength="6" class="form-control input-md" type="password" name="oldpassword">
              </div>
          </div>
          <div class="form-group">
              <label class="col-md-5 control-label" for="textinput"><span class="font-color-red"> * </span> รหัสผ่านใหม่ :</label>
              <div class="col-md-5">
                  <input id="newpassword" maxlength="6" class="form-control input-md" type="password" name="newpassword">
              </div>
          </div>
          <div class="form-group">
              <label class="col-md-5 control-label" for="textinput"><span class="font-color-red"> * </span> ยืนยันรหัสผ่าน :</label>
              <div class="col-md-5">
                  <input id="confirmpassword" maxlength="6" class="form-control input-md" type="password" name="confirmpassword">
              </div>
          </div>
          <div style="height:10px;clear:both;"></div>
        </div>
        <div class="modal-footer">
            <div class="form-group">
                <div class="row">
                    <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                    <button id="login-btn" type="submit" class="form-control btn btn-login btn-lg myfont" type="submit">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        บันทึก
                        </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </form>
  </div>
</div>