<?php
namespace App\Modules\Password\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Password\Models\PasswordModel;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class Password extends Controller
{
   	public function index()
    {
        $personnel_id=Auth::user()->personnel_id;
    	$html=view('password::password',compact('personnel_id'));
        return ['title'=>'เปลี่ยนรหัสผ่าน','body'=>$html->render()];
    }

    public function update(Request $request)
    {
    	$personnel_id      =Auth::user()->personnel_id;
        $oldpassword   =$request->get('oldpassword');
        $newpassword   =$request->get('newpassword');
        $confirmpassword    =$request->get('confirmpassword');
        $check_pass     =PasswordModel::checkPassword($personnel_id,$oldpassword);
        
        $check=array('newpassword','oldpassword','confirmpassword');
        foreach ($check as $item)
        {
            if ($request->get ($item)=='')
            {
                return response(['ขออภัยคะ กรุณากรอกข้อมูลให้ครบทุกช่องคะ'],422);
            }
        }
        if(!$check_pass)
        {
            return response(['ขออภัยคะ รหัสผ่านเดิมไม่ถูกต้อง กรุณาป้อนใหม่อีกครั้งคะ'],422);
        }
        if($newpassword!=$confirmpassword)
        {
            return response(['ขออภัยคะ การยืนยันรหัสผ่านไม่ถูกต้อง กรุณาป้อนใหม่อีกครั้งคะ'], 422);
        }

        PasswordModel::updatePassword($personnel_id,[
            'password'  =>Hash::make($newpassword),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['บันทึกข้อมูลเรียบร้อยแล้วคะ'],200);
    }
}