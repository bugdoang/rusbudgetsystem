<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/29/16
 * Time: 4:51 PM
 */

namespace App\Modules\Group\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class GroupModel extends Model
{
    public static function getAll()
    {
        return DB::table('groups')
            ->whereNull('deleted_at')
            ->get();
    }
}