<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/29/16
 * Time: 4:51 PM
 */

namespace App\Modules\Branch\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class BranchModel extends Model
{
    public static function getAll()
    {
        return DB::table('branches')
            ->whereNull('deleted_at')
            ->get();
    }
}