<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {
    Route::get('profile', '\App\Modules\Profile\Controllers\Profile@index');
    Route::put('profile/{id}', '\App\Modules\Profile\Controllers\UpdateProfile@update');
});