<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 9/19/16
 * Time: 5:30 PM
 */

namespace App\Modules\Profile\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Http\Request;

class UpdateProfile extends Controller
{
    public function update($personnel_id, Request $request)
    {
        $title_name = $request->get('title_name');
        $first_name = $request->get('first_name');
        $position_id = $request->get('position_id');
        $branch_id = $request->get('branch_id');
        $campus_id = $request->get('campus_id');

        $checks = array('title_name','first_name','position_id','branch_id','campus_id');
        foreach ($checks as $check)
        {
            if($request->get($check)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่วงด้วยคะ'],422);
            }
        }
        if(!is_numeric($position_id))
        {
            return response(['กรุณาป้อนตำแหน่งให้ถูกต้องด้วยคะ']);
        }
        if(!is_numeric($branch_id))
        {
            return response(['กรุณาป้อนสาขาให้ถูกต้องด้วยคะ']);
        }
        if(!is_numeric($campus_id))
        {
            return response(['กรุณาป้อนศูนย์พื้นที่ให้ถูกต้องด้วยคะ']);
        }
        ProfileModel::update_profile($personnel_id,[
            'title_name'=>$title_name,
            'first_name'=>$first_name,
            'position_id'=>$position_id,
            'branch_id'=>$branch_id,
            'campus_id'=>$campus_id
        ]);

        return response(['บันทึกข้อมูลเรียบร้อยแล้วคะ']);
    }



}