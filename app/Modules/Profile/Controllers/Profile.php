<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/4/2016
 * Time: 5:08 PM
 */

namespace App\Modules\Profile\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Branch\Models\BranchModel;
use App\Modules\Campus\Models\CampusModel;
use App\Modules\Position\Models\PositionModel;
use App\Modules\Profile\Models\ProfileModel;
use Auth;
use Illuminate\Http\Request;

class Profile extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $personnel_id = Auth::id();//ตรวจสอบว่า login หรือยัง
        $profile_model = new ProfileModel();//สร้าง อินสแตน
        $my_profile = $profile_model->me($personnel_id);
        $title_names = config('myconfig.title_names');
        $positions = PositionModel::getAll();
        $branches = BranchModel::getAll();
        $campuses = CampusModel::getAll();


        return view('profile::profile', [
            'my_profile' => $my_profile,
            'title_names' => $title_names,
            'positions' => $positions,
            'branches' => $branches,
            'campuses' => $campuses

        ]);
    }

}
