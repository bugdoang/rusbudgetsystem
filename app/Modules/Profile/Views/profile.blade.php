@extends('masterlayout')
@section('title','ข้อมูลส่วนตัว')

@section('content')
<!-- Page Content -->
<form id="form-profile" name="form-profile" class="form-horizontal" role="form" method="PUT" action="profile/{{$my_profile->personnel_id}}" >
    {{csrf_field()}}
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h2 class="header-text myfont"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลส่วนตัว</h2>
                <div class="panel panel-info">
                    <ul id="myTab" class="nav nav-tabs nav_tabs">
                        <li class="active myfont" style="font-size: 18px;"><a href="#service-one" data-toggle="tab"> ข้อมูลส่วนตัว </a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="service-one">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4" align="center">
                                        <label class="col-md-4 control-label" for="filebutton">รูปบุคลากร :</label>
                                        <img alt="User Pic" id="personnel_picture" src="{{$my_profile->personnel_picture}}" class="img-circle img-responsive">
                                        <!-- File Button -->
                                        <div class="form-group" style="margin-top: 18px;">
                                            <div class="col-md-4" style="margin: -8px 193px;">
                                                <button data-profile="profile" class="button-picture myfont uploadFile">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    เลือกรูปภาพ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">คำนำหน้าชื่อ : </label>
                                            <div class="col-md-5">
                                                <select class="form-control" name="title_name" id="title_name">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(!empty($title_names))
                                                        @foreach($title_names as $title_name)
                                                            <option {{($my_profile->title_name==$title_name)?' selected ':''}} value="{{$title_name}}">{{$title_name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">ตำแหน่งทางวิชาการ : </label>
                                            <div class="col-md-5">
                                                <select class="form-control" onchange="change_type7(this)" name="position_id" id="position_id">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(!empty($positions))
                                                        @foreach($positions as $position)
                                                            <option {{($my_profile->position_id==$position->position_id)?' selected ':''}} value="{{$position->position_id}}">{{$position->position_name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>


                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">ชื่อ :</label>
                                            <div class="col-md-5">
                                                <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="ชื่อบุคลากร" value="{{$my_profile->first_name}}">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">สาขา : </label>
                                            <div class="col-md-5">
                                                <select class="form-control" name="branch_id" id="branch_id">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(!empty($branches))
                                                        @foreach($branches as $branch)
                                                            <option {{($my_profile->branch_id==$branch->branch_id)?' selected ':''}} value="{{$branch->branch_id}}">{{$branch->branch_name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="textinput">ศูนย์พื้นที่ : </label>
                                            <div class="col-md-5">
                                                <select class="form-control" name="campus_id" id="campus_id">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(!empty($campuses))
                                                        @foreach($campuses as $campus)
                                                            <option {{($my_profile->campus_id==$campus->campus_id)?' selected ':''}} value="{{$campus->campus_id}}">{{$campus->campus_name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                            บันทึก
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop

