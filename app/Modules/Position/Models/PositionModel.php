<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/29/16
 * Time: 4:51 PM
 */

namespace App\Modules\Position\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class PositionModel extends Model
{
    public static function getAll()
    {
        return DB::table('positions')
            ->whereNull('deleted_at')
            ->get();
    }
}