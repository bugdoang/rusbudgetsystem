<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 60%; margin-top: 150px;">

    <!-- Modal content-->
    <form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="POST" action="/project-plan/modal/{{$budget_plan_id}}" >
      {{csrf_field()}}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title myfont" style="font-size: 22px;">ข้อมูลการปรับสถานะโครงการ</h4>
        </div>
        <div class="modal-body">
          <div class="form-group disable-select">
              <label class="col-md-4 control-label" for="textinput"> จำนวนเงินที่เสนอขอ :</label>
              <div class="col-md-4">
                  <input id="total_price_net" style="width: 100%; text-align: right;" class="form-control input-md" type="text" name="total_price_net" value="{{number_format($budgetPlanProject->total_price[$budgetPlanProject->project_id],2)}}">
              </div>
              <label class="off-col-md-1 control-label" for="textinput"> บาท </label>
          </div>
          <div class="form-group">
              <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> จำนวนเงินที่ได้รับการอนุมัติ :</label>
              <div class="col-md-4">
                  <input id="project_money_approve" style="width: 100%; text-align: right;" maxlength="9" class="form-control input-md numberonlydot" type="text" name="project_money_approve" value="">
              </div>
              <label class="off-col-md-1 control-label" for="textinput"> บาท </label>
          </div>
          <div class="form-group" style="margin-top: 18px;">
              <label class="col-md-4 control-label" for="filebutton"> ไฟล์แนบเอกสารที่ได้รับการอนุมัติ :</label>
              <div class="col-md-4" id="uploadFileData-container">
                  <button style="font-size: 16px;" id="data-uploadFileData" type="button" class="myfont uploadFileData form-control">
                      <i class="fa fa-file" aria-hidden="true"></i> 
                      แนบไฟล์
                  </button>
              </div>
              <label class="off-col-md-4 control-label" for="textinput" style="color: #b00;"> .pdf* .xls* .xlsx* .doc* .docx*</label>
          </div>
          <div style="height:10px;clear:both;"></div>
        </div>
    
        <div class="modal-footer">
            <div class="form-group">
                <div class="row">
                    <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                    <button id="login-btn" type="submit" class="form-control btn btn-login btn-lg myfont" type="submit">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        บันทึก
                        </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </form>
  </div>
</div>