<div id="tab10" class="tab-pane">
    <h2 class="header-text myfont">งบประมาณ</h2>
    <div id="list-durable10" class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        {{csrf_field()}}
        <label class="col-md-4 control-label" for="textinput">รายละเอียดค่าใช้จ่ายในการดำเนินโครงการ</label>
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" colspan="3">รายการกิจกรรม</th>

                </tr>
                <tr v-for="(item,index) in items">
                    <td style="width: 120px;">
                        <span>กิจกรรมที่ @{{index+1}}</span>
                    </td>
                    <td>
                        <table class="table table-bordered" style="margin:0;">
                            <tr>
                                <th class="text-center text-middle" style="width: 50px;">#</th>
                                <th class="text-center text-middle">รายการ</th>
                                <th class="text-center text-middle" style="width: 120px;">จำนวนเงิน</th>
                            </tr>
                            <tr v-for="(_item,_index) in item.child">
                                <td class="text-right text-middle">
                                    @{{_index+1}}
                                </td>
                                <td class="text-center text-middle">
                                    <input class="form-control" v-model="_item.project_budget_name" type="text" name="" >
                                </td>
                                <td class="text-center text-middle">
                                    <input class="form-control text-right numberonlydot" onkeyup="key_number()" v-model="_item.project_budget_price" type="text" >
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center text-middle" style="font-weight: bold;" colspan="2">งบประมาณรวม</td>
                                <td class="text-middle text-right">@{{ addCommas(item.project_budget_total.toFixed(2)) }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td  style="border: none;">
                        <table class="table" style="margin:0;">
                            <tr>
                                <th class="text-center text-middle" colspan="2" style="border: none;font-weight: bold;">งบประมาณรวมสุทธิ</th>
                                <td class="text-middle text-right" style="border: none; width: 120px;">
                                    @{{ addCommas(project_budget_net.toFixed(2)) }}
                                </td>
                                <td style="border: none;">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>
@push('scripts')
<script type="text/javascript">
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function key_number()
    {
        var total_sum = 0;
        $(list_durable10.items).each(function(i,item){
            var child_total = 0;
            $(item.child).each(function(i,child){
                if(!isNaN(child.project_budget_price))
                {
                    child_total+= parseInt(child.project_budget_price);
                }
            });
            total_sum+=child_total;
            list_durable10.items[i].project_budget_total=child_total;
        });
       list_durable10.project_budget_net=total_sum;
    }
    var list_durable10 = new Vue({
        el: '#list-durable10',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!},
            project_budget_net:0
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                project_budget_total:0,
                child:[
                    {
                    project_budget_name:'',
                    project_budget_price:0
                    }
                ]
            });
            @endif
            setTimeout(function(){
                key_number();
            },300);
        },
        methods:{
            add:function(){
                this.items.push({
                    project_budget_total:0,
                    child:[
                    {
                        project_budget_name:'',
                        project_budget_price:0
                    }
                    ]                
                })
            },
            addchild:function(index){
                this.items[index].child.push({
                    project_budget_name:'',
                    project_budget_price:0
                });
            },
            remove_item:function(item){
                this.items.splice(item,1);
            },
            remove_item_child:function(index,item){
                this.items[index].child.splice(item,1);
            }
        }
    })
</script>
@endpush