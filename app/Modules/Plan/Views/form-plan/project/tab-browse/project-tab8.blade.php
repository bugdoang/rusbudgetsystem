<div id="tab8" class="tab-pane">
    <h2 class="header-text myfont">ระยะเวลาดำเนินการ</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput">เริ่ม เดือน </label>
            <div class="col-md-3">
                <select class="" name="month_start" id="month_start">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($months))
                        @foreach($months as $m_index=> $month)
                            <option {{(isset($budgetPlanProject->month_start) && $budgetPlanProject->month_start==str_pad(($m_index+1),2,'0',STR_PAD_LEFT))?' selected ':''}}  value="{{str_pad(($m_index+1),2,'0',STR_PAD_LEFT)}}">{{$month}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">พ.ศ. : </label>
            <div class="col-md-2">
                <select class="" name="year_start" id="year_start">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                        <option {{(isset($budgetPlanProject->year_start) && $budgetPlanProject->year_start==$i)?' selected ':''}} value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput">สิ้นสุด เดือน </label>
            <div class="col-md-3">
                <select class="" name="month_end" id="month_end">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($months))
                        @foreach($months as $m_index=> $month)
                            <option {{(isset($budgetPlanProject->month_end) && $budgetPlanProject->month_end==str_pad(($m_index+1),2,'0',STR_PAD_LEFT))?' selected ':''}}  value="{{str_pad(($m_index+1),2,'0',STR_PAD_LEFT)}}">{{$month}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">พ.ศ. : </label>
            <div class="col-md-2">
                <select class="" name="year_end" id="year_end">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                        <option {{(isset($budgetPlanProject->year_end) && $budgetPlanProject->year_end==$i)?' selected ':''}} value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput"><span class="font-color-red"> * </span> สถานที่ดำเนินการ :</label>
            <div class="col-md-6">
                <input id="place" class="form-control input-md" type="text" name="place" placeholder="สถานที่ดำเนินการ" value="{{$budgetPlanProject->place or ''}}">
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>