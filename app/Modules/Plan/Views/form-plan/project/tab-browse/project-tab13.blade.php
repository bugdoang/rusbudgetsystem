<div id="tab13" class="tab-pane">
    <h2 class="header-text myfont">ผู้รับผิดชอบโครงการ</h2>
    <div id="list-durable13" class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        {{csrf_field()}}
        <input type="hidden" name="list9" id="list9">
        <table class="table table-bordered"  style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 10%;">#</th>
                    <th class="text-center text-middle" style="width: 80%;">ชื่อ-นามสกุล</th>
                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td class="text-left">
                        <select class="form-control myselect9"  v-bind:id="index" onchange="select_comittee(this)" name="committee_id" v-model="item.committee_id">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($personnels))
                                @foreach($personnels as $personnel)
                                    <option value="{{$personnel->personnel_id}}">{{$personnel->first_name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <span>หมายเหตุ : ชื่อแรกเป็นรายชื่อคณะกรรมการหลักคะ</span>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">

    function select_comittee(o)
    {
        var el = $(o);
        var index = el.attr('id');
        var name = $('#select2-'+index+'-container').attr('title');
        list_durable13.items[index].committee_id = el.val();
        list_durable13.items[index].committee_name = name;
    }
    var list_durable13 = new Vue({
        el: '#list-durable13',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                committee_id:{{\Auth::id()}},
                committee_name:'{{\Auth::user()->fullName}}'
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    committee_id:'',
                    committee_name:'',
                });
                setTimeout(function(){
                    $('.myselect9').each(function(i,e){$(e).select2Build();});
                },300);
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush

