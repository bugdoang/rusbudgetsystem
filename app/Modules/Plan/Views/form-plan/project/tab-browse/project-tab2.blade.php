<div id="tab2" class="tab-pane">
    <h2 class="header-text myfont">ความสอดคล้องยุทธศาสตร์การพัฒนามหาวิทยาลัย</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">วิสัยทัศน์ : </label>
            <div class="col-md-7">
                <select class="form-control" name="vision_id" id="vision_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($visions))
                        @foreach($visions as $vision)
                            <option {{(isset($budgetPlanProject->vision_id) && $budgetPlanProject->vision_id==$vision->vision_id)?' selected ':''}} value="{{$vision->vision_id}}">{{$vision->vision_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">เป็นภารกิจตามพันธกิจที่ : </label>
            <div class="col-md-7">
                <select class="form-control" onchange="onchange_federal_circuit_id(this)" name="federal_circuit_id" id="federal_circuit_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($federalCircuits))
                        @foreach($federalCircuits as $federalCircuit)
                            <option {{(isset($budgetPlanProject->federal_circuit_id) && $budgetPlanProject->federal_circuit_id==$federalCircuit->federal_circuit_id)?' selected ':''}} value="{{$federalCircuit->federal_circuit_id}}">{{$federalCircuit->federal_circuit_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">ความเชื่อมโยงกับประเด็นยุทธศาสตร์ที่ : </label>
            <div class="col-md-7">
                <select class="form-control" onchange="onchange_issue_id(this)" name="issue_id" id="issue_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($issues))
                        @foreach($issues as $issue)
                            <option {{(isset($budgetPlanProject->issue_id) && $budgetPlanProject->issue_id==$issue->issue_id)?' selected ':''}} value="{{$issue->issue_id}}">{{$issue->issue_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">เป้าประสงค์ที่ : </label>
            <div class="col-md-7">
                <select class="form-control" onchange="onchange_goal_id(this)" name="goal_id" id="goal_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($goals))
                        @foreach($goals as $goal)
                            <option {{(isset($budgetPlanProject->goal_id) && $budgetPlanProject->goal_id==$goal->goal_id)?' selected ':''}} value="{{$goal->goal_id}}">{{$goal->goal_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">ตัวชี้วัดเป้าประสงค์ที่ : </label>
            <div class="col-md-7">
                <select class="form-control" name="objective_indicator_id" id="objective_indicator_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($objectiveIndicators))
                        @foreach($objectiveIndicators as $objectiveIndicator)
                            <option {{(isset($budgetPlanProject->objective_indicator_id) && $budgetPlanProject->objective_indicator_id==$objectiveIndicator->objective_indicator_id)?' selected ':''}} value="{{$objectiveIndicator->objective_indicator_id}}">{{$objectiveIndicator->objective_indicator_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">ความเชื่อมโยงกับกลยุทธ์ที่ : </label>
            <div class="col-md-7">
                <select class="form-control" onchange="onchange_strategy_id(this)" name="strategy_id" id="strategy_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($strategies))
                        @foreach($strategies as $strategy)
                            <option {{(isset($budgetPlanProject->strategy_id) && $budgetPlanProject->strategy_id==$strategy->strategy_id)?' selected ':''}} value="{{$strategy->strategy_id}}">{{$strategy->strategy_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">ตัวชี้วัดกลยุทธ์ที่ : </label>
            <div class="col-md-7">
                <select class="form-control" name="objective_strategy_id" id="objective_strategy_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($objectiveStrategies))
                        @foreach($objectiveStrategies as $objectiveStrategy)
                            <option {{(isset($budgetPlanProject->objective_strategy_id) && $budgetPlanProject->objective_strategy_id==$objectiveStrategy->objective_strategy_id)?' selected ':''}} value="{{$objectiveStrategy->objective_strategy_id}}">{{$objectiveStrategy->objective_strategy_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">ความเชื่อมโยงกับโครงการสำคัญที่ </label>
            <div class="col-md-7">
                <select class="form-control" name="key_project_id" id="key_project_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($keyProjects))
                        @foreach($keyProjects as $keyProject)
                            <option {{(isset($budgetPlanProject->key_project_id) && $budgetPlanProject->key_project_id==$keyProject->key_project_id)?' selected ':''}} value="{{$keyProject->key_project_id}}">{{$keyProject->key_project_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> แผนงาน : </label>
            <div class="col-md-7">
                <select class="form-control" onchange="onchange_plan_id(this)" name="plan_id" id="plan_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($plans))
                        @foreach($plans as $plan)
                            <option {{(isset($budgetPlanProject->plan_id) && $budgetPlanProject->plan_id==$plan->plan_id)?' selected ':''}} value="{{$plan->plan_id}}">{{$plan->plan_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ผลผลิต : </label>
            <div class="col-md-7">
                <select class="form-control" name="product_id" id="product_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($products))
                        @foreach($products as $product)
                            <option {{(isset($budgetPlanProject->product_id) && $budgetPlanProject->product_id==$product->product_id)?' selected ':''}} value="{{$product->product_id}}">{{$product->product_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
    <script type="text/javascript">
        var issues={};
        @if(!empty($issues))
                @foreach($issues as $issue)
        if(typeof issues[{{$issue->issue_id}}]!='object') issues[{{$issue->issue_id}}]=[];
        issues[{{$issue->federal_circuit_id}}].push({issue_id:{{$issue->issue_id}},issue_name:'{{$issue->issue_name}}'});
        @endforeach
        @endif

        function onchange_federal_circuit_id(o)
        {
            $('#issue_id').empty();
            if($(o).val()!='')
            {
                $('#issue_id').empty().append('<option value="">กรุณาเลือก</option>');
                if(typeof issues[$(o).val()]!=='undefined' && issues[$(o).val()]!=null)
                {
                    $(issues[$(o).val()]).each(function(i,o){
                        $('#issue_id').append('<option value="'+o.issue_id+'">'+o.issue_name+'</option>');
                    });
                }
            }
        }


        var goals={};
        @if(!empty($goals))
                @foreach($goals as $goal)
        if(typeof goals[{{$goal->goal_id}}]!='object') goals[{{$goal->goal_id}}]=[];
        goals[{{$goal->issue_id}}].push({goal_id:{{$goal->goal_id}},goal_name:'{{$goal->goal_name}}'});
        @endforeach
        @endif

        function onchange_issue_id(o)
        {
            $('#goal_id').empty();
            if($(o).val()!='')
            {
                $('#goal_id').empty().append('<option value="">กรุณาเลือก</option>');
                if(typeof goals[$(o).val()]!=='undefined' && goals[$(o).val()]!=null)
                {
                    console.log(goals);
                    $(goals[$(o).val()]).each(function(i,o){
                        $('#goal_id').append('<option value="'+o.goal_id+'">'+o.goal_name+'</option>');
                    });
                }
            }
        }


        var objectiveIndicators={};
        @if(!empty($objectiveIndicators))
                @foreach($objectiveIndicators as $objectiveIndicator)
        if(typeof objectiveIndicators[{{$objectiveIndicator->objective_indicator_id}}]!='object') objectiveIndicators[{{$objectiveIndicator->objective_indicator_id}}]=[];
        objectiveIndicators[{{$objectiveIndicator->goal_id}}].push({objective_indicator_id:{{$objectiveIndicator->objective_indicator_id}},objective_indicator_name:'{{$objectiveIndicator->objective_indicator_name}}'});
        @endforeach
        @endif

        var strategies={};
        @if(!empty($strategies))
                @foreach($strategies as $strategy)
        if(typeof strategies[{{$strategy->strategy_id}}]!='object') strategies[{{$strategy->strategy_id}}]=[];
        strategies[{{$strategy->goal_id}}].push({strategy_id:{{$strategy->strategy_id}},strategy_name:'{{$strategy->strategy_name}}'});
        @endforeach
        @endif

        function onchange_goal_id(o)
        {
            $('#objective_indicator_id').empty();
            if($(o).val()!='')
            {
                $('#objective_indicator_id').empty().append('<option value="">กรุณาเลือก</option>')
                if(typeof objectiveIndicators[$(o).val()]!=='undefined' && objectiveIndicators[$(o).val()]!=null)
                {
                    $(objectiveIndicators[$(o).val()]).each(function(i,o){
                        $('#objective_indicator_id').append('<option value="'+o.objective_indicator_id+'">'+o.objective_indicator_name+'</option>');
                    });
                }
            }

            $('#strategy_id').empty();
            if($(o).val()!='')
            {
                $('#strategy_id').empty().append('<option value="">กรุณาเลือก</option>')
                if(typeof strategies[$(o).val()]!=='undefined' && strategies[$(o).val()]!=null)
                {
                    $(strategies[$(o).val()]).each(function(i,o){
                        $('#strategy_id').append('<option value="'+o.strategy_id+'">'+o.strategy_name+'</option>');
                    });
                }
            }
        }


        var objectiveStrategies={};
        @if(!empty($objectiveStrategies))
                @foreach($objectiveStrategies as $objectiveStrategy)
        if(typeof objectiveStrategies[{{$objectiveStrategy->objective_strategy_id}}]!='object') objectiveStrategies[{{$objectiveStrategy->objective_strategy_id}}]=[];
        objectiveStrategies[{{$objectiveStrategy->strategy_id}}].push({objective_strategy_id:{{$objectiveStrategy->objective_strategy_id}},objective_strategy_name:'{{$objectiveStrategy->objective_strategy_name}}'});
        @endforeach
        @endif

        var keyProjects={};
        @if(!empty($keyProjects))
                @foreach($keyProjects as $keyProject)
        if(typeof keyProjects[{{$keyProject->key_project_id}}]!='object') keyProjects[{{$keyProject->key_project_id}}]=[];
        keyProjects[{{$keyProject->strategy_id}}].push({key_project_id:{{$keyProject->key_project_id}},key_project_name:'{{$keyProject->key_project_name}}'});
        @endforeach
        @endif

        function onchange_strategy_id(o)
        {
            $('#objective_strategy_id').empty();
            if($(o).val()!='')
            {
                $('#objective_strategy_id').empty().append('<option value="">กรุณาเลือก</option>')
                if(typeof objectiveStrategies[$(o).val()]!=='undefined' && objectiveStrategies[$(o).val()]!=null)
                {
                    $(objectiveStrategies[$(o).val()]).each(function(i,o){
                        $('#objective_strategy_id').append('<option value="'+o.objective_strategy_id+'">'+o.objective_strategy_name+'</option>');
                    });
                }
            }

            $('#key_project_id').empty();
            if($(o).val()!='')
            {
                $('#key_project_id').empty().append('<option value="">กรุณาเลือก</option>')
                if(typeof keyProjects[$(o).val()]!=='undefined' && keyProjects[$(o).val()]!=null)
                {
                    $(keyProjects[$(o).val()]).each(function(i,o){
                        $('#key_project_id').append('<option value="'+o.key_project_id+'">'+o.key_project_name+'</option>');
                    });
                }
            }
        }


        var products={};
        @if(!empty($products))
                @foreach($products as $product)
        if(typeof products[{{$product->plan_id}}]!='object') products[{{$product->plan_id}}]=[];
        products[{{$product->plan_id}}].push({product_id:{{$product->product_id}},product_name:'{{$product->product_name}}'});
        @endforeach
        @endif

        function onchange_plan_id(o)
        {
            $('#product_id').empty();
            if($(o).val()!='')
            {
                $('#product_id').empty().append('<option value="">กรุณาเลือก</option>')
                if(typeof products[$(o).val()]!=='undefined' && products[$(o).val()]!=null)
                {
                    $(products[$(o).val()]).each(function(i,o){
                        $('#product_id').append('<option value="'+o.product_id+'">'+o.product_name+'</option>');
                    });
                }
            }
        }



    </script>
@endpush

