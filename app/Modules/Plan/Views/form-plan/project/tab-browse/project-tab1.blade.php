<div id="tab1" class="tab-pane active">
    <h2 class="header-text myfont">ข้อมูลพื้นฐาน</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ชื่อโครงการ :</label>
            <div class="col-md-6">
                <input id="project_name" class="form-control input-md" type="text" name="project_name" value="{{$budgetPlanProject->project_name or ''}}" placeholder="ชื่อโครงการ">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> สถานภาพของโครงการ : </label>
            <div class="col-md-6">
                <select class="form-control" name="project_status" id="project_status" data-clear="false">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($project_statuses))
                        @foreach($project_statuses as $project_status)
                    <option {{(isset($budgetPlanProject->project_status) && $budgetPlanProject->project_status==$project_status)?' selected ':''}} value="{{$project_status}}">{{$project_status}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> แหล่งเงินได้ : </label>
            <div class="col-md-6">
                <select class="form-control" name="income_id" id="income_id" data-clear="false">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($incomes))
                        @foreach($incomes as $income)
                    <option {{(isset($budgetPlanProject->income_id) && $budgetPlanProject->income_id==$income->income_id)?' selected ':''}} value="{{$income->income_id}}">{{$income->income_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ประเภทแหล่งเงินได้ : </label>
            <div class="col-md-6">
                <select class="form-control" name="income_type_id" onchange="change_type1(this)" id="income_type_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($incomeTypes))
                        @foreach($incomeTypes as $incomeType)
                            <option {{(isset($budgetPlanProject->income_type_id) && $budgetPlanProject->income_type_id==$incomeType->income_type_id)?' selected ':''}} value="{{$incomeType->income_type_id}}">{{$incomeType->income_type_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">หมวดแหล่งเงินได้ : </label>
            <div class="col-md-6">
                <select class="form-control" name="income_group_id" id="income_group_id">
                    <option value="">กรุณาเลือก</option>
                    @if(isset($budgetPlanProject->income_group_id))
                    @foreach($incomeGroups as $incomeGroup)
                        @if($budgetPlanProject->income_type_id==$incomeGroup->income_type_id)
                        <option {{(isset($budgetPlanProject->income_group_id) && $budgetPlanProject->income_group_id==$incomeGroup->income_group_id)?' selected ':''}} value="{{$incomeGroup->income_group_id}}">{{$incomeGroup->income_group_name}}</option>
                        @endif
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ปีงบประมาณ : </label>
            <div class="col-md-6">
                <select v-model="fiscal_year" onchange="setvue(this)" class="form-control" name="fiscal_year" id="fiscal_year">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                    <option {{(isset($budgetPlanProject->fiscal_year) && $budgetPlanProject->fiscal_year==$i)?' selected ':''}}  value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> รอบภาคการศึกษา : </label>
            <div class="col-md-6">
                <select class="form-control" name="semester" id="semester">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($programs))
                        @foreach($programs as $program)
                            <option {{(isset($budgetPlanProject->semester) && $budgetPlanProject->semester==$program)?' selected ':''}} value="{{$program}}">{{$program}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">

    var list_durable1 = new Vue({
        el: '#list-durable1',
        data:{fiscal_year:{{date('Y')}}},
        watch: 
        {
            fiscal_year: function (val, oldVal) 
            {
                if(!isNaN(val))
                {
                    list_durable9.items.fiscal_year=parseInt(val,10);
                }
            }
        }
    });

    function setvue(o)
    {
        list_durable1.fiscal_year = $(o).val();
    }
    var incomeGroups={};
    @if(!empty($incomeGroups))
    @foreach($incomeGroups as $incomeGroup)
        if(typeof incomeGroups[{{$incomeGroup->income_type_id}}]!='object') incomeGroups[{{$incomeGroup->income_type_id}}]=[];
        incomeGroups[{{$incomeGroup->income_type_id}}].push({income_group_id:{{$incomeGroup->income_group_id}},income_group_name:'{{$incomeGroup->income_group_name}}'});
    @endforeach
    @endif

    function change_type1(o)
    {
        $('#income_group_id').select2('val',null);
        $('#income_group_id').empty();
        if($(o).val()!='')
        {
            $('#income_group_id').empty().append('<option value="">กรุณาเลือก</option>')
            if(typeof incomeGroups[$(o).val()]!=='undefined' && incomeGroups[$(o).val()]!=null)
            {
                $(incomeGroups[$(o).val()]).each(function(i,o){
                    $('#income_group_id').append('<option value="'+o.income_group_id+'">'+o.income_group_name+'</option>');
                });
            }
        }
    }
</script>
@endpush