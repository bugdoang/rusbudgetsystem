<div id="tab5" class="tab-pane">
    <h2 class="header-text myfont">ลักษณะของกิจกรรมที่ดำเนินการ</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <textarea class="form-control" rows="10" id="nature_event" name="nature_event" placeholder="ลักษณะของกิจกรรมที่ดำเนินการ">{{$budgetPlanProject->nature_event or ''}}</textarea>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>
