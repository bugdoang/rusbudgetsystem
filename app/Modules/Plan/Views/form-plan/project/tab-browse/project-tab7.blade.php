<div id="tab7" class="tab-pane">
    <h2 class="header-text myfont">ประโยชน์ที่คาดว่าจะได้รับ</h2>
    <div id="list-durable7" class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        {{csrf_field()}}
        <input type="hidden" name="list7" id="list7">
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 60px;">#</th>
                    <th class="text-center text-middle">ประโยชน์ที่คาดว่าจะได้รับ</th>
                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <input class="form-control input-md" style="width: 100%;" type="text" v-model="item.project_benefit_name">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var list_durable7 = new Vue({
        el: '#list-durable7',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                project_benefit_name: ''
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    project_benefit_name:'',
                })
            },
        }
    })
</script>
@endpush