@extends('masterlayout')
@section('title','แบบฟอร์มรายละเอียดประกอบโครงการ')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดประกอบโครงการ','link'=>'/project-plan'],['label'=>'แบบฟอร์มรายละเอียดประกอบโครงการ','link'=>'project-form']]) !!}
@if(isset($budgetPlanProject) && $budgetPlanProject->status=='รอการตรวจสอบ' && $method!='POST' && $my_group_id!=4)
    <div class="site-breadcrumb">
        <span>ปรับสถานะ :  </span>
        <a class="btn btn-default edit-content" href="/project-plan/modal/{{$budget_plan_id}}"> อนุมัติ</a>
        <a class="btn btn-default reject-content" href="/project-plan/modal/{{$budget_plan_id}}/reject"> ไม่อนุมัติ</a>
    </div>
@endif
<!-- Page Content -->
<form id="form-plan-project" name="form-plan-project" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="items">
    <input type="hidden" name="budgets" id="budgets">
    <input type="hidden" name="budget_total" id="budget_total">
    @if(isset($method) && $method=='PUT')
        <input type="hidden" name="project_id" value="{{$budgetPlanProject->project_id}}" id="project_id">
    @endif
    <div class="row">
        <!--start left-->
        <div class="col-lg-3 col-md-3">
            <div class="row">
                <h2 class="header-text myfont">ประเภทข้อมูล</h2>
                <div id="rootwizard" class="tabs-left shadow">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#tab1">1. ข้อมูลพื้นฐาน</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab2">2. ความสอดคล้องยุทธศาสตร์</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab3">3. หลักการและเหตุผล</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab4">4. วัตถุประสงค์ของโครงการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab5">5. ลักษณะของกิจกรรมที่ดำเนินการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab6">6. กิจกรรมของโครงการ/ระยะเวลาดำเนินการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab7">7. ประโยชน์ที่คาดว่าจะได้รับ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab8">8. ระยะเวลาดำเนินการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab9">9. เป้าหมายในการดำเนินโครงการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab10">10. งบประมาณ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab11">11. ความพร้อมของโครงการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab12">12. ปัญหาอุปสรรค/ข้อเสนอแนะ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab13">13. ผู้รับผิดชอบโครงการ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--start right-->
        <div style="padding-right: 0;" class="col-lg-9 col-md-9">
            <div class="tab-content">
                {!! \App\Modules\Plan\Services\ProjectForm1::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm2::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm3::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm4::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm5::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm6::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm7::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm8::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm9::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm10::get($budgetPlanProject,'view') !!}   
                {!! \App\Modules\Plan\Services\ProjectForm11::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm12::get($budgetPlanProject,'view') !!}
                {!! \App\Modules\Plan\Services\ProjectForm13::get($budgetPlanProject,'view') !!}
            </div>
        </div>
    </div>
</form>
@stop

@section('scripts')

<script>
    function save_success(data)
    {
        MessageBox.alert('ระบบได้บันทึกข้อมูลเรียบร้อยแล้ว',function(){
            window.location.href = data.redirect_url;
        });
    }

    function beforesend_form()
    {
        $('#tab9-item').val(JSON.stringify(list_durable9.items));
        $('#list-durable10').val(JSON.stringify(list_durable10.items));
        $('#list6').val(JSON.stringify(list_durable6.items));
        $('#list9').val(JSON.stringify(list_durable13.items));
        $('#list5').val(JSON.stringify(list_durable4.items));
        $('#list7').val(JSON.stringify(list_durable7.items));
        $('#list12').val(JSON.stringify(list_durable12.items));
        $('#budgets').val(JSON.stringify(list_durable10.items));
        $('#budget_total').val(JSON.stringify(list_durable10.project_budget_net));

    }
    $(document).ready(function() {
        $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
    });
</script>
@stop
