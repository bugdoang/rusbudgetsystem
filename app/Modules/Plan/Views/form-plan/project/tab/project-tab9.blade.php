<style type="text/css">
    .w90{width:90px;}
</style>
<div id="tab9" class="tab-pane">
    <h2 class="header-text myfont">เป้าหมายในการดำเนินโครงการ</h2>
    <div id="list-durable9" class="col-lg-12 col-md-12 form-planbudget shadow">
        <input type="hidden" name="tab9" id="tab9-item">
        <table style="border:0!important;" class="table my-table table-bordered">
            <tr>
                <th class="text-center text-middle" rowspan="2">ตัวชี้วัดความสำเร็จ</th>
                <th class="text-center text-middle" rowspan="2"style="width:70px;">หน่วยนับ</th>
                <th class="text-center" colspan="1" style="width:90px;">ผล</th>
                <th class="text-center" colspan="1" style="width:90px;">แผน/(ผล)</th>
                <th class="text-center" colspan="4" >ค่าเป้าหมาย</th>
            </tr>
            <tr>
                <th v-for="n in 6" class="text-center" :class="{'w90':n>2}">ปี @{{((items.fiscal_year-3)+543)+n}}</th>
            </tr>
            <tr>
                <th>ระดับผลลัพธ์</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงปริมาณ :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row1.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงคุณภาพ :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row2.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
            <tr>
                <th>ระดับผลผลิต</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงปริมาณ :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row3.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงคุณภาพ :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row4.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงเวลา :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row5.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
            <tr>
                <td>ตัวชี้วัดเชิงต้นทุน :</td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.qty" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.result" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.plan" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.target.y1" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.target.y2" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.target.y3" class="my-text text-center" type="text">
                </td>
                <td>
                    <input maxlength="10" v-model="items.items.row6.target.y4" class="my-text text-center" type="text">
                </td>
            </tr>
           
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>
@push('scripts')
<script type="text/javascript">
    var list_durable9 = new Vue({
        el: '#list-durable9',
        data:{
            <?php if(!isset($items) && empty($items)) :?>
            items:{
                fiscal_year:{{date('Y')}},
                items:{
                    row1:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    },
                    row2:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    },
                    row3:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    },
                    row4:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    },
                    row5:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    },
                    row6:{
                        qty:'',
                        result:'',
                        plan:'',
                        target:{
                            y1:'',
                            y2:'',
                            y3:'',
                            y4:''
                        }
                    }

                }
            }
            <?php else: ?>
            items: <?php echo $items ?>
            <?php endif?>
        },
        methods:{
           
        }
    });
</script>
@endpush


