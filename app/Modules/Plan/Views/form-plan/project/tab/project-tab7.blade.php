<div id="tab7" class="tab-pane">
    <h2 class="header-text myfont">ประโยชน์ที่คาดว่าจะได้รับ</h2>
    <div id="list-durable7" class="col-lg-12 col-md-12 form-planbudget shadow">
        {{csrf_field()}}
        <div class="site-breadcrumb" style="margin: -49px -5px;">
            <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> ประโยชน์ที่คาดว่าจะได้รับ</a>
        </div>
        <input type="hidden" name="list7" id="list7">
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 60px;">#</th>
                    <th class="text-center text-middle">ประโยชน์ที่คาดว่าจะได้รับ</th>
                    <th class="text-center text-middle" style="width: 30px;">ลบ</th>

                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <input class="form-control input-md" style="width: 100%;" type="text" v-model="item.project_benefit_name">
                    </td>
                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0"  v-on:click="remove_item(index)"> ลบ</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var list_durable7 = new Vue({
        el: '#list-durable7',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                project_benefit_name: ''
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    project_benefit_name:'',
                })
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush