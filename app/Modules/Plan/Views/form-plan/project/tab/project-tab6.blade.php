<div id="tab6" class="tab-pane">
    <h2 class="header-text myfont">กิจกรรมของโครงการ/ระยะเวลาดำเนินการ</h2>
    <div id="list-durable6" class="col-lg-12 col-md-12 form-planbudget shadow">
        {{csrf_field()}}
        <div class="site-breadcrumb" style="margin: -49px -5px;">
            <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มกิจกรรม</a>
        </div>
        <input type="hidden" name="list6" id="list6">
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 30px;">#</th>
                    <th class="text-center text-middle" style="width: 200px;">กิจกรรม</th>
                    <th class="text-center text-middle" style="width: 80px;">วัน/เดือน/ปี</th>
                    <th class="text-center text-middle" style="width: 30px;">ลบ</th>

                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <input class="form-control input-md" style="width: 100%;" type="text" v-model="item.project_activity_name">
                    </td>
                    <td>
                    	<input class="form-control input-md datepicker-special" :id="index" type="text" onchange="change_value(this)" v-model="item.project_activity_date" placeholder="วัน/เดือน/ปี ที่เริ่มกิจกรรม" style="width: 100%; text-align: center;">
                    </td>
                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0"  v-on:click="remove_item(index)"> ลบ</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    function change_value(o)
    {
        var index = $(o).attr('id');
        list_durable6.items[index].project_activity_date = $(o).val();
    }
    var list_durable6 = new Vue({
        el: '#list-durable6',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                project_activity_name: '',
                project_activity_date: ''
            });
            @endif
            setTimeout(function(){
                $('.datepicker-special').myDatePicker();
            },300);
        },
        methods:{
            add:function(){
                this.items.push({
                    project_activity_name:'',
                	project_activity_date:''
                });
                setTimeout(function(){
	            	$('.datepicker-special').myDatePicker();
	            },300);
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush
