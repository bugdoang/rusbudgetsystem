<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
	.table{width:100%;}
    .fa{font-size:12px;}
    p{margin: 0;}
	.table tr th,.table tr td{border:1px solid #000;padding:2px;}
    .headpdf {font-size: 16px; font-weight: bold;}
    .texttablepdf {font-size: 24px;}
    .textpdf {font-size: 16px;}
    .tab_article {padding-left: 15px;}
</style>
<p style="text-align:right; font-size: 14px;">
    แบบจัดทำคำเสนอของบประมาณ{{$budgetPlanProject->income_name or ''}} 
    ประจำปีงบประมาณ พ.ศ. {{$budgetPlanProject->fiscal_year+543}} 
</p>
<p class="headpdf" style="text-align: center;">รายละเอียดรายการครุภัณฑ์ งบประมาณ{{$budgetPlanProject->income_name or ''}}</p>
<p class="headpdf" style="text-align: center;">คณะ/หน่วยงาน{{$budgetPlanProject->faculty_name}} สาขา{{$budgetPlanProject->branch_name}} ศูนย์{{$budgetPlanProject->campus}}</p>
<p class="textpdf" style="text-align: left;">{{$budgetPlanProject->plan_name or ''}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ผลผลิต{{$budgetPlanProject->product_name or ''}}</p>
<p class="headpdf" style="text-align: left;">ชื่อโครงการ {{$budgetPlanProject->project_name or ''}}</p>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">1. สถานภาพของโครงการ </td>
        <td class="textpdf" style="text-align: left;"> เป็น{{$budgetPlanProject->project_status or ''}}</td>
    </tr>
 </table>
 <table>
    <tr>
        <td class="headpdf" style="text-align: left;">2. ความสอดคล้องยุทธศาสตร์การพัฒนามหาวิทยาลัย </td>
    </tr>
 </table>

<p class="textpdf" style="padding-left: 16px;">2.1 เป็นภารกิจตามพันธกิจที่ {{$budgetPlanProject->federal_circuit_id}} {{$budgetPlanProject->federal_circuit_name}}</p>

<p class="textpdf" style="padding-left: 16px;">2.2 ความเชื่อมโยงกับประเด็นยุทธศาสตร์ที่ {{$budgetPlanProject->issue_id}} {{$budgetPlanProject->issue_name}}</p>

<p class="textpdf" style="padding-left: 16px;">2.3 ความเชื่อมโยงกับกลยุทธ์ที่ {{$budgetPlanProject->strategy_id}} {{$budgetPlanProject->strategy_name}}</p>

<p class="textpdf" style="padding-left: 16px;">2.4 ความเชื่อมโยงกับโครงการสำคัญที่ {{$budgetPlanProject->key_project_id}} {{$budgetPlanProject->key_project_name}}</p>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">3. แผนงาน </td>
        <td class="textpdf" style="text-align: left;"> {{$budgetPlanProject->plan_name}}</td>
    </tr>
 </table>

 <table>
    <tr>
        <td class="headpdf" style="text-align: left;">4. ผลผลิต </td>
        <td class="textpdf" style="text-align: left;"> {{$budgetPlanProject->product_name}}</td>
    </tr>
 </table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">5. หลักการและเหตุผล </td>
    </tr>
 </table>
<?php $detail = explode("\n",$budgetPlanProject->reason);?>
@foreach($detail as $_detail)
<p class="textpdf" style="text-indent: 15px;">{{$_detail}}</p>
@endforeach

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">6. วัตถุประสงค์ของโครงการ</td>
    </tr>
 @foreach($budgetPlanProject->plan_objectives as $index=>$item)
    <tr>
        <td class="textpdf tab_article">6.{{$index+1}} {{$item->objective_name or ''}}</td>
    </tr>
 @endforeach
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">7. ลักษณะของกิจกรรมที่ดำเนินการ </td>
    </tr>
 </table>
<?php $detail = explode("\n",$budgetPlanProject->nature_event);?>
@foreach($detail as $_detail)
<p class="textpdf" style="text-indent: 15px;">{{$_detail}}</p>
@endforeach

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">8. กิจกรรมของโครงการ/ระยะเวลาดำเนินการ </td>
    </tr>
</table>
<table class="table" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th class="headpdf" style="text-align: center;">กิจกรรม</th>
        <th class="headpdf" style="text-align: center;">วันเดือนปี</th>
    </tr>
    @foreach($budgetPlanProject->plan_project_activities as $index=>$item)
     <tr>
         <td class="textpdf tab_article">&nbsp;&nbsp;&nbsp;&nbsp;8.{{$index+1}} {{$item->project_activity_name or ''}}</td>
         <td class="textpdf tab_article" style="text-align: center;">
         {{ \App\Helepers\DateFormat::eng_to_thai($item->project_activity_date)}}
         </td>
     </tr>
    @endforeach
 </table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">9. ประโยชน์ที่คาดว่าจะได้รับ </td>
    </tr>
 @foreach($budgetPlanProject->plan_project_benefits as $index=>$item)
    <tr>
        <td class="textpdf tab_article">9.{{$index+1}} {{$item->project_benefit_name or ''}}</td>
    </tr>
 @endforeach
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">10. ระยะเวลาดำเนินการ </td>
    </tr>
    <tr>
        <td class="textpdf" style="padding-left: 16px;">เริ่ม เดือน {{\App\Helepers\DateFormat::getMonthName($budgetPlanProject->month_start)}} / พ.ศ. {{$budgetPlanProject->year_start+543}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สิ้นสุด เดือน {{\App\Helepers\DateFormat::getMonthName($budgetPlanProject->month_end)}} / พ.ศ. {{$budgetPlanProject->year_end+543}}</td>
    </tr>
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">11. สถานที่ดำเนินการ </td>
    </tr>
    <tr>
        <td class="textpdf tab_article">{{$budgetPlanProject->place or ''}}</td>
    </tr>
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">12. เป้าหมายในการดำเนินโครงการ </td>
    </tr>
</table>
<?php $items = json_decode($budgetPlanProject->project_goal,TRUE);?>
<?php $base_year = ($items['fiscal_year']+543);?>
<?php $items = $items['items'];?>
<?php $header1 =['1'=>'ตัวชี้วัดเชิงปริมาณ :','2'=>'ตัวชี้วัดคุณภาพ :'];?>
<?php $header2 =['1'=>'ตัวชี้วัดเชิงปริมาณ :','2'=>'ตัวชี้วัดคุณภาพ :','3'=>'ตัวชี้วัดเชิงเวลา :','4'=>'ตัวชี้วัดเชิงต้นทุน :'];?>
<table class="table" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th class="headpdf" style="text-align: center;" rowspan="2">ตัวชี้วัดความสำเร็จ</th>
        <th class="headpdf" style="text-align: center;" rowspan="2">หน่วยนับ</th>
        <th class="headpdf" style="text-align: center;" colspan="1">ผล</th>
        <th class="headpdf" style="text-align: center;" colspan="1">แผน/(ผล)</th>
        <th class="headpdf" style="text-align: center;" colspan="4">ค่าเป้าหมาย</th>
    </tr>
    <tr>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year-2}}</th>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year-1}}</th>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year}}</th>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year+1}}</th>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year+2}}</th>
        <th class="headpdf" style="text-align: center;">ปี {{$base_year+3}}</th>
    </tr>
    <tr>
         <td class="headpdf tab_article" style="text-align: left;">ระดับผลลัพธ์</td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
     </tr>
     @for($i=1;$i<=2;$i++)
     <tr>
         <td class="textpdf tab_article" style="text-align: left;">{{$header1[$i]}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['qty']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['result']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['plan']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y1']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y2']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y3']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y4']}}</td>
     </tr>
     @endfor
     <tr>
         <td class="headpdf tab_article" style="text-align: left;">ระดับผลผลิต</td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
         <td class="textpdf tab_article" style="text-align: center;"></td>
     </tr>
     @for($i=1;$i<=4;$i++)
     <tr>
         <td class="textpdf tab_article" style="text-align: left;">{{$header2[$i]}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['qty']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['result']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['plan']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y1']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y2']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y3']}}</td>
         <td class="textpdf tab_article" style="text-align: center;">{{$items['row'.$i]['target']['y4']}}</td>
     </tr>
     @endfor
 </table>

 <table  style="width: 100%" >
    <tr>
        <td class="headpdf" style="text-align: left;">13. งบประมาณ </td>
        <td class="textpdf" style="text-align: right; color: red;">
        {{number_format($budgetPlanProject->total_price[$budgetPlanProject->project_id],2)}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บาท
        </td>
    </tr>
    <tr>
        <td class="textpdf" style="text-align: left;">รายละเอียดค่าใช้จ่ายในการดำเนินโครงการ</td>
    </tr>
    <?php  
        $activitys = [] ;
        $totals = [];
        foreach ($budgetPlanProject->plan_project_budgets as $index=>$item) {

            if (!isset($totals[$item->budget_activity])) {
                 $totals[$item->budget_activity] = $item->project_budget_price;
            } else {
                $totals[$item->budget_activity] += $item->project_budget_price;
            }
        }
    ?>

    @foreach($budgetPlanProject->plan_project_budgets as $index=>$item)
    <?php 

        if (!in_array($item->budget_activity, $activitys)) {
            $activitys[] = $item->budget_activity;
    ?>
            <tr>
                <td class="textpdf tab_article" style="color: red;">กิจกรรมที่ {{$item->budget_activity}} </td>
                <td class="textpdf tab_article" style="color: red; text-align: right;">
                    {{number_format($totals[$item->budget_activity],2)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บาท
                </td>
            </tr>
    <?php
        }
    ?>

    <tr>
        <td class="textpdf tab_article" style="text-align: left;">
        {{$index+1}}. รายการ {{$item->project_budget_name}}
        </td>
        <td class="textpdf tab_article" style="text-align: right;">
        {{number_format($item->project_budget_price,2)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บาท
        </td>
    </tr>
    @endforeach
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">14. ความพร้อมของโครงการ </td>
    </tr>
    <tr>
        <td class="textpdf tab_article">{{$budgetPlanProject->ready or ''}}</td>
    </tr>
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">15. ปัญหาอุปสรรค/ข้อเสนอแนะ </td>
    </tr>
    @foreach($budgetPlanProject->plan_project_suggestions as $index=>$item)
    <tr>
        <td class="textpdf tab_article">15.{{$index+1}} {{$item->project_suggestion_name or ''}}</td>
    </tr>
 @endforeach
</table>

<table>
    <tr>
        <td class="headpdf" style="text-align: left;">16. ผู้รับผิดชอบโครงการ </td>
    </tr>
</table>
<table style="width: 100%;">
 @foreach($budgetPlanProject->plan_committees as $index=>$item)
    @if(($item->personnel_id)==($item->personnel_id))
    <tr>
        <td class="textpdf tab_article" style="text-align: left; vertical-align: top; width: 15px;">{{$index+1}}.</td>
        <td class="textpdf" style="text-align: left; vertical-align: top;">ชื่อ-ชื่อสกุล/ตำแหน่ง/สังกัด/เบอร์โทรศัพท์</td>
        @if($item->position_abbreviation=='อ.')
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">{{$item->title_name}}{{$item->first_name or ''}}</td>
        @else
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">{{$item->position_abbreviation}}{{$item->first_name or ''}}</td>
        @endif
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">สาขาวิชา{{$item->branch_name}} ศูนย์{{$item->campus_name}}</td>
    </tr>
    <tr>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">โทรศัพท์ {{$item->mobile}}</td>
    </tr>
    @endif
@endforeach
 </table>

<br />
 <p style="text-align: left; padding-left: 10px; color: red; font-size: 16px;">* ในกรณีเป็นการประชุมสัมมนา การฝึกอบรม การฝึกอบรมเชิงปฎิบัติการการประชุมสัมมนาฝึกอบรม ให้แนบตารางเวลาเพื่อประกอบการพิจารณา</p>

