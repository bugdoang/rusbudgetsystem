<div id="tab8" class="tab-pane">
    <div class="form-group">
        <div class="row">
            <label class="col-md-5 col-md-offset-1" for="textinput">คณะกรรมการผู้รับผิดชอบรายการสิ่งก่อสร้าง :</label>
            {{csrf_field()}}
            <div class="site-breadcrumb" style="margin: 21px 20px;">
                <a class="btn btn-default" href="#"><i class="fa fa-plus" aria-hidden="true"></i> คณะกรรมการผู้รับผิดชอบ</a>
            </div>
            <div class="col-md-12 control-label">
                <table class="table table-bordered" style="margin: 5px 29px; width: 918px;">
                    <tbody align="center">
                    <tr>
                        <th class="text-center text-middle" style="width: 60px;">#</th>
                        <th class="text-center text-middle">ตำแหน่ง</th>
                        <th class="text-center text-middle" style="width: 30%;">ชื่อ-นามสกุล</th>
                        <th class="text-center text-middle">สังกัด</th>
                        <th class="text-center text-middle">เบอร์โทรศัพท์</th>
                        <th class="text-center text-middle" style="width: 60px;">ลบ</th>
                    </tr>

                    {{--@foreach($products as $product)--}}
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">
                            <select class="form-control" name="position_id" id="position_id">
                                <option value="">กรุณาเลือก</option>
                                <option value="">#</option>
                            </select>
                        </td>
                        <td class="text-left">ภัสราภรณ์ คำนวนฤทธิ์</td>
                        <td class="text-center">
                            <select class="form-control" name="position_id" id="position_id">
                                <option value="">กรุณาเลือก</option>
                                <option value="">#</option>
                            </select>
                        </td>
                        <td class="text-center">090-423-0818</td>
                        <td class="text-center"><a href="#">ลบ</a></td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            บันทึก
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
