<div id="tab4" class="tab-pane">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">ลักษณะโครงสร้างของอาคาร :</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="ลักษณะโครงสร้างของอาคาร" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">จำนวน :</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="จำนวน" value="">
                </div>
                <label class="col-md-2" for="textinput">ชั้น</label>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">จำนวนพื้นที่ :</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="จำนวนพื้นที่" value="">
                </div>
                <label class="col-md-2" for="textinput">ตร.ม.</label>
            </div>
            <hr/>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">ระยะเวลาของการก่อสร้าง :</label>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">เริ่ม</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">สิ้นสุด</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">คิดเป็นระยะเวลา</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="" value="">
                </div>
                <label class="col-md-2" for="textinput">วัน</label>
            </div>
            <hr/>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">งบประมาณทั้งสิ้น :</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="งบประมาณทั้งสิ้น" value="">
                </div>
                <label class="col-md-2" for="textinput">บาท</label>
            </div>
            <hr/>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">สถานทีก่อสร้าง :</label>
                <div class="col-md-6">
                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="สถานทีก่อสร้าง" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="textinput">ระบุศูนย์พื้นที่ :</label>
                <div class="col-md-6">
                    <select class="form-control" name="position_id" id="position_id">
                        <option value="">กรุณาเลือก</option>
                        <option value="">#</option>
                    </select>
                </div>
            </div>
            <hr/>
        </div>
    </div>
</div>