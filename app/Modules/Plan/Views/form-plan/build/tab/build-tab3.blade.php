<div id="tab3" class="tab-pane">
    <div class="form-group">
        <div class="row">
            <label class="col-md-4 col-md-offset-1" for="textinput">วัตถุประสงค์ในการเสนอขอรับการจัดสรร :</label>
            {{csrf_field()}}
            <div class="site-breadcrumb" style="margin: 21px 20px;">
                <a class="btn btn-default" href="#"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มวัตถุประสงค์</a>
            </div>
            <div class="col-md-12 control-label">
                <table class="table table-bordered" style="margin: 5px 29px; width: 918px;">
                    <tbody align="center">
                    <tr>
                        <th class="text-center text-middle" style="width: 60px;">#</th>
                        <th class="text-center text-middle">วัตถุประสงค์</th>
                        <th class="text-center text-middle" style="width: 60px;">ลบ</th>
                    </tr>

                    {{--@foreach($products as $product)--}}
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-left">#########</td>
                        <td class="text-center"><a href="#">ลบ</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">2</td>
                        <td class="text-left">ได้สิ่งปลูกสร้างที่มีประสิทธิภาพ</td>
                        <td class="text-center"><a href="#">ลบ</a></td>
                    </tr>
                    {{--@endforeach--}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>