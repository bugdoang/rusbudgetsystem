@extends('masterlayout')
@section('title','แบบฟอร์มรายละเอียดประกอบค่าที่ดินและสิ่งปลูกสร้าง')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดประกอบค่าที่ดินและสิ่งปลูกสร้าง','link'=>'/build-plan'],['label'=>'แบบฟอร์มรายละเอียดประกอบค่าที่ดินและสิ่งปลูกสร้าง','link'=>'build-form']]) !!}

<!-- Page Content -->
<form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="durable_article_id" value="{{$budgetPlanDurable->durable_article_id or ''}}">
    <div class="row">
        <!--start left-->
        <div class="col-lg-3 col-md-3">
            <div class="row">
                <h2 class="header-text myfont">ประเภทข้อมูล</h2>
                <div id="rootwizard" class="tabs-left shadow">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#tab1">1. ข้อมูลพื้นฐาน</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab2">2. เหตุผลความจำเป็น</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab3">3. วัตถุประสงค์ในการเสนอขอ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab4">4. รายละเอียดการก่อสร้าง</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab5">5. การใช้ประโยชน์ของอาคาร</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab6">6. หากไม่ได้รับการจัดสรรจะเกิดผลอย่างไร</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab7">7. ความพร้อมในการดำเนินการ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab8">8. คณะกรรมการผู้รับผิดชอบ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--start right-->
        <div style="padding-right: 0;" class="col-lg-9 col-md-9">
            <div class="tab-content">
                {!! \App\Modules\Plan\Services\BuildForm1::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm2::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm3::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm4::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm5::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm6::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm7::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm8::get() !!}
                {!! \App\Modules\Plan\Services\BuildForm9::get() !!}
            </div>
        </div>
    </div>
</form>
@stop

@section('scripts')

<script>
    function save_success(data)
    {
        MessageBox.alert('ระบบได้บันทึกข้อมูลเรียบร้อยแล้ว',function(){
            window.location.href = data.redirect_url;
        });
    }
    // function beforesend_form()
    // {
    //     $('#list3').val(JSON.stringify(list_durable3.items));
    //     $('#list8').val(JSON.stringify(list_durable8.items));
    //     $('#list5').val(JSON.stringify(list_durable5.items));
    //     $('#list7').val(JSON.stringify(list_durable7.items));
    //     $('#list9').val(JSON.stringify(list_durable9.items));

    // }
    $(document).ready(function() {
        $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
    });
</script>
@stop
