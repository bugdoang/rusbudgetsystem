@extends('masterlayout')
@section('title','แบบฟอร์มรายละเอียดประกอบค่าครุภัณฑ์')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดประกอบค่าครุภัณฑ์','link'=>'/durable-article-plan'],['label'=>'แบบฟอร์มรายละเอียดประกอบค่าครุภัณฑ์','link'=>'project-form']]) !!}
@if(isset($budgetPlanDurable) && $budgetPlanDurable->status=='รอการตรวจสอบ' && $method!='POST' && $my_group_id!=4)
    <div class="site-breadcrumb">
        <span>ปรับสถานะ :  </span>
        <a class="btn btn-default edit-content" href="/durable-article-plan/modal/{{$budget_plan_id}}"> อนุมัติ</a>
        <a class="btn btn-default reject-content" href="/durable-article-plan/modal/{{$budget_plan_id}}/reject"> ไม่อนุมัติ</a>
    </div>
@endif

<!-- Page Content -->
<form id="form-plan-durable" name="form-plan-durable" class="form-horizontal" role="form" method="{{(isset($method))?$method:''}}" action="{{(isset($action))?$action:''}}" >
    {{csrf_field()}}
    <input type="hidden" name="durable_article_id" value="{{$budgetPlanDurable->durable_article_id or ''}}">
    <div class="row">
        <!--start left-->
        <div class="col-lg-3 col-md-3">
            <div class="row">
                <h2 class="header-text myfont">ประเภทข้อมูล</h2>
                <div id="rootwizard" class="tabs-left shadow">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#tab1">1. ข้อมูลพื้นฐาน</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab2">2. ความสอดคล้องยุทธศาสตร์</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab3">3. รายการครุภัณฑ์</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab4">4. เหตุผลในการเสนอขอ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab5">5. วัตถุประสงค์ในการเสนอขอ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab6">6. ความถี่ในการใช้งาน</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab7">7. หากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab8">8. มาตรฐานและรายละเอียดคุณลักษณะ</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab9">9. คณะกรรมการ / ผู้จัดทำมาตรฐาน</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--start right-->
        <div style="padding-right: 0;" class="col-lg-9 col-md-9">
            <div class="tab-content">
                {!! \App\Modules\Plan\Services\DurableForm1::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm2::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm3::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm4::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm5::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm6::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm7::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm8::get($budgetPlanDurable) !!}
                {!! \App\Modules\Plan\Services\DurableForm9::get($budgetPlanDurable) !!}
            </div>
        </div>
    </div>
</form>
@stop

@section('scripts')

<script>
    function save_success(data)
    {
        MessageBox.alert('ระบบได้บันทึกข้อมูลเรียบร้อยแล้วคะ',function(){
            window.location.href = data.redirect_url;
        });
    }
    function beforesend_form()
    {
        $('#list3').val(JSON.stringify(list_durable3.items));
        $('#list8').val(JSON.stringify(list_durable8.items));
        $('#list5').val(JSON.stringify(list_durable5.items));
        $('#list7').val(JSON.stringify(list_durable7.items));
        $('#list9').val(JSON.stringify(list_durable9.items));

    }
    $(document).ready(function() {
        $('#rootwizard').bootstrapWizard({'tabClass': 'nav nav-tabs'});
    });
</script>
@stop
