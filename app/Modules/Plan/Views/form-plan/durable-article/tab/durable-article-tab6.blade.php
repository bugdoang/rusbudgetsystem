<div id="tab6" class="tab-pane">
    <h2 class="header-text myfont">ความถี่ในการใช้งาน</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tbody align="center">
                        <tr>
                            <th class="text-center text-middle" style="width: 60px;">#</th>
                            <th class="text-center text-middle">ความถี่ในการใช้งาน</th>
                            <th class="text-center text-middle">ค่าความถี่เฉลี่ย</th>
                            <th class="text-center text-middle">หน่วย</th>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-left">งานจัดการเรียนการสอน เฉลี่ย</td>
                            <td><input class="numberonlydot text-center" name="durable_article_frequency1" value="{{(isset($items[1]))?$items[1]:0}}" type="text" style="border:1px solid #ccc; margin: 0 2px;"></td>
                            <td class="text-center">ชม./สป./ภาคการศึกษา</td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td class="text-left">งานวิจัย เฉลี่ย</td>
                            <td><input class="numberonlydot text-center" name="durable_article_frequency2" value="{{(isset($items[2]))?$items[2]:0}}" type="text" style="border:1px solid #ccc; margin: 0 2px;"></td>
                            <td class="text-center">ชม./ปี</td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td class="text-left">งานบริหารวิชาการ เฉลี่ย</td>
                            <td><input class="numberonlydot text-center" name="durable_article_frequency3" value="{{(isset($items[3]))?$items[3]:0}}" type="text" style="border:1px solid #ccc; margin: 0 2px;"></td>
                            <td class="text-center">ชม./ปี</td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td class="text-left">งานทำนุบำรุงศิลปวัฒนธรรม เฉลี่ย</td>
                            <td><input class="numberonlydot text-center" name="durable_article_frequency4" value="{{(isset($items[4]))?$items[4]:0}}" type="text" style="border:1px solid #ccc; margin: 0 2px;" value=""></td>
                            <td class="text-center">ชม./ปี</td>
                        </tr>
                        <tr>
                            <td class="text-center">5</td>
                            <td class="text-left">การบริหารจัดการ เฉลี่ย</td>
                            <td><input class="numberonlydot text-center" name="durable_article_frequency5" value="{{(isset($items[5]))?$items[5]:0}}" type="text" style="border:1px solid #ccc; margin: 0 2px;" value=""></td>
                            <td class="text-center">ชม./ปี</td>
                        </tr>
                    </tbody>
                </table>
                <hr/>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> สถานที่นำไปใช้งาน :</label>
                    <div class="col-md-6">
                        <input id="durable_article_place" class="form-control input-md" type="text" name="durable_article_place" placeholder="สถานที่นำไปใช้งาน" value="{{$budgetPlanDurable->durable_article_place or ''}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>