<div id="tab9" class="tab-pane">
    <h2 class="header-text myfont">คณะกรรมการ/ผู้จัดทำมาตรฐานและรายละเอียดคุณลักษณะ</h2>
    <div id="list-durable9" class="col-lg-12 col-md-12 form-planbudget shadow">
        {{csrf_field()}}
        <div class="site-breadcrumb" style="margin: -49px -5px;">
            <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> คณะกรรมการ/ผู้จัดทำมาตรฐาน</a>
        </div>
        <input type="hidden" name="list9" id="list9">
        <table class="table table-bordered"  style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 10%;">#</th>
                    <th class="text-center text-middle" style="width: 80%;">ชื่อ-นามสกุล</th>
                    <th class="text-center text-middle" style="width: 10%;">ลบ</th>
                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td class="text-left">
                        <select class="form-control myselect9"  v-bind:id="index" onchange="select_comittee(this)" name="committee_id" v-model="item.committee_id">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($personnels))
                                @foreach($personnels as $personnel)
                                    <option value="{{$personnel->personnel_id}}">{{$personnel->first_name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </td>
                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0" v-on:click="remove_item(index)"> ลบ</a></td>
                </tr>
            </tbody>
        </table>
        <span>หมายเหตุ : รายชื่อแรกเป็นรายชื่อคณะกรรมการหลักคะ</span>
        <div class="form-group">
            <div class="" style="width:20%; display:block; text-align:center; margin: auto;">
                <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    บันทึก
                </button>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    function select_comittee(o)
    {
        var el = $(o);
        var index = el.attr('id');
        var name = $('#select2-'+index+'-container').attr('title');
        list_durable9.items[index].committee_id = el.val();
        list_durable9.items[index].committee_name = name;
    }
    var list_durable9 = new Vue({
        el: '#list-durable9',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                committee_id:{{\Auth::id()}},
                committee_name:'{{\Auth::user()->fullName}}'
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    committee_id:'',
                    committee_name:'',
                });
                setTimeout(function(){
                    $('.myselect9').each(function(i,e){$(e).select2Build();});
                },300);
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush

