<div id="tab5" class="tab-pane">
    <h2 class="header-text myfont">วัตถุประสงค์ในการเสนอขอ</h2>
    <div id="list-durable5" class="col-lg-12 col-md-12 form-planbudget shadow">
        {{csrf_field()}}
        <div class="site-breadcrumb" style="margin: -49px -5px;">
            <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มวัตถุประสงค์</a>
        </div>
        <input type="hidden" name="list5" id="list5">
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 60px;">#</th>
                    <th class="text-center text-middle">วัตถุประสงค์</th>
                    <th class="text-center text-middle" style="width: 30px;">ลบ</th>

                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <input class="form-control input-md" style="width: 100%;" type="text" v-model="item.objective_name">
                    </td>
                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0"  v-on:click="remove_item(index)"> ลบ</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var list_durable5 = new Vue({
        el: '#list-durable5',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                objective_name: ''
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    objective_name:'',
                })
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })
</script>
@endpush