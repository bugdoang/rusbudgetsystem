<div id="tab7" class="tab-pane">
    <h2 class="header-text myfont">หากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร</h2>
    <div id="list-durable7" class="col-lg-12 col-md-12 form-planbudget shadow">
        <div class="site-breadcrumb" style="margin: -49px -5px;">
            <a class="btn btn-default" href="javascript:;" v-on:click="add"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มหากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร</a>
        </div>
        <input type="hidden" name="list7" id="list7">
        <table class="table table-bordered" style="margin-top: 42px;">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 60px;">#</th>
                    <th class="text-center text-middle">หากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร</th>
                    <th class="text-center text-middle" style="width: 30px;">ลบ</th>
                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <input class="form-control input-md" name="durable_articles_allocate_name" id="durable_articles_allocate_name" style="width: 100%;" type="text" v-model="item.durable_articles_allocate_name">
                    </td>
                    <td class="text-center text-middle"><a href="javascript:;" v-if="index>0" v-on:click="remove_item(index)"> ลบ</a></td>
                </tr>
            </tbody>
        </table>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> แผนจัดซื้อ : </label>
            <div class="col-md-3">
                <select class="" name="durable_article_purchase" onchange="change_type7(this)" id="durable_article_purchase">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($purchases))
                        @foreach($purchases as $purchase)
                            <option {{(isset($budgetPlanDurable->durable_article_purchase) && $budgetPlanDurable->durable_article_purchase==$purchase)?' selected ':''}} value="{{$purchase}}">{{$purchase}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div id="display-purchase-only" class="form-group">
            <label class="col-md-4 control-label" for="textinput">ภายในเดือน : </label>
            <div class="col-md-3">
                <select class="" name="durable_article_purchase_month" id="durable_article_purchase_month">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($months))
                        @foreach($months as $m_index=> $month)
                            <option {{(isset($budgetPlanDurable->durable_article_purchase_month) && $budgetPlanDurable->durable_article_purchase_month==str_pad(($m_index+1),2,'0',STR_PAD_LEFT))?' selected ':''}}  value="{{str_pad(($m_index+1),2,'0',STR_PAD_LEFT)}}">{{$month}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">พ.ศ. : </label>
            <div class="col-md-2">
                <select class="" name="durable_article_purchase_year" id="durable_article_purchase_year">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                        <option {{(isset($budgetPlanDurable->durable_article_purchase_year) && $budgetPlanDurable->durable_article_purchase_year==$i)?' selected ':''}} value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ส่งมอบพร้อมติดตั้ง : </label>
        </div>
        <div class="form-group ">
            <label class="col-md-4 control-label" for="textinput">ภายในเดือน : </label>
            <div class="col-md-3">
                <select class="" name="durable_article_install_month" id="durable_article_install_month">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($months))
                        @foreach($months as $m_index=>$month)
                            <option {{(isset($budgetPlanDurable->durable_article_install_month) && $budgetPlanDurable->durable_article_install_month==str_pad(($m_index+1),2,'0',STR_PAD_LEFT))?' selected ':''}}  value="{{str_pad(($m_index+1),2,'0',STR_PAD_LEFT)}}">{{$month}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">พ.ศ. : </label>
            <div class="col-md-2">
                <select class="" name="durable_article_install_year" id="durable_article_install_year">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                        <option {{(isset($budgetPlanDurable->durable_article_install_year) && $budgetPlanDurable->durable_article_install_year==$i)?' selected ':''}} value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> แผนการใช้จ่าย : </label>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">เบิกจ่ายภายในเดือน : </label>
            <div class="col-md-3">
                <select class="" name="durable_article_spend_month" id="durable_article_spend_month">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($months))
                        @foreach($months as $m_index=>$month)
                            <option {{(isset($budgetPlanDurable->durable_article_spend_month) && $budgetPlanDurable->durable_article_spend_month==str_pad(($m_index+1),2,'0',STR_PAD_LEFT))?' selected ':''}} value="{{str_pad(($m_index+1),2,'0',STR_PAD_LEFT)}}">{{$month}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">พ.ศ. : </label>
            <div class="col-md-2">
                <select class="" name="durable_article_spend_year" id="durable_article_spend_year">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                        <option {{(isset($budgetPlanDurable->durable_article_spend_year) && $budgetPlanDurable->durable_article_spend_year==$i)?' selected ':''}} value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var list_durable7= new Vue({
        el: '#list-durable7',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!}
        },
        created:function() {
            @if(empty($items))
            this.items.push({
                durable_articles_allocate_name: ''
            });
            @endif
        },
        methods:{
            add:function(){
                this.items.push({
                    durable_articles_allocate_name:''
                });
                setTimeout(function(){
                    $('.data-select').each(function(i,e){$(e).select2Build();});
                },300);
            },
            remove_item:function(item){
                this.items.splice(item,1);
            }
        }
    })


</script>
@endpush

@push('scripts')
    <script type="text/javascript">
    @if(isset($budgetPlanDurable->durable_article_purchase) && $budgetPlanDurable->durable_article_purchase=='สั่งซื้อได้ทันที')
        $('#display-purchase-only').find('select').prop("disabled", true);
    @endif
    function change_type7(o)
        {
            if($(o).val()!='' && $(o).val()=='สั่งซื้อได้ทันที')
            {
                $('#display-purchase-only').find('select').prop("disabled", true);
            }
            else {
                $('#display-purchase-only').find('select').prop("disabled", false);

            }
        }
</script>
@endpush
