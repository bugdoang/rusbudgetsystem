<div id="tab8" class="tab-pane">
    <h2 class="header-text myfont">มาตรฐานและรายละเอียดคุณลักษณะ</h2>
    <div id="list-durable8" class="col-lg-12 col-md-12 form-planbudget shadow">
        <input type="hidden" name="list8" id="list8">
        <table class="table table-bordered">
            <tbody align="center">
                <tr>
                    <th class="text-center text-middle" style="width: 60px;">#</th>
                    <th class="text-center text-middle">รายการ</th>
                </tr>
                <tr v-for="(item,index) in items">
                    <td>
                        <span>@{{index+1}}</span>
                    </td>
                    <td>
                        <p class="text-left">@{{ item.durable_article_standard_item }}
                            <a style="margin:0;" class="btn btn-default pull-right" href="javascript:;" v-on:click="add(index)"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</a>
                        </p>
                        <table v-if="item.children.length>0" style="margin-top:21px;" class="table table-bordered">
                            <tr v-for="(child,_index) in item.children">
                                <td style="width:50px;" class="text-right">@{{(index+1)+'.'+(_index+1)}}</td>
                                <td><input class="form-control" type="text" v-model="item.children[_index].durable_article_allocate"/></td>
                                <td class="text-center text-middle"><a v-if="_index>0" href="javascript:;" v-on:click="remove_item(index,_index)"> ลบ</a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var list_durable8= new Vue({
        el: '#list-durable8',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!},
        },
        methods:{
            add:function(index){
                var old = this.items[index];
                old.children.push({durable_article_allocate:''});
                this.$set(this.items,index,old);
            },
            remove_item:function(i,ii){
                var old = this.items[i];
                old.children.splice(ii,1);
                this.$set(this.items,i,old);
            }
        },
    })
</script>
@endpush
