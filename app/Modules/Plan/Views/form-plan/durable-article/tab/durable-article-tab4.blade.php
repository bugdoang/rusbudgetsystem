<div id="tab4" class="tab-pane">
    <h2 class="header-text myfont">เหตุผลในการเสนอขอ</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow">
        <textarea class="form-control" rows="10" id="reason" name="reason" placeholder="เหตุผลความจำเป็น">{{$budgetPlanDurable->reason or ''}}</textarea>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>
