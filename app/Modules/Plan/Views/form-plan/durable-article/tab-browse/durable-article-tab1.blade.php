<div id="tab1" class="tab-pane active">
    <h2 class="header-text myfont">ข้อมูลพื้นฐาน</h2>
    <div class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ชื่อรายการครุภัณฑ์ :</label>
            <div class="col-md-6">
                <input id="durable_article_name" class="form-control input-md" type="text" name="durable_article_name" value="{{$budgetPlanDurable->durable_article_name or ''}}" placeholder="ชื่อรายการครุภัณฑ์">
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> แหล่งเงินได้ : </label>
            <div class="col-md-6 disable-select">
                <select class="form-control" name="income_id" id="income_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($incomes))
                        @foreach($incomes as $income)
                    <option {{(isset($budgetPlanDurable->income_id) && $budgetPlanDurable->income_id==$income->income_id)?' selected ':''}} value="{{$income->income_id}}">{{$income->income_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ประเภทแหล่งเงินได้ : </label>
            <div class="col-md-6 disable-select">
                <select class="form-control" name="income_type_id" onchange="change_type1(this)" id="income_type_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($incomeTypes))
                        @foreach($incomeTypes as $incomeType)
                            <option {{(isset($budgetPlanDurable->income_type_id) && $budgetPlanDurable->income_type_id==$incomeType->income_type_id)?' selected ':''}} value="{{$incomeType->income_type_id}}">{{$incomeType->income_type_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">หมวดแหล่งเงินได้ : </label>
            <div class="col-md-6 disable-select">
                <select class="form-control" name="income_group_id" id="income_group_id">
                    <option value="">กรุณาเลือก</option>
                    @if(isset($budgetPlanDurable->income_group_id))
                    @foreach($incomeGroups as $incomeGroup)
                        @if($budgetPlanDurable->income_type_id==$incomeGroup->income_type_id)
                        <option {{(isset($budgetPlanDurable->income_group_id) && $budgetPlanDurable->income_group_id==$incomeGroup->income_group_id)?' selected ':''}} value="{{$incomeGroup->income_group_id}}">{{$incomeGroup->income_group_name}}</option>
                        @endif
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> ปีงบประมาณ : </label>
            <div class="col-md-6 disable-select">
                <select class="form-control" name="fiscal_year" id="fiscal_year">
                    <option value="">กรุณาเลือก</option>
                    @for($i=date('Y')+1;$i>=$startYear;$i--)
                    <option {{(isset($budgetPlanDurable->fiscal_year) && $budgetPlanDurable->fiscal_year==$i)?' selected ':''}}  value="{{$i}}">{{$i+543}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput"><span class="font-color-red"> * </span> รอบภาคการศึกษา : </label>
            <div class="col-md-6 disable-select">
                <select class="form-control" name="semester" id="semester">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($programs))
                        @foreach($programs as $program)
                            <option {{(isset($budgetPlanDurable->semester) && $budgetPlanDurable->semester==$program)?' selected ':''}} value="{{$program}}">{{$program}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    var incomeGroups={};
    @if(!empty($incomeGroups))
    @foreach($incomeGroups as $incomeGroup)
        if(typeof incomeGroups[{{$incomeGroup->income_type_id}}]!='object') incomeGroups[{{$incomeGroup->income_type_id}}]=[];
        incomeGroups[{{$incomeGroup->income_type_id}}].push({income_group_id:{{$incomeGroup->income_group_id}},income_group_name:'{{$incomeGroup->income_group_name}}'});
    @endforeach
    @endif

    function change_type1(o)
    {
        $('#income_group_id').select2('val',null);
        $('#income_group_id').empty();
        if($(o).val()!='')
        {
            $('#income_group_id').empty().append('<option value="">กรุณาเลือก</option>')
            if(typeof incomeGroups[$(o).val()]!=='undefined' && incomeGroups[$(o).val()]!=null)
            {
                $(incomeGroups[$(o).val()]).each(function(i,o){
                    $('#income_group_id').append('<option value="'+o.income_group_id+'">'+o.income_group_name+'</option>');
                });
            }
        }
    }
</script>
@endpush