<div id="tab3" class="tab-pane">
    <h2 class="header-text myfont">รายการครุภัณฑ์</h2>
    <div id="list-durable3" class="col-lg-12 col-md-12 form-planbudget shadow disable-select">
        <input type="hidden" name="list3" id="list3">
        <table style="border:0!important;" class="table">
            <tr>
                <td style="width:200px;padding:2px 8px;border:0;" class="text-right text-middle"><span class="font-color-red"> * </span> แบบรายการ :</td>
                <td style="padding:2px 8px;border:0;">
                    <select name="durable_article_list" onchange="change_type3(this)" id="durable_article_list">
                        @if(!empty($lists))
                            @foreach($lists as $list)
                                <option {{(isset($budgetPlanDurable->durable_article_list) && $budgetPlanDurable->durable_article_list==$list)?' selected ':''}} value="{{$list}}">{{$list}}</option>
                            @endforeach
                        @endif
                    </select>
                </td>
            </tr>
        </table>
        <table style="border:0!important;" class="table my-table table-bordered">
            <tr>
                <th class="text-center" style="width:30px;">ที่</th>
                <th class="text-center">รายการ</th>
                <th class="text-center" style="width:70px;">หน่วยนับ</th>
                <th class="text-center" style="width:50px;">จำนวน</th>
                <th class="text-center" style="width:90px;">ราคา/หน่วย</th>
                <th class="text-center" style="width:90px;">จำนวนเงิน</th>
                <th class="text-center" style="width:190px;">สถานะของครุภัณฑ์</th>
            </tr>
            <tr  v-for="(item,index) in items">
                <td class="text-center" style="font-size:12px;">@{{index+1}}.</td>
                <td><input type="text" v-model="item.durable_article_standard_item" class="my-text"></td>
                <td><input type="text" v-model="item.durable_article_unit" maxlength="10" class="my-text text-center"></td>
                <td><input type="text" v-model="item.durable_article_qty" maxlength="5" v-on:keyup="calc(index)"  class="my-text numberonly text-right"></td>
                <td><input type="text" v-model="item.durable_article_priceunit" maxlength="9" v-on:keyup="calc(index)"  class="my-text numberonlydot text-right"></td>
                <td class="text-right">@{{ (item.durable_article_num.toFixed(2)) }}</td>
                <td>
                    <select class="data-select" v-bind:id="index" v-model="item.durable_article_status" onchange="change_status(this)" data-clear="true">
                        <option value="">กรุณาเลือก</option>
                        <option value="Y">ใช้การได้ เพิ่มประสิทธิภาพ</option>
                        <option value="N">ใช้การไม่ได้ ทดแทนของเดิม</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="padding-left:120px!important;" class="text-left">รวม</td>
                <td class="text-right">@{{ addCommas(price_net.toFixed(2)) }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5" style="padding-left:120px!important;" class="text-left">Vat 7%</td>
                <td class="text-right">@{{ addCommas((price_net*0.07).toFixed(2)) }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5"  class="text-center">รวมทั้งสิ้น</td>
                <td class="text-right">@{{ addCommas((price_net + (price_net*0.07)).toFixed(2))}}</td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div style="height:10px;clear:both;"></div>
</div>

@push('scripts')
<script type="text/javascript">
    function change_status(o)
    {
        var index = $(o).attr('id');
        list_durable3.items[index].durable_article_status = $(o).val();
        list_durable3.items[index].durable_article_reason = $(o).val()=='Y'?'N':'Y';
    }
    var list_durable3 = new Vue({
        el: '#list-durable3',
        data: {
            items:{!! json_encode($items,JSON_NUMERIC_CHECK) !!},
            price_net:{{$price_net}}
        },
        created:function(){
            @if(empty($items))
            this.items.push({
                durable_article_standard_item:'',
                durable_article_unit:'',
                durable_article_qty:0,
                durable_article_priceunit:0,
                durable_article_num:0,
                durable_article_status:'',
                durable_article_reason:''
            });

            var _this = this;
            setTimeout(function(){
                $(_this.items).each(function(i,item)
                {
                    var old = typeof list_durable8._data.items[i]!=='undefined'?list_durable8._data.items[i]:null;
                    if(old!=null)
                    {
                        item.children = old.children;
                        list_durable8._data.items[i] = item;
                    }
                    else
                    {
                        item.children = [];
                        list_durable8._data.items.push(item);
                    }
                });
            },500);
            @endif
        },
        methods:{
            add:function(){
                if($('#durable_article_list').val()=='รายการชุด') {
                    this.items.push({
                        durable_article_standard_item: '',
                        durable_article_unit: '',
                        durable_article_qty: 0,
                        durable_article_priceunit: 0,
                        durable_article_num: 0,
                        durable_article_status: '',
                        durable_article_reason: ''
                    });
                    setTimeout(function(){
                        $('.data-select').each(function(i,e){$(e).select2Build();});
                    },300);
                }
            },
            remove_item:function(item){
                this.items.splice(item,1);
                list_durable8._data.items.splice(item,1);
                var _this = this;
                _this.price_net=0;
                $(this.items).each(function(i,item){
                    if(!isNaN(item.durable_article_num))
                    {
                        _this.price_net +=item.durable_article_num;
                    }
                });
            },
            calc:function(index)
            {
                if(!isNaN(this.items[index]['durable_article_qty']) && !isNaN(this.items[index]['durable_article_priceunit']))
                {
                    this.items[index]['durable_article_num'] = (this.items[index]['durable_article_qty']*this.items[index]['durable_article_priceunit']);
                }
                var _this = this;
                _this.price_net=0;
                $(this.items).each(function(i,item){
                    if(!isNaN(item.durable_article_num))
                    {
                        _this.price_net +=item.durable_article_num;
                    }
                });
            }
        }
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }


    list_durable3.$watch('items', function (newVal, oldVal) {
        //list_durable8._data.items = [];
        $(newVal).each(function(i,item)
        {
            var old = typeof list_durable8._data.items[i]!=='undefined'?list_durable8._data.items[i]:null;
            if(old!=null)
            {
                item.children = old.children;
                list_durable8._data.items[i] = item;
            }
            else
            {
                item.children = [];
                list_durable8._data.items.push(item);
            }
        });
    });
</script>
@endpush

@push('scripts')
<script type="text/javascript">
    @if(!isset($budgetPlanDurable->durable_article_list) || $budgetPlanDurable->durable_article_list!='รายการชุด')
    $('#display-list-only').attr('disabled',true);
    @endif
    function change_type3(o)
    {
        if($(o).val()!='' && $(o).val()=='รายการชุด')
        {
            $('#display-list-only').attr('disabled',false);
        }
        else {
            $('#display-list-only').attr('disabled',true);
            for(var i=list_durable3._data.items.length;i>0;i--)
            {
                list_durable8.items.splice(i,1);
                list_durable3.items.splice(i,1);
            }
        }
    }
</script>
@endpush