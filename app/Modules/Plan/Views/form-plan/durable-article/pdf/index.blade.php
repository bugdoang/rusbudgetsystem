<link href="/assets/css/font-awesome.min.css" rel="stylesheet">

<style type="text/css">
	.table{width:100%;}
    .fa{font-size:12px;}
    p{margin: 0;}
	.table tr th,.table tr td{border:1px solid #000;padding:2px;}
    .headpdf {font-size: 16px; font-weight: bold;}
    .texttablepdf {font-size: 24px;}
    .textpdf {font-size: 16px;}
    .tab_article {padding-left: 15px;}
</style>
<p style="text-align:right; font-size: 14px;">
    แบบจัดทำคำเสนอของบประมาณ{{$budgetPlanDurable->income_name or ''}} 
    ประจำปีงบประมาณ พ.ศ. {{$budgetPlanDurable->fiscal_year+543}} 
</p>

<p class="headpdf" style="text-align: center;">รายละเอียดรายการครุภัณฑ์ งบประมาณ{{$budgetPlanDurable->income_name or ''}}</p>
<p class="headpdf" style="text-align: center;">คณะ/หน่วยงาน{{$budgetPlanDurable->faculty_name}} สาขา{{$budgetPlanDurable->branch_name}} ศูนย์{{$budgetPlanDurable->campus}}</p>
<p class="textpdf" style="text-align: left;">{{$budgetPlanDurable->plan_name or ''}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ผลผลิต{{$budgetPlanDurable->product_name or ''}}</p>
<p class="headpdf" style="text-align: left;">ลำดับที่ 1 ชื่อรายการครุภัณฑ์ {{$budgetPlanDurable->durable_article_name or ''}}</p>
<p style="text-align: left; font-size: 14px;">กรณีเป็น{{$budgetPlanDurable->durable_article_list or ''}}</p>
<table class="table" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th class="texttablepdf" rowspan="2" style="width:30px;">ที่</th>
        <th class="texttablepdf" rowspan="2" style="width:400px;">รายการ</th>
        <th class="texttablepdf" rowspan="2" style="width:70px;">หน่วยนับ</th>
        <th class="texttablepdf" rowspan="2" style="width:50px;">จำนวน</th>
        <th class="texttablepdf" rowspan="2" style="width:90px;">ราคา/หน่วย</th>
        <th class="texttablepdf" rowspan="2" style="width:90px;">จำนวนเงิน</th>
        <th class="texttablepdf" colspan="2" style="width:150px;">สถานะของครุภัณฑ์</th>
        <th class="texttablepdf" colspan="2" style="width:150px;">เหตุผลสรุป</th>
    </tr>
    <tr>
    	<th class="texttablepdf">ใช้การได้</th>
    	<th class="texttablepdf">ใช้การไม่ได้</th>
    	<th class="texttablepdf">ทดแทนของเดิม</td>
    	<th class="texttablepdf">เพิ่มประสิทธิภาพ</th>
    </tr>
    @foreach($budgetPlanDurable->plan_durable_article_items as $index=>$item)
    <tr>
        @if(($item->durable_article_id)==($item->durable_article_id))
        <td class="texttablepdf" style="text-align: center;">{{$index+1}}</td>
        <td class="texttablepdf" style="text-align: left;">{{$item->durable_article_item_name}}</td>
        <td class="texttablepdf" style="text-align: center;">{{$item->durable_article_unit}}</td>
        <td class="texttablepdf" style="text-align: center;">{{$item->durable_article_qty}}</span></td>
        <td class="texttablepdf" style="text-align: right;">{{number_format($item->durable_article_priceunit,2)}}</td>
        <td class="texttablepdf" style="text-align: right;">{{number_format($item->durable_article_num,2)}}</td>
        <td class="texttablepdf" style="text-align: center;">{!!$item->durable_article_status=='Y'?'<i class="fa">&#xf00c;</i>':''!!}</td>
        <td class="texttablepdf" style="text-align: center;">{!!$item->durable_article_status=='Y'?'':'<i class="fa">&#xf00c;</i>'!!}</td>
        <td class="texttablepdf" style="text-align: center;">{!!$item->durable_article_reason=='Y'?'<i class="fa">&#xf00c;</i>':''!!}</td>
        <td class="texttablepdf" style="text-align: center;">{!!$item->durable_article_reason=='Y'?'':'<i class="fa">&#xf00c;</i>'!!}</td>
        @endif
    </tr>
    @endforeach
    <tr>    
        <td class="texttablepdf" colspan="5" style="padding-left:50px!important;">รวม</td>
        <td class="texttablepdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_total,2)}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="texttablepdf" colspan="5" style="padding-left:50px!important;">Vat 7%</td>
        <td class="texttablepdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_vat,2)}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="texttablepdf" colspan="5" style="text-align: center;">รวมทั้งสิ้น</td>
        <td class="texttablepdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_net,2)}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>

<p class="headpdf" style="padding-left: 4px;">เหตุผลความจำเป็น</p>
<?php $detail = explode("\n",$budgetPlanDurable->reason);?>
@foreach($detail as $_detail)
<p class="textpdf" style="text-indent: 15px;">{{$_detail}}</p>
@endforeach

<table>
    <tr>
        <td class="headpdf">วัตถุประสงค์ในการเสนอขอรับการจัดสรร</td>
    </tr>
 @foreach($budgetPlanDurable->plan_objectives as $index=>$item)
 	<tr>
 		<td class="textpdf tab_article">{{$index+1}}. {{$item->objective_name or ''}}</td>
 	</tr>
@endforeach
 </table>

<table>
    <tr>
        <td class="headpdf">ความถี่ในการใช้งาน</td>
    </tr>
    <tr>
        <td class="textpdf tab_article">1. งานจัดการเรียนการสอนเฉลี่ย{{($budgetPlanDurable->durable_article_frequency[1] !=0)?$budgetPlanDurable->durable_article_frequency[1]:'.........' }}ชม./สป./ภาคการศึกษา</td>
    </tr>
    <tr>
        <td class="textpdf tab_article">2. งานวิจัย เฉลี่ย {{($budgetPlanDurable->durable_article_frequency[2] !=0)?$budgetPlanDurable->durable_article_frequency[2]:'.........' }} ชม./ปี</td>
    </tr>
    <tr>
        <td class="textpdf tab_article">3. งานบริหารวิชาการ เฉลี่ย {{($budgetPlanDurable->durable_article_frequency[3] !=0)?$budgetPlanDurable->durable_article_frequency[3]:'.........' }} ชม./ปี</td>
    </tr>
    <tr>
        <td class="textpdf tab_article">4. งานทำนุบำรุงศิลปวัฒนธรรม เฉลี่ย {{($budgetPlanDurable->durable_article_frequency[4] !=0)?$budgetPlanDurable->durable_article_frequency[4]:'.........' }} ชม./ปี</td>
    </tr>
    <tr>
        <td class="textpdf tab_article">5. การบริหารจัดการเฉลี่ย {{($budgetPlanDurable->durable_article_frequency[5] !=0)?$budgetPlanDurable->durable_article_frequency[5]:'.........' }} ชม./ปี</td>
    </tr>
</table>

<table>
    <tr>
        <td class="headpdf">สถานที่นำไปใช้งาน</td> 
        <td class="textpdf">{{$budgetPlanDurable->durable_article_place or ''}}</td>
    </tr>
</table>

<table>
    <tr>
        <td class="headpdf">หากไม่ได้รับการจัดสรรจะเกิดผลเสียหายอย่างไร</td>
    </tr>
 @foreach($budgetPlanDurable->plan_durable_articles_allocates as $index=>$item)
 	<tr>
 		<td class="textpdf tab_article">{{$index+1}}. {{$item->durable_articles_allocate_name or ''}}</td>
 	</tr>
@endforeach
</table>

<table>
    <tr>
        <td class="headpdf">แผนจัดซื้อ</td>
        @if($budgetPlanDurable->durable_article_purchase=='ก่อหนี้ผูกพัน')
        <td class="textpdf">
            {{$budgetPlanDurable->durable_article_purchase or ''}} 
            ภายในเดือน {{\App\Helepers\DateFormat::getMonthName($budgetPlanDurable->durable_article_purchase_month)}}
             พ.ศ. {{$budgetPlanDurable->durable_article_purchase_year+543}} 
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         </td>
        @else
        <td class="textpdf">
            {{$budgetPlanDurable->durable_article_purchase or ''}} 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td></td>
        @endif
        <td class="headpdf">ส่งมอบพร้อมติดตั้ง</td>
        <td class="textpdf">
            ภายในเดือน {{\App\Helepers\DateFormat::getMonthName($budgetPlanDurable->durable_article_install_month)}}
             พ.ศ. {{$budgetPlanDurable->durable_article_install_year+543}}
         </td>
    </tr>
    <tr>
        <td class="headpdf">แผนการใช้จ่าย</td>
        <td class="textpdf">
            เบิกจ่ายภายในเดือน {{\App\Helepers\DateFormat::getMonthName($budgetPlanDurable->durable_article_spend_month)}}
             พ.ศ. {{$budgetPlanDurable->durable_article_spend_year+543}}
        </td>
        <td></td>
        <td></td>
    </tr>
</table>

<p class="headpdf" style="page-break-before:always; padding-left: 4px;">มาตรฐานและรายละเอียดคุณลักษณะ (ให้จัดทำรายละเอียดที่สามารถดำเนินการจัดซื้อจัดจ้างได้)</p>
<table style="width: 100%;">
 @foreach($budgetPlanDurable->plan_durable_article_items as $index=>$item)
 	<tr>
 		<td class="textpdf tab_article" style="text-align: right; width:15px; vertical-align: top;">{{$index+1}}.</td>
        <td class="textpdf" style="text-align: left; vertical-align: top;">{{$item->durable_article_item_name or ''}}</td>
        <td class="textpdf" style="text-align: left; vertical-align: top; width:50px;">{{$item->durable_article_unit or ''}}</td>
        <td class="textpdf" style="text-align: left; vertical-align: top; width:50px;">{{$item->durable_article_qty or ''}}</td>
        <td class="textpdf" style="text-align: right; vertical-align: top; width:50px;">{{number_format($item->durable_article_priceunit,2)}}</td>
        <td class="textpdf" style="text-align: right; vertical-align: top; width:50px;">{{number_format($item->durable_article_num,2)}}</td>
 	</tr>
    @if(!empty($item->durable_article_item_detail))
        <?php $children = json_decode($item->durable_article_item_detail,TRUE);?>
        @if(!empty($children))
        <tr>
            <td colspan="6" style="padding-left:25px;">
                <table style="margin:0px;">
                @foreach($children as $ch_index=>$ch)
                    <tr>
                        <td class="textpdf" style="width:15px;text-align:left; vertical-align: top;">{{($index+1).'.'.($ch_index+1)}}</td>
                        <td class="textpdf" style="text-align:left; vertical-align: top;">{{$ch['durable_article_allocate']}}</td>
                    </tr>
                @endforeach
                </table>
            </td>
        </tr>
        @endif
    @endif
@endforeach
</table>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="textpdf" style="padding-left: 28px;">รวม</td>
        <td class="textpdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_total,2)}}</td>
    </tr>
    <tr>
        <td class="textpdf" style="padding-left: 28px;">Vat 7%</td>
        <td class="textpdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_vat,2)}}</td>
    </tr>
    <tr>
        <td class="textpdf" style="padding-left: 28px;">รวมสุทธิ</td>
        <td class="textpdf" style="text-align: right;">{{number_format($budgetPlanDurable->durable_article_budget_net,2)}}</td>
    </tr>
</table>


 <p class="headpdf" style="padding-left: 4px;">คณะกรรมการ/ผู้จัดทำมาตรฐานและรายละเอียดคุณลักษณะ</p>
 <table style="width: 100%;">
 @foreach($budgetPlanDurable->plan_committees as $index=>$item)
    @if(($item->personnel_id)==($item->personnel_id))
 	<tr>
 		<td class="textpdf tab_article" style="text-align: left; vertical-align: top; width: 15px;">{{$index+1}}.</td>
        <td class="textpdf" style="text-align: left; vertical-align: top;">ชื่อ-ชื่อสกุล/ตำแหน่ง/สังกัด/เบอร์โทรศัพท์</td>
        @if($item->position_abbreviation=='อ.')
 		<td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">{{$item->title_name}}{{$item->first_name or ''}}</td>
        @else
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">{{$item->position_abbreviation}}{{$item->first_name or ''}}</td>
 		@endif
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">สาขาวิชา{{$item->branch_name}} ศูนย์{{$item->campus_name}}</td>
 	</tr>
    <tr>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;"></td>
        <td class="textpdf" style="text-align: left; padding-left: 10px; vertical-align: top;">โทรศัพท์ {{$item->mobile}}</td>
    </tr>
    @endif
@endforeach
 </table>




    
 	