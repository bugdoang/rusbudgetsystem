@extends('masterlayout')
@section('title','รายละเอียดประกอบค่าครุภัณฑ์')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดประกอบค่าครุภัณฑ์','link'=>'/durable-article-plan']]) !!}
{{csrf_field()}}
<div class="site-breadcrumb">
    <a class="btn btn-default" href="/durable-article-plan/form"><i class="fa fa-plus" aria-hidden="true"></i> แบบฟอร์มรายละเอียดประกอบค่าครุภัณฑ์</a>
</div>
<!-- Page Content -->
<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">
        <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา</h2>
        <div class="form-planbudget shadow">
            <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="get" action="/durable-article-plan" >
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ชื่อรายการครุภัณฑ์ : </label>
                    <div class="col-md-12">
                        <input id="durable_article_name" class="form-control input-md" type="text" name="durable_article_name" value="{{$params['durable_article_name'] or ''}}" placeholder="ชื่อรายการครุภัณฑ์">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">สถานะรายการครุภัณฑ์ : </label>
                    <div class="col-md-12">
                        <select class="" name="status" id="status">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($statuses))
                                @foreach($statuses as $status)
                            <option {{($status==$params['status'])?' selected ':''}} value="{{$status}}">{{$status}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">รายชื่อผู้จัดแผนเสนอของบประมาณ : </label>
                    <div class="col-md-12">
                        <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{$params['first_name'] or ''}}" placeholder="รายชื่อผู้จัดแผนเสนอของบประมาณ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ช่วงวันที่เสนอขอ เริ่มต้น - สิ้นสุด : </label>
                    <div class="col-md-6">
                        <input id="start_date" class="form-control input-md datepicker" type="text" name="start_date" value="{{$params['start_date'] or ''}}" placeholder="ช่วงวันที่เริ่มต้น">
                    </div>
                    <div class="col-md-6">
                        <input id="end_date" class="form-control input-md datepicker" type="text" name="end_date" value="{{$params['end_date'] or ''}}" placeholder="ช่วงวันที่สิ้นสุด">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            ค้นหา
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="height:10px;clear:both;"></div>
    </div>
    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการรายละเอียดประกอบค่าครุภัณฑ์</h2>
        <div class="form-planbudget shadow">

            @if(count($budgetPlan_list)>0)
                <table class="table table-bordered">
                    <tbody align="center">
                    <tr style="background-color: #F5F5F5;">
                        <th class="text-center text-middle" rowspan="2" style="width: 30px;" >#</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 400px;">ชื่อรายการครุภัณฑ์</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 70px;">วันที่เสนอขอ</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 130px;">สถานะ</th>
                        <th class="text-center" colspan="2" style="width: 80px;">จำนวนเงิน</th>
                        <th class="text-center text-middle" rowspan="2"  style="width: 80px;">#</th>
                    </tr>
                    <tr style="background-color: #F5F5F5;">
                        <th class="text-center">เสนอขอ</th>
                        <th class="text-center">อนุมัติ</th>
                    </tr>
                    @foreach($budgetPlan_list as $index=>$item)
                    <tr>
                        <td>
                            <span name="durable_article_id" id="durable_article_id">{{$budgetPlan_list->firstItem()+$index}}</span>
                        </td>
                        <td class="text-left">
                            <span name="durable_article_name" id="durable_article_name">
                                {{$item->durable_article_name}} [ {{$item->position_abbreviation}} {{$item->first_name}} ]
                            </span>
                        </td>
                        <td>
                            <span name="durable_article_date" id="durable_article_date">{{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}</span>
                        </td>
                        <td>
                            @if($item->status=='รอการตรวจสอบ')
                                <span style="color: #f07c4a;" name="status" id="status"><i class="fa fa-spinner" aria-hidden="true"></i>
                                    {{$item->status}}
                                </span>
                            @elseif($item->status=='อนุมัติแล้ว')
                                <span style="color: #1CB94E;" name="status" id="status"><i class="fa fa-check" aria-hidden="true"></i>
                                    {{$item->status}}
                                </span>
                            @else
                                <span style="color: #b00;" name="status" id="status"><i class="fa fa-times" aria-hidden="true"></i>
                                    {{$item->status}}
                                </span>
                            @endif
                        </td>

                        <td class="text-right" style="width: 40px;">
                            <span name="durable_article_budget_net" id="durable_article_budget_net">{{number_format($item->durable_article_budget_net,2)}}</span>
                        </td>
                        <td class="text-right" style="width: 40px;">
                            <span name="durable_article_num_after" id="durable_article_num_after">{{number_format($item->money_approve,2)}}</span>
                        </td>
                        <td class="text-center">
                        @if($item->status!='รอการตรวจสอบ')
                            <a href="/durable-article-plan/browse/{{$item->budget_plan_id}}" title="เรียกดู">
                                <i class="fa fa-search" style="color:#3d3d3d;"></i>
                            </a>
                            <a href="/durable-article-plan/{{$item->budget_plan_id}}/pdf" title="ไฟล์ PDF">
                                <i class="fa fa-file-pdf-o" style="color:#3d3d3d;"></i>
                            </a>
                            <a href="/download?path={{$item->upload_file}}&name={{$item->upload_file_name}}" title="ไฟล์แนบเอกสาร">
                                <i class="fa fa-file-o" style="color:#3d3d3d;"></i>
                            </a>
                        @else
                            @if(\Auth::user()->group_id==3)
                                <a href="/durable-article-plan/browse/{{$item->budget_plan_id}}" title="เรียกดู">
                                    <i class="fa fa-search" style="color:#3d3d3d;"></i>
                                </a>
                                <a href="/durable-article-plan/{{$item->budget_plan_id}}/pdf" title="ไฟล์ PDF">
                                    <i class="fa fa-file-pdf-o" style="color:#3d3d3d;"></i>
                                </a>
                                <a href="/durable-article-plan/{{$item->budget_plan_id}}/" title="แก้ไข">
                                    <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                                </a>
                                @if($item->personnel_id==\Auth::id())
                                    <a class="delete-content" href="/durable-article-plan/{{$item->budget_plan_id}}" title="ลบ">
                                        <i class="fa fa-times" style="color:#b00;"></i>
                                    </a>
                                @endif
                            @else
                                <a href="/durable-article-plan/browse/{{$item->budget_plan_id}}" title="เรียกดู">
                                    <i class="fa fa-search" style="color:#3d3d3d;"></i>
                                </a>
                                <a href="/durable-article-plan/{{$item->budget_plan_id}}/pdf" title="ไฟล์ PDF">
                                    <i class="fa fa-file-pdf-o" style="color:#3d3d3d;"></i>
                                </a>
                                <a href="/durable-article-plan/{{$item->budget_plan_id}}/" title="แก้ไข">
                                    <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                                </a>
                                <a class="delete-content" href="/durable-article-plan/{{$item->budget_plan_id}}" title="ลบ">
                                    <i class="fa fa-times" style="color:#b00;"></i>
                                </a>
                            @endif
                        @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{$budgetPlan_list->render()}}
                </div>
            @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลรายการครุภัณฑ์คะ ::</span>
                </div>
            @endif
        </div>
    </div>
</div>

@stop

