@extends('masterlayout')
@section('title','รายละเอียดประกอบค่าที่ดินและสิ่งก่อสร้าง')

@section('content')
{!! \App\Services\UI::breadcrumb([['label'=>'รายละเอียดประกอบค่าที่ดินและสิ่งก่อสร้าง','link'=>'/build-plan']]) !!}
{{csrf_field()}}
<div class="site-breadcrumb">
    <a class="btn btn-default" href="/build-plan/form"><i class="fa fa-plus" aria-hidden="true"></i> แบบฟอร์มรายละเอียดประกอบค่าที่ดินและสิ่งก่อสร้าง</a>
</div>
<!-- Page Content -->
<div class="row">
    <!--start left-->
    <div class="col-lg-3 col-md-3" style="padding-left: 0; padding-right: 0;">
        <h2 class="header-text myfont"><i class="fa fa-search" aria-hidden="true"></i> รายการค้นหา</h2>
        <div class="form-planbudget shadow">
            <form id="form-plan" name="form-plan" class="form-horizontal no-ajax" role="form" method="get" action="/durable-article-plan" >
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ชื่อรายการที่ดินและสิ่งก่อสร้าง: </label>
                    <div class="col-md-12">
                        <input id="durable_article_name" class="form-control input-md" type="text" name="durable_article_name" value="{{-- {{$params['durable_article_name'] or ''}} --}}" placeholder="ชื่อรายการที่ดินและสิ่งก่อสร้าง">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ช่วงวันที่เสนอขอเริ่มต้น : </label>
                    <div class="col-md-12">
                        <input id="start_date" class="form-control input-md datepicker" type="text" name="start_date" value="{{-- {{$params['start_date'] or ''}} --}}" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">ช่วงวันที่เสนอขอสิ้นสุด : </label>
                    <div class="col-md-12">
                        <input id="end_date" class="form-control input-md datepicker" type="text" name="end_date" value="{{-- {{$params['end_date'] or ''}} --}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">สถานะรายการที่ดินและสิ่งก่อสร้าง : </label>
                    <div class="col-md-12">
                        <select class="" name="status" id="status">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($statuses))
                                @foreach($statuses as $status)
                            <option value="{{$status}}">{{$status}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="textinput">รายชื่อผู้จัดแผนเสนอของบประมาณ : </label>
                    <div class="col-md-12">
                        <input id="first_name" class="form-control input-md" type="text" name="first_name" value="{{-- {{$params['first_name'] or ''}} --}}" placeholder="รายชื่อผู้จัดแผนเสนอของบประมาณ">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            ค้นหา
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="height:10px;clear:both;"></div>
    </div>
    <!--start right-->
    <div style="padding-right: 0;" class="col-lg-9 col-md-9">
        <h2 class="header-text myfont"><i class="fa fa-th-list" aria-hidden="true"></i> รายการรายละเอียดประกอบค่าที่ดินและสิ่งก่อสร้าง</h2>
        <div class="form-planbudget shadow">

            {{-- @if(count($budgetPlan_list)>0) --}}
                <table class="table table-bordered">
                    <tbody align="center">
                    <tr>
                        <th class="text-center text-middle" rowspan="2" style="width: 30px;" >#</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 400px;">ชื่อรายการที่ดินและสิ่งก่อสร้าง</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 70px;">วันที่เสนอขอ</th>
                        <th class="text-center text-middle" rowspan="2" style="width: 110px;">สถานะ</th>
                        <th class="text-center" colspan="2" style="width: 80px;">จำนวนเงิน</th>
                        <th class="text-center text-middle" rowspan="2"  style="width: 80px;">#</th>
                    </tr>
                    <tr>
                        <th class="text-center">เสนอขอ</th>
                        <th class="text-center">อนุมัติ</th>
                    </tr>
                    {{-- @foreach($budgetPlan_list as $index=>$item) --}}
                    <tr>
                        <td>
                            <span name="durable_article_id" id="durable_article_id">{{-- {{$index+1}} --}}</span>
                        </td>
                        <td class="text-left">
                            <span name="durable_article_name" id="durable_article_name">
                                {{-- {{$item->durable_article_name}} [ {{$item->position_abbreviation}} {{$item->first_name}} ] --}}
                            </span>
                        </td>
                        <td>
                            <span name="durable_article_date" id="durable_article_date">{{-- {{\App\Helepers\DateFormat::Date_Clock($item->created_at)}} --}}</span>
                        </td>
                        <td>
                            <span name="durable_article_status" id="durable_article_status">{{-- {{$item->status}} --}}</span>
                        </td>

                        <td class="text-right" style="width: 40px;">
                            <span name="durable_article_budget_net" id="durable_article_budget_net">{{-- {{number_format($item->durable_article_budget_net,2)}} --}}</span>
                        </td>
                        <td class="text-right" style="width: 40px;">
                            <span name="durable_article_num_after" id="durable_article_num_after">{{-- {{number_format($item->amount,2)}} --}}</span>
                        </td>
                        <td class="text-center text-middle">
                            <a href="">
                                <i class="fa fa-search" style="color:#3d3d3d;"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-file-pdf-o" style="color:#3d3d3d;"></i>
                            </a>
                            <a href="">
                                <i class="fa fa-pencil-square-o" style="color:#3d3d3d;"></i>
                            </a>
                            <a class="delete-content" href="">
                                <i class="fa fa-times" style="color:#b00;"></i>
                            </a>
                        </td>
                    </tr>
                    {{-- @endforeach --}}
                    </tbody>
                </table>
            {{-- @else
                <div class="text-center text-middle">
                    <span>:: ไม่พบข้อมูลรายการที่ดินและสิ่งก่อสร้างคะ ::</span>
                </div>
            @endif --}}
        </div>
    </div>
</div>

@stop

