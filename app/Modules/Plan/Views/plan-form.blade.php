<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title myfont" style="font-size: 22px; font-weight:bold;">แบบฟอร์มรายละเอียดประกอบโครงการ</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="textinput">ชื่อโครงการ :</label>
                                <div class="col-md-9">
                                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="ชื่อโครงการ" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="textinput">ชื่อโครงการ :</label>
                                <div class="col-md-9">
                                    <input id="first_name" class="form-control input-md" type="text" name="first_name" placeholder="ชื่อโครงการ" value="">
                                </div>
                            </div>



                            <div class="form-group myfont" style="font-size: 18px;">
                                <div class="row">
                                    <label class="col-md-3 control-label" for="textinput">รายการแบบฟอร์ม : </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="title_name" id="title_name">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="">#</option>
                                            <option value="">#</option>
                                            <option value="">#</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group myfont" style="font-size: 18px;" >
                                <div class="row">
                                <label class="col-md-3 control-label" for="textinput">ชื่อแบบฟอร์ม : </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="title_name" id="title_name">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="">#</option>
                                            <option value="">#</option>
                                            <option value="">#</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="pull-right" style="width:70%; display:block; text-align:center; margin: auto;">
                                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                            บันทึก
                                        </button>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="pull-left" style="width:70%; display:block; text-align:center; margin: auto; ">
                                        <button id="login-btn" class="form-control btn btn-login btn-lg myfont" type="submit">
                                            ยกเลิก
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>