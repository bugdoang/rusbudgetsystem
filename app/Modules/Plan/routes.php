<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/15/2016
 * Time: 11:46 AM
 */

Route::group(['middleware'=>['auth']],function() {

    // **Durable Article**
    // List Page
    Route::get('durable-article-plan', '\App\Modules\Plan\Controllers\DurableArticlePlan@index');

    // Create Form
    Route::get('durable-article-plan/form', '\App\Modules\Plan\Controllers\DurableArticlePlan@create');
    Route::post('durable-article-plan/form', '\App\Modules\Plan\Controllers\DurableArticlePlan@save');

    // PDF
    Route::get('durable-article-plan/{id}/pdf', '\App\Modules\Plan\Controllers\DurableArticlePlanPdf@index');

    // Update Form
    Route::get('durable-article-plan/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@edit');
    Route::put('durable-article-plan/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@update');

    // Delete Form
    Route::delete('durable-article-plan/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@delete');

    // Browse Form
    Route::get('durable-article-plan/browse/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@browse');

    // Modal Form
    Route::get('durable-article-plan/modal/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@satatus');
    Route::post('durable-article-plan/modal/{id}', '\App\Modules\Plan\Controllers\DurableArticlePlan@saveSatatus');
    Route::delete('durable-article-plan/modal/{id}/reject', '\App\Modules\Plan\Controllers\DurableArticlePlan@rejectStatus');

    // **Project**
    // List Page
    Route::get('project-plan', '\App\Modules\Plan\Controllers\ProjectPlan@index');

    // Create Form
    Route::get('project-plan/form', '\App\Modules\Plan\Controllers\ProjectPlan@create');
    Route::post('project-plan/form', '\App\Modules\Plan\Controllers\ProjectPlan@save');

    // Update Form
    Route::get('project-plan/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@edit');
    Route::put('project-plan/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@update');

    // Browse Form
    Route::get('project-plan/browse/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@browse');

    // Delete Form
    Route::delete('project-plan/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@delete');

    // PDF
    Route::get('project-plan/{id}/pdf', '\App\Modules\Plan\Controllers\ProjectPlanPdf@index');

    // Modal Form
    Route::get('project-plan/modal/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@satatus');
    Route::post('project-plan/modal/{id}', '\App\Modules\Plan\Controllers\ProjectPlan@saveSatatus');
    Route::delete('project-plan/modal/{id}/reject', '\App\Modules\Plan\Controllers\ProjectPlan@rejectStatus');

    // **build**
    // List Page
    Route::get('build-plan', '\App\Modules\Plan\Controllers\BuildPlan@index');

    // Create Form
    Route::get('build-plan/form', '\App\Modules\Plan\Controllers\BuildPlan@create');

});