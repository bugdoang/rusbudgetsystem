<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/28/16
 * Time: 9:48 AM
 */

namespace App\Modules\Plan\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Plan\Models\PlanProjectModel;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Modules\Plan\Services\BudgetPlan;
use App\Modules\Plan\Services\Committee;
use App\Modules\Plan\Services\Objective;
use App\Modules\Plan\Services\Project;



class ProjectPlan extends Controller
{
    public function index(Request $request)
    {
        $params=[];
        $params['project_name']= $request->get('project_name');
        $params['start_date'] = $request->get('start_date');
        $params['end_date'] = $request->get('end_date');
        $params['status'] = $request->get('status');
        $params['first_name'] = $request->get('first_name');

        $personnel_id = 0;
        if(Auth::user()->group_id==4)
        {
            $personnel_id = Auth::id();
        }
        $statuses = config('myconfig.status');
        $proJect_list = PlanProjectModel::getBudgetPlan($personnel_id,$params);
        return view('plan::list-plan/project/project-plan',[
            'proJect_list'=>$proJect_list['items'],
            'result'=>$proJect_list['result'],
            'params'=>$params,
            'statuses'=>$statuses
        ]);

    }

    public function create()
    {
        $method='POST';
        $action='/project-plan/form';
        return view('plan::form-plan/project/project-form',[
            'budgetPlanProject'=>null,
            'method'=>$method,
            'action'=>$action
        ]);
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        $result_budget_plan     = BudgetPlan::save($request,'โครงการ');
        if(gettype($result_budget_plan)=='string')
        {
            DB::rollBack();
            return response([$result_budget_plan],422);
        }
        $budget_plan_id = $result_budget_plan['budget_plan_id'];

        $result_project  = Project::save($request,$budget_plan_id);

        if(gettype($result_project)=='string')
        {
            DB::rollBack();
            return response([$result_project],422);
        }

        $result_committees = Committee::save($request,$budget_plan_id);
        if(gettype($result_committees)=='string')
        {
            DB::rollBack();
            return response([$result_committees],422);
        }

        $result_objective = Objective::save($request,$budget_plan_id);
        if(gettype($result_objective)=='string')
        {
            DB::rollBack();
            return response([$result_objective],422);
        }

        DB::commit();

        return response(['callback'=>'save_success','redirect_url'=>'/project-plan'],200);
    }

    public function edit($budget_plan_id)
    {
        $method='PUT';
        $action='/project-plan/'.$budget_plan_id;
        $budgetPlanProject = PlanProjectModel::budgetPlanProjectId($budget_plan_id);
        return view('plan::form-plan/project/project-form',[
            'budgetPlanProject'=>$budgetPlanProject,
            'method'=>$method,
            'action'=>$action,
            'budget_plan_id'=>$budget_plan_id,
            'my_group_id'=>Auth::user()->group_id
        ]);
    }

    public function update($id,Request $request)
    {
        DB::beginTransaction();
        $result_budget_plan  = BudgetPlan::update($id,$request);
        if(gettype($result_budget_plan)=='string')
        {
            DB::rollBack();
            return response([$result_budget_plan],422);
        }
        $budget_plan_id = $id;

        $result_durable  = Project::update($request,$budget_plan_id);

        if(gettype($result_durable)=='string')
        {
            DB::rollBack();
            return response([$result_durable],422);
        }


        $result_committees = Committee::update($request,$budget_plan_id);
        if(gettype($result_committees)=='string')
        {
            DB::rollBack();
            return response([$result_committees],422);
        }


        $result_objective = Objective::update($request,$budget_plan_id);
        if(gettype($result_objective)=='string')
        {
            DB::rollBack();
            return response([$result_objective],422);
        }

        DB::commit();

        return response(['สำเร็จเรียบร้อยแล้ว'],200);
    }

    public function browse($budget_plan_id)
    {
        $method='GET';
        $action='/project-plan/'.$budget_plan_id;
        $budgetPlanProject = PlanProjectModel::budgetPlanProjectId($budget_plan_id,'view');
        return view('plan::form-plan/Project/project-form-browse',[
            'budgetPlanProject'=>$budgetPlanProject,
            'method'=>$method,
            'action'=>$action,
            'budget_plan_id'=>$budget_plan_id,
            'my_group_id'=>Auth::user()->group_id
            
        ]);
    }

    public function delete($budget_plan_id)
    {
        DB::beginTransaction();
        BudgetPlan::delete($budget_plan_id);
        Project::delete($budget_plan_id);
        Committee::delete($budget_plan_id);
        Objective::delete($budget_plan_id);
        DB::commit();
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

    public function satatus($budget_plan_id)
    {
        $budgetPlanProject = PlanProjectModel::budgetPlanProjectId($budget_plan_id);
        $html=view('plan::form-plan/project/project-modal',compact('budget_plan_id'),[
            'budgetPlanProject'=>$budgetPlanProject,
            ]);
        return ['title'=>'ปรับสถานะโครงการ','body'=>$html->render()];
    }

    public function saveSatatus($budget_plan_id,Request $request)
    {
        $upload_file = $request->get('upload_file');
        $upload_file_name = $request->get('upload_file_name');
        $project_money_approve = $request->get('project_money_approve');
        $budgetPlanProject = PlanProjectModel::budgetPlanProjectId($budget_plan_id);

        if(!is_numeric($project_money_approve))
        {
            return response(['กรุณาป้อนจำนวนเงินที่ได้รับอนุมัติด้วยคะ'],422);
        }
        if(($budgetPlanProject->total_price[$budgetPlanProject->project_id])>=($project_money_approve))
        {
        PlanProjectModel::update_budget_plan($budget_plan_id,'อนุมัติแล้ว');
        PlanProjectModel::update_project($budget_plan_id,$project_money_approve);
        PlanProjectModel::update_upload_file($budget_plan_id,$upload_file,$upload_file_name);
        return response(['callback'=>'save_success','redirect_url'=>'/project-plan'],200);
        }
        else
        {
            return response(['ขออภัยคะ จำนวนเงินที่ได้รับอนุมัติมีค่ามากกว่าจำนวนเงินที่เสนอขอคะ'],422);
        }
    }

    public function rejectStatus($budget_plan_id)
    {
        PlanProjectModel::update_budget_plan($budget_plan_id,'ไม่ได้รับการอนุมัติ');
        return response(['callback'=>'save_success','redirect_url'=>'/project-plan'],200);

    }
}