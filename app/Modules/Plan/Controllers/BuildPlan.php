<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/28/16
 * Time: 9:48 AM
 */

namespace App\Modules\Plan\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Plan\Models\PlanBuildModel;
use Auth;
use DB;
use Illuminate\Http\Request;


class BuildPlan extends Controller
{
    public function index()
    {
        // $params=[];
        // $params['project_name']= $request->get('project_name');
        // $params['start_date'] = $request->get('start_date');
        // $params['end_date'] = $request->get('end_date');
        // $params['status'] = $request->get('status');
        // $params['first_name'] = $request->get('first_name');

        $personnel_id = 0;
        if(Auth::user()->group_id==4)
        {
            $personnel_id = Auth::id();
        }
        // $statuses = config('myconfig.status');
        // $budgetPlan_list = PlanProjectModel::getBudgetPlan($personnel_id,$params);
        return view('plan::list-plan/build/build-plan',[
            // 'budgetPlan_list'=>$budgetPlan_list,
            // 'params'=>$params,
            // 'statuses'=>$statuses
        ]);
    }

    public function create()
    {
        // $method='POST';
        // $action='/project-plan/form';
        return view('plan::form-plan/build/build-form',[
            // 'budgetPlanDurable'=>null,
            // 'method'=>$method,
            // 'action'=>$action
        ]);
    }

    
}