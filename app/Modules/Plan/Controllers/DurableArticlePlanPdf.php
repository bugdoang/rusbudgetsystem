<?php

namespace App\Modules\Plan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Plan\Models\PlanInformationModel;
use App\Modules\Plan\Services\BudgetPlan;
use App\Modules\Plan\Services\Committee;
use App\Modules\Plan\Services\DurableArticle;
use App\Modules\Plan\Services\Objective;
use App\Services\MahaPDF;
use Auth;
use DB;
use Illuminate\Http\Request;

class DurableArticlePlanPdf extends Controller
{
	public function index($budget_plan_id)
    {
        $budgetPlanDurable = PlanInformationModel::budgetPlanDurableId($budget_plan_id);
        if(!empty($budgetPlanDurable))
        {
	        $budgetPlanDurable->durable_article_frequency=json_decode($budgetPlanDurable->durable_article_frequency,true);
	        $html = view('plan::form-plan/durable-article/pdf/index',[
	            'budgetPlanDurable'=>$budgetPlanDurable
	        ]);
	        MahaPDF::html($html,'preview');
	    }
	    else
	    {
	    	echo 'ไม่พบข้อมูลคะ';exit;
	    }
    }
}

