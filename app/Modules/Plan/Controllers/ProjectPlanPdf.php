<?php

namespace App\Modules\Plan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Plan\Models\PlanProjectModel;
use App\Modules\Plan\Services\BudgetPlan;
use App\Modules\Plan\Services\Committee;
use App\Modules\Plan\Services\Project;
use App\Modules\Plan\Services\Objective;
use App\Services\MahaPDF;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Helepers\DateFormat;


class ProjectPlanPdf extends Controller
{
	public function index($budget_plan_id)
    {
        $budgetPlanProject = PlanProjectModel::budgetPlanProjectId($budget_plan_id);
        if(!empty($budgetPlanProject))
        {
	        $html = view('plan::form-plan/project/pdf/index',[
	            'budgetPlanProject'=>$budgetPlanProject
	        ]);
	        // return $html->render();
	        MahaPDF::html($html,'preview');
	    }
	    else
	    {
	    	echo 'ไม่พบข้อมูลคะ';exit;
	    }
    }
}

