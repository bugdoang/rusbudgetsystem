<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/21/16
 * Time: 4:08 PM
 */

namespace App\Modules\Plan\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Plan\Models\PlanInformationModel;
use App\Modules\Plan\Services\BudgetPlan;
use App\Modules\Plan\Services\Committee;
use App\Modules\Plan\Services\DurableArticle;
use App\Modules\Plan\Services\Objective;
use Auth;
use DB;
use Illuminate\Http\Request;

class DurableArticlePlan extends Controller
{
    public function index(Request $request)
    {
        $params=[];
        $params['durable_article_name']= $request->get('durable_article_name');
        $params['start_date'] = $request->get('start_date');
        $params['end_date'] = $request->get('end_date');
        $params['status'] = $request->get('status');
        $params['first_name'] = $request->get('first_name');

        $personnel_id = 0;
        if(Auth::user()->group_id==4)
        {
            $personnel_id = Auth::id();
        }
        // การตรวจสอบวันที่
        // if(empty($params['start_date']))
        // {
        //     $params['start_date']=date('Y-m-d');
        // }
        // if(empty($params['end_date']))
        // {
        //     $params['end_date']=date('Y-m-d');
        // }

        // $start_date = strtotime($params['start_date']);
        // $end_date = strtotime($params['end_date']);

        // if($start_date > $end_date)
        // {
        //     return response(['ขออภัยคะ วันที่เริ่มต้นต้อง'],422);
        // }
        $statuses = config('myconfig.status');
        $incomes=PlanInformationModel::getIncome();
        $budgetPlan_list = PlanInformationModel::getBudgetPlan($personnel_id,$params);
        // dd($budgetPlan_list);
        return view('plan::list-plan/durable-article/durable-article-plan',[
            'budgetPlan_list'=>$budgetPlan_list,
            'params'=>$params,
            'statuses'=>$statuses,
            'incomes'=>$incomes
        ]);
    }

    public function create()
    {
        $method='POST';
        $action='/durable-article-plan/form';
        return view('plan::form-plan/durable-article/durable-article-form',[
            'budgetPlanDurable'=>null,
            'method'=>$method,
            'action'=>$action
        ]);
    }

    public function edit($budget_plan_id)
    {
        $method='PUT';
        $action='/durable-article-plan/'.$budget_plan_id;
        $budgetPlanDurable = PlanInformationModel::budgetPlanDurableId($budget_plan_id);
        return view('plan::form-plan/durable-article/durable-article-form',[
            'budgetPlanDurable'=>$budgetPlanDurable,
            'method'=>$method,
            'action'=>$action,
            'budget_plan_id'=>$budget_plan_id,
            'my_group_id'=>Auth::user()->group_id
        ]);
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        $result_budget_plan     = BudgetPlan::save($request,'ครุภัณฑ์');
        if(gettype($result_budget_plan)=='string')
        {
            DB::rollBack();
            return response([$result_budget_plan],422);
        }
        $budget_plan_id = $result_budget_plan['budget_plan_id'];

        $result_durable  = DurableArticle::save($request,$budget_plan_id);

        if(gettype($result_durable)=='string')
        {
            DB::rollBack();
            return response([$result_durable],422);
        }


        $result_committees = Committee::save($request,$budget_plan_id);
        if(gettype($result_committees)=='string')
        {
            DB::rollBack();
            return response([$result_committees],422);
        }


        $result_objective = Objective::save($request,$budget_plan_id);
        if(gettype($result_objective)=='string')
        {
            DB::rollBack();
            return response([$result_objective],422);
        }

        DB::commit();

        return response(['callback'=>'save_success','redirect_url'=>'/durable-article-plan'],200);
    }

    public function update($id,Request $request)
    {
        DB::beginTransaction();
        $result_budget_plan  = BudgetPlan::update($id,$request);
        if(gettype($result_budget_plan)=='string')
        {
            DB::rollBack();
            return response([$result_budget_plan],422);
        }
        $budget_plan_id = $id;

        $result_durable  = DurableArticle::update($request,$budget_plan_id);

        if(gettype($result_durable)=='string')
        {
            DB::rollBack();
            return response([$result_durable],422);
        }


        $result_committees = Committee::update($request,$budget_plan_id);
        if(gettype($result_committees)=='string')
        {
            DB::rollBack();
            return response([$result_committees],422);
        }


        $result_objective = Objective::update($request,$budget_plan_id);
        if(gettype($result_objective)=='string')
        {
            DB::rollBack();
            return response([$result_objective],422);
        }

        DB::commit();

        return response(['สำเร็จเรียบร้อยแล้ว'],200);
    }

    public function delete($budget_plan_id)
    {
        DB::beginTransaction();
        BudgetPlan::delete($budget_plan_id);
        DurableArticle::delete($budget_plan_id);
        Committee::delete($budget_plan_id);
        Objective::delete($budget_plan_id);
        DB::commit();
        return response(['ลบข้อมูลเรียบร้อยแล้วคะ'],200);
    }

    public function browse($budget_plan_id)
    {
        $method='GET';
        $action='/durable-article-plan/'.$budget_plan_id;
        $budgetPlanDurable = PlanInformationModel::budgetPlanDurableId($budget_plan_id,'view');
        return view('plan::form-plan/durable-article/durable-article-form-browse',[
            'budgetPlanDurable'=>$budgetPlanDurable,
            'method'=>$method,
            'action'=>$action,
            'budget_plan_id'=>$budget_plan_id,
            'my_group_id'=>Auth::user()->group_id
            
        ]);
    }

    public function satatus($budget_plan_id)
    {
        $budgetPlanDurable = PlanInformationModel::budgetPlanDurableId($budget_plan_id);
        $html=view('plan::form-plan/durable-article/durable-article-modal',compact('budget_plan_id'),[
            'budgetPlanDurable'=>$budgetPlanDurable,
            ]);
        return ['title'=>'ปรับสถานะรายการครุภัณฑ์','body'=>$html->render()];
    }

    public function saveSatatus($budget_plan_id,Request $request)
    {
        $upload_file = $request->get('upload_file');
        $upload_file_name = $request->get('upload_file_name');
        $money_approve = $request->get('money_approve');
        $budgetPlanDurable = PlanInformationModel::budgetPlanDurableId($budget_plan_id);

        if(!is_numeric($money_approve))
        {
            return response(['กรุณาป้อนจำนวนเงินที่ได้รับอนุมัติด้วยคะ'],422);
        }
        if(($budgetPlanDurable->durable_article_budget_net)>=($money_approve))
        {
        PlanInformationModel::update_budget_plan($budget_plan_id,'อนุมัติแล้ว');
        PlanInformationModel::update_durable($budget_plan_id,$money_approve);
        PlanInformationModel::update_upload_file($budget_plan_id,$upload_file,$upload_file_name);
        return response(['callback'=>'save_success','redirect_url'=>'/durable-article-plan'],200);
        }
        else
        {
            return response(['ขออภัยคะ จำนวนเงินที่ได้รับอนุมัติมีค่ามากกว่าจำนวนเงินที่เสนอขอคะ'],422);
        }
    }

    public function rejectStatus($budget_plan_id)
    {
        PlanInformationModel::update_budget_plan($budget_plan_id,'ไม่ได้รับการอนุมัติ');
        return response(['callback'=>'save_success','redirect_url'=>'/durable-article-plan'],200);

    }
}