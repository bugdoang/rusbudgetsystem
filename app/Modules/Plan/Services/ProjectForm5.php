<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;


class ProjectForm5
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
    	if(!empty($view))
    	{
    		$html= view('plan::form-plan/project/tab-browse/project-tab5',[
            'budgetPlanProject'=>$budgetPlanProject
        	]);
    	}
    	else
    	{
    		$html= view('plan::form-plan/project/tab/project-tab5',[
            'budgetPlanProject'=>$budgetPlanProject
        	]);
    	}
        return $html->render();
    }
}