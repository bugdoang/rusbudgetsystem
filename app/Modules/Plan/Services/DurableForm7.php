<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;


class DurableForm7
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $items = array();
        if (isset($budgetPlanDurable->plan_durable_articles_allocates) && !empty($budgetPlanDurable->plan_durable_articles_allocates)) {
            foreach ($budgetPlanDurable->plan_durable_articles_allocates as $_item) {
                if(!empty($_item->durable_articles_allocate_name))
                {
                    $items[] = [
                        'durable_articles_allocate_name' => $_item->durable_articles_allocate_name
                    ];
                }
            }
        }
        $purchases=config('myconfig.purchases');
        $months=config('myconfig.months');
        $startYear=config('myconfig.start_year');
        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab7', [
                'items' => $items,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'purchases' => $purchases,
                'months' => $months,
                'startYear' => $startYear
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab7', [
                'items' => $items,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'purchases' => $purchases,
                'months' => $months,
                'startYear' => $startYear
            ]);
        }

        return $html->render();
    }
}