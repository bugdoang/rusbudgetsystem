<?php
namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class DurableForm2
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $visions=PlanInformationModel::getVision();
        $federalCircuits=PlanInformationModel::getFederalCircuit();
        $issues=PlanInformationModel::getIssue();
        $goals=PlanInformationModel::getGoal();
        $objectiveIndicators=PlanInformationModel::getObjectiveIndicator();
        $strategies=PlanInformationModel::getStrategy();
        $objectiveStrategies=PlanInformationModel::getObjectiveStrategy();
        $keyProjects=PlanInformationModel::getKeyProject();
        $plans=PlanInformationModel::getPlan();
        $products=PlanInformationModel::getProduct();

        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab2',[
                'visions' =>  $visions,
                'strategies' => $strategies,
                'federalCircuits' => $federalCircuits,
                'issues' => $issues,
                'goals' => $goals,
                'objectiveIndicators' => $objectiveIndicators,
                'objectiveStrategies' => $objectiveStrategies,
                'keyProjects' => $keyProjects,
                'plans' => $plans,
                'products' => $products,
                'budgetPlanDurable'=>$budgetPlanDurable
            ]);
        }

        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab2',[
                'visions' =>  $visions,
                'strategies' => $strategies,
                'federalCircuits' => $federalCircuits,
                'issues' => $issues,
                'goals' => $goals,
                'objectiveIndicators' => $objectiveIndicators,
                'objectiveStrategies' => $objectiveStrategies,
                'keyProjects' => $keyProjects,
                'plans' => $plans,
                'products' => $products,
                'budgetPlanDurable'=>$budgetPlanDurable
            ]);
        }

        return $html->render();
    }
}