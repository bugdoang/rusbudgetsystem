<?php
namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class DurableForm1
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $incomes=PlanInformationModel::getIncome();
        $incomeTypes=PlanInformationModel::getIncomeType();
        $incomeGroups=PlanInformationModel::getIncomeGroup();
        $faculties=PlanInformationModel::getFaculty();
        $campuses=PlanInformationModel::getCampus();
        $programs=config('myconfig.programs');
        $startYear=config('myconfig.start_year');
        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab1',[
                'incomes' =>  $incomes,
                'incomeTypes' => $incomeTypes,
                'incomeGroups' => $incomeGroups,
                'faculties' => $faculties,
                'campuses' => $campuses,
                'programs' => $programs,
                'startYear' => $startYear,
                'budgetPlanDurable'=> $budgetPlanDurable,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab1',[
                'incomes' =>  $incomes,
                'incomeTypes' => $incomeTypes,
                'incomeGroups' => $incomeGroups,
                'faculties' => $faculties,
                'campuses' => $campuses,
                'programs' => $programs,
                'startYear' => $startYear,
                'budgetPlanDurable'=>$budgetPlanDurable,
            ]);
        }
        return $html->render();
    }
}