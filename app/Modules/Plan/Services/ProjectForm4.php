<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;

class ProjectForm4 
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        if (isset($budgetPlanProject->plan_objectives) && !empty($budgetPlanProject->plan_objectives)) {
            foreach ($budgetPlanProject->plan_objectives as $_item) {
                if(!empty($_item->objective_name))
                {
                    $items[] = [
                        'objective_name' => $_item->objective_name
                    ];
                }

            }
        }
        if(!empty($view))
        {
            $html = view('plan::form-plan/project/tab-browse/project-tab4', [
            'items_4' => $items,
            'budgetPlanProject' => $budgetPlanProject,
            ]);
        }
        else
        {
            $html = view('plan::form-plan/project/tab/project-tab4', [
            'items_4' => $items,
            'budgetPlanProject' => $budgetPlanProject,
            ]);  
        }
        return $html->render();
    }
}