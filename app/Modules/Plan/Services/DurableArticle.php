<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;
use DB;
use Illuminate\Support\Facades\Auth;

class DurableArticle
{
    public static function save($request,$budget_plan_id)
    {

        $durable_article_name = $request->durable_article_name;
        $durable_article_place = $request->durable_article_place;
        $durable_article_frequency1 = $request->durable_article_frequency1;
        $durable_article_frequency2 = $request->durable_article_frequency2;
        $durable_article_frequency3 = $request->durable_article_frequency3;
        $durable_article_frequency4 = $request->durable_article_frequency4;
        $durable_article_frequency5 = $request->durable_article_frequency5;

        $durable_article_purchase = $request->durable_article_purchase;
        $durable_article_purchase_month = $request->durable_article_purchase_month;
        $durable_article_purchase_year = $request->durable_article_purchase_year;
        $durable_article_install_month = $request->durable_article_install_month;
        $durable_article_install_year = $request->durable_article_install_year;
        $durable_article_spend_month = $request->durable_article_spend_month;
        $durable_article_spend_year = $request->durable_article_spend_year;
        $durable_article_list = $request->durable_article_list;

//        $durable_article_budget_total = $request->durable_article_budget_total;
//        $durable_article_budget_vat = $request->durable_article_budget_vat;
//        $durable_article_budget_net = $request->durable_article_budget_net;

        $list8 = $request->list8;

        $messages = array();

        $durable_article_budget_total=0;
        $durable_article_budget_vat=0;
        $durable_article_budget_net=0;
        $plan_durable_article_items = [];
        if(!empty($list8))
        {
            $list8 = json_decode($list8,true);
            if (!empty($list8) && is_array($list8))
            {
                $durable_article_budget_total = 0;
                foreach ($list8 as $item)
                {
                    if(is_numeric($item['durable_article_qty'])
                        && $item['durable_article_qty']>0
                        && is_numeric($item['durable_article_priceunit'])
                        && $item['durable_article_priceunit']>0
                        && !empty($item['durable_article_standard_item'])
                        && !empty($item['durable_article_unit'])
                        && !empty($item['durable_article_status'])
                        && !empty($item['durable_article_reason'])
                    )
                    {
                        $durable_article_detail = (isset($item['children']) && !empty($item['children']))?json_encode($item['children']):'[]';

                        $durable_article_num = ($item['durable_article_qty'] * $item['durable_article_priceunit']);
                        $durable_article_budget_total += $durable_article_num;
                        $plan_durable_article_items[]=[
                            'durable_article_item_name'=>trim($item['durable_article_standard_item']),
                            'durable_article_unit'=>trim($item['durable_article_unit']),
                            'durable_article_qty'=>trim($item['durable_article_qty']),
                            'durable_article_priceunit'=>trim($item['durable_article_priceunit']),
                            'durable_article_num'=>$durable_article_num,
                            'durable_article_status'=>trim($item['durable_article_status']),
                            'durable_article_reason'=>($item['durable_article_status']=='Y')?'N':'Y',
                            'durable_article_item_detail'=>$durable_article_detail,
                            'durable_article_id'=>0,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                $durable_article_budget_vat=$durable_article_budget_total * 0.07;
                $durable_article_budget_net=$durable_article_budget_vat+$durable_article_budget_total;
            }
        }

        if (empty($durable_article_name)) {
            $message = "กรุณากรอกชื่อรายการครุภัณฑ์ด้วยคะ";
            return $message;
        }

        if (empty($durable_article_list)) {
            $messages[] = "กรุณาเลือกแบบรายการครุภัณฑ์ด้วยคะ";
        }

        if (!is_numeric($durable_article_frequency1) && !is_numeric($durable_article_frequency2) && !is_numeric($durable_article_frequency3) && !is_numeric($durable_article_frequency4) && !is_numeric($durable_article_frequency5)) {
            $messages[] = "กรุณากรอกความถี่การใช้งานด้วยคะ";
        }

        if (empty($durable_article_place)) {
            $message = "กรุณากรอกสถานที่การใช้งานด้วยคะ";
            return $message;
        }
        if (empty($durable_article_purchase)) {
            $messages[] = "กรุณาเลือกแผนจัดซื้อด้วยคะ";
        }

        if ($durable_article_purchase == 'ก่อหนี้ผูกพัน')
        {
            if (empty($durable_article_purchase_month))
            {
                $messages[] = "กรุณาเลือกแผนจัดซื้อ ภายในเดือนด้วยคะ";
            }
            if (empty($durable_article_purchase_year))
            {
                $messages[] = "กรุณาเลือกแผนจัดซื้อ ภายในปี พ.ศ.ด้วยคะ";
            }
        }

        if(empty($durable_article_install_month))
        {
            $messages[] = "กรุณาเลือกแผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง ภายในเดือนด้วยคะ";
        }

        if(empty($durable_article_install_year))
        {
            $messages[] = "กรุณาเลือกแผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง ภายในปี พ.ศ.ด้วยคะ";
        }

        if(empty($durable_article_spend_month))
        {
            $messages[] = "กรุณาเลือกแผนการใช้จ่าย ภายในเดือนด้วยคะ";
        }

        if(empty($durable_article_spend_year))
        {
            $messages[] = "กรุณาเลือกแผนการใช้จ่าย ภายในปี พ.ศ.ด้วยคะ";
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }
        $durable_article_frequency = [];
        for($i=1;$i<=5;$i++)
        {
            $key = 'durable_article_frequency'.$i;
            $key = $$key;
            $durable_article_frequency[$i] = (!empty($key))?$key:0;
        }

        $check_name_durable = DB::table('plan_durable_articles')
            ->where('durable_article_name',$durable_article_name)
            ->whereNull('deleted_at')
            ->first();
        if(!empty($check_name_durable))
        {
            return 'ขออภัยค่ะ ชื่อรายการครุภัณฑ์มีในระบบแล้ว กรุณาตรวจสอบด้วยคะ';
        }

        $durable_article_id = DB::table('plan_durable_articles')->insertGetId([
            'durable_article_name'=>$durable_article_name,
            'durable_article_place'=>$durable_article_place,
            'durable_article_frequency'=>json_encode($durable_article_frequency),
            'durable_article_purchase'=>$durable_article_purchase,
            'durable_article_purchase_month'=>$durable_article_purchase_month,
            'durable_article_purchase_year'=>$durable_article_purchase_year,
            'durable_article_install_month'=>$durable_article_install_month,
            'durable_article_install_year'=>$durable_article_install_year,
            'durable_article_spend_month'=>$durable_article_spend_month,
            'durable_article_spend_year'=>$durable_article_spend_year,
            'durable_article_list'=>$durable_article_list,
            'budget_plan_id'=>$budget_plan_id,
            'durable_article_budget_total'=>$durable_article_budget_total,
            'durable_article_budget_vat'=>$durable_article_budget_vat,
            'durable_article_budget_net'=>$durable_article_budget_net,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if(is_numeric($durable_article_id) && $durable_article_id>0)
        {
            $result_durable_article = DurableAllocate::save($request,$durable_article_id);

            if(gettype($result_durable_article)=='string')
            {
                return $result_durable_article;
            }

            if(!empty($plan_durable_article_items))
            {
                foreach ($plan_durable_article_items as $plan_durable_article_item)
                {
                    $plan_durable_article_item['durable_article_id']=$durable_article_id;
                    DB::table('plan_durable_article_items')->insertGetId($plan_durable_article_item);
                }
            }
            return ['durable_article_id'=>$durable_article_id];
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของรายการครุภัณฑ์ได้คะ';
        }
    }

    //update table durable_article
    public static function update($request,$budget_plan_id)
    {
        $durable_article_name = $request->get('durable_article_name');
        $durable_article_id = $request->get('durable_article_id');

        $check_name_durable = DB::table('plan_durable_articles')
            ->where('durable_article_id','!=',$durable_article_id)
            ->where('durable_article_name',$durable_article_name)
            ->whereNull('deleted_at')
            ->first();
        if(!empty($check_name_durable))
        {
            return 'ขออภัยค่ะ ชื่อรายการครุภัณฑ์มีในระบบแล้ว กรุณาตรวจสอบด้วยคะ';
        }

        $durable_article_place = $request->durable_article_place;
        $durable_article_frequency1 = $request->durable_article_frequency1;
        $durable_article_frequency2 = $request->durable_article_frequency2;
        $durable_article_frequency3 = $request->durable_article_frequency3;
        $durable_article_frequency4 = $request->durable_article_frequency4;
        $durable_article_frequency5 = $request->durable_article_frequency5;

        $durable_article_purchase = $request->durable_article_purchase;
        $durable_article_purchase_month = $request->durable_article_purchase_month;
        $durable_article_purchase_year = $request->durable_article_purchase_year;
        $durable_article_install_month = $request->durable_article_install_month;
        $durable_article_install_year = $request->durable_article_install_year;
        $durable_article_spend_month = $request->durable_article_spend_month;
        $durable_article_spend_year = $request->durable_article_spend_year;
        $durable_article_list = $request->durable_article_list;

//        $durable_article_budget_total = $request->durable_article_budget_total;
//        $durable_article_budget_vat = $request->durable_article_budget_vat;
//        $durable_article_budget_net = $request->durable_article_budget_net;

        $list8 = $request->list8;
        $list5 = $request->list5;

        $messages = array();

        $durable_article_budget_total=0;
        $durable_article_budget_vat=0;
        $durable_article_budget_net=0;
        $plan_durable_article_items = [];

        DB::table('plan_durable_article_items')
            ->where('durable_article_id',$durable_article_id)
            ->delete();

        if(!empty($list8))
        {
            $list8 = json_decode($list8,true);
            if (!empty($list8) && is_array($list8))
            {
                $durable_article_budget_total = 0;
                foreach ($list8 as $item)
                {
                    if(is_numeric($item['durable_article_qty'])
                        && $item['durable_article_qty']>0
                        && is_numeric($item['durable_article_priceunit'])
                        && $item['durable_article_priceunit']>0
                        && !empty($item['durable_article_standard_item'])
                        && !empty($item['durable_article_unit'])
                        && !empty($item['durable_article_status'])
                        && !empty($item['durable_article_reason'])
                    )
                    {
                        $durable_article_detail = (isset($item['children']) && !empty($item['children']))?json_encode($item['children']):'[]';

                        $durable_article_num = ($item['durable_article_qty'] * $item['durable_article_priceunit']);
                        $durable_article_budget_total += $durable_article_num;
                        $plan_durable_article_items[]=[
                            'durable_article_item_name'=>trim($item['durable_article_standard_item']),
                            'durable_article_unit'=>trim($item['durable_article_unit']),
                            'durable_article_qty'=>trim($item['durable_article_qty']),
                            'durable_article_priceunit'=>trim($item['durable_article_priceunit']),
                            'durable_article_num'=>$durable_article_num,
                            'durable_article_status'=>trim($item['durable_article_status']),
                            'durable_article_reason'=>($item['durable_article_status']=='Y')?'N':'Y',
                            'durable_article_item_detail'=>$durable_article_detail,
                            'durable_article_id'=>0,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                $durable_article_budget_vat=$durable_article_budget_total * 0.07;
                $durable_article_budget_net=$durable_article_budget_vat+$durable_article_budget_total;
            }
        }

        if (empty($durable_article_name)) {
            $message = "กรุณากรอกชื่อรายการครุภัณฑ์ด้วยคะ";
            return $message;
        }

        if (empty($durable_article_list)) {
            $messages[] = "กรุณาเลือกแบบรายการครุภัณฑ์ด้วยคะ";
        }

        if (!is_numeric($durable_article_frequency1) && !is_numeric($durable_article_frequency2) && !is_numeric($durable_article_frequency3) && !is_numeric($durable_article_frequency4) && !is_numeric($durable_article_frequency5)) {
            $messages[] = "กรุณากรอกความถี่การใช้งานด้วยคะ";
        }

        if (empty($durable_article_place)) {
            $message = "กรุณากรอกสถานที่การใช้งานด้วยคะ";
            return $message;
        }
        if (empty($durable_article_purchase)) {
            $messages[] = "กรุณาเลือกแผนจัดซื้อด้วยคะ";
        }

        if ($durable_article_purchase == 'ก่อหนี้ผูกพัน')
        {
            if (empty($durable_article_purchase_month))
            {
                $messages[] = "กรุณาเลือกแผนจัดซื้อ ภายในเดือนด้วยคะ";
            }
            if (empty($durable_article_purchase_year))
            {
                $messages[] = "กรุณาเลือกแผนจัดซื้อ ภายในปี พ.ศ.ด้วยคะ";
            }
        }

        if(empty($durable_article_install_month))
        {
            $messages[] = "กรุณาเลือกแผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง ภายในเดือนด้วยคะ";
        }

        if(empty($durable_article_install_year))
        {
            $messages[] = "กรุณาเลือกแผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง ภายในปี พ.ศ.ด้วยคะ";
        }

        if(empty($durable_article_spend_month))
        {
            $messages[] = "กรุณาเลือกแผนการใช้จ่าย ภายในเดือนด้วยคะ";
        }

        if(empty($durable_article_spend_year))
        {
            $messages[] = "กรุณาเลือกแผนการใช้จ่าย ภายในปี พ.ศ.ด้วยคะ";
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }
        $durable_article_frequency = [];
        for($i=1;$i<=5;$i++)
        {
            $key = 'durable_article_frequency'.$i;
            $key = $$key;
            $durable_article_frequency[$i] = (!empty($key))?$key:0;
        }

         DB::table('plan_durable_articles')->where('durable_article_id',$durable_article_id)->update([
            'durable_article_name'=>$durable_article_name,
            'durable_article_place'=>$durable_article_place,
            'durable_article_frequency'=>json_encode($durable_article_frequency),
            'durable_article_purchase'=>$durable_article_purchase,
            'durable_article_purchase_month'=>$durable_article_purchase_month,
            'durable_article_purchase_year'=>$durable_article_purchase_year,
            'durable_article_install_month'=>$durable_article_install_month,
            'durable_article_install_year'=>$durable_article_install_year,
            'durable_article_spend_month'=>$durable_article_spend_month,
            'durable_article_spend_year'=>$durable_article_spend_year,
            'durable_article_list'=>$durable_article_list,
            'budget_plan_id'=>$budget_plan_id,
            'durable_article_budget_total'=>$durable_article_budget_total,
            'durable_article_budget_vat'=>$durable_article_budget_vat,
            'durable_article_budget_net'=>$durable_article_budget_net,
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if(is_numeric($durable_article_id) && $durable_article_id>0)
        {
            $result_durable_article = DurableAllocate::update($request,$durable_article_id);

            if(gettype($result_durable_article)=='string')
            {
                return $result_durable_article;
            }

            if(!empty($plan_durable_article_items))
            {
                foreach ($plan_durable_article_items as $plan_durable_article_item)
                {
                    $plan_durable_article_item['durable_article_id']=$durable_article_id;
                    DB::table('plan_durable_article_items')->insertGetId($plan_durable_article_item);
                }
            }
            return ['durable_article_id'=>$durable_article_id];
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของรายการครุภัณฑ์ได้คะ';
        }
    }

    public static function delete($budget_plan_id)
    {
        DB::table('plan_durable_article_items')
            ->whereExists(function ($query) use($budget_plan_id){
                $query->select(DB::raw(1))
                    ->from('plan_durable_articles')
                    ->where('plan_durable_articles.budget_plan_id',$budget_plan_id)
                    ->whereRaw('plan_durable_article_items.durable_article_id = plan_durable_articles.durable_article_id');
            })
            ->update([
                'deleted_at'=>date('Y-m-d H:i:s')
            ]);

        DB::table('plan_durable_articles')->where('budget_plan_id',$budget_plan_id)->update([
            'deleted_at'=>date('Y-m-d H:i:s')
        ]);
    }
}