<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;

class ProjectForm7 
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        if (isset($budgetPlanProject->plan_project_benefits) && !empty($budgetPlanProject->plan_project_benefits)) {
            foreach ($budgetPlanProject->plan_project_benefits as $_item) {
                if(!empty($_item->project_benefit_name))
                {
                    $items[] = [
                        'project_benefit_name' => $_item->project_benefit_name
                    ];
                }

            }
        }
        if(!empty($view))
        {
            $html = view('plan::form-plan/project/tab-browse/project-tab7', [
            'items' => $items,
            'budgetPlanProject' => $budgetPlanProject,
            ]);
        }
        else
        {
            $html = view('plan::form-plan/project/tab/project-tab7', [
            'items' => $items,
            'budgetPlanProject' => $budgetPlanProject,
            ]);
        }
        
        return $html->render();
    }
}