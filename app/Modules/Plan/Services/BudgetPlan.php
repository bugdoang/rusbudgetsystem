<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BudgetPlan
{
    public static function save($request,$budget_plan_category)
    {
        $reason = $request->reason;
        $fiscal_year = $request->fiscal_year;
        $semester = $request->semester;
        $personnel_id=Auth::id();
        $product_id = $request->product_id;
        $plan_id = $request->plan_id;
        $income_id = $request->income_id;
        $income_type_id = $request->income_type_id;
        $income_group_id = $request->income_group_id;
        $key_project_id = $request->key_project_id;
        $objective_strategy_id = $request->objective_strategy_id;
        $strategy_id = $request->strategy_id;
        $goal_id = $request->goal_id;
        $objective_indicator_id = $request->objective_indicator_id;
        $issue_id = $request->issue_id;
        $federal_circuit_id = $request->federal_circuit_id;
        $vision_id = $request->vision_id;

        $messages = array();

        if(!is_numeric($income_id))
        {
            $messages[] = "กรุณาเลือกแหล่งเงินได้ด้วยคะ";
        }

        if(!is_numeric($income_type_id))
        {
            $messages[] = "กรุณาเลือกประเภทแหล่งเงินได้ด้วยคะ";
        }

        if(in_array($income_type_id,[1,2,3]))
        {
            if(!is_numeric($income_group_id))
            {
                $messages[] = "กรุณาเลือกหมวดแหล่งเงินได้ด้วยคะ";
            }
        }

        if(!is_numeric($fiscal_year))
        {
            $messages[] = "กรุณาเลือกปีงบประมาณด้วยคะ";
        }

        if(empty($semester))
        {
            $messages[] = "กรุณาเลือกรอบภาคการศึกษาด้วยคะ";
        }

        if(!is_numeric($vision_id))
        {
            $messages[] = "กรุณาเลือกวิสัยทัศน์ด้วยคะ";
        }

        if(!is_numeric($federal_circuit_id))
        {
            $messages[] = "กรุณาเลือกพันธกิจด้วยคะ";
        }

        if(!is_numeric($issue_id))
        {
            $messages[] = "กรุณาเลือกประเด็นยุทศาสตร์ด้วยคะ";
        }

        if(!is_numeric($goal_id))
        {
            $messages[] = "กรุณาเลือกเป้าประสงค์ด้วยคะ";
        }

        if(!is_numeric($objective_indicator_id))
        {
            $messages[] = "กรุณาเลือกตัวชี้วัดเป้าประสงค์ด้วยคะ";
        }

        if(!is_numeric($strategy_id))
        {
            $messages[] = "กรุณาเลือกกลยุทธ์ด้วยคะ";
        }

        if(!is_numeric($objective_strategy_id))
        {
            $messages[] = "กรุณาเลือกตัวชี้วัดกลยุทธ์ด้วยคะ";
        }

        if(!is_numeric($key_project_id))
        {
            $messages[] = "กรุณาเลือกโครงการสำคัญด้วยคะ";
        }

        if(!is_numeric($plan_id))
        {
            $messages[] = "กรุณาเลือกแผนงานด้วยคะ";
        }

        if(!is_numeric($product_id))
        {
            $messages[] = "กรุณาเลือกผลผลิตด้วยคะ";
        }

        if($reason == '')
        {
            $messages[] = "กรุณากรอกเหตุผลความจำเป็นด้วยคะ";
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }
        $branches = DB::table('branches')->where('branch_id',Auth::user()->branch_id)->first();
        if(empty($branches))
        {
            return 'ขออภัยค่ะ ไม่พบสาขาในระบบ กรุณาตรวจสอบด้วยค่ะ';
        }
        $campuses = DB::table('campuses')->where('campus_id',Auth::user()->campus_id)->first();
        if(empty($campuses))
        {
            return 'ขออภัยค่ะ ไม่พบศูนย์พื้นที่ในระบบ กรุณาตรวจสอบด้วยค่ะ';
        }
        $faculty = $branches->faculty_id;

        $budget_plan_id = DB::table('budget_plans')->insertGetId([
            'budget_plan_category'=>$budget_plan_category,
            'reason'=>$reason,
            'fiscal_year'=>$fiscal_year,
            'faculty'=>$faculty,
            'semester'=>$semester,
            'personnel_id'=>$personnel_id,
            'product_id'=>$product_id,
            'plan_id'=>$plan_id,
            'income_id'=>$income_id,
            'income_type_id'=>$income_type_id,
            'income_group_id'=>(is_numeric($income_group_id))?$income_group_id:null,
            'key_project_id'=>$key_project_id,
            'objective_strategy_id'=>$objective_strategy_id,
            'strategy_id'=>$strategy_id,
            'goal_id'=>$goal_id,
            'objective_indicator_id'=>$objective_indicator_id,
            'issue_id'=>$issue_id,
            'federal_circuit_id'=>$federal_circuit_id,
            'vision_id'=>$vision_id,
            'campus'=>$campuses->campus_name,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if(is_numeric($budget_plan_id) && $budget_plan_id>0)
        {
            return ['budget_plan_id'=>$budget_plan_id];
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของการจัดแผนเสนอของบประมาณได้คะ';
        }
    }

    //update table budget_plan
    public static function update($budget_plan_id, Request $request)
    {
        $reason = $request->reason;
        $fiscal_year = $request->fiscal_year;
        $semester = $request->semester;
        $personnel_id=Auth::id();
        $product_id = $request->product_id;
        $plan_id = $request->plan_id;
        $income_id = $request->income_id;
        $income_type_id = $request->income_type_id;
        $income_group_id = $request->income_group_id;
        $key_project_id = $request->key_project_id;
        $objective_strategy_id = $request->objective_strategy_id;
        $strategy_id = $request->strategy_id;
        $goal_id = $request->goal_id;
        $objective_indicator_id = $request->objective_indicator_id;
        $issue_id = $request->issue_id;
        $federal_circuit_id = $request->federal_circuit_id;
        $vision_id = $request->vision_id;

        $messages = array();

        if(!is_numeric($income_id))
        {
            $messages[] = "กรุณาเลือกแหล่งเงินได้ด้วยคะ";
        }

        if(!is_numeric($income_type_id))
        {
            $messages[] = "กรุณาเลือกประเภทแหล่งเงินได้ด้วยคะ";
        }

        if(in_array($income_type_id,[1,2,3]))
        {
            if(!is_numeric($income_group_id))
            {
                $messages[] = "กรุณาเลือกหมวดแหล่งเงินได้ด้วยคะ";
            }
        }

        if(!is_numeric($fiscal_year))
        {
            $messages[] = "กรุณาเลือกปีงบประมาณด้วยคะ";
        }

        if(empty($semester))
        {
            $messages[] = "กรุณาเลือกรอบภาคการศึกษาด้วยคะ";
        }

        if(!is_numeric($vision_id))
        {
            $messages[] = "กรุณาเลือกวิสัยทัศน์ด้วยคะ";
        }

        if(!is_numeric($federal_circuit_id))
        {
            $messages[] = "กรุณาเลือกพันธกิจด้วยคะ";
        }

        if(!is_numeric($issue_id))
        {
            $messages[] = "กรุณาเลือกประเด็นยุทศาสตร์ด้วยคะ";
        }

        if(!is_numeric($goal_id))
        {
            $messages[] = "กรุณาเลือกเป้าประสงค์ด้วยคะ";
        }

        if(!is_numeric($objective_indicator_id))
        {
            $messages[] = "กรุณาเลือกตัวชี้วัดเป้าประสงค์ด้วยคะ";
        }

        if(!is_numeric($strategy_id))
        {
            $messages[] = "กรุณาเลือกกลยุทธ์ด้วยคะ";
        }

        if(!is_numeric($objective_strategy_id))
        {
            $messages[] = "กรุณาเลือกตัวชี้วัดกลยุทธ์ด้วยคะ";
        }

        if(!is_numeric($key_project_id))
        {
            $messages[] = "กรุณาเลือกโครงการสำคัญด้วยคะ";
        }

        if(!is_numeric($plan_id))
        {
            $messages[] = "กรุณาเลือกแผนงานด้วยคะ";
        }

        if(!is_numeric($product_id))
        {
            $messages[] = "กรุณาเลือกผลผลิตด้วยคะ";
        }

        if($reason == '')
        {
            $messages[] = "กรุณากรอกเหตุผลความจำเป็นด้วยคะ";
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }
        $branches = DB::table('branches')->where('branch_id',Auth::user()->branch_id)->first();
        if(empty($branches))
        {
            return 'ขออภัยค่ะ ไม่พบสาขาในระบบ กรุณาตรวจสอบด้วยค่ะ';
        }
        $campuses = DB::table('campuses')->where('campus_id',Auth::user()->campus_id)->first();
        if(empty($campuses))
        {
            return 'ขออภัยค่ะ ไม่พบศูนย์พื้นที่ในระบบ กรุณาตรวจสอบด้วยค่ะ';
        }
        $faculty = $branches->faculty_id;
        DB::table('budget_plans')->where('budget_plan_id',$budget_plan_id)->update([
            'reason'=>$reason,
            'fiscal_year'=>$fiscal_year,
            'faculty'=>$faculty,
            'semester'=>$semester,
            'product_id'=>$product_id,
            'plan_id'=>$plan_id,
            'income_id'=>$income_id,
            'income_type_id'=>$income_type_id,
            'income_group_id'=>(is_numeric($income_group_id))?$income_group_id:null,
            'key_project_id'=>$key_project_id,
            'objective_strategy_id'=>$objective_strategy_id,
            'strategy_id'=>$strategy_id,
            'goal_id'=>$goal_id,
            'objective_indicator_id'=>$objective_indicator_id,
            'issue_id'=>$issue_id,
            'federal_circuit_id'=>$federal_circuit_id,
            'vision_id'=>$vision_id,
            'campus'=>$campuses->campus_name,
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
    }

    public static function delete($budget_plan_id)
    {
        DB::table('budget_plans')->where('budget_plan_id',$budget_plan_id)->update([
            'deleted_at'=>date('Y-m-d H:i:s')
        ]);

    }

}