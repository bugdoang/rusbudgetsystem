<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class Objective
{
    public static function save($request,$budget_plan_id)
    {
        $list5 = $request->list5;
        if(!empty($list5))
        {
            $list5 = json_decode($list5, true);
            if (!empty($list5) && is_array($list5))
            {
                $plan_objectives = [];
                foreach ($list5 as $index=>$item)
                {
                    if(isset($item['objective_name']) && !empty($item['objective_name']))
                    {
                        $plan_objectives[] = [
                            'objective_name'=>$item['objective_name'],
                            'budget_plan_id'=>$budget_plan_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_objectives))
                {
                    DB::table('plan_objectives')->insert($plan_objectives);
                    return true;
                }
            }
        }
        return 'กรุณาระบุวัตถุประสงค์ด้วยคะ';
    }

    public static function update($request,$budget_plan_id)
    {
        $list5 = $request->list5;

        DB::table('plan_objectives')
            ->where('budget_plan_id',$budget_plan_id)
            ->delete();

        if(!empty($list5))
        {
            $list5 = json_decode($list5, true);
            if (!empty($list5) && is_array($list5))
            {
                $plan_objectives = [];
                foreach ($list5 as $index=>$item)
                {
                    if(isset($item['objective_name']) && !empty($item['objective_name']))
                    {
                        $plan_objectives[] = [
                            'objective_name'=>$item['objective_name'],
                            'budget_plan_id'=>$budget_plan_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_objectives))
                {
                    DB::table('plan_objectives')->insert($plan_objectives);
                    return true;
                }
            }
        }
        return 'กรุณาระบุวัตถุประสงค์ด้วยคะ';
    }

    public static function delete($budget_plan_id)
    {
        DB::table('plan_objectives')->where('budget_plan_id',$budget_plan_id)->update([
            'deleted_at'=>date('Y-m-d H:i:s')
        ]);

    }
}