<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;

use DB;

class ProjectPeriod
{
    public static function save($request,$project_id)
    {
        $month_start = $request->month_start;
        $year_start = $request->year_start;
        $month_end = $request->month_end;
        $year_end = $request->year_end;

        $messages = array();

        if(empty($month_start))
        {
            $messages[] = "กรุณาเลือกเดือนที่เริ่มด้วยคะ";
        }

        if(empty($year_start))
        {
            $messages[] = "กรุณาเลือก พ.ศ. ที่เริ่มด้วยคะ";
        }

        if(empty($month_end))
        {
            $messages[] = "กรุณาเลือกเดือนที่สิ้นสุดด้วยคะ";
        }

        if(empty($year_end))
        {
            $messages[] = "กรุณาเลือก พ.ศ. ที่สิ้นสุด ด้วยคะ";
        }

        $project_period_id = DB::table('plan_project_periods')->insertGetId([
            'month_start'=>$month_start,
            'year_start'=>$year_start,
            'month_end'=>$month_end,
            'year_end'=>$year_end,
            'project_id'=>$project_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if(is_numeric($project_period_id) && $project_period_id>0)
        {
            return ['project_period_id'=>$project_period_id];
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของระยะเวลาดำเนินการได้คะ';
        }
    }

    public static function update($request,$project_id)
    {
        $month_start = $request->month_start;
        $year_start = $request->year_start;
        $month_end = $request->month_end;
        $year_end = $request->year_end;

        $messages = array();

        if(empty($month_start))
        {
            $messages[] = "กรุณาเลือกเดือนที่เริ่มด้วยคะ";
        }

        if(empty($year_start))
        {
            $messages[] = "กรุณาเลือก พ.ศ. ที่เริ่มด้วยคะ";
        }

        if(empty($month_end))
        {
            $messages[] = "กรุณาเลือกเดือนที่สิ้นสุดด้วยคะ";
        }

        if(empty($year_end))
        {
            $messages[] = "กรุณาเลือก พ.ศ. ที่สิ้นสุด ด้วยคะ";
        }
        DB::table('plan_project_periods')->where('project_id',$project_id)->update([
            'month_start'=>$month_start,
            'year_start'=>$year_start,
            'month_end'=>$month_end,
            'year_end'=>$year_end,
            'project_id'=>$project_id,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
    }   
}