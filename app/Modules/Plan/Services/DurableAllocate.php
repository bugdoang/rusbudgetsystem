<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class DurableAllocate
{
    public static function save($request,$durable_article_id)
    {
        $list7 = $request->list7;
        if(!empty($list7))
        {
            $list7 = json_decode($list7, true);
            if (!empty($list7) && is_array($list7))
            {
                $plan_durable_articles_allocates = [];
                foreach ($list7 as $index=>$item)
                {
                    if(isset($item['durable_articles_allocate_name']) &&  !empty($item['durable_articles_allocate_name']) )
                    {
                        $plan_durable_articles_allocates[] = [
                            'durable_articles_allocate_name'=>$item['durable_articles_allocate_name'],
                            'durable_article_id'=>$durable_article_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_durable_articles_allocates))
                {
                    DB::table('plan_durable_articles_allocates')->insert($plan_durable_articles_allocates);
                    return true;
                }
            }
        }
        return 'กรุณาระบุหากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไรด้วยคะ';
    }

    public static function update($request,$durable_article_id)
    {
        $list7 = $request->list7;

        DB::table('plan_durable_articles_allocates')
            ->where('durable_article_id',$durable_article_id)
            ->delete();

        if(!empty($list7))
        {
            $list7 = json_decode($list7, true);
            if (!empty($list7) && is_array($list7))
            {
                $plan_durable_articles_allocates = [];
                foreach ($list7 as $index=>$item)
                {
                    if(isset($item['durable_articles_allocate_name']) &&  !empty($item['durable_articles_allocate_name']) )
                    {
                        $plan_durable_articles_allocates[] = [
                            'durable_articles_allocate_name'=>$item['durable_articles_allocate_name'],
                            'durable_article_id'=>$durable_article_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_durable_articles_allocates))
                {
                    DB::table('plan_durable_articles_allocates')->insert($plan_durable_articles_allocates);
                    return true;
                }
            }
        }
        return 'กรุณาระบุหากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไรด้วยคะ';
    }

    public static function delete($durable_article_id)
    {
        DB::table('plan_durable_articles_allocates')->where('durable_article_id',$durable_article_id)->update([
            'deleted_at'=>date('Y-m-d H:i:s')
        ]);

    }
}