<?php
namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanProjectModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class ProjectForm2
{
    public static function get($budgetPlanProject=NULL,$view=NULL)
    {
        $visions=PlanProjectModel::getVision();
        $federalCircuits=PlanProjectModel::getFederalCircuit();
        $issues=PlanProjectModel::getIssue();
        $goals=PlanProjectModel::getGoal();
        $objectiveIndicators=PlanProjectModel::getObjectiveIndicator();
        $strategies=PlanProjectModel::getStrategy();
        $objectiveStrategies=PlanProjectModel::getObjectiveStrategy();
        $keyProjects=PlanProjectModel::getKeyProject();
        $plans=PlanProjectModel::getPlan();
        $products=PlanProjectModel::getProduct();
        if(!empty($view))
        {
            $html= view('plan::form-plan/project/tab-browse/project-tab2',[
            'visions' =>  $visions,
            'strategies' => $strategies,
            'federalCircuits' => $federalCircuits,
            'issues' => $issues,
            'goals' => $goals,
            'objectiveIndicators' => $objectiveIndicators,
            'objectiveStrategies' => $objectiveStrategies,
            'keyProjects' => $keyProjects,
            'plans' => $plans,
            'products' => $products,
            'budgetPlanProject'=>$budgetPlanProject
            ]);
        }
        else
        {
            $html= view('plan::form-plan/project/tab/project-tab2',[
            'visions' =>  $visions,
            'strategies' => $strategies,
            'federalCircuits' => $federalCircuits,
            'issues' => $issues,
            'goals' => $goals,
            'objectiveIndicators' => $objectiveIndicators,
            'objectiveStrategies' => $objectiveStrategies,
            'keyProjects' => $keyProjects,
            'plans' => $plans,
            'products' => $products,
            'budgetPlanProject'=>$budgetPlanProject
            ]);
        }
        return $html->render();
    }
}