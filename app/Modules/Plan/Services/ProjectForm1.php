<?php
namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanProjectModel;
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:16 AM
 */
class ProjectForm1
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $incomes=PlanProjectModel::getIncome();
        $incomeTypes=PlanProjectModel::getIncomeType();
        $incomeGroups=PlanProjectModel::getIncomeGroup();
        $faculties=PlanProjectModel::getFaculty();
        $campuses=PlanProjectModel::getCampus();
        $programs=config('myconfig.programs');
        $startYear=config('myconfig.start_year');
        $project_statuses=config('myconfig.project_statuses');

        if(!empty($view))
        {
            $html= view('plan::form-plan/project/tab-browse/project-tab1',[
            'incomes' =>  $incomes,
            'incomeTypes' => $incomeTypes,
            'incomeGroups' => $incomeGroups,
            'faculties' => $faculties,
            'campuses' => $campuses,
            'programs' => $programs,
            'startYear' => $startYear,
            'project_statuses' => $project_statuses,
            'budgetPlanProject'=> $budgetPlanProject,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/project/tab/project-tab1',[
            'incomes' =>  $incomes,
            'incomeTypes' => $incomeTypes,
            'incomeGroups' => $incomeGroups,
            'faculties' => $faculties,
            'campuses' => $campuses,
            'programs' => $programs,
            'startYear' => $startYear,
            'project_statuses' => $project_statuses,
            'budgetPlanProject'=> $budgetPlanProject,
            ]);
        }
        return $html->render();
    }
}