<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class ProjectBenefit
{
    public static function save($request,$project_id)
    {
        $list7 = $request->list7;
        if(!empty($list7))
        {
            $list7 = json_decode($list7, true);
            if (!empty($list7) && is_array($list7))
            {
                $plan_project_benefits = [];
                foreach ($list7 as $index=>$item)
                {
                    if(isset($item['project_benefit_name']) &&  !empty($item['project_benefit_name']))
                    {
                        $plan_project_benefits[] = [
                            'project_benefit_name'=>$item['project_benefit_name'],
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_benefits))
                {
                    DB::table('plan_project_benefits')->insert($plan_project_benefits);
                    return true;
                }
            }
        }
        return 'กรุณากรอกประโยชน์ที่คาดว่าจะได้รับด้วยคะ';
    }

    public static function update($request,$project_id)
    {
        $list7 = $request->list7;

        DB::table('plan_project_benefits')
            ->where('project_id',$project_id)
            ->delete();

        if(!empty($list7))
        {
            $list7 = json_decode($list7, true);
            if (!empty($list7) && is_array($list7))
            {
                $plan_project_benefits = [];
                foreach ($list7 as $index=>$item)
                {
                    if(isset($item['project_benefit_name']) &&  !empty($item['project_benefit_name']))
                    {
                        $plan_project_benefits[] = [
                            'project_benefit_name'=>$item['project_benefit_name'],
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_benefits))
                {
                    DB::table('plan_project_benefits')->insert($plan_project_benefits);
                    return true;
                }
            }
        }
        return 'กรุณากรอกชื่อประโยชน์ที่คาดว่าจะได้รับด้วยคะ';
    }
}