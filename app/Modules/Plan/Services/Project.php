<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;
use DB;
use Illuminate\Support\Facades\Auth;


class Project
{
    public static function save($request,$budget_plan_id)
    {
        $project_name = $request->project_name;
        $project_status = $request->project_status;
        $nature_event = $request->nature_event;
        $place = $request->place;
        $ready = $request->ready;
        $items= $request->tab9;

        if (empty($project_name)) {
            $message = "กรุณากรอกชื่อโครงการด้วยคะ";
            return $message;
        }

        if (empty($project_status)) {
            $messages[] = "กรุณาเลือกสถานภาพของโครงการด้วยคะ";
        }

        if (empty($nature_event)) {
            $message = "กรุณากรอกลักษณะของกิจกรรมที่ดำเนินการด้วยคะ";
            return $message;
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        $check_name_project = DB::table('plan_projects')
            ->where('project_name',$project_name)
            ->whereNull('deleted_at')
            ->first();
        if(!empty($check_name_project))
        {
            return 'ขออภัยค่ะ ชื่อโครงการมีในระบบแล้ว กรุณาตรวจสอบด้วยคะ';
        }

        $project_id = DB::table('plan_projects')->insertGetId([
                'project_name'=>$project_name,
                'project_status'=>$project_status,
                'nature_event'=>$nature_event,
                'place'=>$place,
                'ready'=>$ready,
                'nature_event'=>$nature_event,
                'project_goal'=>$items,
                'budget_plan_id'=>$budget_plan_id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);

    
        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_activity = ProjectActivity::save($request,$project_id);

            if(gettype($result_project_activity)=='string')
            {
                return $result_project_activity;
            }

            if(!empty($plan_project_activities))
            {
                foreach ($plan_project_activities as $plan_project_activity)
                {
                    $plan_project_activity['project_id']=$project_id;
                    DB::table('plan_project_activities')->insertGetId($plan_project_activity);
                }
            }

        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของกิจกรรมของโครงการ/ระยะเวลาดำเนินการได้คะ';
        }

        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_benefit = ProjectBenefit::save($request,$project_id);

            if(gettype($result_project_benefit)=='string')
            {
                return $result_project_benefit;
            }

            if(!empty($plan_project_benefits))
            {
                foreach ($plan_project_benefits as $plan_project_benefit)
                {
                    $plan_project_benefit['project_id']=$project_id;
                    DB::table('plan_project_benefits')->insertGetId($plan_project_benefit);
                }
            }

        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของประโยชน์ที่คาดว่าจะได้รับได้คะ';
        }

        
        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_budget = ProjectBudget::save($request,$project_id);

            if(gettype($result_project_budget)=='string')
            {
                return $result_project_budget;
            }

            if(!empty($plan_project_budgets))
            {
                foreach ($plan_project_budgets as $plan_project_budget)
                {
                    $plan_project_budget['project_id']=$project_id;
                    DB::table('plan_project_budgets')->insertGetId($plan_project_budget);
                }
            }
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของงบประมาณได้คะ';
        }


        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_period = ProjectPeriod::save($request,$project_id);

            if(gettype($result_project_period)=='string')
            {
                return $result_project_period;
            }

            if(!empty($plan_project_periods))
            {
                foreach ($plan_project_periods as $plan_project_period)
                {
                    $plan_project_period['project_id']=$project_id;
                    DB::table('plan_project_periods')->insertGetId($plan_project_period);
                }
            }

        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของระยะเวลาดำเนินการได้คะ';
        }


        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_suggestion = ProjectSuggestion::save($request,$project_id);

            if(gettype($result_project_suggestion)=='string')
            {
                return $result_project_suggestion;
            }

            if(!empty($plan_project_suggestions))
            {
                foreach ($plan_project_suggestions as $plan_project_suggestion)
                {
                    $plan_project_suggestion['project_id']=$project_id;
                    DB::table('plan_project_suggestions')->insertGetId($plan_project_suggestion);
                }
            }

            return ['project_id'=>$project_id];

        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของปัญหาอุปสรรค/ข้อเสนอแนะได้คะ';
        }


    }

    //update table project
    public static function update($request,$budget_plan_id)
    {
        $project_name = $request->get('project_name');
        $project_id = $request->get('project_id');

        $check_name_project = DB::table('plan_projects')
            ->where('project_id','!=',$project_id)
            ->where('project_name',$project_name)
            ->whereNull('deleted_at')
            ->first();
        if(!empty($check_name_project))
        {
            return 'ขออภัยค่ะ ชื่อโครงการมีในระบบแล้ว กรุณาตรวจสอบด้วยคะ';
        }

        $project_status = $request->get('project_status');
        $nature_event = $request->get('nature_event');
        $place = $request->get('place');
        $ready = $request->get('ready');

        if (empty($project_name)) {
            $message = "กรุณากรอกชื่อโครงการด้วยคะ";
            return $message;
        }

        if (empty($project_status)) {
            $messages[] = "กรุณาเลือกสถานภาพของโครงการด้วยคะ";
        }

        if (empty($nature_event)) {
            $message = "กรุณากรอกลักษณะของกิจกรรมที่ดำเนินการด้วยคะ";
            return $message;
        }

        if(!empty($messages))
        {
            return join("<br/>",$messages);
        }

        $items= $request->tab9;


         DB::table('plan_projects')->where('project_id',$project_id)->update([
            'project_name'=>$project_name,
            'project_status'=>$project_status,
            'nature_event'=>$nature_event,
            'place'=>$place,
            'ready'=>$ready,
            'budget_plan_id'=>$budget_plan_id,
            'project_goal'=>$items,
            'updated_at'=>date('Y-m-d H:i:s')
        ]);


        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_activity = ProjectActivity::update($request,$project_id);

            if(gettype($result_project_activity)=='string')
            {
                return $result_project_activity;
            }

            if(!empty($plan_project_activities))
            {
                foreach ($plan_project_activities as $plan_project_activity)
                {
                    $plan_project_activity['project_id']=$project_id;
                    DB::table('plan_project_activities')->insertGetId($plan_project_activity);
                }
            }
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของโครงการได้คะ';
        }

        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_benefit = ProjectBenefit::update($request,$project_id);

            if(gettype($result_project_benefit)=='string')
            {
                return $result_project_benefit;
            }

            if(!empty($plan_project_benefits))
            {
                foreach ($plan_project_benefits as $plan_project_benefit)
                {
                    $plan_project_benefit['project_id']=$project_id;
                    DB::table('plan_project_benefits')->insertGetId($plan_project_benefit);
                }
            }
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของประโยชน์ที่คาดว่าจะได้รับได้คะ';
        }

        
        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_budget = ProjectBudget::update($request,$project_id);

            if(gettype($result_project_budget)=='string')
            {
                return $result_project_budget;
            }

            if(!empty($plan_project_budgets))
            {
                foreach ($plan_project_budgets as $plan_project_budget)
                {
                    $plan_project_budget['project_id']=$project_id;
                    DB::table('plan_project_budgets')->insertGetId($plan_project_budget);
                }
            }
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของงบประมาณได้คะ';
        }


        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_period = ProjectPeriod::update($request,$project_id);

            if(gettype($result_project_period)=='string')
            {
                return $result_project_period;
            }

            if(!empty($plan_project_periods))
            {
                foreach ($plan_project_periods as $plan_project_period)
                {
                    $plan_project_period['project_id']=$project_id;
                    DB::table('plan_project_periods')->insertGetId($plan_project_period);
                }
            }
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของระยะเวลาดำเนินการได้คะ';
        }


        if(is_numeric($project_id) && $project_id>0)
        {
            $result_project_suggestion = ProjectSuggestion::update($request,$project_id);

            if(gettype($result_project_suggestion)=='string')
            {
                return $result_project_suggestion;
            }

            if(!empty($plan_project_suggestions))
            {
                foreach ($plan_project_suggestions as $plan_project_suggestion)
                {
                    $plan_project_suggestion['project_id']=$project_id;
                    DB::table('plan_project_suggestions')->insertGetId($plan_project_suggestion);
                }
            }
            return ['project_id'=>$project_id];
        }
        else{
            return 'ไม่สามารถบันทึกข้อมูลในส่วนของปัญหาอุปสรรค/ข้อเสนอแนะได้คะ';
        }
    }

    public static function delete($budget_plan_id)
    {
       $project =  DB::table('plan_projects')->where('budget_plan_id',$budget_plan_id)->first();
       if(!empty($project))
       {
            $project_id = $project->project_id;
            DB::table('plan_projects')->where('budget_plan_id',$budget_plan_id)->update([
                'deleted_at'=>date('Y-m-d H:i:s')
            ]);

            DB::table('plan_project_activities')->where('project_id',$project_id)->update([
                 'deleted_at'=>date('Y-m-d H:i:s')
            ]);

            DB::table('plan_project_benefits')->where('project_id',$project_id)->update([
                 'deleted_at'=>date('Y-m-d H:i:s')
            ]);

            DB::table('plan_project_budgets')->where('project_id',$project_id)->update([
                 'deleted_at'=>date('Y-m-d H:i:s')
            ]);
            DB::table('plan_project_periods')->where('project_id',$project_id)->update([
                 'deleted_at'=>date('Y-m-d H:i:s')
            ]);
            DB::table('plan_project_suggestions')->where('project_id',$project_id)->update([
                 'deleted_at'=>date('Y-m-d H:i:s')
            ]);
       }
    }
}