<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;


class ProjectForm9
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
    	$items=[];
        if(!empty($budgetPlanProject))
        {
            $items=$budgetPlanProject->project_goal;
        }
        if(!empty($view))
        {
            $html= view('plan::form-plan/project/tab-browse/project-tab9', [
            'budgetPlanProject'=> $budgetPlanProject,
            'items'=> (!empty($items)) ? $items : null,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/project/tab/project-tab9', [
            'budgetPlanProject'=> $budgetPlanProject,
            'items'=> (!empty($items)) ? $items : null,
            ]);
        }
        return $html->render();
    }
}