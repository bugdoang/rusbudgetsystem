<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class ProjectBudget
{
    public static function save($request,$project_id)
    {
        $budgets = $request->budgets;
        if(!empty($budgets))
        {
            $budgets = json_decode($budgets, true);
            if (!empty($budgets) && is_array($budgets))
            {
                $plan_project_budgets = [];
                foreach ($budgets as $index=>$item)
                {
                    if(is_numeric($index) && isset($item['child']) &&  !empty($item['child']))
                    {
                        foreach ($item['child'] as $child) {
                            $plan_project_budgets[] = [
                                'project_budget_name'=>$child['project_budget_name'],
                                'project_budget_price'=>$child['project_budget_price'],
                                'budget_activity'=>$index+1,
                                'project_id'=>$project_id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                    }
                }
                if(!empty($plan_project_budgets))
                {
                    DB::table('plan_project_budgets')->insert($plan_project_budgets);
                    return true;
                }
            }
        }
        return 'กรุณากรอกข้อมูลงบประมาณด้วยคะ';
    }

    public static function update($request,$project_id)
    {
        $budgets = $request->budgets;

        DB::table('plan_project_budgets')
            ->where('project_id',$project_id)
            ->delete();

        if(!empty($budgets))
        {
            $budgets = json_decode($budgets, true);
            if (!empty($budgets) && is_array($budgets))
            {
                $plan_project_budgets = [];
                foreach ($budgets as $index=>$item)
                {
                    if(is_numeric($index) && isset($item['child']) &&  !empty($item['child']))
                    {
                        foreach ($item['child'] as $child) {
                            $plan_project_budgets[] = [
                                'project_budget_name'=>$child['project_budget_name'],
                                'project_budget_price'=>$child['project_budget_price'],
                                'budget_activity'=>$index+1,
                                'project_id'=>$project_id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                    }
                }
                if(!empty($plan_project_budgets))
                {
                    DB::table('plan_project_budgets')->insert($plan_project_budgets);
                    return true;
                }
            }
        }
        return 'กรุณากรอกในส่วนของงบประมาณด้วยคะ';
    }
}