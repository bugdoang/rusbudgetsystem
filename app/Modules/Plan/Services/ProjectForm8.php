<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;


class ProjectForm8
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {   
        $months=config('myconfig.months');
        $startYear=config('myconfig.start_year');
        if(!empty($view))
        {
            $html= view('plan::form-plan/project/tab-browse/project-tab8', [
            'months' => $months,
            'startYear' => $startYear,
            'budgetPlanProject'=>$budgetPlanProject,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/project/tab/project-tab8', [
            'months' => $months,
            'startYear' => $startYear,
            'budgetPlanProject'=>$budgetPlanProject,
            ]);
        }
        return $html->render();
    }
}