<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;
use App\Helepers\DateFormat;

class ProjectForm6
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        if (isset($budgetPlanProject->plan_project_activities) && !empty($budgetPlanProject->plan_project_activities)) {
            foreach ($budgetPlanProject->plan_project_activities as $_item) {
                if(!empty($_item->project_activity_name))
                {
                    $items[] = [
                        'project_activity_name' => $_item->project_activity_name,
                        'project_activity_date' => DateFormat::eng_to_thai($_item->project_activity_date)
                    ];
                }

            }
        }
        if(!empty($view))
        {
            $html = view('plan::form-plan/project/tab-browse/project-tab6', [
            'items' => $items
            ]);
        }
        else
        {
            $html = view('plan::form-plan/project/tab/project-tab6', [
            'items' => $items
            ]);
        }
        return $html->render();
    }
}