<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */
namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;

class DurableForm3
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $items = array();
        $price_net = 0;
        if(isset($budgetPlanDurable->plan_durable_article_items) && !empty($budgetPlanDurable->plan_durable_article_items))
        {
            foreach($budgetPlanDurable->plan_durable_article_items as $_item)
            {
                $items[] = [
                    'durable_article_standard_id'=>$_item->durable_article_item_id,
                    'durable_article_standard_item' =>$_item->durable_article_item_name,
                    'durable_article_unit' =>$_item->durable_article_unit,
                    'durable_article_qty' =>$_item->durable_article_qty,
                    'durable_article_priceunit' =>$_item->durable_article_priceunit,
                    'durable_article_num' =>$_item->durable_article_num,
                    'durable_article_status' =>$_item->durable_article_status,
                    'durable_article_reason' =>$_item->durable_article_reason,
                    'children'=>json_decode($_item->durable_article_item_detail,TRUE)
                ];
                $price_net+=$_item->durable_article_num;
            }
        }
        $lists=config('myconfig.lists');

        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab3',[
                'lists' => $lists,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'items'=>$items,
                'price_net'=>$price_net
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab3',[
                'lists' => $lists,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'items'=>$items,
                'price_net'=>$price_net
            ]);
        }

        return $html->render();
    }
}