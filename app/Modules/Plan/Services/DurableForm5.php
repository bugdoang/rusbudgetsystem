<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;

class DurableForm5 
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $items = array();
        if (isset($budgetPlanDurable->plan_objectives) && !empty($budgetPlanDurable->plan_objectives)) {
            foreach ($budgetPlanDurable->plan_objectives as $_item) {
                if(!empty($_item->objective_name))
                {
                    $items[] = [
                        'objective_name' => $_item->objective_name
                    ];
                }

            }
        }
        if (!empty($view))
        {
            $html = view('plan::form-plan/durable-article/tab-browse/durable-article-tab5', [
                'items' => $items
            ]);
        }
        else
        {
            $html = view('plan::form-plan/durable-article/tab/durable-article-tab5', [
                'items' => $items
            ]);
        }

        return $html->render();
    }
}