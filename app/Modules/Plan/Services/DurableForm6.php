<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;

class DurableForm6
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $items=[];
        if(!empty($budgetPlanDurable))
        {
            $items=json_decode($budgetPlanDurable->durable_article_frequency,true);
        }
        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab6',[
                'budgetPlanDurable'=>$budgetPlanDurable,
                'items'=>$items
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab6',[
                'budgetPlanDurable'=>$budgetPlanDurable,
                'items'=>$items
            ]);
        }

        return $html->render();
    }
}