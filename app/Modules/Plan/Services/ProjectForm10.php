<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;

class ProjectForm10
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        $project_budget_net = 0;
        if(isset($budgetPlanProject->plan_project_budgets) && !empty($budgetPlanProject->plan_project_budgets))
        {
            foreach($budgetPlanProject->plan_project_budgets as $_item)
            {   
                // project_budget_total:0,
                // child:[
                //     {
                //     project_budget_name:'',
                //     project_budget_price:0
                //     }
                // ]

                    $key = $_item->budget_activity-1;
                if(!isset($items[$key]))
                {
                    $items[$key] = [];
                    $items[$key]['project_budget_total'] = 0;

                }
                $items[$key]['project_budget_total'] += $_item->project_budget_price;

                $items[$key]['child'][] = [
                    'project_budget_name' =>$_item->project_budget_name,
                    'project_budget_price' =>$_item->project_budget_price,
                ];
                $project_budget_net+=$_item->project_budget_price;
            }
        }
        if(!empty($view))
        {
            $html = view('plan::form-plan/project/tab-browse/project-tab10', [
            'items' => $items,
            'project_budget_net' => $project_budget_net,
            'budgetPlanProject' => $budgetPlanProject,
            ]);
        }
        else
        {
            $html = view('plan::form-plan/project/tab/project-tab10', [
            'items' => $items,
            'project_budget_net' => $project_budget_net,
            'budgetPlanProject' => $budgetPlanProject,
            ]);
        }
        return $html->render();
    }
}