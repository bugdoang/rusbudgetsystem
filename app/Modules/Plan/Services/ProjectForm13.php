<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanProjectModel;

class ProjectForm13
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        if (isset($budgetPlanProject->plan_committees) && !empty($budgetPlanProject->plan_committees)) {
            foreach ($budgetPlanProject->plan_committees as $_item) {
                if(!empty($_item->committee_id))
                {
                    $items[] = [
                        'committee_id' => $_item->personnel_id,
                        'committee_name' => $_item->committee_name
                    ];
                }
            }
        }
        $personnels=PlanProjectModel::getPersonnel();
        if(!empty($view))
        {
            $html= view('plan::form-plan/project/tab-browse/project-tab13',[
            'items' => $items,
            'budgetPlanProject'=>$budgetPlanProject,
            'personnels' =>  $personnels,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/project/tab/project-tab13',[
            'items' => $items,
            'budgetPlanProject'=>$budgetPlanProject,
            'personnels' =>  $personnels,
            ]);
        }
        return $html->render();
    }
}