<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class ProjectSuggestion
{
    public static function save($request,$project_id)
    {
        $list12 = $request->list12;
        if(!empty($list12))
        {
            $list12 = json_decode($list12, true);
            if (!empty($list12) && is_array($list12))
            {
                $plan_project_suggestions = [];
                foreach ($list12 as $index=>$item)
                {
                    if(isset($item['project_suggestion_name']) &&  !empty($item['project_suggestion_name']))
                    {
                        $plan_project_suggestions[] = [
                            'project_suggestion_name'=>$item['project_suggestion_name'],
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_suggestions))
                {
                    DB::table('plan_project_suggestions')->insert($plan_project_suggestions);
                    return true;
                }
            }
        }
        return 'กรุณากรอกปัญหาอุปสรรค/ข้อเสนอแนะด้วยคะ';
    }

    public static function update($request,$project_id)
    {
        $list12 = $request->list12;

        DB::table('plan_project_suggestions')
            ->where('project_id',$project_id)
            ->delete();

        if(!empty($list12))
        {
            $list12 = json_decode($list12, true);
            if (!empty($list12) && is_array($list12))
            {
                $plan_project_suggestions = [];
                foreach ($list12 as $index=>$item)
                {
                    if(isset($item['project_suggestion_name']) &&  !empty($item['project_suggestion_name']))
                    {
                        $plan_project_suggestions[] = [
                            'project_suggestion_name'=>$item['project_suggestion_name'],
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_suggestions))
                {
                    DB::table('plan_project_suggestions')->insert($plan_project_suggestions);
                    return true;
                }
            }
        }
        return 'กรุณากรอกปัญหาอุปสรรค/ข้อเสนอแนะด้วยคะ';
    }
}