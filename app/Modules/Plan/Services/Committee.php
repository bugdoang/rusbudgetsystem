<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;


use DB;

class Committee
{
    public static function save($request,$budget_plan_id)
    {
        $list9 = $request->list9;
        if(!empty($list9))
        {
            $list9 = json_decode($list9, true);
            if (!empty($list9) && is_array($list9))
            {
                $plan_committees_check = [];
                $plan_committees = [];
                foreach ($list9 as $index=>$item)
                {
                    if(is_numeric($item['committee_id']) && $item['committee_id'] > 0 )
                    {
                        if(!in_array($item['committee_id'], $plan_committees_check))
                        {
                            $plan_committees_check[] = $item['committee_id'];
                            $plan_committees[] = [
                                'committee_status'=>($index==0)?'ผู้รับผิดชอบหลัก':'กรรมการ',
                                'budget_plan_id'=>$budget_plan_id,
                                'personnel_id'=>$item['committee_id'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                        else
                        {
                            return 'ขออภัย มีรายชื่อคณะกรรมการซ้ำกันคะ';
                        } 
                    }
                }
                if(!empty($plan_committees))
                {
                    DB::table('plan_committees')->insert($plan_committees);
                    return true;
                }
            }
        }
        return 'กรุณาระบุรายชื่อคณะกรรมการด้วยคะ';
    }

    public static function update($request,$budget_plan_id)
    {
        $list9 = $request->list9;

        DB::table('plan_committees')
            ->where('budget_plan_id',$budget_plan_id)
            ->delete();

        if(!empty($list9))
        {
            $list9 = json_decode($list9, true);
            if (!empty($list9) && is_array($list9))
            {
                $plan_committees_check = [];
                $plan_committees = [];
                foreach ($list9 as $index=>$item)
                {
                    if(is_numeric($item['committee_id']) && $item['committee_id'] > 0 )
                    {
                        if(!in_array($item['committee_id'], $plan_committees_check))
                        {
                            $plan_committees_check[] = $item['committee_id'];
                            $plan_committees[] = [
                                'committee_status'=>($index==0)?'ผู้รับผิดชอบหลัก':'กรรมการ',
                                'budget_plan_id'=>$budget_plan_id,
                                'personnel_id'=>$item['committee_id'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                        else
                        {
                            return 'ขออภัย มีรายชื่อคณะกรรมการซ้ำกันคะ';
                        } 
                    }
                }
                if(!empty($plan_committees))
                {
                    DB::table('plan_committees')->insert($plan_committees);
                    return true;
                }
            }
        }
        return 'กรุณาระบุรายชื่อคณะกรรมการด้วยคะ';
    }

    public static function delete($budget_plan_id)
    {
        DB::table('plan_committees')->where('budget_plan_id',$budget_plan_id)->update([
            'deleted_at'=>date('Y-m-d H:i:s')
        ]);

    }

}