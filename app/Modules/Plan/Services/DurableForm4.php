<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;


class DurableForm4
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab4',[
                'budgetPlanDurable'=>$budgetPlanDurable
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab4',[
                'budgetPlanDurable'=>$budgetPlanDurable
            ]);
        }

        return $html->render();
    }
}