<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/14/16
 * Time: 11:57 AM
 */

namespace App\Modules\Plan\Services;
use App\Helepers\DateFormat;

use DB;

class ProjectActivity
{
    public static function save($request,$project_id)
    {
        $list6 = $request->list6;
        if(!empty($list6))
        {
            $list6 = json_decode($list6, true);
            if (!empty($list6) && is_array($list6))
            {
                $plan_project_activities = [];
                foreach ($list6 as $index=>$item)
                {
                    if(isset($item['project_activity_name']) &&  !empty($item['project_activity_name']))
                    {
                        $project_activity_date = DateFormat::thai_to_eng($item['project_activity_date']);
                        $plan_project_activities[] = [
                            'project_activity_name'=>$item['project_activity_name'],
                            'project_activity_date'=>$project_activity_date,
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_activities))
                {
                    DB::table('plan_project_activities')->insert($plan_project_activities);
                    return true;
                }
            }
        }
        return 'กรุณากรอกชื่อกิจกรรมและวันเดือนปีของกิจกรรมด้วยคะ';
    }

    public static function update($request,$project_id)
    {
        $list6 = $request->list6;

        DB::table('plan_project_activities')
            ->where('project_id',$project_id)
            ->delete();

        if(!empty($list6))
        {
            $list6 = json_decode($list6, true);
            if (!empty($list6) && is_array($list6))
            {
                $plan_project_activities = [];
                foreach ($list6 as $index=>$item)
                {
                    if(isset($item['project_activity_name']) &&  !empty($item['project_activity_name']))
                    {
                        $project_activity_date = DateFormat::thai_to_eng($item['project_activity_date']);
                        $plan_project_activities[] = [
                            'project_activity_name'=>$item['project_activity_name'],
                            'project_activity_date'=>$project_activity_date,
                            'project_id'=>$project_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }
                if(!empty($plan_project_activities))
                {
                    DB::table('plan_project_activities')->insert($plan_project_activities);
                    return true;
                }
            }
        }
        return 'กรุณากรอกชื่อกิจกรรมและวันเดือนปีของกิจกรรมด้วยคะ';
    }
}