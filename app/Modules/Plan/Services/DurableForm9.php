<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:05 PM
 */

namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanInformationModel;

class DurableForm9
{
    public static function get($budgetPlanDurable=NULL,$view=NULL)
    {
        $items = array();
        if (isset($budgetPlanDurable->plan_committees) && !empty($budgetPlanDurable->plan_committees)) {
            foreach ($budgetPlanDurable->plan_committees as $_item) {
                if(!empty($_item->committee_id))
                {
                    $items[] = [
                        'committee_id' => $_item->personnel_id,
                        'committee_name' => $_item->committee_name
                    ];
                }
            }
        }
        $personnels=PlanInformationModel::getPersonnel();
        if (!empty($view))
        {
            $html= view('plan::form-plan/durable-article/tab-browse/durable-article-tab9',[
                'items' => $items,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'personnels' =>  $personnels,
            ]);
        }
        else
        {
            $html= view('plan::form-plan/durable-article/tab/durable-article-tab9',[
                'items' => $items,
                'budgetPlanDurable'=>$budgetPlanDurable,
                'personnels' =>  $personnels,
            ]);
        }

        return $html->render();
    }
}