<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 3:04 PM
 */

namespace App\Modules\Plan\Services;
use App\Modules\Plan\Controllers\Plan;
use App\Modules\Plan\Models\PlanProjectModel;

class ProjectForm12 
{
    public static function get($budgetPlanProject = NULL,$view = NULL)
    {
        $items = array();
        if (isset($budgetPlanProject->plan_project_suggestions) && !empty($budgetPlanProject->plan_project_suggestions)) {
            foreach ($budgetPlanProject->plan_project_suggestions as $_item) {
                if(!empty($_item->project_suggestion_name))
                {
                    $items[] = [
                        'project_suggestion_name' => $_item->project_suggestion_name
                    ];
                }
            }
        }
        if(!empty($view))
        {
            $html = view('plan::form-plan/project/tab-browse/project-tab12', [
            'items' => $items
            ]);
        }
        else
        {
            $html = view('plan::form-plan/project/tab/project-tab12', [
            'items' => $items
            ]);
        }
        return $html->render();
    }
}