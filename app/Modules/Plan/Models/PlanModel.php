<?php
namespace App\Modules\Plan\Models;

/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 8/26/16
 * Time: 3:38 PM
 */
use \Illuminate\Database\Eloquent\Model;
use DB;

class PlanModel extends Model
{
    public function me($personnel_id)
    {
        return DB::table('personnels')
            ->whereNull('deleted_at')
            ->where('personnel_id',$personnel_id)//โดย เมมเบอร์ไอดีต้องเท่ากับเมมเบอร์ไอดี
            ->first();
    }
}


