<?php
/**
 * Created by PhpStorm.
 * User: patsarapornkomnunrit
 * Date: 10/10/16
 * Time: 11:25 AM
 */

namespace App\Modules\Plan\Models;
use App\Helepers\DateFormat;
use DB;

class PlanInformationModel
{
    // durable article
    public static function getBudgetPlan($personnel_id=0,$params)
    {
        $q = DB::table('budget_plans')
            ->leftJoin('plan_durable_articles','plan_durable_articles.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('personnels','personnels.personnel_id','=','budget_plans.personnel_id')
            ->leftJoin('positions','positions.position_id','=','personnels.position_id')
            ->leftJoin('groups','groups.group_id','=','personnels.group_id')
            ->select([
                'budget_plans.*',
                'durable_article_name',
                'durable_article_budget_net',
                'upload_file',
                'first_name',
                'position_abbreviation', 
                'group_name',
                // 'group_id',
                'status',
                'amount',
                'money_approve',
                'upload_file_name'
            ]);
            $q->where('budget_plan_category','ครุภัณฑ์');
        if(!empty($params['durable_article_name']))
        {
            $q->where('plan_durable_articles.durable_article_name','LIKE','%'.trim($params['durable_article_name']).'%');
        }
        if(!empty($params['start_date']))
        {
            $start_date = DateFormat::thai_to_eng($params['start_date']);
            if(!empty($start_date))
            {
                $q->where('budget_plans.created_at','>=',$start_date.' 00:00:00');
            }
        }
        if(!empty($params['end_date']))
        {
            $end_date = DateFormat::thai_to_eng($params['end_date']);
            if(!empty($end_date))
            {
                $q->where('budget_plans.created_at','<=',$end_date.' 23:59:59');
            }
        }
        if(!empty($params['status']))
        {
            $q->where('budget_plans.status','LIKE','%'.trim($params['status']).'%');
        }
        if(!empty($params['first_name']))
        {
            $q->where('personnels.first_name','LIKE','%'.trim($params['first_name']).'%');
        }
        if($personnel_id!=0)
        {
            $q->where('budget_plans.personnel_id',$personnel_id);
        }
        $q->orderBy('budget_plan_id','DESC');
        return $q->whereNull('budget_plans.deleted_at')->paginate(10);

    }

    public static function budgetPlanDurableId($budget_plan_id)
    {
        $budget_plan = DB::table('budget_plans')
            ->leftJoin('plan_products','plan_products.product_id','=','budget_plans.product_id')
            ->leftJoin('plan_plans','plan_plans.plan_id','=','budget_plans.plan_id')
            ->leftJoin('plan_incomes','plan_incomes.income_id','=','budget_plans.income_id')
            ->leftJoin('plan_income_types','plan_income_types.income_type_id','=','budget_plans.income_type_id')
            ->leftJoin('plan_income_groups','plan_income_groups.income_group_id','=','budget_plans.income_group_id')
            ->leftJoin('masterplan_key_projects','masterplan_key_projects.key_project_id','=','budget_plans.key_project_id')
            ->leftJoin('masterplan_objective_strategies','masterplan_objective_strategies.objective_strategy_id','=','budget_plans.objective_strategy_id')
            ->leftJoin('masterplan_strategies','masterplan_strategies.strategy_id','=','budget_plans.strategy_id')
            ->leftJoin('masterplan_goals','masterplan_goals.goal_id','=','budget_plans.goal_id')
            ->leftJoin('masterplan_objective_indicators','masterplan_objective_indicators.objective_indicator_id','=','budget_plans.objective_indicator_id')
            ->leftJoin('masterplan_issues','masterplan_issues.issue_id','=','budget_plans.issue_id')
            ->leftJoin('masterplan_federal_circuits','masterplan_federal_circuits.federal_circuit_id','=','budget_plans.federal_circuit_id')
            ->leftJoin('masterplan_visions','masterplan_visions.vision_id','=','budget_plans.vision_id')
            ->leftJoin('plan_durable_articles','plan_durable_articles.budget_plan_id','=','budget_plans.budget_plan_id')
            ->leftJoin('personnels','personnels.personnel_id','=','budget_plans.personnel_id')
            ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
            ->leftJoin('faculties','faculties.faculty_id','=','branches.faculty_id')
            ->where('budget_plans.budget_plan_id',$budget_plan_id)
            ->whereNull('budget_plans.deleted_at')
            ->first();
        if(!empty($budget_plan))
        {
            $temp = DB::table('plan_durable_articles_allocates')
                ->whereNull('deleted_at')
                ->where('durable_article_id',$budget_plan->durable_article_id)
                ->get();
            $budget_plan->plan_durable_articles_allocates = $temp;

            $temp = DB::table('plan_durable_article_items')
                ->whereNull('deleted_at')
                ->where('durable_article_id',$budget_plan->durable_article_id)
                ->get();
            $budget_plan->plan_durable_article_items = $temp;

            $temp = DB::table('plan_objectives')
                ->whereNull('deleted_at')
                ->where('budget_plan_id',$budget_plan->budget_plan_id)
                ->get();
            $budget_plan->plan_objectives = $temp;

            $temp = DB::table('plan_committees')
                ->leftJoin('personnels','personnels.personnel_id','=','plan_committees.personnel_id')
                ->leftJoin('positions','positions.position_id','=','personnels.position_id')
                ->leftJoin('branches','branches.branch_id','=','personnels.branch_id')
                ->leftJoin('campuses','campuses.campus_id','=','personnels.campus_id')
                ->whereNull('plan_committees.deleted_at')
                ->where('budget_plan_id',$budget_plan->budget_plan_id)
                ->get();
            $budget_plan->plan_committees = $temp;
        }
        return $budget_plan;
    }

    public static function getIncome()
    {
        return DB::table('plan_incomes')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIncomeType()
    {
        return DB::table('plan_income_types')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIncomeGroup($income_type_id=0)
    {
        $q= DB::table('plan_income_groups')
            ->whereNull('deleted_at');
        if(is_numeric($income_type_id) && $income_type_id>0)
        {
            $q->where('income_type_id',$income_type_id);
        }
          return  $q->get();
    }
    public static function getFaculty()
    {
        return DB::table('faculties')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getCampus()
    {
        return DB::table('campuses')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getVision()
    {
        return DB::table('masterplan_visions')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getFederalCircuit()
    {
        return DB::table('masterplan_federal_circuits')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getIssue($federal_circuit_id=0)
    {
        $q= DB::table('masterplan_issues')
            ->whereNull('deleted_at');
        if(is_numeric($federal_circuit_id) && $federal_circuit_id>0)
        {
            $q->where('federal_circuit_id',$federal_circuit_id);
        }
        return  $q->get();
    }
    public static function getGoal($issue_id=0)
    {
        $q= DB::table('masterplan_goals')
            ->whereNull('deleted_at');
        if(is_numeric($issue_id) && $issue_id>0)
        {
            $q->where('issue_id',$issue_id);
        }
        return  $q->get();
    }
    public static function getObjectiveIndicator($goal_id=0)
    {
        $q= DB::table('masterplan_objective_indicators')
            ->whereNull('deleted_at');
        if(is_numeric($goal_id) && $goal_id>0)
        {
            $q->where('goal_id',$goal_id);
        }
        return  $q->get();
    }
    public static function getStrategy($goal_id=0)
    {
        $q= DB::table('masterplan_strategies')
            ->whereNull('deleted_at');
        if(is_numeric($goal_id) && $goal_id>0)
        {
            $q->where('goal_id',$goal_id);
        }
        return  $q->get();
    }
    public static function getObjectiveStrategy($strategy_id=0)
    {
        $q= DB::table('masterplan_objective_strategies')
            ->whereNull('deleted_at');
        if(is_numeric($strategy_id) && $strategy_id>0)
        {
            $q->where('strategy_id',$strategy_id);
        }
        return  $q->get();
    }
    public static function getKeyProject($strategy_id=0)
    {
        $q= DB::table('masterplan_key_projects')
            ->whereNull('deleted_at');
        if(is_numeric($strategy_id) && $strategy_id>0)
        {
            $q->where('strategy_id',$strategy_id);
        }
        return  $q->get();
    }
    public static function getPlan()
    {
        return DB::table('plan_plans')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getProduct($plan_id=0)
    {
        $q= DB::table('plan_products')
            ->whereNull('deleted_at');
        if(is_numeric($plan_id) && $plan_id>0)
        {
            $q->where('plan_id',$plan_id);
        }
        return  $q->get();
    }
    public static function getPersonnel()
    {
        return DB::table('personnels')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function getPosition()
    {
        return DB::table('positions')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function update_budget_plan($budget_plan_id,$status)
    {
        return DB::table('budget_plans')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'status'=>$status,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }
    public static function update_durable($budget_plan_id,$money_approve)
    {
        return DB::table('plan_durable_articles')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'money_approve'=>$money_approve,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

    public static function update_upload_file($budget_plan_id,$upload_file,$upload_file_name)
    {
        return DB::table('plan_durable_articles')
            ->where('budget_plan_id',$budget_plan_id)
            ->update([
                'upload_file'=>$upload_file,
                'upload_file_name'=>$upload_file_name,
                'updated_at'=>date('Y-m-d H:i:s')
                ]);
    }

}