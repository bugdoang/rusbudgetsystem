<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    protected $loginPath='/login';
    protected $username='username';

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest',['except'=>['logout','getLogout']]);
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        //print_r($request->all());exit;

        $email =$request->get('username');
        $password =$request->get('password');


        if(empty($email))
        {
            return redirect($this->loginPath)
                ->withInput($request->only('username','remember'))
                ->withErrors([
                    $this->loginUsername()=>'กรุณาป้อนชื่อผู้ใช้ก่อนนะคะ'
                ]);
        }

        if(empty($password))
        {
            return redirect($this->loginPath)
                ->withInput($request->only('password','remember'))
                ->withErrors([
                    $this->loginUsername()=>'กรุณาป้อนรหัสผ่านก่อนนะคะ'
                ]);
        }

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request))
        {
            return $this->sendLockoutResponse($request);
        }

        $credentials    = $this->getCredentials($request);
        $username       = $credentials['username'] ;

        $user = User::where('username',$username)->first();
        if(!empty($user))
        {
            if(Auth::attempt($credentials, $request->has('remember')))
            {
                $throttles = false;
                return $this->handleUserWasAuthenticated($request, $throttles);
            }
            else
            {
                return redirect($this->loginPath)
                    ->withInput($request->only('password','remember'))
                    ->withErrors([
                        $this->loginUsername()=>'ขออภัยค่ะ ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาตรวจสอบด้วยค่ะ'
                    ]);
            }
        }
        else
        {
            return redirect($this->loginPath)
                    ->withInput($request->only('password','remember'))
                    ->withErrors([
                        $this->loginUsername()=>'ขออภัยค่ะ ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาตรวจสอบด้วยค่ะ'
                    ]);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles)
        {
            $this->incrementLoginAttempts($request);
        }
        return redirect($this->loginPath)
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาตรวจสอบด้วยค่ะ',
            ]);
    }
}
