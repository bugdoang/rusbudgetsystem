<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


/**
 * Created by PhpStorm.
 * User: User
 * Date: 27/9/2559
 * Time: 10:31
 */
class DownloadController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {

        $userfile=$request->get('path');
        $userfilename=$request->get('name');
        $userfilename = str_replace(' ', '-', $userfilename);
        $userfile = trim($userfile,'/');
        if(is_file($userfile) && !is_dir($userfile) && file_exists($userfile))
        {
            $filename = basename($userfile);
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment;filename=$userfilename ");
            header("Content-Transfer-Encoding: binary ");
            readfile($userfile);
            exit;
        }
    }
}