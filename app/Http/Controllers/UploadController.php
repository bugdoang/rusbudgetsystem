<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


/**
 * Created by PhpStorm.
 * User: User
 * Date: 27/9/2559
 * Time: 10:31
 */
class UploadController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $userfile=$request->file('userfile');
        $datatype=$request->get('datatype');
        $extension=strtolower($userfile->getClientOriginalExtension());
        if(in_array($extension,['jpg','png','jpeg','pdf','xls','xlsx','doc','docx']))
        {
            if($datatype=='profile')
            {
                $personnel_id = Auth::user()->personnel_id; //ตรวจสอบการล็อคอิน
                file_put_contents('./uploads/profile/' . $personnel_id . '.' . $extension, file_get_contents($userfile->getRealPath()));
                DB::table('personnels')
                    ->where('personnel_id',$personnel_id)
                    ->update([
                        'personnel_picture'=>'/uploads/profile/' . $personnel_id . '.' . $extension
                    ]);
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => '/uploads/profile/' . $personnel_id . '.' . $extension,
                    'status' => 200
                ];
            }
            else if($datatype=='profile-form')
            {
                $file = md5(microtime());
                file_put_contents('./uploads/profile/' .$file. '.' . $extension, file_get_contents($userfile->getRealPath()));
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => '/uploads/profile/' . $file . '.' . $extension,
                    'status' => 200
                ];
            }
            else if($datatype=='upload-plan')
            {
                $file = md5(microtime());
                file_put_contents('./uploads/plan/' .$file. '.' . $extension, file_get_contents($userfile->getRealPath()));
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => '/uploads/plan/' . $file . '.' . $extension,
                    'status' => 200
                ];
            }
        }
        else
        {

        }

        /*  Storage::put(
              $saveFullPath,
              file_get_contents($requestFile->getRealPath())
          );*/
    }
}