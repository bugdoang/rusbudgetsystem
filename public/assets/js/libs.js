var Helper = {

    editContent:function($uri,callback)
    {
        Helper.ajax($uri,'GET','',callback);
    },

    deleteContent:function($uri,callback)
    {
        Helper.ajax($uri,'DELETE','',callback);
    },

    cancelContent:function($uri,callback)
    {
        Helper.ajax($uri,'PUT','',callback);
    },

    ajax:function(url,method,params,callback)
    {
        MessageBox.loading('open');
        var _token=$('#_token').attr('content');
        if(typeof params==='object')
        {
            if(method!='GET') params['_method']=method;
        }
        else
        {
            if(method!='GET')
            {
                if(typeof params=='string' && params!='')
                {
                    params = params+'&_method='+method;
                    params = params+'&_token='+_token;
                }
                else
                {
                    params = {};
                    params['_method']=method;
                    params['_token']=_token;
                }
            }
        }

        $.ajax({
            url:url,
            type:method,
            dataType:'json',
            data:params,
            statusCode: {
                503: function (data)
                {
                    MessageBox.loading('hide');
                    MessageBox.error('<p style="color:red">ขออภัยค่ะ ท่านไม่มีสิทธิในการเข้าใช้งาน กรุณาติดต่อเจ้าหน้าที่</p>',function(){
                        window.location.reload(true);
                    });
                },
                500: function (data)
                {
                    MessageBox.loading('hide');
                    MessageBox.error('<p style="color:red">ขออภัยค่ะ เกิดความผิดพลาด กรุณาติดต่อเจ้าหน้าที่</p>');
                },
                401: function (data)
                {
                    MessageBox.loading('hide');
                    MessageBox.error('<p style="color:red">ขออภัยค่ะ กรุณาล็อกอินเข้าสู่ระบบก่อนค่ะ</p>',function(){
                        window.location.reload(true);
                    });
                },
                422: function (data)
                {
                    MessageBox.loading('hide');
                    if(data.responseJSON.length>1)
                    {
                        var message = '<div style="text-align:left;"><ul style="list-style:none;margin:0;padding:0;">';
                        $.each(data.responseJSON, function (i, item) {
                            message += '<li style="font-size:13px;"><i style="color:red;" class="fa fa-caret-right"></i> ' + item + '</li>';
                        });
                        message += '</ul></div>';
                    }
                    else
                    {
                        var message = '';
                        $.each(data.responseJSON, function (i, item) {
                            message +=item + '<br/>';
                        });
                    }
                    MessageBox.error(message);
                }
            },
            success:function(response)
            {
                MessageBox.loading('hide');
                if(typeof response.export!=='undefined')
                {
                    var fm='<form action="/download" method="get" id="download_export_file"><input value="'+response.file_name+'" type="hidden" name="n"/><input value="'+response.path+'" type="hidden" name="f"/><input type="submit" id="downloadfilesubmit"/></form>';
                    $('body').append(fm);
                    $('#downloadfilesubmit').get(0).click();
                    $('#download_export_file').remove();
                    return false;
                }
                callback(response);
            },
            error:function(response)
            {
                MessageBox.loading('hide');
                if (typeof(response.responseJSON)==='object')
                {
                    var msg = response.responseJSON;
                    var html_msg = '';
                    $.each(msg,function(i,item){
                        html_msg += '<p class="text-danger">'+item+'</p>';
                    });
                    MessageBox.alert(html_msg);
                }
                else
                {
                    MessageBox.error('เกิดความผิดพลาด กรุณาติดต่อเจ้าหน้าที่');
                }
            }
        });
    }
};

(function( $ ) {
    $.fn.myForm = function()
    {
        var $el  = $(this);
        if(!$el.hasClass('no-ajax'))
        {
            $el.submit(function (event) 
            {
                if(typeof CKEDITOR!=='undefined')
                {
                    for(var editor in CKEDITOR.instances)
                    {
                        if(typeof CKEDITOR.instances[editor] !=='undefined')
                        {
                            CKEDITOR.instances[editor].updateElement();
                        }
                    }
                }

                if(typeof beforesend_form==='function')
                {
                    beforesend_form();
                }

                $.ajax({
                    type: $el.attr('method'),
                    url: $el.attr('action'),
                    data: $el.serialize() + '&_method=' + $el.attr('method'),
                    dataType: 'json',
                    encode: true,
                    beforeSend: function (xhr)
                    {
                        if(typeof beforesend_form==='function')
                        {
                            return beforesend_form();
                        }
                        MessageBox.loading('open');
                    },
                    statusCode: {
                        503: function (data)
                        {
                            MessageBox.loading('hide');
                            MessageBox.error('<p style="color:red">ขออภัยค่ะ ท่านไม่มีสิทธิในการเข้าใช้งาน กรุณาติดต่อเจ้าหน้าที่</p>',function(){
                                window.location.reload(true);
                            });
                        },
                        500: function (data)
                        {
                            MessageBox.loading('hide');
                            MessageBox.error('<p style="color:red">ขออภัยค่ะ เกิดความผิดพลาด กรุณาติดต่อเจ้าหน้าที่</p>');
                        },
                        401: function (data)
                        {
                            MessageBox.loading('hide');
                            MessageBox.error('<p style="color:red">ขออภัยค่ะ กรุณาล็อกอินเข้าสู่ระบบก่อนค่ะ</p>',function(){
                                window.location.reload(true);
                            });
                        },
                        422: function (data)
                        {
                            MessageBox.loading('hide');
                            if(data.responseJSON.length >1)
                            {
                                var message = '<div style="text-align:left;"><ul style="list-style:none;margin:0;padding:0;">';
                                $.each(data.responseJSON, function (i, item) {
                                    message += '<li style="font-size:13px;"><i style="color:red;" class="fa fa-caret-right"></i> ' + item + '</li>';
                                });
                                message += '</ul></div>';
                            }
                            else
                            {
                                var message = '';
                                $.each(data.responseJSON, function (i, item) {
                                    message +=item + '<br/>';
                                });
                            }
                            MessageBox.error(message);
                        }
                    }
                }).done(function (data) {
                    MessageBox.loading('hide');
                    if(typeof data.export!=='undefined')
                    {
                        var fm='<form action="/download" method="get" id="download_export_file"><input value="'+data.file_name+'" type="hidden" name="n"/><input value="'+data.path+'" type="hidden" name="f"/><input type="submit" id="downloadfilesubmit"/></form>';
                        $('body').append(fm);
                        $('#downloadfilesubmit').get(0).click();
                        $('#download_export_file').remove();
                        return false;
                    }
                    else if(typeof data.show_html!=='undefined')
                    {
                        var fm='<form action="/download" method="get" id="download_export_file"><input value="'+data.file_name+'" type="hidden" name="n"/><input value="'+data.path+'" type="hidden" name="f"/><input type="submit" id="downloadfilesubmit"/></form>';
                        $('body').append(fm);
                        $('#downloadfilesubmit').get(0).click();
                        $('#download_export_file').remove();
                        return false;
                    }
                    var callback = function () {};
                    if (typeof data.callback !== 'undefined' && typeof window[data.callback]=='function')
                    {
                        window[data.callback](data);
                    }
                    else if (typeof callback_ajax === 'function')
                    {
                        callback_ajax(data);
                    }
                    else
                    {
                        if(typeof data.message!=='undefined')
                        {
                            MessageBox.alert(data.message, callback);
                        }
                        else
                        {
                            MessageBox.alert(data[0],function(){
                                window.location.reload(true);
                            });
                        }
                    }
                });
                event.preventDefault();
            });
        }
    };

    $.fn.popupEditForm = function(url)
    {
        Helper.editContent(url,function(obj){
            if(typeof obj !='undefined')
            {
                if(typeof obj.export!=='undefined')
                {
                    var fm='<form action="/download" method="get" id="download_export_file"><input value="'+obj.file_name+'" type="hidden" name="n"/><input value="'+obj.path+'" type="hidden" name="f"/><input type="submit" id="downloadfilesubmit"/></form>';
                    $('body').append(fm);
                    $('#downloadfilesubmit').get(0).click();
                    $('#download_export_file').remove();
                    return false;
                }
                else if(typeof obj.message !=='undefined')
                {
                    MessageBox.alert(obj.message,function(){
                        window.location.reload(true);
                    });
                }
                var contener_edit = $('#modal-edit-content');
                $('.modal-backdrop').remove();
                contener_edit.find('.modal').modal('hide');
                contener_edit.empty().append(obj.body);
                contener_edit.find('form').each(function(i,e)
                {
                    $(e).myForm();
                });
                contener_edit.find('input.datepicker').each(function(i,e)
                {
                    $(e).myDatePicker();
                });

                contener_edit.find('select').each(function(i,e)
                {
                    $(e).select2Build();
                });
                contener_edit.find('.modal').modal('show');

                contener_edit.find('.uploadfile-btn').each(function(i,e){
                    $(e).uploadFile();
                });
                contener_edit.find('.uploadFileData').each(function(i,e){
                    $(e).uploadFileData();
                });
                
            }
        });
    }
}( jQuery ));

(function( $ ) {
    $.fn.select2Build = function()
    {
        var $el  = $(this);
        var obj = {placeholder: 'กรุณาเลือก', allowClear: true};
        var url = $el.attr('data-ajax--url');
        var clear = $el.attr('data-clear');
        if(typeof clear !=='undefined')
        {
            obj['allowClear'] = false;
        }
        if (typeof url != 'undefined')
        {
            obj['ajax'] = {
                url: url,
                dataType: 'json',
                delay: 250,
                data: function (params)
                {
                    return {
                        q: params.term,
                        page: params.page,
                    };
                },
                processResults: function (data, params)
                {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 25) < data.total_count
                        }
                    };
                },
                cache: true
            }
        }
        else
        {
            if($el.children().length<10)
            {
                obj['minimumResultsForSearch']='Infinity';
            }
        }
        $el.select2(obj);
        if (typeof url != 'undefined')
        {
            $el.on("select2:select", function (e)
            {
                var data = e.params.data;
                var name = $(e.currentTarget).attr('name');
                $('#' + name + "_text").remove();
                $("<input type='hidden' id='" + name + "_text' name='" + name + "_text'>").val(data.text).insertAfter($(e.currentTarget));
            });
        }
    };
}( jQuery ));


(function( $ ) {
    $.fn.uploadFileData = function () {
        var $el = $(this);
        var url = $el.attr('data-url');
        var name  = $el.attr('data-name');
        var callback = $el.attr('data-callback');
        var callbackerror = $el.attr('data-callbackerror');
        var _token=$('#_token').attr('content');
        new ss.SimpleUpload({
            button: $el.get(0),
            url: '/upload',
            data:{_token:_token,datatype:'upload-plan'},
            allowedExtensions: ['pdf','xls','xlsx','doc','docx'],
            name: 'userfile',
            multipart: true,
            responseType: 'json',
            onSubmit: function(){
                MessageBox.loading('open');
            },
            onComplete: function( filename, response )
            {
                MessageBox.loading('hide');
                if(response.status!=200)
                {
                    MessageBox.error(response.message);
                    return false;
                }
                else
                {
                    $('#uploadFileData-container .myfile').remove();
                    $('#uploadFileData-container').append('<a class="myfile" href="'+response.full_path+'"><span>'+response.file_name+'</span></a>');
                    //$('#data-uploadFileData').hide();
                    $("<input type='hidden' id='upload_file' class='myfile' name='upload_file' value='"+response.full_path+"'/>").insertBefore($el);
                    $("<input type='hidden' id='upload_file_name' class='myfile' name='upload_file_name' value='"+response.file_name+"'/>").insertBefore($el);

                }
            },
            onError: function() {
                MessageBox.loading('hide');
            }
        });
    }
}( jQuery ));


(function( $ ) {
    $.fn.uploadFileExpenditureFirst = function () {
        var $el = $(this);
        var url = $el.attr('data-url');
        var name  = $el.attr('data-name');
        var callback = $el.attr('data-callback');
        var callbackerror = $el.attr('data-callbackerror');
        var _token=$('#_token').attr('content');
        new ss.SimpleUpload({
            button: $el.get(0),
            url: '/upload',
            data:{_token:_token,datatype:'upload-plan'},
            allowedExtensions: ['pdf','xls','xlsx','doc','docx'],
            name: 'userfile',
            multipart: true,
            responseType: 'json',
            onSubmit: function(){
                MessageBox.loading('open');
            },
            onComplete: function( filename, response )
            {
                MessageBox.loading('hide');
                if(response.status!=200)
                {
                    MessageBox.error(response.message);
                    return false;
                }
                else
                {
                    $('#uploadFileExpenditureFirst-container .myfile').remove();
                    $('#uploadFileExpenditureFirst-container').append('<a class="myfile" href="'+response.full_path+'"><span>'+response.file_name+'</span></a>');
                    //$('#data-uploadFileData').hide();
                    $("<input type='hidden' id='file_received_first' class='myfile' name='file_received_first' value='"+response.full_path+"'/>").insertBefore($el);
                    $("<input type='hidden' id='filename_received_first' class='myfile' name='filename_received_first' value='"+response.file_name+"'/>").insertBefore($el);

                }
            },
            onError: function() {
                MessageBox.loading('hide');
            }
        });
    }
}( jQuery ));

(function( $ ) {
    $.fn.uploadFileExpenditure = function () {
        var $el = $(this);
        var url = $el.attr('data-url');
        var name  = $el.attr('data-name');
        var callback = $el.attr('data-callback');
        var callbackerror = $el.attr('data-callbackerror');
        var _token=$('#_token').attr('content');
        new ss.SimpleUpload({
            button: $el.get(0),
            url: '/upload',
            data:{_token:_token,datatype:'upload-plan'},
            allowedExtensions: ['pdf','xls','xlsx','doc','docx'],
            name: 'userfile',
            multipart: true,
            responseType: 'json',
            onSubmit: function(){
                MessageBox.loading('open');
            },
            onComplete: function( filename, response )
            {
                MessageBox.loading('hide');
                if(response.status!=200)
                {
                    MessageBox.error(response.message);
                    return false;
                }
                else
                {
                    $('#uploadFileExpenditure-container .myfile').remove();
                    $('#uploadFileExpenditure-container').append('<a class="myfile" href="'+response.full_path+'"><span>'+response.file_name+'</span></a>');
                    //$('#data-uploadFileData').hide();
                    $("<input type='hidden' id='file_received' class='myfile' name='file_received' value='"+response.full_path+"'/>").insertBefore($el);
                    $("<input type='hidden' id='filename_received' class='myfile' name='filename_received' value='"+response.file_name+"'/>").insertBefore($el);

                }
            },
            onError: function() {
                MessageBox.loading('hide');
            }
        });
    }
}( jQuery ));

(function( $ ) {
    $.fn.uploadFile = function () {
        var $el = $(this);
        var url = $el.attr('data-url');
        var name  = $el.attr('data-name');
        var callback = $el.attr('data-callback');
        var callbackerror = $el.attr('data-callbackerror');
        var profile = $el.attr('data-profile');
        var _token=$('#_token').attr('content');
        new ss.SimpleUpload({
            button: $el.get(0),
            url: '/upload',
            data:{_token:_token,datatype:profile},
            allowedExtensions: ['jpg','jpeg','png'],
            name: 'userfile',
            multipart: true,
            responseType: 'json',
            onSubmit: function(){
                MessageBox.loading('open');
            },
            onComplete: function( filename, response )
            {
                MessageBox.loading('hide');
                if(response.status!=200)
                {
                    MessageBox.error(response.message);
                    return false;
                }
                else
                {
                    if(profile=='profile'){
                        $('#personnel_picture').attr('src',response.full_path+'?time='+(new Date()).getTime());
                    }
                    else
                    {
                        $('#image_picter').remove();
                        $('#personnel_picture').attr('src',response.full_path+'?time='+(new Date()).getTime());
                        $("<input type='hidden' id='image_picter' name='image_picter' value='"+response.full_path+"'/>").insertBefore($el);
                    }
                }
            },
            onError: function() {
                MessageBox.loading('hide');
            }
        });
    }
}( jQuery ));

(function( $ ) {
    $.fn.myDatePicker = function()
    {
        var $el = $(this);
        $el.removeClass('datepicker-special');
        var $myparent = $el.prev();
        if($myparent.find('.div-for-picker').size()!=0) return;
        $('<div class="div-for-picker"></div>').insertBefore($el);
        $myparent.append($el);
        $myparent.append('<i class="fa icon-picker fa-calendar"></i>');
        $el.attr('data-date-language','th-th');
        var is_top = $el.attr('data-top');
        var obj={autoclose:true};
        if(typeof is_top!=='undefined') obj['show_top'] = true;
        $el.datepicker(obj);
        $el.on('keypress',function(event){
            var key = event.keyCode || event.charCode;
            if( key == 8 || key == 46 ) return true;
            else return false;
        });
    };

    $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        })
    }

}( jQuery ));
$('.datepicker').each(function(i,e){$(e).myDatePicker()});


$(function(){
    $('.uploadFile').each(function(i,e){$(e).uploadFile();});
    $('.uploadFileData').each(function(i,e){$(e).uploadFileData();});
    $('.uploadFileExpenditureFirst').each(function(i,e){$(e).uploadFileExpenditureFirst();});
    $('.uploadFileExpenditureTwo').each(function(i,e){$(e).uploadFileExpenditureTwo();});
    $('.uploadFileExpenditure').each(function(i,e){$(e).uploadFileExpenditure();});
    $('form').each(function(i,e){$(e).myForm();});
    $.fn.select2.defaults.set("width", "100%");
    $('select').each(function(i,e){$(e).select2Build();});
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    
    $(document).on('click','.edit-content',function(event){
        event.preventDefault();
        var url = $(this).attr('href');
        $.fn.popupEditForm(url);
    });

    $(document).on('click','.cancel-content',function(event){
        event.preventDefault();
        var _that = this;
        var title = $(this).attr('title');
        var message = 'ท่านต้องการที่จะยกเลิกรายการนี้ใช่หรือไม่';
        if(typeof title!='undefined' && title!=='')
        {
            message='ท่านต้องการที่จะยกเลิก <strong>'+title+'</strong> ใช่หรือไม่';
        }
        MessageBox.confirm(message,function()
        {
            var uri = $(_that).attr('href');
            Helper.cancelContent(uri, function (obj) {
                if (typeof obj != 'undefined') {
                    MessageBox.alert(obj[0],function () {
                        window.location.reload(true);
                    });
                }
            });
        });
    });

    $(document).on('click','.delete-content',function(event){
        event.preventDefault();
        var _that = this;
        var title = $(this).attr('title');
        var message = 'ท่านต้องการที่จะลบรายการนี้ใช่หรือไม่';
        if(typeof title!='undefined' && title!=='')
        {
            message='ท่านต้องการที่จะลบ <strong>'+title+'</strong> ใช่หรือไม่';
        }
        MessageBox.confirm(message,function()
        {
            var uri = $(_that).attr('href');
            Helper.deleteContent(uri, function (obj) {
                if (typeof obj != 'undefined') {
                    MessageBox.alert(obj[0],function () {
                        window.location.reload(true);
                    });
                }
            });
        });
    });

    $(document).on('click','.reject-content',function(event){
        event.preventDefault();
        var _that = this;
        var title = $(this).attr('title');
        var message = 'ท่านต้องการที่จะไม่อนุมัติรายการนี้ใช่หรือไม่';
        MessageBox.confirm(message,function()
        {
            var uri = $(_that).attr('href');
            Helper.deleteContent(uri, function (obj) {
                if (typeof obj != 'undefined') {
                    MessageBox.alert('ระบบได้บันทึกข้อมูลเรียบร้อยแล้วคะ',function () {
                        window.location.href=obj.redirect_url;
                    });
                }
            });
        });
    });

    $(document).on('keypress',".numberonly",function (evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=37 && charCode!=39 && charCode!=8)
            return false;
        return true;
    });
    $(document).on('keypress',".numberonlydot",function (evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46 && charCode!=37 && charCode!=39 && charCode!=8)
            return false;
        return true;
    });
    $(document).on('keypress',".numberonlydotlob",function (evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46 && charCode!=45 && charCode!=37 && charCode!=39 && charCode!=8)
            return false;
        return true;
    });
});
