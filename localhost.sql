-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2017 at 10:29 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `motorbigbike_db`
--
CREATE DATABASE IF NOT EXISTS `motorbigbike_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `motorbigbike_db`;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(10) UNSIGNED NOT NULL,
  `brand_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อยี่ห้อ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Benelli', '2016-09-05 20:29:55', NULL, NULL),
(2, 'BMW', '2016-09-05 20:29:55', NULL, NULL),
(3, 'Harley_Davidson', '2016-09-05 20:29:55', NULL, NULL),
(4, 'Honda', '2016-09-05 20:29:55', NULL, NULL),
(5, 'Indian', '2016-09-05 20:29:55', NULL, NULL),
(6, 'Ducati', '2016-09-05 20:29:55', NULL, NULL),
(7, 'Kawasaki', '2016-09-05 20:29:55', NULL, NULL),
(8, 'Suzuki', '2016-09-05 20:29:55', NULL, NULL),
(9, 'Zero Engineering', '2016-09-05 20:29:55', NULL, NULL),
(10, 'Royal Enfield', '2016-09-05 20:29:55', NULL, NULL),
(11, 'Yamaha', '2016-09-05 20:29:55', NULL, NULL),
(12, 'Mv Agusta', '2016-09-05 20:29:55', NULL, NULL),
(13, 'Victory', '2016-09-05 20:29:55', NULL, NULL),
(14, 'KTM', '2016-09-05 20:29:55', NULL, NULL),
(15, 'Triumph', '2016-09-05 20:29:55', NULL, NULL),
(16, 'Zero Motorcycles', '2016-09-05 20:29:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อประเภทรถ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sport', NULL, NULL, NULL),
(2, 'Naked', NULL, NULL, NULL),
(3, 'Crulser', NULL, NULL, NULL),
(4, 'Adventure', NULL, NULL, NULL),
(5, 'Sport Touring', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `color_id` int(10) UNSIGNED NOT NULL,
  `color_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อสี',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`color_id`, `color_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'สีขาว', '2016-09-05 20:29:56', NULL, NULL),
(2, 'สีดำ', '2016-09-05 20:29:56', NULL, NULL),
(3, 'สีแดง', '2016-09-05 20:29:56', NULL, NULL),
(4, 'สีน้ำเงิน', '2016-09-05 20:29:56', NULL, NULL),
(5, 'สีบรอนซ์', '2016-09-05 20:29:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อการติดต่อสอบถาม',
  `contact_detail` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รายละเอียดการติดต่อสอบถาม',
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสินค้า',
  `member_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสมาชิก',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `generations`
--

CREATE TABLE `generations` (
  `generation_id` int(10) UNSIGNED NOT NULL,
  `generation_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อรุ่น',
  `brand_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสยี่ห้อ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `generations`
--

INSERT INTO `generations` (`generation_id`, `generation_name`, `brand_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Benelli BN 600i', 1, '2016-09-05 20:29:56', NULL, NULL),
(2, 'Benelli BN 600j', 1, '2016-09-05 20:29:56', NULL, NULL),
(3, 'Benelli Titanium', 1, '2016-09-05 20:29:56', NULL, NULL),
(4, 'Benelli TRE 1130 K', 1, '2016-09-05 20:29:56', NULL, NULL),
(5, 'BMW C', 2, '2016-09-05 20:29:56', NULL, NULL),
(6, 'BMW F', 2, '2016-09-05 20:29:56', NULL, NULL),
(7, 'BMW K', 2, '2016-09-05 20:29:56', NULL, NULL),
(8, 'BMW R', 2, '2016-09-05 20:29:56', NULL, NULL),
(9, 'BMW S', 2, '2016-09-05 20:29:56', NULL, NULL),
(10, 'Harley_Davidson CVO', 3, '2016-09-05 20:29:56', NULL, NULL),
(11, 'Harley_Davidson Dyna', 3, '2016-09-05 20:29:56', NULL, NULL),
(12, 'Harley_Davidson Night Rod Special', 3, '2016-09-05 20:29:56', NULL, NULL),
(13, 'Harley_Davidson Softail', 3, '2016-09-05 20:29:56', NULL, NULL),
(14, 'Harley_Davidson Sportster', 3, '2016-09-05 20:29:56', NULL, NULL),
(15, 'Harley_Davidson Street', 3, '2016-09-05 20:29:56', NULL, NULL),
(16, 'Honda CB', 4, '2016-09-05 20:29:56', NULL, NULL),
(17, 'Honda CBR', 4, '2016-09-05 20:29:56', NULL, NULL),
(18, 'Honda CRF', 4, '2016-09-05 20:29:56', NULL, NULL),
(19, 'Honda CTX', 4, '2016-09-05 20:29:56', NULL, NULL),
(20, 'Honda Goidwing', 4, '2016-09-05 20:29:56', NULL, NULL),
(21, 'Honda Integra', 4, '2016-09-05 20:29:56', NULL, NULL),
(22, 'Indian Chief Ciassic''', 5, '2016-09-05 20:29:56', NULL, NULL),
(23, 'Indian Chief Vintage', 5, '2016-09-05 20:29:56', NULL, NULL),
(24, 'Indian Chisefrain', 5, '2016-09-05 20:29:56', NULL, NULL),
(25, 'Indian Roadmaster', 5, '2016-09-05 20:29:56', NULL, NULL),
(26, 'Indian Scout', 5, '2016-09-05 20:29:56', NULL, NULL),
(27, 'Ducati 1299 Panigale', 6, '2016-09-05 20:29:56', NULL, NULL),
(28, 'Ducati 959 Panigale', 6, '2016-09-05 20:29:56', NULL, NULL),
(29, 'Ducati Diavel', 6, '2016-09-05 20:29:56', NULL, NULL),
(30, 'Ducati Hypermotard', 6, '2016-09-05 20:29:56', NULL, NULL),
(31, 'Ducati Monster', 6, '2016-09-05 20:29:56', NULL, NULL),
(32, 'Ducati Multistarda', 6, '2016-09-05 20:29:56', NULL, NULL),
(33, 'Ducati Panigale R', 6, '2016-09-05 20:29:56', NULL, NULL),
(34, 'Ducati Scrambler', 6, '2016-09-05 20:29:56', NULL, NULL),
(35, 'Kawasaki 1400GTR', 7, '2016-09-05 20:29:56', NULL, NULL),
(36, 'Kawasaki ER-6n ABS', 7, '2016-09-05 20:29:56', NULL, NULL),
(37, 'Kawasaki Ninja', 7, '2016-09-05 20:29:56', NULL, NULL),
(38, 'Kawasaki Versys', 7, '2016-09-05 20:29:56', NULL, NULL),
(39, 'Kawasaki Vulcan', 7, '2016-09-05 20:29:56', NULL, NULL),
(40, 'Kawasaki Z', 7, '2016-09-05 20:29:56', NULL, NULL),
(41, 'Suzuki Boulevard', 8, '2016-09-05 20:29:56', NULL, NULL),
(42, 'Suzuki Gladius', 8, '2016-09-05 20:29:56', NULL, NULL),
(43, 'Suzuki GSX-R 1000', 8, '2016-09-05 20:29:56', NULL, NULL),
(44, 'Suzuki GSX-S', 8, '2016-09-05 20:29:56', NULL, NULL),
(45, 'Suzuki GSX1300R', 8, '2016-09-05 20:29:56', NULL, NULL),
(46, 'Suzuki SV650', 8, '2016-09-05 20:29:56', NULL, NULL),
(47, 'Suzuki V-Strom', 8, '2016-09-05 20:29:56', NULL, NULL),
(48, 'Zero Engineering Type 5', 9, '2016-09-05 20:29:56', NULL, NULL),
(49, 'Zero Engineering Type 9', 9, '2016-09-05 20:29:56', NULL, NULL),
(50, 'Royal Enfield Bullet 500', 10, '2016-09-05 20:29:56', NULL, NULL),
(51, 'Royal Enfield Classic 500', 10, '2016-09-05 20:29:56', NULL, NULL),
(52, 'Royal Enfield Classic Chrome', 10, '2016-09-05 20:29:56', NULL, NULL),
(53, 'Royal Enfield Continental GT', 10, '2016-09-05 20:29:56', NULL, NULL),
(54, 'Yamaha Bolt', 11, '2016-09-05 20:29:56', NULL, NULL),
(55, 'Yamaha FJ-09', 11, '2016-09-05 20:29:56', NULL, NULL),
(56, 'Yamaha FJR1300A', 11, '2016-09-05 20:29:56', NULL, NULL),
(57, 'Yamaha FZ-09', 11, '2016-09-05 20:29:56', NULL, NULL),
(58, 'Yamaha FZ1 Fazer', 11, '2016-09-05 20:29:56', NULL, NULL),
(59, 'Yamaha FZ8-N', 11, '2016-09-05 20:29:56', NULL, NULL),
(60, 'Yamaha MT-07', 11, '2016-09-05 20:29:56', NULL, NULL),
(61, 'Yamaha MT-09', 11, '2016-09-05 20:29:56', NULL, NULL),
(62, 'Yamaha SR400', 11, '2016-09-05 20:29:56', NULL, NULL),
(63, 'Yamaha Super Tenere', 11, '2016-09-05 20:29:56', NULL, NULL),
(64, 'Yamaha TMAX', 11, '2016-09-05 20:29:56', NULL, NULL),
(65, 'Yamaha YZF-R1', 11, '2016-09-05 20:29:56', NULL, NULL),
(66, 'Mv Agusta Brutale', 12, '2016-09-05 20:29:56', NULL, NULL),
(67, 'Mv Agusta F3', 12, '2016-09-05 20:29:56', NULL, NULL),
(68, 'Mv Agusta F4', 12, '2016-09-05 20:29:56', NULL, NULL),
(69, 'Mv Agusta Rivale 800', 12, '2016-09-05 20:29:56', NULL, NULL),
(70, 'Mv Agusta Stradale 800', 12, '2016-09-05 20:29:56', NULL, NULL),
(71, 'Victory Arlen Ness Victory Vision', 13, '2016-09-05 20:29:56', NULL, NULL),
(72, 'Victory Boaedwalk', 13, '2016-09-05 20:29:56', NULL, NULL),
(73, 'Victory Cross Country ', 13, '2016-09-05 20:29:56', NULL, NULL),
(74, 'Victory Cross Country Tour', 13, '2016-09-05 20:29:56', NULL, NULL),
(75, 'Victory Cross Roads Classic', 13, '2016-09-05 20:29:56', NULL, NULL),
(76, 'Victory Hammer 8-Ball', 13, '2016-09-05 20:29:56', NULL, NULL),
(77, 'Victory Hard-Ball', 13, '2016-09-05 20:29:56', NULL, NULL),
(78, 'Victory High-Ball', 13, '2016-09-05 20:29:56', NULL, NULL),
(79, 'Victory Jackpot', 13, '2016-09-05 20:29:56', NULL, NULL),
(80, 'Victory Judge', 13, '2016-09-05 20:29:56', NULL, NULL),
(81, 'Victory Vegas 8-Ball', 13, '2016-09-05 20:29:56', NULL, NULL),
(82, 'Victory Vision Tour', 13, '2016-09-05 20:29:56', NULL, NULL),
(83, 'KTM 1190 Adventure', 14, '2016-09-05 20:29:56', NULL, NULL),
(84, 'KTM 1190 Adventure R', 14, '2016-09-05 20:29:56', NULL, NULL),
(85, 'KTM 1190 RC8', 14, '2016-09-05 20:29:56', NULL, NULL),
(86, 'KTM 1290', 14, '2016-09-05 20:29:56', NULL, NULL),
(87, 'KTM 450 EKC', 14, '2016-09-05 20:29:56', NULL, NULL),
(88, 'KTM 500 EKC', 14, '2016-09-05 20:29:56', NULL, NULL),
(89, 'KTM 690', 14, '2016-09-05 20:29:56', NULL, NULL),
(90, 'KTM 690 Duke', 14, '2016-09-05 20:29:56', NULL, NULL),
(91, 'KTM 990 Supermoto R', 14, '2016-09-05 20:29:56', NULL, NULL),
(92, 'Triumph Bonneville ', 15, '2016-09-05 20:29:56', NULL, NULL),
(93, 'Triumph Street Triple', 15, '2016-09-05 20:29:56', NULL, NULL),
(94, 'Triumph Street Twin', 15, '2016-09-05 20:29:56', NULL, NULL),
(95, 'Triumph Thrxton', 15, '2016-09-05 20:29:56', NULL, NULL),
(96, 'Triumph Tiger', 15, '2016-09-05 20:29:56', NULL, NULL),
(97, 'Zero Motorcycles DS', 16, '2016-09-05 20:29:56', NULL, NULL),
(98, 'Zero Motorcycles FX', 16, '2016-09-05 20:29:56', NULL, NULL),
(99, 'Zero Motorcycles S', 16, '2016-09-05 20:29:56', NULL, NULL),
(100, 'Zero Motorcycles SR', 16, '2016-09-05 20:29:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `machine_id` int(10) UNSIGNED NOT NULL,
  `machine_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อขนาดเครื่องยนต์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`machine_id`, `machine_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '600.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(2, '1,131.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(3, '647.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(4, '798.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(5, '1,649.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(6, '1,700.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(7, '999.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(8, '1,690.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(9, '1,585.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(10, '1,247.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(11, '883.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(12, '749.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(13, '670.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(14, '1,832.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(15, '745.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(16, '1,731.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(17, '1,285.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(18, '955.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(19, '1,262.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(20, '937.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(21, '821.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(22, '1,198.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(23, '803.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(24, '1,352.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(25, '649.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(26, '1,043.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(27, '805.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(28, '645.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(29, '990.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(30, '1,340.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(31, '1,337.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(32, '1,449.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(33, '499.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(34, '942.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(35, '847.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(36, '1,298.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(37, '847.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(38, '399.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(39, '1,199.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(40, '530.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(41, '998.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(42, '798.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(43, '675.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(44, '798.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(45, '1,731.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(46, '1,195.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(47, '1,301.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(48, '449.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(49, '510.40 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(50, '692.77 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(51, '999.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(52, '865.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(53, '675.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(54, '900.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(55, '1.200.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(56, '800.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL),
(57, '1,000.00 CC. (4 จังหวะ)', '2016-09-05 20:29:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `member_id` int(10) UNSIGNED NOT NULL,
  `member_name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'คำนำหน้าชื่อ',
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `gender` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพศ M = Male F = Female O = Other',
  `dob` date DEFAULT NULL COMMENT 'วันเดือน ปี เกิด',
  `card_id` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รหัสประจำตัวประชาชน',
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เบอร์ติดต่อ',
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'อีเมล์ ใช้ในการล็อกอินเข้าสู่ระบบ',
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'รหัสผ่านที่เข้ารหัสเรียบร้อยแล้ว',
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ที่อยู่',
  `member_pictures` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รูปภาพ',
  `type_of_member` enum('admin','member') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'member' COMMENT 'ประเภทสมาชิก',
  `status_member` enum('inactive','verified','unverified') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unverified' COMMENT 'สถานะสมาชิก',
  `province_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจังหวัด',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`member_id`, `member_name`, `first_name`, `last_name`, `gender`, `dob`, `card_id`, `mobile`, `email`, `password`, `address`, `member_pictures`, `type_of_member`, `status_member`, `province_id`, `created_at`, `updated_at`, `deleted_at`, `remember_token`) VALUES
(1, 'นาย', 'ผู้ดูแลระบบ', 'pppp', 'F', '2016-08-11', '123456789', '099999999', 'chompoo@hotmail.com', '$2y$10$pk.QAR.Yl0yfbDLt1PO8rO5mk.YdEinpnEO1PJK8bdPgD5k3EpvOO', 'xxxx', '/uploads/default/tiffany.png', 'admin', 'verified', 1, NULL, '2016-09-05 21:29:24', NULL, 'e2FUOGZKTusN3tUa1LpVWl5LKjTtlzsD94IpWvhKkAlpYxpGCywCIowiCHk9'),
(2, 'นางสาว', 'ผู้ทดสอบ', 'xxxxx', 'M', '2016-08-11', '123456789', '099999999', 'chompoo@hotmail.com', '$2y$10$LCfmLWo4K9gryQbVeNprVOaApjmJbPyqBv1jgOuAZzCTYEpwsbz4q', 'xxxx', '/uploads/default/tiffany.png', 'admin', 'verified', 8, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_08_22_084638_members', 1),
('2016_08_22_102050_products', 1),
('2016_08_24_085335_brands', 1),
('2016_08_24_091511_generations', 1),
('2016_08_24_094316_colors', 1),
('2016_08_25_030644_pictures', 1),
('2016_08_25_032757_provinces', 1),
('2016_08_25_033058_add_relation_all_table', 1),
('2016_08_26_025407_contacts', 1),
('2016_08_26_040932_machines', 1),
('2016_08_26_042821_add_relation_all_table1', 1),
('2016_08_30_054935_AlterMemberAddToken', 1),
('2016_08_31_093640_categories', 1),
('2016_08_31_094130_add_relation_all_table2', 1),
('2016_09_07_071331_AlterPictureAddisDefault', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `picture_id` int(10) UNSIGNED NOT NULL,
  `picture_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อรูปภาพ',
  `original_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อรูปภาพ',
  `picture_path` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เส้นทางเก็บข้อมูลรูปภาพ',
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสินค้า',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'สถานะรูปหน้า list เริ่มต้น'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`picture_id`, `picture_name`, `original_name`, `picture_path`, `product_id`, `created_at`, `updated_at`, `deleted_at`, `is_default`) VALUES
(1, 'cbr650f', 'cbr', '/uploads/default/cbr650f.png', 1, NULL, NULL, NULL, 'Y'),
(2, 'cb650f', 'cb', '/uploads/default/tiffany2.jpg', 1, NULL, NULL, NULL, 'N'),
(3, 'cc', 'cb', '/uploads/default/tiffany.png', 1, NULL, NULL, NULL, 'N'),
(4, 'xx', 'cb', '/uploads/default/tiffany2.jpg', 2, NULL, NULL, NULL, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อสินค้า',
  `product_price` decimal(8,2) DEFAULT NULL COMMENT 'ราคาสินค้า',
  `product_detail` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รายละเอียดสินค้า',
  `product_date` date DEFAULT NULL COMMENT 'วันที่ลงประกาศ',
  `product_view` int(11) DEFAULT NULL COMMENT 'จำนวนผู้เข้าชม',
  `member_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสมาชิก',
  `generation_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสรุ่น',
  `color_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสี',
  `machine_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสขนาดเครื่องยนต์',
  `category_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเภทรถ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_price`, `product_detail`, `product_date`, `product_view`, `member_id`, `generation_id`, `color_id`, `machine_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CBR650F', '300000.00', 'xxxxxxxx', '2559-08-24', 2, 1, 1, 1, 1, 4, NULL, NULL, NULL),
(2, 'CB650F', '200000.00', 'cccccccccccc', '2559-08-11', 3, 2, 1, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `province_id` int(10) UNSIGNED NOT NULL,
  `province_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อจังหวัด',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`province_id`, `province_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'กรุงเทพมหานคร', NULL, NULL, NULL),
(2, 'สมุทรปราการ', NULL, NULL, NULL),
(3, 'นนทบุรี', NULL, NULL, NULL),
(4, 'ปทุมธานี', NULL, NULL, NULL),
(5, 'พระนครศรีอยุธยา', NULL, NULL, NULL),
(6, 'อ่างทอง', NULL, NULL, NULL),
(7, 'ลพบุรี', NULL, NULL, NULL),
(8, 'สิงห์บุรี', NULL, NULL, NULL),
(9, 'ชัยนาท', NULL, NULL, NULL),
(10, 'สระบุรี', NULL, NULL, NULL),
(11, 'ชลบุรี', NULL, NULL, NULL),
(12, 'ระยอง', NULL, NULL, NULL),
(13, 'จันทบุรี', NULL, NULL, NULL),
(14, 'ตราด', NULL, NULL, NULL),
(15, 'ฉะเชิงเทรา', NULL, NULL, NULL),
(16, 'ปราจีนบุรี', NULL, NULL, NULL),
(17, 'นครนายก', NULL, NULL, NULL),
(18, 'สระแก้ว', NULL, NULL, NULL),
(19, 'นครราชสีมา', NULL, NULL, NULL),
(20, 'บุรีรัมย์', NULL, NULL, NULL),
(21, 'สุรินทร์', NULL, NULL, NULL),
(22, 'ศรีสะเกษ', NULL, NULL, NULL),
(23, 'อุบลราชธานี', NULL, NULL, NULL),
(24, 'ยโสธร', NULL, NULL, NULL),
(25, 'ชัยภูมิ', NULL, NULL, NULL),
(26, 'อำนาจเจริญ', NULL, NULL, NULL),
(27, 'หนองบัวลำภู', NULL, NULL, NULL),
(28, 'ขอนแก่น', NULL, NULL, NULL),
(29, 'อุดรธานี', NULL, NULL, NULL),
(30, 'เลย', NULL, NULL, NULL),
(31, 'หนองคาย', NULL, NULL, NULL),
(32, 'มหาสารคาม', NULL, NULL, NULL),
(33, 'ร้อยเอ็ด', NULL, NULL, NULL),
(34, 'กาฬสินธุ์', NULL, NULL, NULL),
(35, 'สกลนคร', NULL, NULL, NULL),
(36, 'นครพนม', NULL, NULL, NULL),
(37, 'มุกดาหาร', NULL, NULL, NULL),
(38, 'เชียงใหม่', NULL, NULL, NULL),
(39, 'ลำพูน', NULL, NULL, NULL),
(40, 'ลำปาง', NULL, NULL, NULL),
(41, 'อุตรดิตถ์', NULL, NULL, NULL),
(42, 'แพร่', NULL, NULL, NULL),
(43, 'น่าน', NULL, NULL, NULL),
(44, 'พะเยา', NULL, NULL, NULL),
(45, 'เชียงราย', NULL, NULL, NULL),
(46, 'แม่ฮ่องสอน', NULL, NULL, NULL),
(47, 'นครสวรรค์', NULL, NULL, NULL),
(48, 'อุทัยธานี', NULL, NULL, NULL),
(49, 'กำแพงเพชร', NULL, NULL, NULL),
(50, 'ตาก', NULL, NULL, NULL),
(51, 'สุโขทัย', NULL, NULL, NULL),
(52, 'พิษณุโลก', NULL, NULL, NULL),
(53, 'พิจิตร', NULL, NULL, NULL),
(54, 'เพชรบูรณ์', NULL, NULL, NULL),
(55, 'ราชบุรี', NULL, NULL, NULL),
(56, 'กาญจนบุรี', NULL, NULL, NULL),
(57, 'สุพรรณบุรี', NULL, NULL, NULL),
(58, 'นครปฐม', NULL, NULL, NULL),
(59, 'สมุทรสาคร', NULL, NULL, NULL),
(60, 'สมุทรสงคราม', NULL, NULL, NULL),
(61, 'เพชรบุรี', NULL, NULL, NULL),
(62, 'ประจวบคีรีขันธ์', NULL, NULL, NULL),
(63, 'นครศรีธรรมราช', NULL, NULL, NULL),
(64, 'กระบี่', NULL, NULL, NULL),
(65, 'พังงา', NULL, NULL, NULL),
(66, 'ภูเก็ต', NULL, NULL, NULL),
(67, 'สุราษฎร์ธานี', NULL, NULL, NULL),
(68, 'ระนอง', NULL, NULL, NULL),
(69, 'ชุมพร', NULL, NULL, NULL),
(70, 'สงขลา', NULL, NULL, NULL),
(71, 'สตูล', NULL, NULL, NULL),
(72, 'ตรัง', NULL, NULL, NULL),
(73, 'พัทลุง', NULL, NULL, NULL),
(74, 'ปัตตานี', NULL, NULL, NULL),
(75, 'ยะลา', NULL, NULL, NULL),
(76, 'นราธิวาส', NULL, NULL, NULL),
(77, 'บึงกาฬ', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`),
  ADD KEY `contacts_product_id_foreign` (`product_id`),
  ADD KEY `contacts_member_id_foreign` (`member_id`);

--
-- Indexes for table `generations`
--
ALTER TABLE `generations`
  ADD PRIMARY KEY (`generation_id`),
  ADD KEY `generations_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`machine_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `members_province_id_foreign` (`province_id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`picture_id`),
  ADD KEY `pictures_product_id_foreign` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `products_member_id_foreign` (`member_id`),
  ADD KEY `products_generation_id_foreign` (`generation_id`),
  ADD KEY `products_color_id_foreign` (`color_id`),
  ADD KEY `products_machine_id_foreign` (`machine_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`province_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `color_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `generations`
--
ALTER TABLE `generations`
  MODIFY `generation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `machine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `member_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `province_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`),
  ADD CONSTRAINT `contacts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `generations`
--
ALTER TABLE `generations`
  ADD CONSTRAINT `generations_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`);

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`province_id`);

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  ADD CONSTRAINT `products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`color_id`),
  ADD CONSTRAINT `products_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `generations` (`generation_id`),
  ADD CONSTRAINT `products_machine_id_foreign` FOREIGN KEY (`machine_id`) REFERENCES `machines` (`machine_id`),
  ADD CONSTRAINT `products_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`);
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

--
-- Dumping data for table `pma__designer_settings`
--

INSERT INTO `pma__designer_settings` (`username`, `settings_data`) VALUES
('root', '{"angular_direct":"direct","snap_to_grid":"off","relation_lines":"true"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{"db":"rusbudgetsystem_db","table":"plan_project_periods"},{"db":"rusbudgetsystem_db","table":"plan_project_benefits"},{"db":"rusbudgetsystem_db","table":"plan_project_suggestions"},{"db":"rusbudgetsystem_db","table":"plan_durable_article_items"},{"db":"rusbudgetsystem_db","table":"plan_project_budgets"},{"db":"rusbudgetsystem_db","table":"plan_project_activities"},{"db":"rusbudgetsystem_db","table":"plan_projects"},{"db":"rusbudgetsystem_db","table":"plan_committees"},{"db":"rusbudgetsystem_db","table":"plan_durable_articles"},{"db":"rusbudgetsystem_db","table":"personnels"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'rusbudgetsystem_db', 'budget_plans', '{"sorted_col":"`budget_plans`.`approval_status_1` ASC"}', '2017-06-26 02:46:28'),
('root', 'rusbudgetsystem_db', 'plan_durable_articles', '{"sorted_col":"`plan_durable_articles`.`money_approve` ASC"}', '2017-06-20 05:04:13'),
('root', 'rusbudgetsystem_db', 'plan_incomes', '{"sorted_col":"`plan_incomes`.`income_name` ASC"}', '2017-06-20 07:41:08'),
('root', 'rusbudgetsystem_db', 'plan_project_budgets', '{"sorted_col":"`plan_project_budgets`.`project_budget_price`  ASC"}', '2017-06-29 04:30:35'),
('root', 'rusbudgetsystem_db', 'plan_projects', '{"sorted_col":"`plan_projects`.`project_id` ASC"}', '2017-06-20 07:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2016-08-24 04:02:09', '{"collation_connection":"utf8mb4_unicode_ci"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `project_test`
--
CREATE DATABASE IF NOT EXISTS `project_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `project_test`;

-- --------------------------------------------------------

--
-- Table structure for table `borrikans`
--

CREATE TABLE `borrikans` (
  `borrikan_id` int(10) UNSIGNED NOT NULL,
  `borrikan_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อบริการ',
  `borrikan_sum` decimal(10,2) DEFAULT NULL COMMENT 'ราคา',
  `car_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branchs`
--

CREATE TABLE `branchs` (
  `branch_id` int(10) UNSIGNED NOT NULL,
  `branch_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `faculty_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branchs`
--

INSERT INTO `branchs` (`branch_id`, `branch_name`, `faculty_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '555555', 2, '2017-06-18 21:43:03', '2017-06-18 21:43:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_types`
--

CREATE TABLE `car_types` (
  `car_type_id` int(10) UNSIGNED NOT NULL,
  `car_type_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อประเภทบริการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_types`
--

INSERT INTO `car_types` (`car_type_id`, `car_type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '555555', '2017-06-18 20:49:53', '2017-06-18 20:51:02', NULL),
(3, 'kmkkkmoikm', '2017-06-18 22:10:33', '2017-06-18 22:10:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `car_id` int(10) UNSIGNED NOT NULL,
  `car_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ทะเบียน',
  `run_id` int(10) UNSIGNED NOT NULL,
  `lookka_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `class_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อระดับชั้น',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emp`
--

CREATE TABLE `emp` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อภาคการศึกษา',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emps`
--

CREATE TABLE `emps` (
  `emp_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `emp_address` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ที่อยู่',
  `tel_address` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เบอร์โทรศัพท์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emps`
--

INSERT INTO `emps` (`emp_id`, `first_name`, `last_name`, `emp_address`, `tel_address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ภัสราภรณ์', 'คำนวนฤทธิ์', 'ที่อยู่', '0904230818', '2017-06-18 20:01:58', '2017-06-18 20:19:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `faculty_id` int(10) UNSIGNED NOT NULL,
  `faculty_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`faculty_id`, `faculty_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ทมงวม', '2017-06-18 21:42:54', '2017-06-18 21:42:54', NULL),
(2, 'บ้า', '2017-06-18 21:43:25', '2017-06-18 21:43:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lookkas`
--

CREATE TABLE `lookkas` (
  `lookka_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `lookka_address` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ที่อยู่',
  `lookka_tel` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เบอร์โทรศัพท์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lookkas`
--

INSERT INTO `lookkas` (`lookka_id`, `first_name`, `last_name`, `lookka_address`, `lookka_tel`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'สุรางคณา', 'นรินทรางกูร ณ อยุธยา', 'ไร้ที่อยู่', '0904230818', '2017-06-18 20:32:20', '2017-06-18 20:35:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_01_16_030221_migrations', 1),
(2, '2017_01_17_021231_terms', 1),
(3, '2017_01_17_021501_teachers', 1),
(4, '2017_01_17_022314_faculties', 1),
(5, '2017_03_06_163730_branchs', 1),
(6, '2017_03_06_164004_relation_1', 1),
(7, '2017_03_07_125949_student', 1),
(8, '2017_03_09_070022_relation_2', 1),
(9, '2017_03_14_150953_project', 1),
(10, '2017_06_15_030615_classes', 1),
(11, '2017_06_15_030650_years', 1),
(12, '2017_06_15_031748_project_types', 1),
(13, '2017_06_15_034220_alterTeacherAddBranch', 1),
(14, '2017_06_15_034720_relation_4', 1),
(15, '2017_06_19_021725_emps', 1),
(16, '2017_06_19_022401_lookkas', 2),
(17, '2017_06_19_022544_yeehors', 3),
(18, '2017_06_19_022700_runs', 4),
(19, '2017_06_19_022918_car_types', 5),
(20, '2017_06_19_023052_borrikans', 6),
(21, '2017_06_19_023332_cars', 7),
(22, '2017_06_19_023553_use_borrikans', 8),
(23, '2017_06_19_041111_relation_5', 9),
(24, '2017_06_19_045736_relation_6', 10);

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `project_type_id` int(10) UNSIGNED NOT NULL,
  `project_type_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อประเภทโครงงาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `project_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อโครงงาน',
  `project_dob` date DEFAULT NULL COMMENT 'วดป.ที่จัดทำโครงงาน',
  `terms_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `runs`
--

CREATE TABLE `runs` (
  `run_id` int(10) UNSIGNED NOT NULL,
  `run_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อรุ่น',
  `yeehor_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `runs`
--

INSERT INTO `runs` (`run_id`, `run_name`, `yeehor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ซิตี้', 1, '2017-06-18 21:29:15', '2017-06-18 21:54:07', NULL),
(2, 'วีออส', 2, '2017-06-18 21:29:28', '2017-06-18 21:29:28', NULL),
(4, 'แอตทราจ', 3, '2017-06-18 21:51:34', '2017-06-18 21:54:42', NULL),
(6, ',', 3, '2017-06-18 22:09:34', '2017-06-18 22:10:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `gender` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพศ m=male f=female o=other',
  `branch_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `gender` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพศ m=male f=female o=other',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `use_borrikans`
--

CREATE TABLE `use_borrikans` (
  `use_borrikan_id` int(10) UNSIGNED NOT NULL,
  `use_borrikan_dob` date DEFAULT NULL COMMENT 'วดป.',
  `use_borrikan_sum` decimal(10,2) DEFAULT NULL COMMENT 'ราคารวม',
  `car_id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `year_id` int(10) UNSIGNED NOT NULL,
  `year_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อปีการศึกษา',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `yeehors`
--

CREATE TABLE `yeehors` (
  `yeehor_id` int(10) UNSIGNED NOT NULL,
  `yeehor_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อยี่ห้อ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `yeehors`
--

INSERT INTO `yeehors` (`yeehor_id`, `yeehor_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ฮอนด้า', '2017-06-18 21:04:19', '2017-06-18 21:04:19', NULL),
(2, 'โตโยต้า', '2017-06-18 21:04:27', '2017-06-18 21:04:27', NULL),
(3, 'มิตซู', '2017-06-18 21:04:34', '2017-06-18 21:08:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `borrikans`
--
ALTER TABLE `borrikans`
  ADD PRIMARY KEY (`borrikan_id`),
  ADD KEY `borrikans_car_type_id_foreign` (`car_type_id`);

--
-- Indexes for table `branchs`
--
ALTER TABLE `branchs`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `branchs_faculty_id_foreign` (`faculty_id`);

--
-- Indexes for table `car_types`
--
ALTER TABLE `car_types`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `emp`
--
ALTER TABLE `emp`
  ADD PRIMARY KEY (`term_id`);

--
-- Indexes for table `emps`
--
ALTER TABLE `emps`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `lookkas`
--
ALTER TABLE `lookkas`
  ADD PRIMARY KEY (`lookka_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`project_type_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`run_id`),
  ADD KEY `runs_yeehor_id_foreign` (`yeehor_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`teacher_id`),
  ADD KEY `teachers_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `use_borrikans`
--
ALTER TABLE `use_borrikans`
  ADD PRIMARY KEY (`use_borrikan_id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`year_id`);

--
-- Indexes for table `yeehors`
--
ALTER TABLE `yeehors`
  ADD PRIMARY KEY (`yeehor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `borrikans`
--
ALTER TABLE `borrikans`
  MODIFY `borrikan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `branchs`
--
ALTER TABLE `branchs`
  MODIFY `branch_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `car_types`
--
ALTER TABLE `car_types`
  MODIFY `car_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `car_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emp`
--
ALTER TABLE `emp`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emps`
--
ALTER TABLE `emps`
  MODIFY `emp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `faculty_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lookkas`
--
ALTER TABLE `lookkas`
  MODIFY `lookka_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `project_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `runs`
--
ALTER TABLE `runs`
  MODIFY `run_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `teacher_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `use_borrikans`
--
ALTER TABLE `use_borrikans`
  MODIFY `use_borrikan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `year_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `yeehors`
--
ALTER TABLE `yeehors`
  MODIFY `yeehor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `borrikans`
--
ALTER TABLE `borrikans`
  ADD CONSTRAINT `borrikans_car_type_id_foreign` FOREIGN KEY (`car_type_id`) REFERENCES `car_types` (`car_type_id`);

--
-- Constraints for table `branchs`
--
ALTER TABLE `branchs`
  ADD CONSTRAINT `branchs_faculty_id_foreign` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`faculty_id`);

--
-- Constraints for table `runs`
--
ALTER TABLE `runs`
  ADD CONSTRAINT `runs_yeehor_id_foreign` FOREIGN KEY (`yeehor_id`) REFERENCES `yeehors` (`yeehor_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branchs` (`branch_id`);

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branchs` (`branch_id`);
--
-- Database: `project_test2`
--
CREATE DATABASE IF NOT EXISTS `project_test2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `project_test2`;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(10) UNSIGNED NOT NULL,
  `branch_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อสาขา',
  `faculty_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `faculty_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'เทคโนโลยีสารสนเทศธุรกิจ', 1, '2017-03-14 03:39:01', '2017-03-14 03:39:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `faculty_id` int(10) UNSIGNED NOT NULL,
  `faculty_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อคณะ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`faculty_id`, `faculty_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'บริหารธุรกิจและเทคโนโลยีสารสนเทศ', '2017-03-14 03:38:49', '2017-03-14 03:38:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lavels`
--

CREATE TABLE `lavels` (
  `lavel_id` int(10) UNSIGNED NOT NULL,
  `lavel_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อระดับชั้น',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lavels`
--

INSERT INTO `lavels` (`lavel_id`, `lavel_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ปริญญาตรี', '2017-03-14 03:39:28', '2017-03-14 04:45:03', NULL),
(2, 'ปวส.', '2017-03-14 04:45:14', '2017-03-14 04:45:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_03_14_042109_faculty', 1),
(2, '2017_03_14_050004_branch', 1),
(3, '2017_03_14_055153_year', 1),
(4, '2017_03_14_063011_lavel', 1),
(5, '2017_03_14_064926_project_type', 1),
(6, '2017_03_14_071417_professor', 1),
(7, '2017_03_14_072016_relation_branch', 1),
(8, '2017_03_14_081227_project', 1),
(9, '2017_03_14_092433_students', 1),
(10, '2017_03_14_093910_project_datail', 1),
(11, '2017_03_14_100746_relation_proferror', 1),
(12, '2017_03_14_103353_relation_student', 2);

-- --------------------------------------------------------

--
-- Table structure for table `professors`
--

CREATE TABLE `professors` (
  `professor_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่ออาจารย์',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `gender` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพศ m=male f=female o=other',
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `professors`
--

INSERT INTO `professors` (`professor_id`, `first_name`, `last_name`, `gender`, `branch_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'วัชรชัย', 'คูหาสวัสดิ์', 'M', 1, '2017-03-14 03:40:04', '2017-03-14 03:40:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `project_type_id` int(10) UNSIGNED NOT NULL,
  `project_type_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อประเภทโครงงาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`project_type_id`, `project_type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'เว็บไซต์', '2017-03-14 03:39:41', '2017-03-14 03:39:41', NULL),
(2, 'ระบบงาน', '2017-03-14 03:39:47', '2017-03-14 03:39:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `project_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อโครงงาน',
  `year_id` int(10) UNSIGNED DEFAULT NULL,
  `project_type_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อนักศึกษา',
  `last_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'นามสกุล',
  `gender` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพศ m=male f=female o=other',
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `lavel_id` int(10) UNSIGNED DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `first_name`, `last_name`, `gender`, `branch_id`, `lavel_id`, `project_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'สุรางคณา', 'นรินทรางกูร ณ อยุธยา', 'F', 1, 1, NULL, '2017-03-14 04:09:15', '2017-03-14 04:10:20', NULL),
(3, 'ภัสราภรณ์', 'คำนวนฤทธิ์', 'F', 1, 1, NULL, '2017-03-14 04:09:38', '2017-03-14 04:09:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `year_id` int(10) UNSIGNED NOT NULL,
  `year_name` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อปีการศึกษา',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `years`
--

INSERT INTO `years` (`year_id`, `year_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2556', '2017-03-14 03:39:12', '2017-03-14 03:39:12', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `branches_faculty_id_foreign` (`faculty_id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `lavels`
--
ALTER TABLE `lavels`
  ADD PRIMARY KEY (`lavel_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`professor_id`),
  ADD KEY `professors_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`project_type_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_branch_id_foreign` (`branch_id`),
  ADD KEY `students_project_id_foreign` (`project_id`),
  ADD KEY `students_lavel_id_foreign` (`lavel_id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`year_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `branch_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `faculty_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lavels`
--
ALTER TABLE `lavels`
  MODIFY `lavel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `professors`
--
ALTER TABLE `professors`
  MODIFY `professor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `project_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `year_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_faculty_id_foreign` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`faculty_id`);

--
-- Constraints for table `professors`
--
ALTER TABLE `professors`
  ADD CONSTRAINT `professors_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`branch_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`branch_id`),
  ADD CONSTRAINT `students_lavel_id_foreign` FOREIGN KEY (`lavel_id`) REFERENCES `lavels` (`lavel_id`),
  ADD CONSTRAINT `students_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);
--
-- Database: `rusbudgetsystem_db`
--
CREATE DATABASE IF NOT EXISTS `rusbudgetsystem_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `rusbudgetsystem_db`;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(10) UNSIGNED NOT NULL,
  `branch_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อสาขา',
  `faculty_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสคณะ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `faculty_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ระบบเทคโนโลยีสารสนเทศธุรกิจ', 1, '2017-06-30 08:24:02', NULL, NULL),
(2, 'การบัญชี', 1, '2017-06-30 08:24:02', NULL, NULL),
(3, 'การจัดการ', 1, '2017-06-30 08:24:02', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `budget_plans`
--

CREATE TABLE `budget_plans` (
  `budget_plan_id` int(10) UNSIGNED NOT NULL,
  `budget_plan_category` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ประเภทแผน',
  `reason` text COLLATE utf8_unicode_ci COMMENT 'เหตุผลความจำเป็น',
  `fiscal_year` int(10) UNSIGNED NOT NULL COMMENT 'ปีงบประมาณ',
  `faculty` int(10) UNSIGNED NOT NULL COMMENT 'คณะ/หน่วยงาน',
  `semester` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รอบภาคการศึกษา',
  `personnel_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสบุคลากร(เขียนแผนเสนอของบ)',
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสผลผลิต',
  `plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสแผนงาน',
  `income_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสแหล่งเงินได้',
  `income_type_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเภทแหล่งเงินได้',
  `income_group_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'รหัสหมวดแหล่งเงินได้',
  `key_project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการสำคัญ',
  `objective_strategy_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสตัวชี้วัดกลยุทธ์',
  `strategy_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสกลยุทธ์',
  `goal_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสเป้าประสงค์',
  `objective_indicator_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสตัวชี้วัดเป้าประสงค์',
  `issue_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเด็นยุทธศาสตร์',
  `federal_circuit_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสพันธกิจ',
  `vision_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสวิสัยทัศน์',
  `date_approve` datetime DEFAULT NULL COMMENT 'วันที่เสนอขอ',
  `amount` int(10) UNSIGNED NOT NULL COMMENT 'จำนวนเงิน',
  `status` enum('ไม่ได้รับการอนุมัติ','รอการตรวจสอบ','อนุมัติแล้ว') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'รอการตรวจสอบ' COMMENT 'สถานะจัดแผนเสนอของบประมาณ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `campus` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ศูนย์พื้นที่',
  `approval_status` enum('','รอการตรวจสอบ','ไม่อนุมัติ','อนุมัติ') COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'สถานะการขออนุมัติใช้งบประมาณ',
  `approval_status_1` enum('','รอการตรวจสอบ','ไม่อนุมัติ','อนุมัติ') COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'สถานะการขออนุมัติใช้งบประมาณ1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campuses`
--

CREATE TABLE `campuses` (
  `campus_id` int(10) UNSIGNED NOT NULL,
  `campus_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อศูนย์พื้นที่',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `campuses`
--

INSERT INTO `campuses` (`campus_id`, `campus_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'นนทบุรี', '2017-06-30 08:23:59', NULL, NULL),
(2, 'วาสุกรี', '2017-06-30 08:23:59', NULL, NULL),
(3, 'หันตรา', '2017-06-30 08:23:59', NULL, NULL),
(4, 'สุพรรณบุรี', '2017-06-30 08:23:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `faculty_id` int(10) UNSIGNED NOT NULL,
  `faculty_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อคณะ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`faculty_id`, `faculty_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'บริหารธุรกิจและเทคโนโลยีสารสนเทศ', '2017-06-30 08:23:57', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อกลุ่มการใช้งาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ผู้ดูแลระบบ', '2017-06-30 08:24:07', NULL, NULL),
(2, 'ผู้บริหาร', '2017-06-30 08:24:07', NULL, NULL),
(3, 'ฝ่ายแผนและงบประมาณ', '2017-06-30 08:24:07', NULL, NULL),
(4, 'อาจารย์', '2017-06-30 08:24:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `license_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโมดูล',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสกลุ่ม',
  `view` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เรียกดู',
  `edit` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'แก้ไข',
  `add` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'เพิ่ม',
  `delete` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'ลบ',
  `export` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'รายงาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`license_id`, `module_id`, `group_id`, `view`, `edit`, `add`, `delete`, `export`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Y', 'Y', 'Y', 'Y', 'Y', NULL, NULL, NULL),
(2, 1, 2, 'Y', 'N', 'Y', 'N', 'Y', NULL, NULL, NULL),
(3, 1, 2, 'Y', 'Y', 'N', 'Y', 'Y', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_federal_circuits`
--

CREATE TABLE `masterplan_federal_circuits` (
  `federal_circuit_id` int(10) UNSIGNED NOT NULL,
  `federal_circuit_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อพันธกิจ',
  `vision_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสวิสัยทัศน์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_federal_circuits`
--

INSERT INTO `masterplan_federal_circuits` (`federal_circuit_id`, `federal_circuit_name`, `vision_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'การผลิตบัณฑิตที่มีคุณภาพ', 1, '2017-06-30 08:24:14', NULL, NULL),
(2, 'การสร้างงานวิจัยนวัตกรรม', 1, '2017-06-30 08:24:14', NULL, NULL),
(3, 'การบริการวิชาการสู่สังคมชุมชน', 1, '2017-06-30 08:24:14', NULL, NULL),
(4, 'การทํานุบํารุงศิลปวัฒนธรรม  อนุรักษ์ธรรมชาติสิ่งแวดล้อม', 1, '2017-06-30 08:24:14', NULL, NULL),
(5, 'บริหารจัดการที่ทันสมัย  และมีธรรมาภิบาล', 1, '2017-06-30 08:24:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_goals`
--

CREATE TABLE `masterplan_goals` (
  `goal_id` int(10) UNSIGNED NOT NULL,
  `goal_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อเป้าประสงค์',
  `issue_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเด็นยุทธศาสตร์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_goals`
--

INSERT INTO `masterplan_goals` (`goal_id`, `goal_name`, `issue_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ผลิตบัญฑิตนักปฏิบัติ (Hands-on) ด้านวิชาชีพและเทคโนโลยี มีคุณภาพตรงต่อความต้องการของตลาดแรงงาน ชุมชน สังคม ระดับชาติและนานาชาติ', 1, '2017-06-30 08:24:14', NULL, NULL),
(2, 'งานวิจัย หรืองานสร้างสรรค์ มีคุณภาพสามารถนำไปพัฒนาชุมชน สังคมให้เข้มแข็งและเพิ่มศักยภาพในการแข่งขันแก่ธุรกิจอุตสาหกรรมและเกษตรกรรม', 2, '2017-06-30 08:24:14', NULL, NULL),
(3, 'เป็นแหล่งบริการวิชาการและถ่ายทอดเทคโนโลยีแก่ชุมชนอย่างต่อเนื่องและยั่งยืน เพื่อให้ชุมชนเป็นสังคมฐานความรู้ (Knowledge Based Society) ที่มีความเข้มแข็ง พึ่งพาตนเองได้ และมีคุณภาพชีวิตที่ดีขึ้น', 3, '2017-06-30 08:24:14', NULL, NULL),
(4, 'บุคลากรและนักศึกษามีความรู้ความเข้าใจและตระหนักในคุณค่าของศิลปวัฒนธรรมไทย และสิ่งแวดล้อม', 4, '2017-06-30 08:24:14', NULL, NULL),
(5, 'ส่งเสริมการจัดกิจกรรมนักศึกษาแบบรอบด้านเพื่อให้นักศึกษามีคุณลักษณะที่พึงประสงค์ตามอัตลักษณ์ของผู้เรียน', 5, '2017-06-30 08:24:14', NULL, NULL),
(6, 'อาจารย์และบุคลากรสายการสนับสนุน มีศักยภาพในการปฏิบัติงาน และมีขวัญกำลังใจในการปฏิบัติงาน', 6, '2017-06-30 08:24:14', NULL, NULL),
(7, 'การบริหารงานมหาวิทยาลัยมีประสิทธิภาพ และเป็นไปหลักธรรมาภิบาลและปรัชญาเศรษฐพอเพียง', 7, '2017-06-30 08:24:14', NULL, NULL),
(8, 'มหาวิทยาลัยมีศักยภาพในการเข้าสู่ประชาคมอาเซียน', 8, '2017-06-30 08:24:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_issues`
--

CREATE TABLE `masterplan_issues` (
  `issue_id` int(10) UNSIGNED NOT NULL,
  `issue_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อประเด็นยุทธศาสตร์',
  `federal_circuit_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสพันธกิจ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_issues`
--

INSERT INTO `masterplan_issues` (`issue_id`, `issue_name`, `federal_circuit_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'พัฒนาการจัดการศึกษาให้มีคุณภาพมาตราฐานในระชาติและนานาชาติ', 1, '2017-06-30 08:24:14', NULL, NULL),
(2, 'สร้างและพัฒนางานวิจัย หรืองานสร้างสรรค์ เพื่อพัฒนาชุมชน สังคมและประเทศชาติ', 2, '2017-06-30 08:24:14', NULL, NULL),
(3, 'ปรับปรุงและพัฒนาการให้บริการวิชาการเพื่อสร้างความเข้มแข็งให้ชุมชน สังคม และสร้างรายได้ให้กับมหาวิทยาลัย', 3, '2017-06-30 08:24:14', NULL, NULL),
(4, 'สืบสารและทำนุบำรุงศิลปวัฒนธรรม ภูมิปัญญาท้องถิ่น และอนุรักษ์สิ่งแวดล้อมให้เกิดความยั่งยืน', 4, '2017-06-30 08:24:14', NULL, NULL),
(5, 'สนับสนุนและส่งเสริมเพื่อให้นักศึกษามีพัฒนาการการครบทุกด้าน', 1, '2017-06-30 08:24:14', NULL, NULL),
(6, 'พัฒนาคุณภาพของอาจารย์และบุคคลากรสายสนับสนุน', 1, '2017-06-30 08:24:14', NULL, NULL),
(7, 'บริหารจัดการมหาลัยเชิงบรูณาการด้วยหลักธรรมภิบาล และปรัชญาเศรษฐกิจพอเพียง', 5, '2017-06-30 08:24:14', NULL, NULL),
(8, 'พัฒนามหาวิทยาลัยเข้าสู่ระบบอาเซียน', 1, '2017-06-30 08:24:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_key_projects`
--

CREATE TABLE `masterplan_key_projects` (
  `key_project_id` int(10) UNSIGNED NOT NULL,
  `key_project_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อโครงการสำคัญ',
  `strategy_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสกลยุทธ์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_key_projects`
--

INSERT INTO `masterplan_key_projects` (`key_project_id`, `key_project_name`, `strategy_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'โครงการจัดตั้งโรงเรียนเตรียมวิชาชีพ', 1, '2017-06-30 08:24:25', NULL, NULL),
(2, 'โครงการพัฒนาศักยภาพบัณฑิตนักปฏิบัติ (สมรรณะ IT  ภาษาอังกฤษ)', 1, '2017-06-30 08:24:25', NULL, NULL),
(3, 'โครงการทดสอบสมรรณะทางวิชาชีพ', 1, '2017-06-30 08:24:25', NULL, NULL),
(4, 'โครงการทดสอบความสามารถทางด้าน IT ', 1, '2017-06-30 08:24:25', NULL, NULL),
(5, 'โครงการทดสอบความสามารถทางภาษาอังกฤษ', 1, '2017-06-30 08:24:25', NULL, NULL),
(6, 'โครงการสร้างเครือข่ายความร่วมมือกับสถาบันการศึกษาและหรือสถาบันประกอบการเพื่อพัฒนาสมรรณะทางวิชาชีพแก่นักศึกษา', 1, '2017-06-30 08:24:25', NULL, NULL),
(7, 'โครงการสร้างความร่วมมือกับสถาบันการศึกษา/สถานประกอบการทั้งในประเทศหรือต่างประเทศเพื่อพัฒนาศักยภาพอาจารย์ทางด้านวิชาการ', 1, '2017-06-30 08:24:25', NULL, NULL),
(8, 'โครงการสรรหาอาจารย์ผู้มีประสบการณ์ทางด้านวิชาการ วิชาชีพจากสถานประกอบการ', 1, '2017-06-30 08:24:25', NULL, NULL),
(9, 'โครงการพัฒนาอาจารย์ให้มีตำแหน่งทางวิชาการและคุณวุฒิทางการศึกษาที่สูงขึ้น', 2, '2017-06-30 08:24:25', NULL, NULL),
(10, 'โครงการหลักสูตร หลักสูตรฐานสมรรถนะ บัณฑิตศึกษา หลักสูตรภาคภาษาอังกฤษ (English Program)', 3, '2017-06-30 08:24:25', NULL, NULL),
(11, 'โครงการพัฒนาห้องเรียน ห้องปฏิบัติการและสื่อการเรียนการสอนให้ทันสมัย', 4, '2017-06-30 08:24:25', NULL, NULL),
(12, 'โครงการเสริมความรู้ทางวิชาการสำหรับนักศึกษา', 4, '2017-06-30 08:24:25', NULL, NULL),
(13, 'โครงการพัฒนานักวิจัยรุ่นใหม่โดยมีระบบพี่เลี้ยง (Mentor)', 5, '2017-06-30 08:24:25', NULL, NULL),
(14, 'โครงการพัฒนาศักยภาพนักวิจัย', 5, '2017-06-30 08:24:25', NULL, NULL),
(15, 'โครงการพัฒนาระบบฐานข้อมูลงานวิจัยมหาวิทยาลัย', 5, '2017-06-30 08:24:25', NULL, NULL),
(16, 'โครงการสร้างเครือข่ายความร่วมมือเพื่อทำวิจัยเชิงบูรณการ', 6, '2017-06-30 08:24:25', NULL, NULL),
(17, 'โครงการสร้างเครือข่ายความร่วมมือเพื่อนำผลงานวิจัย/งานสร้างสรรค์ไปเผยแพร่/ใช้ประโยชน์', 7, '2017-06-30 08:24:25', NULL, NULL),
(18, 'โครงการจัดประชุมวิชาการ', 8, '2017-06-30 08:24:25', NULL, NULL),
(19, 'โครงการสร้างเครือข่ายความร่วมมือเพื่อบริการทางวิชาการ', 9, '2017-06-30 08:24:25', NULL, NULL),
(20, 'โครงการยกระดับคุณภาพชีวิต ชุมชน สังคม ด้วยวิชาชีพและเทคโนโลยี', 9, '2017-06-30 08:24:25', NULL, NULL),
(21, 'โครงการจัดตั้งศูนย์การเรียนรู้และปฏิบัติการภาคสนามและตลาดราชมงคล', 10, '2017-06-30 08:24:25', NULL, NULL),
(22, 'โครงการพัฒนาหลักสูตรอบรมวิชาชีพเพื่อสร้างรายได้', 10, '2017-06-30 08:24:25', NULL, NULL),
(23, 'โครงการจัดตั้งศูนย์และแหล่งเรียนรู้ทางด้านศิลปะ วัฒนธรรม และสิ่งแวดล้อม', 11, '2017-06-30 08:24:25', NULL, NULL),
(24, 'โครงการจัดตั้งลานวัฒนธรรมราชมงคลสุวรรณภูมิ และ Night Market ณ ศูนย์วาสุกรี', 11, '2017-06-30 08:24:25', NULL, NULL),
(25, 'โครงการทำนุบำรุงศิลปวัฒนธรรม ภูมิปัญญาท้องถิ่น และสิ่งแวดล้อม', 11, '2017-06-30 08:24:25', NULL, NULL),
(26, 'โครงการบูรณาการศิลปวัฒนธรรมและภูมิปัญญาท้องถิ่นกับการเรียนการสอน การวิจัยหรือการบริการวิชาการ', 12, '2017-06-30 08:24:25', NULL, NULL),
(27, 'โครงการพัฒนานักศึกษาให้มีคุณลักษณะที่พึงประสงค์', 13, '2017-06-30 08:24:25', NULL, NULL),
(28, 'โครงการพัฒนาศักยภาพของอาจารย์ทางด้านวิชาการ วิชาชีพ และภาษาอังกฤษ', 14, '2017-06-30 08:24:25', NULL, NULL),
(29, 'โครงการพัฒนาศักยภาพของบุคลากรสายสนับสนุนทางด้านการปฏิบัติงานและภาษาอังกฤษ', 14, '2017-06-30 08:24:25', NULL, NULL),
(30, 'โครงการพัฒนาการบุคลากรสายสนันสนุนในการจัดทำผลงานเพื่อความก้าวหน้าในสายงานอาชีพ', 14, '2017-06-30 08:24:25', NULL, NULL),
(31, 'โครงการจจัดสวัสดิการให้แก่อาจารย์และบุคลากรสายสนับสนุน', 15, '2017-06-30 08:24:25', NULL, NULL),
(32, 'โครงการจัดตั้งกองทุนพัฒนาบุคลากร', 15, '2017-06-30 08:24:25', NULL, NULL),
(33, 'โครงการพัฒนาศักยภาพผู้บริหารทุกระดับ', 16, '2017-06-30 08:24:25', NULL, NULL),
(34, 'โครงการปรับปรุงแก้ไข ระเบียบ/ข้อบังคับ/ข้อกำหนด/กฎเกณฑ์ต่าง ๆ ให้การบริหารงานของมหาวิทยาลัยเป็นไปอย่างคร่องตัวโปร่งใสตรวจสอบได้', 16, '2017-06-30 08:24:25', NULL, NULL),
(35, 'โครงการพัฒนาการประกันคุณภาพการศึกษาการควบคุมภายในและการบริหารความเสี่ยง', 16, '2017-06-30 08:24:25', NULL, NULL),
(36, 'โครงการจัดทำแผนการตลาด แผนโฆษณาแระชาสัมพันธ์เชิงรุก', 17, '2017-06-30 08:24:25', NULL, NULL),
(37, 'โครงการเสริมสร้างภาพลักษณ์มหาวิทยาลัย', 17, '2017-06-30 08:24:25', NULL, NULL),
(38, 'โครงการพัฒนาระบบเทคโนโลยีสารสนเทศเพื่อการบริหารงานการเงินประงบประมาณ', 18, '2017-06-30 08:24:25', NULL, NULL),
(39, 'โครงการจัดทำแผนแม่บท (Master Plan) การใช้พื้นที่ของมหาวิทยาลัย', 19, '2017-06-30 08:24:25', NULL, NULL),
(40, 'โครงการพัฒนามหาวิทยาลัยสู่การเป็น Green University', 19, '2017-06-30 08:24:25', NULL, NULL),
(41, 'โครงการพัฒนาระบบสารสนเทศเพื่อการบริหาร (MIS)', 20, '2017-06-30 08:24:25', NULL, NULL),
(42, 'โครงการ/กิจกรรม ที่เสริมสร้างความรู้/ความสัมพันธ์ สู่อาเซียน', 21, '2017-06-30 08:24:25', NULL, NULL),
(43, 'โครงการจัดตั้งศูนย์ภาษาและอาเซียนศึกษา', 21, '2017-06-30 08:24:25', NULL, NULL),
(44, 'โครงการดำเนินการตามความร่วมมือกับสถาบันการศึกษา หรือองค์กรในกลุ่มอาเซียน', 21, '2017-06-30 08:24:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_objective_indicators`
--

CREATE TABLE `masterplan_objective_indicators` (
  `objective_indicator_id` int(10) UNSIGNED NOT NULL,
  `objective_indicator_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อตัวชี้วัดเป้าประสงค์',
  `goal_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสเป้าประสงค์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_objective_indicators`
--

INSERT INTO `masterplan_objective_indicators` (`objective_indicator_id`, `objective_indicator_name`, `goal_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ร้อยละของบัณฑิตนักปฏิบัติที่ได้งานทำ หรือประกอบอาชีพอิสระภายใน 1 ปี', 1, '2017-06-30 08:24:17', NULL, NULL),
(2, 'ร้อยละเฉลี่ยความพึงพอใจของผู้ใช้บัฒฑิตนักปฏิบัติ', 1, '2017-06-30 08:24:17', NULL, NULL),
(3, 'ร้อยละของบัณฑิตนักปฏิบัติได้ทำงานหรือประกอบอาชีพอิสระตรงสาขาวิชาที่สำเร็จการศึกษา', 1, '2017-06-30 08:24:17', NULL, NULL),
(4, 'จำนวนงานวิจัยหรืองานสร้างสรรค์ ที่ตีพิมพ์ เผยแพร่ในระดับชาติหรือนานาชาติ', 2, '2017-06-30 08:24:17', NULL, NULL),
(5, 'จำนวนงานวิจัย หรืองานสร้างสรรค์ ที่นำไปใช้ประโยชน์', 2, '2017-06-30 08:24:17', NULL, NULL),
(6, 'ร้อยละของโครงการ/กิจกรรมบริการทางวิชาการที่มีการบูรณาการกับการจัดการเรียนการสอนและ/หรืองานวิจัย', 3, '2017-06-30 08:24:17', NULL, NULL),
(7, 'ร้อยละของโครงการ/กิจกรรมบริการทางวิชาการที่มีการเรียนรู้และ/หรือมีส่วนร่วมของชุมชน สังคม', 3, '2017-06-30 08:24:17', NULL, NULL),
(8, 'จำนวนเงินสนับสนุนโครงการ/กิจกรรมด้านศิลปวัฒนธรรม/ภูมิปัญญาท้องถิ่น/อนุรักษ์สิ่งแวดล้อม', 4, '2017-06-30 08:24:17', NULL, NULL),
(9, 'ร้อยละของความรู้เฉลี่ยที่ผู้เข้าร่วมโครงการ/กิจกรรม ได้รับความรู้หลังจากเข้าร่วมโครงการ/กิจกรรม', 4, '2017-06-30 08:24:17', NULL, NULL),
(10, 'ร้อยละของผลรวมถ่วงน้ำหนักการเข้าร่วมการประกวด/แข่งขันหรือได้รางวัลจากผลงานด้านศิลปวัฒนธรรม/ภูมิปัญญาท้องถิ่น/อนุรักษ์สิ่งแวดล้อม ของนักศึกษาต่อจำนวนนักศึกษาทั้งหมด', 4, '2017-06-30 08:24:17', NULL, NULL),
(11, 'จำนวนนักศึกษา หรือผลงานนักศึกษาได้รับรางวัล/การยกย่องชมเชยระดับท้องถิ่น ชาติ และนานาชาติ', 5, '2017-06-30 08:24:17', NULL, NULL),
(12, 'ร้อยละคุณภาพของบัณฑิตตามกรอบมาตรฐานคุณวุฒิระดับอุดมศึกษาแห่งชาติ', 5, '2017-06-30 08:24:17', NULL, NULL),
(13, 'ร้อยละค่าเฉลี่ยความพึงพอใจของนนักศึกษาต่อการสอนของอาจารย์', 6, '2017-06-30 08:24:17', NULL, NULL),
(14, 'ร้อยละเฉลี่ยความพึงพอใจบริการของผู้รับบริการต่อการดำเนินงานของบุคลากร', 6, '2017-06-30 08:24:17', NULL, NULL),
(15, 'ร้อยละเฉลี่ยความพึงพอใจของอาจารย์และบุคลากรในการบริหารจัดการของมหาวิทยาลัย', 6, '2017-06-30 08:24:17', NULL, NULL),
(16, 'ร้อยละการบรรลุเป้าหมายตามตัวบ่งชี้ในแผนยุทธศาสตร์การพัฒนามหาวิทยาลัย', 7, '2017-06-30 08:24:17', NULL, NULL),
(17, 'จำนวนโครงการ/กิจกรรมที่ดำเนินการตามความร่วมมือกับสถาบันการศึกษา หรือองค์ในกลุ่มอาเซียน', 8, '2017-06-30 08:24:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_objective_strategies`
--

CREATE TABLE `masterplan_objective_strategies` (
  `objective_strategy_id` int(10) UNSIGNED NOT NULL,
  `objective_strategy_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อตัวชี้วัดกลยุทธ์',
  `strategy_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสกลยุทธ์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_objective_strategies`
--

INSERT INTO `masterplan_objective_strategies` (`objective_strategy_id`, `objective_strategy_name`, `strategy_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ร้อยละของนักศึกษาปีสุดท้ายที่สอบผ่านเกณฑ์การทดสอบสมรรถนะทางวิชาชีพ', 1, '2017-06-30 08:24:22', NULL, NULL),
(2, 'ร้อยละของนักศึกษาปีสุดท้ายที่สอบผ่านเกณฑ์การทดสอบสมรรถนะทางด้าน IT', 1, '2017-06-30 08:24:22', NULL, NULL),
(3, 'ร้อยละของนักศึกษาปีสุดท้ายที่สอบผ่านเกณฑ์การทดสอบสมรรถนะทางด้าน ภาษาอังกฤษ', 1, '2017-06-30 08:24:22', NULL, NULL),
(4, 'ร้อยละของอาจารย์ประจำที่ได้รับการฝึกประสบการณ์วิชาชีพในสถานประกอบการ', 1, '2017-06-30 08:24:22', NULL, NULL),
(5, 'ร้อยละของอาจารย์ประจำมีวุฒิปริญญาเอก', 2, '2017-06-30 08:24:22', NULL, NULL),
(6, 'ร้อยละของอาจารย์ที่มีตำแหน่งทางวิชการตั้งแต่ระดับผู้ช่วยศาสตราจารย์', 2, '2017-06-30 08:24:22', NULL, NULL),
(7, 'ร้อยละของหลักสูตรฐานสมรรถนะต่อหลักสูตรทั้งหมด', 3, '2017-06-30 08:24:22', NULL, NULL),
(8, 'จำนวนหลักสูตรใหม่ระดับบัณฑิตศึกษา', 3, '2017-06-30 08:24:22', NULL, NULL),
(9, 'จำนวนหลักสูตรภาคภาษาอังกฤษ (English Program)', 3, '2017-06-30 08:24:22', NULL, NULL),
(10, 'ร้อยละของนักศึกษาที่พ้นสภาพการเป็นนักศึกษา', 4, '2017-06-30 08:24:22', NULL, NULL),
(11, 'ร้อยละของผู้สำเร็จการศึกษาจบการศึกษาตามหลักสูตรภายในระยะเวลาที่กำหนด', 4, '2017-06-30 08:24:22', NULL, NULL),
(12, 'ร้อยละความพึงพอใจเฉลี่ยของผู้เรียนต่อคุณภาพห้องเรียนห้องปฏิบัติการทรัพยากรการเรียนรู้ และเทคโนโลยีสารสนเทศ', 4, '2017-06-30 08:24:22', NULL, NULL),
(13, 'ร้อยละของอาจารย์ประจำและนักวิจัยประจำที่ได้รับการพัฒนาด้านการวิจัยต่อปี', 5, '2017-06-30 08:24:22', NULL, NULL),
(14, 'เงินสนับสนุนงานวิจัย/งานสร้างสรรค์ กลุ่มสาขาวิชาวิทยาศาสตร์และเทคโนโลยี /ต่อคน/ปี', 5, '2017-06-30 08:24:22', NULL, NULL),
(15, 'เงินสนับสนุนงานวิจัย/งานสร้างสรรค์ กลุ่มสาขาวิชามนุษย์ศาสตร์และเทคโนโลยี /ต่อคน/ปี', 5, '2017-06-30 08:24:22', NULL, NULL),
(16, 'จำนวนอนุสิทธิบัตร/สิทธิบัตร/ลิขสิทธิ์/เครื่องหมายการค้า', 5, '2017-06-30 08:24:22', NULL, NULL),
(17, 'จำนวนโครงการวิจัยเชิงบูรณาการ', 6, '2017-06-30 08:24:22', NULL, NULL),
(18, 'ร้อยละของอาจารย์ประจำและนักวิจัยประจำที่นำผลงานวิจัย/งานสร้างสรรค์ไปเผยแพร่', 7, '2017-06-30 08:24:22', NULL, NULL),
(19, 'ร้อยละของอาจารย์ประจำและนักวิจัยประจำที่นำผลงานวิจัย/งานสร้างสรรค์ไปใช้ประโยชน์', 7, '2017-06-30 08:24:22', NULL, NULL),
(20, 'จำนวนครั้งของการจัดประชุมวิชาการ', 8, '2017-06-30 08:24:22', NULL, NULL),
(21, 'จำนวนชุมชนหรือองค์กรเป้าหมายของการให้บริการวิชาการเชิงบูรณาการ โดยการร่วมมือระหว่างคณะหรือหน่วยงานเทียบเท่า', 9, '2017-06-30 08:24:22', NULL, NULL),
(22, 'ร้อยละความพึงพอใจเฉลี่ยของผู้รับบริการต่อประโยชน์จากการให้บริการวิชาการและวิชาชีพ', 9, '2017-06-30 08:24:22', NULL, NULL),
(23, 'จำนวนศูนย์การเรียนรู้และปฏิบัติการภาคสนาม', 10, '2017-06-30 08:24:22', NULL, NULL),
(24, 'จำนวนหลักสูตรที่สร้างรายได้', 10, '2017-06-30 08:24:22', NULL, NULL),
(25, 'รายได้จากการบรการวิชาการในรอบ 1 ปี', 10, '2017-06-30 08:24:22', NULL, NULL),
(26, 'จำนวนแหล่งเรียนรู้ทางด้านศิลปะ วัฒนธรรม และสิ่งแวดล้อม', 11, '2017-06-30 08:24:22', NULL, NULL),
(27, 'จำนวนโครงการ/กิจกรรมด้านการทำนุบำรุงศิลปวัฒนธรรมภูมิปัญญาท้องถิ่นและสิ่งแวดล้อมที่มีส่วนร่วมกับชุมชน', 11, '2017-06-30 08:24:22', NULL, NULL),
(28, 'ร้อยละความพึงพอใจเฉลี่ยของผู้เข้าร่วมโครงการต่อประโยชน์ของการทำนุบำรุงศิลปวัฒนธรรม ภูมิปัญญาท้องถิ่นและสิ่งแวดล้อม', 11, '2017-06-30 08:24:22', NULL, NULL),
(29, 'ร้อยละของโครงการ/กิจกรรมทำนุบำรุงศิลปวัฒนธรรมที่มีการบูรณาการกับพันธกิจอื่น ๆ', 12, '2017-06-30 08:24:22', NULL, NULL),
(30, 'ร้อยละของนักศึกษาปีสุดท้ายได้รับการปลูกฝังคุณลักษณะที่พึงประสงค์ครบ 5 ด้าน', 13, '2017-06-30 08:24:22', NULL, NULL),
(31, 'ร้อยละของอาจารย์ประจำที่ได้รับการเพิ่มพูนความรู้/ประสบการณ์ทางวิชาการ วิชาชีพ ตามหลักเกณฑ์ที่กำหนดในเวลา 1 ปี', 14, '2017-06-30 08:24:22', NULL, NULL),
(32, 'ร้อยละของอาจารย์ประจำที่ได้รับการเพิ่มพูนความรู้ด้านภาษาอังกฤษ ตามหลักเกณฑ์ที่กำหนดในเวลา 1 ปี', 14, '2017-06-30 08:24:22', NULL, NULL),
(33, 'ร้อยละของบุคลากรสายสนับสนุนได้รับการเพิ่มพูนความรู้/ประสบการณ์ที่เกี่ยวข้องกับการปฏิบัติงานและภาษาอังกฤษตามกดเกณฑ์ที่กำหนดในเวลา 1 ปี', 14, '2017-06-30 08:24:22', NULL, NULL),
(34, 'ร้อยละของบุคลากรสายสนับสนุนที่มีคุณสมบัติครบตามเกณฑ์ได้รับการพัฒนาเพื่อรองรับการเข้าสู่ตำแหน่งที่สูงขึ้น', 14, '2017-06-30 08:24:22', NULL, NULL),
(35, 'จำนวนงานวิจัยสถาบันในรอบ 1 ปี', 14, '2017-06-30 08:24:22', NULL, NULL),
(36, 'ร้อยละความพึงพอใจเฉลี่ยของบุคลากรทุกระดับ (สายบริหาร สายวิชาการ สายสนับสนุน) ที่มีต่อการได้ปฏิบัติงานและสวัสดิการของมหาวิทยาลัย', 15, '2017-06-30 08:24:22', NULL, NULL),
(37, 'ร้อยละของผู้บริหารที่ได้รับการเพิ่มพูนความรู้ ประสบการณ์ทางการบริหาร ในเวลา 1 ปี', 16, '2017-06-30 08:24:22', NULL, NULL),
(38, 'ร้อยละของความสำเร็จของผลการดำเนินงานตามแผนบริหารความเสี่ยง', 16, '2017-06-30 08:24:22', NULL, NULL),
(39, 'จำนวนข่าวที่ได้รับการเผยแพร่ในสื่อต่าง ๆ ', 17, '2017-06-30 08:24:22', NULL, NULL),
(40, 'จำนวนครั้งของหน่วยงาน/บุคคลที่ใช้บริการของมหาวิทยาลัย', 17, '2017-06-30 08:24:22', NULL, NULL),
(41, 'ร้อยละของอาจารย์ที่เป็นอาจารย์พิเศษ วิทยากร คณะกรรมการหรือที่ปรึกษาให้กับหน่วยงานภายนอก', 17, '2017-06-30 08:24:22', NULL, NULL),
(42, 'ร้อยละความสำเร็จการเบิกจ่ายเงินงบประมาณรายจ่ายประจำปี', 18, '2017-06-30 08:24:22', NULL, NULL),
(43, 'ร้อยละความพึงพอใจเฉลี่ยของบุคลากร นักศึกษา ต่อการพัฒนาสุนทรียภาพของอาคารสถานที่ และระบบสาธารณูปโภคภายในมหาวิทยาลัย', 19, '2017-06-30 08:24:22', NULL, NULL),
(44, 'จำนวนระบบสารสนเทศใหม่ที่นำเข้ามาใช้เพื่อการบริหารจัดการ', 20, '2017-06-30 08:24:22', NULL, NULL),
(45, 'จำนวนโครงการ/กิจกรรมที่เสริมสร้างความรู้/ความสัมพันธ์สู่อาเซียน', 21, '2017-06-30 08:24:22', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_strategies`
--

CREATE TABLE `masterplan_strategies` (
  `strategy_id` int(10) UNSIGNED NOT NULL,
  `strategy_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อกลยุทธ์',
  `goal_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสเป้าประสงค์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_strategies`
--

INSERT INTO `masterplan_strategies` (`strategy_id`, `strategy_name`, `goal_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'พัฒนาศักยภาพบัณฑิตให้รู้จริงปฏิบัติ', 1, '2017-06-30 08:24:20', NULL, NULL),
(2, 'พัฒนาศักยภาพอาจารย์ให้เป็นไปตามเกณฑ์มาตรฐานอุดมศึกษา', 1, '2017-06-30 08:24:20', NULL, NULL),
(3, 'พัฒนาหลักสูตรที่ได้มาตรฐานและทันสมัย', 1, '2017-06-30 08:24:20', NULL, NULL),
(4, 'พัฒนาระบบการจัดการเรียนการสอนที่มีคุณภาพ', 1, '2017-06-30 08:24:20', NULL, NULL),
(5, 'พัฒนางานวิจัยให้มีคุณภาพสูง', 2, '2017-06-30 08:24:20', NULL, NULL),
(6, 'ปรับทิศทางการวิจัยสู่การวิจัยเชิงบูรณาการ', 2, '2017-06-30 08:24:20', NULL, NULL),
(7, 'ส่งเสริมสนับสนุนการนำผลงานวิจัยและงานสร้างสรรค์ไปเผยแพร่หรือนำไปใช้ประโยชน์', 2, '2017-06-30 08:24:20', NULL, NULL),
(8, 'ส่งเสริมสนับสนุนการจัดประชุมวิชาการ', 2, '2017-06-30 08:24:20', NULL, NULL),
(9, 'ส่งเสริมการเรียนรู้ในชุมชนและนำเสนอองค์ความรู้ที่มหาวิทยาลัยมีความเชี่ยวชาญ', 3, '2017-06-30 08:24:20', NULL, NULL),
(10, 'พัฒนาระบบและกลไกการบริหารจัดการการให้บริการทางวิชาการให้เอื้ออำนวยต่อการสร้างรายได้', 3, '2017-06-30 08:24:20', NULL, NULL),
(11, 'สร้างจิตสำนึกให้เกิดความรัก ความภาคภูมิใจ เห็นคุณค่าและตระหนักในความสำคัญของศิลปวัฒนธรรมและสิ่งแวดล้อม', 4, '2017-06-30 08:24:20', NULL, NULL),
(12, 'ส่งเสริม สนับสนุนให้มีการบูรณาการการทำนุบำรุงศิลปวัฒนธรรม กับพันธกิจอื่นๆ', 4, '2017-06-30 08:24:20', NULL, NULL),
(13, 'ส่งเสริมการจัดกิจกรรมนักศึกษาแบบรอบด้านเพื่อให้นักศึกษามีคุณลักษณะที่พึงประสงค์', 5, '2017-06-30 08:24:20', NULL, NULL),
(14, 'พัฒนาศักยภาพของอาจารย์และบุคลากรสายสนับสนุน', 6, '2017-06-30 08:24:20', NULL, NULL),
(15, 'พัฒนาคุณภาพชีวิตของอาจารย์และบุคลากรสายสนันสนุน', 6, '2017-06-30 08:24:20', NULL, NULL),
(16, 'พัฒนาระบบการบริหารและการติดตามที่มีประสิทธิภาพ', 7, '2017-06-30 08:24:20', NULL, NULL),
(17, 'ส่งเสริมการสร้างภาพลักษณ์ของมหาวิทยาลัยให้เป็นที่รู้จัก เป็นที่ยอมรับในระดับท้องถิ่น ประเทศ และนานาชาติ', 7, '2017-06-30 08:24:20', NULL, NULL),
(18, 'การบริหารงานการเงินและงบประมาณให้เกิดประโยชน์สูงสุด', 7, '2017-06-30 08:24:20', NULL, NULL),
(19, 'พัฒนาด้านศักยภาพ และสภาพแวดล้อมโดยใช้แนวทาง Green University', 7, '2017-06-30 08:24:20', NULL, NULL),
(20, 'นำเทคโนโลยีสารสนเทศมาใช้ในการบริหารจัดการ', 7, '2017-06-30 08:24:20', NULL, NULL),
(21, 'การเข้าสู่ประชาคมอาเซียน', 8, '2017-06-30 08:24:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masterplan_visions`
--

CREATE TABLE `masterplan_visions` (
  `vision_id` int(10) UNSIGNED NOT NULL,
  `vision_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อวิสัยทัศน์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masterplan_visions`
--

INSERT INTO `masterplan_visions` (`vision_id`, `vision_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'มหาวิทยาลัยชั้นนําด้านวิชาชีพและเทคโนโลยีเพื่อชุมชน สังคม ประเทศ และนานาชาติ', '2017-06-30 08:24:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_10_03_083728_Modules', 1),
('2016_10_03_084049_Licenses', 1),
('2016_10_03_084432_Groups', 1),
('2016_10_03_084658_Campuses', 1),
('2016_10_03_084825_Positions', 1),
('2016_10_03_085005_Faculties', 1),
('2016_10_03_085116_Branches', 1),
('2016_10_03_085240_Personnels', 1),
('2016_10_03_085954_Plan_Products', 1),
('2016_10_03_091145_Plan_Plans', 1),
('2016_10_03_091706_Plan_Income_Types', 1),
('2016_10_03_091939_Plan_Incomes', 1),
('2016_10_03_092235_Plan_Income_Groups', 1),
('2016_10_03_093949_MasterPlan_Visions', 1),
('2016_10_03_094233_MasterPlan_Federal_Circuits', 1),
('2016_10_03_094514_MasterPlan_Issues', 1),
('2016_10_03_095258_MasterPlan_Objective_Indicators', 1),
('2016_10_03_095946_MasterPlan_Goals', 1),
('2016_10_03_100222_MasterPlan_Strategies', 1),
('2016_10_03_100514_MasterPlan_Objective_Strategies', 1),
('2016_10_03_100830_MasterPlan_Key_Projects', 1),
('2016_10_03_101259_Budget_Plans', 1),
('2016_10_05_032908_Plan_Projects', 1),
('2016_10_05_033230_Plan_Durable_Articles', 1),
('2016_10_05_033314_Plan_Buildings', 1),
('2016_10_05_033813_AlterPersonnelAddToken', 1),
('2016_10_05_051753_Plan_Objectives', 1),
('2016_10_05_051837_Plan_Committees', 1),
('2016_10_05_060942_add_relation_all_table', 1),
('2016_10_05_104007_Plan_Project_Activities', 1),
('2016_10_05_104219_Plan_Project_Benefits', 1),
('2016_10_05_104426_Plan_Project_Periods', 1),
('2016_10_05_104550_Plan_Project_Budgets', 1),
('2016_10_05_104657_Plan_Project_Suggestions', 1),
('2016_10_05_165250_Plan_Durable_Article_Items', 1),
('2016_10_06_043013_Plan_Building_Utilizations', 1),
('2016_10_06_043629_add_relation_all_table1', 1),
('2016_10_07_044954_Plan_Income_Details', 1),
('2016_10_07_045504_add_relation_all_table2', 1),
('2016_10_10_054916_AlterBudgetPlanAddCampus', 1),
('2016_10_17_052154_Plan_Durable_Article_Allocates', 1),
('2016_10_17_052850_add_relation_all_table3', 1),
('2016_11_11_131423_AlterPlanArticleAddMoneyApprove', 1),
('2016_11_14_163723_AlterPlanArticleAddUploadFile', 1),
('2016_11_15_110807_AlterPlanArticleAddUploadFileName', 1),
('2017_01_31_135743_AlterPlanProjectPeriodAddDate', 1),
('2017_01_31_140645_AlterPlanProjectAddProjectGoal', 1),
('2017_02_03_141341_AlterPlanProjectbudgetAddActivityId', 1),
('2017_02_09_152437_AlterPlanProjectAddProjectMoneyApprove', 1),
('2017_02_09_153720_AlterPlanProjectBudgetAddProjectBudgetNet', 1),
('2017_02_14_154905_AlterPlanProjectAddUploadFile', 1),
('2017_04_25_123748_AlterBudgetPlanAddApprovalStatus', 1),
('2017_06_07_102710_AlterPlanProjectAddMoneyReceivedFirst', 1),
('2017_06_13_112851_AlterBudgetPlanAddApprovalStatus1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) UNSIGNED NOT NULL,
  `module_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อโมดูลหน้ารายการต่างๆ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'บุคลากร', '2017-06-30 08:24:09', NULL, NULL),
(2, 'สาขา', '2017-06-30 08:24:09', NULL, NULL),
(3, 'คณะ', '2017-06-30 08:24:09', NULL, NULL),
(4, 'ตำแหน่ง', '2017-06-30 08:24:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personnels`
--

CREATE TABLE `personnels` (
  `personnel_id` int(10) UNSIGNED NOT NULL,
  `personnel_picture` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'รูปภาพบุคลากร',
  `title_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'คำนำหน้าชื่อ',
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ-นามสกุลบุคลากร',
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เบอร์โทรศัพท์',
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อผู้ใช้ ใช้ในการล็อกอินเข้าสู่ระบบ',
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'รหัสผ่านที่เข้ารหัสเรียบร้อยแล้ว',
  `position_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสตำแหน่งทางวิชาการ',
  `branch_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสสาขา',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสกลุ่มการใช้งาน',
  `campus_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสศูนย์พื้นที่',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personnels`
--

INSERT INTO `personnels` (`personnel_id`, `personnel_picture`, `title_name`, `first_name`, `mobile`, `username`, `password`, `position_id`, `branch_id`, `group_id`, `campus_id`, `created_at`, `updated_at`, `deleted_at`, `remember_token`) VALUES
(1, '/uploads/default/profile-img.jpg', 'นางสาว', 'ภัสราภรณ์ คำนวนฤทธิ์', '0904230818', 'chompoo', '$2y$10$NveDHVrp9mKPXQui3ZYuCON.6fVgnnku/uurRD98bQ3e0ETZZ7Vm.', 1, 1, 1, 1, NULL, NULL, NULL, NULL),
(2, '/uploads/default/profile-img2.jpg', 'นาย', 'วัชรชัย คูหาสวัสดิ์', '0895697977', 'aoae', '$2y$10$HxArR5kH/xxn0HBHy38HM.DnSDLi2HLP.AmPcObupeO5sN4vYrU8O', 1, 1, 3, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_building_utilizations`
--

CREATE TABLE `plan_building_utilizations` (
  `building_utilization_id` int(10) UNSIGNED NOT NULL,
  `building_utilization_name` text COLLATE utf8_unicode_ci COMMENT 'รายการการใช้ประโยชน์ของอาคาร',
  `building_utilization_size` int(11) DEFAULT NULL COMMENT 'ขนาด',
  `building_utilization_number` int(11) DEFAULT NULL COMMENT 'จำนวนห้อง',
  `building_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสที่ดินและสิ่งก่อนสร้าง',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_buildings`
--

CREATE TABLE `plan_buildings` (
  `building_id` int(10) UNSIGNED NOT NULL,
  `building_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อที่ดินและสิ่งก่อสร้าง',
  `budget_plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจัดแผนเสนอของบประมาณ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_committees`
--

CREATE TABLE `plan_committees` (
  `committee_id` int(10) UNSIGNED NOT NULL,
  `committee_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อ-นามสกุลผู้รับผิดชอบ',
  `committee_status` enum('ผู้รับผิดชอบหลัก','กรรมการ') COLLATE utf8_unicode_ci NOT NULL COMMENT 'สถานะผู้รับผิดชอบ',
  `budget_plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจัดแผนเสนอของบประมาณ',
  `personnel_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสบุคลากร',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_durable_article_items`
--

CREATE TABLE `plan_durable_article_items` (
  `durable_article_item_id` int(10) UNSIGNED NOT NULL,
  `durable_article_item_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อรายการครุภัณฑ์',
  `durable_article_unit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'หน่วยนับ',
  `durable_article_qty` int(11) DEFAULT NULL COMMENT 'จำนวน',
  `durable_article_priceunit` decimal(10,2) DEFAULT NULL COMMENT 'ราคา/หน่วย',
  `durable_article_num` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนเงิน',
  `durable_article_status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'สถานะครุภัณฑ์',
  `durable_article_reason` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'เหตุผลสรุป',
  `durable_article_item_detail` text COLLATE utf8_unicode_ci COMMENT 'มาตรฐานและรายละเอียดคุณลักษณะ',
  `durable_article_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสครุภัณฑ์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_durable_articles`
--

CREATE TABLE `plan_durable_articles` (
  `durable_article_id` int(10) UNSIGNED NOT NULL,
  `durable_article_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อครุภัณฑ์',
  `durable_article_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'สถานที่นำไปใช้',
  `durable_article_frequency` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ความถี่ในการใช้งาน',
  `durable_article_purchase` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'แผนจัดซื้อ',
  `durable_article_purchase_month` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'แผนจัดซื้อ-ในเดือน',
  `durable_article_purchase_year` int(11) DEFAULT NULL COMMENT 'แผนจัดซื้อ-ในปี พ.ศ.',
  `durable_article_install_month` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'แผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง-ในเดือน',
  `durable_article_install_year` int(11) DEFAULT NULL COMMENT 'แผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง-ในปี พ.ศ.',
  `durable_article_spend_month` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'แผนการใช้จ่าย-ในเดือน',
  `durable_article_spend_year` int(11) DEFAULT NULL COMMENT 'แผนการใช้จ่าย-ในปี พ.ศ.',
  `durable_article_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'แบบรายการครุภัณฑ์',
  `durable_article_budget_total` decimal(10,2) DEFAULT NULL COMMENT 'ราคารวม',
  `durable_article_budget_vat` decimal(10,2) DEFAULT NULL COMMENT 'ภาษี',
  `durable_article_budget_net` decimal(10,2) DEFAULT NULL COMMENT 'ราคาสุทธิ',
  `budget_plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจัดแผนเสนอของบประมาณ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `money_approve` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนเงินที่เสนอขอ',
  `upload_file` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ไฟล์แนบเอกสารที่ได้รับการอนุมัติ',
  `upload_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อไฟล์แนบเอกสารที่ได้รับการอนุมัติ',
  `money_received` decimal(10,2) DEFAULT NULL COMMENT 'ยอดเงินขออนุมัติใช้ครั้งแรก',
  `date_received` datetime DEFAULT NULL COMMENT 'วันที่ขออนุมัติใช้ครั้งแรก',
  `pecen_received` int(11) DEFAULT NULL COMMENT 'เปอร์เซนต์ขออนุมัติใช้ครั้งแรก',
  `file_received` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก',
  `filename_received` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_durable_articles_allocates`
--

CREATE TABLE `plan_durable_articles_allocates` (
  `durable_articles_allocate_id` int(10) UNSIGNED NOT NULL,
  `durable_articles_allocate_name` text COLLATE utf8_unicode_ci COMMENT 'รายการหากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร',
  `durable_article_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสครุภัณฑ์',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_income_details`
--

CREATE TABLE `plan_income_details` (
  `income_detail_id` int(10) UNSIGNED NOT NULL,
  `income_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสแหล่งเงินได้',
  `income_type_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเภทแหล่งเงินได้',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_income_details`
--

INSERT INTO `plan_income_details` (`income_detail_id`, `income_id`, `income_type_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2017-06-30 08:24:25', NULL, NULL),
(2, 1, 2, '2017-06-30 08:24:25', NULL, NULL),
(3, 1, 3, '2017-06-30 08:24:25', NULL, NULL),
(4, 1, 4, '2017-06-30 08:24:25', NULL, NULL),
(5, 1, 5, '2017-06-30 08:24:25', NULL, NULL),
(6, 2, 1, '2017-06-30 08:24:25', NULL, NULL),
(7, 2, 2, '2017-06-30 08:24:25', NULL, NULL),
(8, 2, 3, '2017-06-30 08:24:25', NULL, NULL),
(9, 2, 4, '2017-06-30 08:24:25', NULL, NULL),
(10, 2, 5, '2017-06-30 08:24:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_income_groups`
--

CREATE TABLE `plan_income_groups` (
  `income_group_id` int(10) UNSIGNED NOT NULL,
  `income_group_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อหมวดแหล่งเงินได้',
  `income_type_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสประเภทแหล่งเงินได้',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_income_groups`
--

INSERT INTO `plan_income_groups` (`income_group_id`, `income_group_name`, `income_type_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'เงินเดือน', 1, '2017-06-30 08:24:25', NULL, NULL),
(2, 'ค่าตอบแทนประจำตำแหน่ง', 1, '2017-06-30 08:24:25', NULL, NULL),
(3, 'ครุภัณฑ์', 2, '2017-06-30 08:24:25', NULL, NULL),
(4, 'ที่ดิน', 2, '2017-06-30 08:24:25', NULL, NULL),
(5, 'สิ่งก่อสร้าง', 2, '2017-06-30 08:24:25', NULL, NULL),
(6, 'ค่าตอบแทน', 3, '2017-06-30 08:24:25', NULL, NULL),
(7, 'ค่าใช้สอย', 3, '2017-06-30 08:24:25', NULL, NULL),
(8, 'ค่าวัสดุ', 3, '2017-06-30 08:24:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_income_types`
--

CREATE TABLE `plan_income_types` (
  `income_type_id` int(10) UNSIGNED NOT NULL,
  `income_type_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อประเภทแหล่งเงินได้',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_income_types`
--

INSERT INTO `plan_income_types` (`income_type_id`, `income_type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'งบบุคลากร', '2017-06-30 08:24:25', NULL, NULL),
(2, 'งบลงทุน', '2017-06-30 08:24:25', NULL, NULL),
(3, 'งบดำเนินงาน', '2017-06-30 08:24:25', NULL, NULL),
(4, 'งบอุดหนุน', '2017-06-30 08:24:25', NULL, NULL),
(5, 'งบรายจ่ายอื่น ๆ ', '2017-06-30 08:24:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_incomes`
--

CREATE TABLE `plan_incomes` (
  `income_id` int(10) UNSIGNED NOT NULL,
  `income_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อแหล่งเงินได้',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_incomes`
--

INSERT INTO `plan_incomes` (`income_id`, `income_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'รายจ่าย (งบประมาณแผ่นดิน)', '2017-06-30 08:24:25', NULL, NULL),
(2, 'รายได้ (ผลประโยชน์)', '2017-06-30 08:24:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_objectives`
--

CREATE TABLE `plan_objectives` (
  `objective_id` int(10) UNSIGNED NOT NULL,
  `objective_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อวัตถุประสงค์',
  `budget_plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจัดแผนเสนอของบประมาณ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_plans`
--

CREATE TABLE `plan_plans` (
  `plan_id` int(10) UNSIGNED NOT NULL,
  `plan_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อแผนงาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_plans`
--

INSERT INTO `plan_plans` (`plan_id`, `plan_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'แผนงานรองยกระดับคุณภาพการศึกษาและการเรียนรู้ตลอดชีวิต', '2017-06-30 08:24:12', NULL, NULL),
(2, 'แผนงานรองส่งเสริมและพัฒนาศาสนาศิลปะ และวัฒนธรรม', '2017-06-30 08:24:12', NULL, NULL),
(3, 'แผนงานรองบุคลากรภาครัฐยกระดับคุณภาพการศึกษาและการเรียนรู้ตลอดชีวิต', '2017-06-30 08:24:12', NULL, NULL),
(4, 'แผนงานรองส่งเสริมการวิจัยและพัฒนา', '2017-06-30 08:24:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_products`
--

CREATE TABLE `plan_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อผลผลิต',
  `plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสแผนงาน',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_products`
--

INSERT INTO `plan_products` (`product_id`, `product_name`, `plan_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ผู้สำเร็จการศึกษาด้านสังคมศาสตร์', 1, '2017-06-30 08:24:14', NULL, NULL),
(2, 'ผลงานการให้บริการวิชาการ', 1, '2017-06-30 08:24:14', NULL, NULL),
(3, 'ผู้สำเร็จการศึกษาด้านวิทยาศาสตร์และเทคโนโลยี', 1, '2017-06-30 08:24:14', NULL, NULL),
(4, 'ผลงานทำนุบำรุงศิลปวัฒนธรรม', 2, '2017-06-30 08:24:14', NULL, NULL),
(5, 'รายการค่าใช้จ่ายบุคลากรภาครัฐ ยกระดับคุณภาพการศึกษาและการเรียนรู้ตลอดชีวิต', 3, '2017-06-30 08:24:14', NULL, NULL),
(6, 'ผลงานวิจัยเพื่อถ่ายทอดเทคโนโลยี', 4, '2017-06-30 08:24:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_project_activities`
--

CREATE TABLE `plan_project_activities` (
  `project_activity_id` int(10) UNSIGNED NOT NULL,
  `project_activity_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อกิจกรรม',
  `project_activity_date` date DEFAULT NULL COMMENT 'วันเดือนปี',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_project_benefits`
--

CREATE TABLE `plan_project_benefits` (
  `project_benefit_id` int(10) UNSIGNED NOT NULL,
  `project_benefit_name` text COLLATE utf8_unicode_ci COMMENT 'รายการประโยชน์',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_project_budgets`
--

CREATE TABLE `plan_project_budgets` (
  `project_budget_id` int(10) UNSIGNED NOT NULL,
  `project_budget_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อรายการ',
  `project_budget_price` decimal(8,2) DEFAULT NULL COMMENT 'จำนวนเงิน',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `budget_activity` int(11) DEFAULT NULL COMMENT 'รหัสของกิจกรรม',
  `project_budget_net` decimal(10,2) DEFAULT NULL COMMENT 'ราคาสุทธิ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_project_periods`
--

CREATE TABLE `plan_project_periods` (
  `project_period_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `month_start` int(11) DEFAULT NULL COMMENT 'เดือนที่เริ่ม',
  `year_start` int(11) DEFAULT NULL COMMENT 'ปีที่เริ่ม',
  `month_end` int(11) DEFAULT NULL COMMENT 'เดือนที่สิ้นสุด',
  `year_end` int(11) DEFAULT NULL COMMENT 'เดือนที่สิ้นสุด'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_project_suggestions`
--

CREATE TABLE `plan_project_suggestions` (
  `project_suggestion_id` int(10) UNSIGNED NOT NULL,
  `project_suggestion_name` text COLLATE utf8_unicode_ci COMMENT 'รายการปัญหาอุปสรรค/ข้อเสนอแนะ',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสโครงการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_projects`
--

CREATE TABLE `plan_projects` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `project_name` text COLLATE utf8_unicode_ci COMMENT 'ชื่อโครงการ',
  `project_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'สภานภาพโครงการ',
  `nature_event` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ลักษณะของกิจกรรมที่ดำเนินการ',
  `place` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'สถานที่ดำเนินการ',
  `ready` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ความพร้อมของโครงการ',
  `budget_plan_id` int(10) UNSIGNED NOT NULL COMMENT 'รหัสจัดแผนเสนอของบประมาณ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `project_goal` text COLLATE utf8_unicode_ci COMMENT 'เป้าหมายในการดำเนินโครงการ',
  `project_money_approve` decimal(10,2) DEFAULT NULL COMMENT 'จำนวนเงินที่เสนอขอ',
  `upload_file` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ไฟล์แนบเอกสารที่ได้รับการอนุมัติ',
  `upload_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อไฟล์แนบเอกสารที่ได้รับการอนุมัติ',
  `money_received_first` decimal(10,2) DEFAULT NULL COMMENT 'ยอดเงินขออนุมัติใช้ครั้งแรก',
  `money_received_two` decimal(10,2) DEFAULT NULL COMMENT 'ยอดเงินขออนุมัติใช้ครั้งที่สอง',
  `date_received_first` datetime DEFAULT NULL COMMENT 'วันที่ขออนุมัติใช้ครั้งแรก',
  `date_received_two` datetime DEFAULT NULL COMMENT 'วันที่ขออนุมัติใช้ครั้งที่สอง',
  `pecen_received_first` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เปอร์เซนต์ขออนุมัติใช้ครั้งแรก',
  `pecen_received_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เปอร์เซนต์ขออนุมัติใช้ครั้งที่สอง',
  `file_received_first` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก',
  `file_received_two` varchar(180) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งที่สอง',
  `filename_received_first` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก',
  `filename_received_two` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งที่สอง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `position_id` int(10) UNSIGNED NOT NULL,
  `position_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ชื่อตำแหน่งทางวิชาการ',
  `position_abbreviation` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'คำย่อทางวิชาการ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `position_name`, `position_abbreviation`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'อาจารย์', 'อ.', '2017-06-30 08:24:04', NULL, NULL),
(2, 'ศาสตราจารย์', 'ศ.', '2017-06-30 08:24:04', NULL, NULL),
(3, 'รองศาสตราจารย์', 'รศ.', '2017-06-30 08:24:04', NULL, NULL),
(4, 'ผู้ช่วยศาสตราจารย์', 'ผศ.', '2017-06-30 08:24:04', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `branches_faculty_id_foreign` (`faculty_id`);

--
-- Indexes for table `budget_plans`
--
ALTER TABLE `budget_plans`
  ADD PRIMARY KEY (`budget_plan_id`),
  ADD KEY `budget_plans_personnel_id_foreign` (`personnel_id`),
  ADD KEY `budget_plans_product_id_foreign` (`product_id`),
  ADD KEY `budget_plans_plan_id_foreign` (`plan_id`),
  ADD KEY `budget_plans_income_id_foreign` (`income_id`),
  ADD KEY `budget_plans_income_type_id_foreign` (`income_type_id`),
  ADD KEY `budget_plans_income_group_id_foreign` (`income_group_id`),
  ADD KEY `budget_plans_key_project_id_foreign` (`key_project_id`),
  ADD KEY `budget_plans_objective_strategy_id_foreign` (`objective_strategy_id`),
  ADD KEY `budget_plans_strategy_id_foreign` (`strategy_id`),
  ADD KEY `budget_plans_goal_id_foreign` (`goal_id`),
  ADD KEY `budget_plans_objective_indicator_id_foreign` (`objective_indicator_id`),
  ADD KEY `budget_plans_issue_id_foreign` (`issue_id`),
  ADD KEY `budget_plans_federal_circuit_id_foreign` (`federal_circuit_id`),
  ADD KEY `budget_plans_vision_id_foreign` (`vision_id`);

--
-- Indexes for table `campuses`
--
ALTER TABLE `campuses`
  ADD PRIMARY KEY (`campus_id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`license_id`),
  ADD KEY `licenses_module_id_foreign` (`module_id`),
  ADD KEY `licenses_group_id_foreign` (`group_id`);

--
-- Indexes for table `masterplan_federal_circuits`
--
ALTER TABLE `masterplan_federal_circuits`
  ADD PRIMARY KEY (`federal_circuit_id`),
  ADD KEY `masterplan_federal_circuits_vision_id_foreign` (`vision_id`);

--
-- Indexes for table `masterplan_goals`
--
ALTER TABLE `masterplan_goals`
  ADD PRIMARY KEY (`goal_id`),
  ADD KEY `masterplan_goals_issue_id_foreign` (`issue_id`);

--
-- Indexes for table `masterplan_issues`
--
ALTER TABLE `masterplan_issues`
  ADD PRIMARY KEY (`issue_id`),
  ADD KEY `masterplan_issues_federal_circuit_id_foreign` (`federal_circuit_id`);

--
-- Indexes for table `masterplan_key_projects`
--
ALTER TABLE `masterplan_key_projects`
  ADD PRIMARY KEY (`key_project_id`),
  ADD KEY `masterplan_key_projects_strategy_id_foreign` (`strategy_id`);

--
-- Indexes for table `masterplan_objective_indicators`
--
ALTER TABLE `masterplan_objective_indicators`
  ADD PRIMARY KEY (`objective_indicator_id`),
  ADD KEY `masterplan_objective_indicators_goal_id_foreign` (`goal_id`);

--
-- Indexes for table `masterplan_objective_strategies`
--
ALTER TABLE `masterplan_objective_strategies`
  ADD PRIMARY KEY (`objective_strategy_id`),
  ADD KEY `masterplan_objective_strategies_strategy_id_foreign` (`strategy_id`);

--
-- Indexes for table `masterplan_strategies`
--
ALTER TABLE `masterplan_strategies`
  ADD PRIMARY KEY (`strategy_id`);

--
-- Indexes for table `masterplan_visions`
--
ALTER TABLE `masterplan_visions`
  ADD PRIMARY KEY (`vision_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `personnels`
--
ALTER TABLE `personnels`
  ADD PRIMARY KEY (`personnel_id`),
  ADD KEY `personnels_campus_id_foreign` (`campus_id`),
  ADD KEY `personnels_group_id_foreign` (`group_id`),
  ADD KEY `personnels_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `plan_building_utilizations`
--
ALTER TABLE `plan_building_utilizations`
  ADD PRIMARY KEY (`building_utilization_id`),
  ADD KEY `plan_building_utilizations_building_id_foreign` (`building_id`);

--
-- Indexes for table `plan_buildings`
--
ALTER TABLE `plan_buildings`
  ADD PRIMARY KEY (`building_id`),
  ADD KEY `plan_buildings_budget_plan_id_foreign` (`budget_plan_id`);

--
-- Indexes for table `plan_committees`
--
ALTER TABLE `plan_committees`
  ADD PRIMARY KEY (`committee_id`),
  ADD KEY `plan_committees_budget_plan_id_foreign` (`budget_plan_id`),
  ADD KEY `plan_committees_personnel_id_foreign` (`personnel_id`);

--
-- Indexes for table `plan_durable_article_items`
--
ALTER TABLE `plan_durable_article_items`
  ADD PRIMARY KEY (`durable_article_item_id`),
  ADD KEY `plan_durable_article_items_durable_article_id_foreign` (`durable_article_id`);

--
-- Indexes for table `plan_durable_articles`
--
ALTER TABLE `plan_durable_articles`
  ADD PRIMARY KEY (`durable_article_id`),
  ADD KEY `plan_durable_articles_budget_plan_id_foreign` (`budget_plan_id`);

--
-- Indexes for table `plan_durable_articles_allocates`
--
ALTER TABLE `plan_durable_articles_allocates`
  ADD PRIMARY KEY (`durable_articles_allocate_id`),
  ADD KEY `plan_durable_articles_allocates_durable_article_id_foreign` (`durable_article_id`);

--
-- Indexes for table `plan_income_details`
--
ALTER TABLE `plan_income_details`
  ADD PRIMARY KEY (`income_detail_id`),
  ADD KEY `plan_income_details_income_id_foreign` (`income_id`),
  ADD KEY `plan_income_details_income_type_id_foreign` (`income_type_id`);

--
-- Indexes for table `plan_income_groups`
--
ALTER TABLE `plan_income_groups`
  ADD PRIMARY KEY (`income_group_id`),
  ADD KEY `plan_income_groups_income_type_id_foreign` (`income_type_id`);

--
-- Indexes for table `plan_income_types`
--
ALTER TABLE `plan_income_types`
  ADD PRIMARY KEY (`income_type_id`);

--
-- Indexes for table `plan_incomes`
--
ALTER TABLE `plan_incomes`
  ADD PRIMARY KEY (`income_id`);

--
-- Indexes for table `plan_objectives`
--
ALTER TABLE `plan_objectives`
  ADD PRIMARY KEY (`objective_id`),
  ADD KEY `plan_objectives_budget_plan_id_foreign` (`budget_plan_id`);

--
-- Indexes for table `plan_plans`
--
ALTER TABLE `plan_plans`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `plan_products`
--
ALTER TABLE `plan_products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `plan_products_plan_id_foreign` (`plan_id`);

--
-- Indexes for table `plan_project_activities`
--
ALTER TABLE `plan_project_activities`
  ADD PRIMARY KEY (`project_activity_id`),
  ADD KEY `plan_project_activities_project_id_foreign` (`project_id`);

--
-- Indexes for table `plan_project_benefits`
--
ALTER TABLE `plan_project_benefits`
  ADD PRIMARY KEY (`project_benefit_id`),
  ADD KEY `plan_project_benefits_project_id_foreign` (`project_id`);

--
-- Indexes for table `plan_project_budgets`
--
ALTER TABLE `plan_project_budgets`
  ADD PRIMARY KEY (`project_budget_id`),
  ADD KEY `plan_project_budgets_project_id_foreign` (`project_id`);

--
-- Indexes for table `plan_project_periods`
--
ALTER TABLE `plan_project_periods`
  ADD PRIMARY KEY (`project_period_id`),
  ADD KEY `plan_project_periods_project_id_foreign` (`project_id`);

--
-- Indexes for table `plan_project_suggestions`
--
ALTER TABLE `plan_project_suggestions`
  ADD PRIMARY KEY (`project_suggestion_id`),
  ADD KEY `plan_project_suggestions_project_id_foreign` (`project_id`);

--
-- Indexes for table `plan_projects`
--
ALTER TABLE `plan_projects`
  ADD PRIMARY KEY (`project_id`),
  ADD KEY `plan_projects_budget_plan_id_foreign` (`budget_plan_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `branch_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `budget_plans`
--
ALTER TABLE `budget_plans`
  MODIFY `budget_plan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `campuses`
--
ALTER TABLE `campuses`
  MODIFY `campus_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `faculty_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `license_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `masterplan_federal_circuits`
--
ALTER TABLE `masterplan_federal_circuits`
  MODIFY `federal_circuit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masterplan_goals`
--
ALTER TABLE `masterplan_goals`
  MODIFY `goal_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `masterplan_issues`
--
ALTER TABLE `masterplan_issues`
  MODIFY `issue_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `masterplan_key_projects`
--
ALTER TABLE `masterplan_key_projects`
  MODIFY `key_project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `masterplan_objective_indicators`
--
ALTER TABLE `masterplan_objective_indicators`
  MODIFY `objective_indicator_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `masterplan_objective_strategies`
--
ALTER TABLE `masterplan_objective_strategies`
  MODIFY `objective_strategy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `masterplan_strategies`
--
ALTER TABLE `masterplan_strategies`
  MODIFY `strategy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `masterplan_visions`
--
ALTER TABLE `masterplan_visions`
  MODIFY `vision_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `personnels`
--
ALTER TABLE `personnels`
  MODIFY `personnel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `plan_building_utilizations`
--
ALTER TABLE `plan_building_utilizations`
  MODIFY `building_utilization_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_buildings`
--
ALTER TABLE `plan_buildings`
  MODIFY `building_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_committees`
--
ALTER TABLE `plan_committees`
  MODIFY `committee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_durable_article_items`
--
ALTER TABLE `plan_durable_article_items`
  MODIFY `durable_article_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_durable_articles`
--
ALTER TABLE `plan_durable_articles`
  MODIFY `durable_article_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_durable_articles_allocates`
--
ALTER TABLE `plan_durable_articles_allocates`
  MODIFY `durable_articles_allocate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_income_details`
--
ALTER TABLE `plan_income_details`
  MODIFY `income_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `plan_income_groups`
--
ALTER TABLE `plan_income_groups`
  MODIFY `income_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `plan_income_types`
--
ALTER TABLE `plan_income_types`
  MODIFY `income_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `plan_incomes`
--
ALTER TABLE `plan_incomes`
  MODIFY `income_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `plan_objectives`
--
ALTER TABLE `plan_objectives`
  MODIFY `objective_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_plans`
--
ALTER TABLE `plan_plans`
  MODIFY `plan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `plan_products`
--
ALTER TABLE `plan_products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `plan_project_activities`
--
ALTER TABLE `plan_project_activities`
  MODIFY `project_activity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_project_benefits`
--
ALTER TABLE `plan_project_benefits`
  MODIFY `project_benefit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_project_budgets`
--
ALTER TABLE `plan_project_budgets`
  MODIFY `project_budget_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_project_periods`
--
ALTER TABLE `plan_project_periods`
  MODIFY `project_period_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_project_suggestions`
--
ALTER TABLE `plan_project_suggestions`
  MODIFY `project_suggestion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan_projects`
--
ALTER TABLE `plan_projects`
  MODIFY `project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_faculty_id_foreign` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`faculty_id`);

--
-- Constraints for table `budget_plans`
--
ALTER TABLE `budget_plans`
  ADD CONSTRAINT `budget_plans_federal_circuit_id_foreign` FOREIGN KEY (`federal_circuit_id`) REFERENCES `masterplan_federal_circuits` (`federal_circuit_id`),
  ADD CONSTRAINT `budget_plans_goal_id_foreign` FOREIGN KEY (`goal_id`) REFERENCES `masterplan_goals` (`goal_id`),
  ADD CONSTRAINT `budget_plans_income_group_id_foreign` FOREIGN KEY (`income_group_id`) REFERENCES `plan_income_groups` (`income_group_id`),
  ADD CONSTRAINT `budget_plans_income_id_foreign` FOREIGN KEY (`income_id`) REFERENCES `plan_incomes` (`income_id`),
  ADD CONSTRAINT `budget_plans_income_type_id_foreign` FOREIGN KEY (`income_type_id`) REFERENCES `plan_income_types` (`income_type_id`),
  ADD CONSTRAINT `budget_plans_issue_id_foreign` FOREIGN KEY (`issue_id`) REFERENCES `masterplan_issues` (`issue_id`),
  ADD CONSTRAINT `budget_plans_key_project_id_foreign` FOREIGN KEY (`key_project_id`) REFERENCES `masterplan_key_projects` (`key_project_id`),
  ADD CONSTRAINT `budget_plans_objective_indicator_id_foreign` FOREIGN KEY (`objective_indicator_id`) REFERENCES `masterplan_objective_indicators` (`objective_indicator_id`),
  ADD CONSTRAINT `budget_plans_objective_strategy_id_foreign` FOREIGN KEY (`objective_strategy_id`) REFERENCES `masterplan_objective_strategies` (`objective_strategy_id`),
  ADD CONSTRAINT `budget_plans_personnel_id_foreign` FOREIGN KEY (`personnel_id`) REFERENCES `personnels` (`personnel_id`),
  ADD CONSTRAINT `budget_plans_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plan_plans` (`plan_id`),
  ADD CONSTRAINT `budget_plans_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `plan_products` (`product_id`),
  ADD CONSTRAINT `budget_plans_strategy_id_foreign` FOREIGN KEY (`strategy_id`) REFERENCES `masterplan_strategies` (`strategy_id`),
  ADD CONSTRAINT `budget_plans_vision_id_foreign` FOREIGN KEY (`vision_id`) REFERENCES `masterplan_visions` (`vision_id`);

--
-- Constraints for table `licenses`
--
ALTER TABLE `licenses`
  ADD CONSTRAINT `licenses_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `licenses_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`);

--
-- Constraints for table `masterplan_federal_circuits`
--
ALTER TABLE `masterplan_federal_circuits`
  ADD CONSTRAINT `masterplan_federal_circuits_vision_id_foreign` FOREIGN KEY (`vision_id`) REFERENCES `masterplan_visions` (`vision_id`);

--
-- Constraints for table `masterplan_goals`
--
ALTER TABLE `masterplan_goals`
  ADD CONSTRAINT `masterplan_goals_issue_id_foreign` FOREIGN KEY (`issue_id`) REFERENCES `masterplan_issues` (`issue_id`);

--
-- Constraints for table `masterplan_issues`
--
ALTER TABLE `masterplan_issues`
  ADD CONSTRAINT `masterplan_issues_federal_circuit_id_foreign` FOREIGN KEY (`federal_circuit_id`) REFERENCES `masterplan_federal_circuits` (`federal_circuit_id`);

--
-- Constraints for table `masterplan_key_projects`
--
ALTER TABLE `masterplan_key_projects`
  ADD CONSTRAINT `masterplan_key_projects_strategy_id_foreign` FOREIGN KEY (`strategy_id`) REFERENCES `masterplan_strategies` (`strategy_id`);

--
-- Constraints for table `masterplan_objective_indicators`
--
ALTER TABLE `masterplan_objective_indicators`
  ADD CONSTRAINT `masterplan_objective_indicators_goal_id_foreign` FOREIGN KEY (`goal_id`) REFERENCES `masterplan_goals` (`goal_id`);

--
-- Constraints for table `masterplan_objective_strategies`
--
ALTER TABLE `masterplan_objective_strategies`
  ADD CONSTRAINT `masterplan_objective_strategies_strategy_id_foreign` FOREIGN KEY (`strategy_id`) REFERENCES `masterplan_strategies` (`strategy_id`);

--
-- Constraints for table `personnels`
--
ALTER TABLE `personnels`
  ADD CONSTRAINT `personnels_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`branch_id`),
  ADD CONSTRAINT `personnels_campus_id_foreign` FOREIGN KEY (`campus_id`) REFERENCES `campuses` (`campus_id`),
  ADD CONSTRAINT `personnels_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`);

--
-- Constraints for table `plan_building_utilizations`
--
ALTER TABLE `plan_building_utilizations`
  ADD CONSTRAINT `plan_building_utilizations_building_id_foreign` FOREIGN KEY (`building_id`) REFERENCES `plan_buildings` (`building_id`);

--
-- Constraints for table `plan_buildings`
--
ALTER TABLE `plan_buildings`
  ADD CONSTRAINT `plan_buildings_budget_plan_id_foreign` FOREIGN KEY (`budget_plan_id`) REFERENCES `budget_plans` (`budget_plan_id`);

--
-- Constraints for table `plan_committees`
--
ALTER TABLE `plan_committees`
  ADD CONSTRAINT `plan_committees_budget_plan_id_foreign` FOREIGN KEY (`budget_plan_id`) REFERENCES `budget_plans` (`budget_plan_id`),
  ADD CONSTRAINT `plan_committees_personnel_id_foreign` FOREIGN KEY (`personnel_id`) REFERENCES `personnels` (`personnel_id`);

--
-- Constraints for table `plan_durable_article_items`
--
ALTER TABLE `plan_durable_article_items`
  ADD CONSTRAINT `plan_durable_article_items_durable_article_id_foreign` FOREIGN KEY (`durable_article_id`) REFERENCES `plan_durable_articles` (`durable_article_id`);

--
-- Constraints for table `plan_durable_articles`
--
ALTER TABLE `plan_durable_articles`
  ADD CONSTRAINT `plan_durable_articles_budget_plan_id_foreign` FOREIGN KEY (`budget_plan_id`) REFERENCES `budget_plans` (`budget_plan_id`);

--
-- Constraints for table `plan_durable_articles_allocates`
--
ALTER TABLE `plan_durable_articles_allocates`
  ADD CONSTRAINT `plan_durable_articles_allocates_durable_article_id_foreign` FOREIGN KEY (`durable_article_id`) REFERENCES `plan_durable_articles` (`durable_article_id`);

--
-- Constraints for table `plan_income_details`
--
ALTER TABLE `plan_income_details`
  ADD CONSTRAINT `plan_income_details_income_id_foreign` FOREIGN KEY (`income_id`) REFERENCES `plan_incomes` (`income_id`),
  ADD CONSTRAINT `plan_income_details_income_type_id_foreign` FOREIGN KEY (`income_type_id`) REFERENCES `plan_income_types` (`income_type_id`);

--
-- Constraints for table `plan_income_groups`
--
ALTER TABLE `plan_income_groups`
  ADD CONSTRAINT `plan_income_groups_income_type_id_foreign` FOREIGN KEY (`income_type_id`) REFERENCES `plan_income_types` (`income_type_id`);

--
-- Constraints for table `plan_objectives`
--
ALTER TABLE `plan_objectives`
  ADD CONSTRAINT `plan_objectives_budget_plan_id_foreign` FOREIGN KEY (`budget_plan_id`) REFERENCES `budget_plans` (`budget_plan_id`);

--
-- Constraints for table `plan_products`
--
ALTER TABLE `plan_products`
  ADD CONSTRAINT `plan_products_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plan_plans` (`plan_id`);

--
-- Constraints for table `plan_project_activities`
--
ALTER TABLE `plan_project_activities`
  ADD CONSTRAINT `plan_project_activities_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `plan_projects` (`project_id`);

--
-- Constraints for table `plan_project_benefits`
--
ALTER TABLE `plan_project_benefits`
  ADD CONSTRAINT `plan_project_benefits_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `plan_projects` (`project_id`);

--
-- Constraints for table `plan_project_budgets`
--
ALTER TABLE `plan_project_budgets`
  ADD CONSTRAINT `plan_project_budgets_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `plan_projects` (`project_id`);

--
-- Constraints for table `plan_project_periods`
--
ALTER TABLE `plan_project_periods`
  ADD CONSTRAINT `plan_project_periods_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `plan_projects` (`project_id`);

--
-- Constraints for table `plan_project_suggestions`
--
ALTER TABLE `plan_project_suggestions`
  ADD CONSTRAINT `plan_project_suggestions_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `plan_projects` (`project_id`);

--
-- Constraints for table `plan_projects`
--
ALTER TABLE `plan_projects`
  ADD CONSTRAINT `plan_projects_budget_plan_id_foreign` FOREIGN KEY (`budget_plan_id`) REFERENCES `budget_plans` (`budget_plan_id`);
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`` PROCEDURE `AddGeometryColumn` (`catalog` VARCHAR(64), `t_schema` VARCHAR(64), `t_name` VARCHAR(64), `geometry_column` VARCHAR(64), `t_srid` INT)  begin
  set @qwe= concat('ALTER TABLE ', t_schema, '.', t_name, ' ADD ', geometry_column,' GEOMETRY REF_SYSTEM_ID=', t_srid); PREPARE ls from @qwe; execute ls; deallocate prepare ls; end$$

CREATE DEFINER=`` PROCEDURE `DropGeometryColumn` (`catalog` VARCHAR(64), `t_schema` VARCHAR(64), `t_name` VARCHAR(64), `geometry_column` VARCHAR(64))  begin
  set @qwe= concat('ALTER TABLE ', t_schema, '.', t_name, ' DROP ', geometry_column); PREPARE ls from @qwe; execute ls; deallocate prepare ls; end$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
