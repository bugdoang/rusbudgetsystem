<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanStrategies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_strategies',function (Blueprint $table){
            $table->increments('strategy_id');
            $table->text('strategy_name')->nullable()->comment('ชื่อกลยุทธ์');
            $table->integer('goal_id')->unsigned()->comment('รหัสเป้าประสงค์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_strategies');
    }
}
