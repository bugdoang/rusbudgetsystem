<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_goals',function (Blueprint $table){
            $table->increments('goal_id');
            $table->text('goal_name')->nullable()->comment('ชื่อเป้าประสงค์');
            $table->integer('issue_id')->unsigned()->comment('รหัสประเด็นยุทธศาสตร์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_goals');
    }
}
