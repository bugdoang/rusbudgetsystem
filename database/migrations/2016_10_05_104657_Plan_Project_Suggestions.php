<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjectSuggestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_project_suggestions', function (Blueprint $table) {
            $table->increments('project_suggestion_id');
            $table->text('project_suggestion_name')->nullable()->comment('รายการปัญหาอุปสรรค/ข้อเสนอแนะ');
            $table->integer('project_id')->unsigned()->comment('รหัสโครงการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_project_suggestions');
    }
}
