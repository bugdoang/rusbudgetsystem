<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectbudgetAddActivityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_project_budgets',function (Blueprint $table){
            $table->dropColumn('project_budget_unit');
            $table->dropColumn('project_budget_total');
            $table->integer('budget_activity')->nullable()->comment('รหัสของกิจกรรม');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_project_budgets',function (Blueprint $table){
            $table->dropColumn('budget_activity');
            $table->integer('project_budget_unit')->nullable()->comment('จำนวนเงิน');
            $table->integer('project_budget_total')->nullable()->comment('ราคารวม');
        });    
    }
}
