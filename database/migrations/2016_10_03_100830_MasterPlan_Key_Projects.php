<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanKeyProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_key_projects',function (Blueprint $table){
            $table->increments('key_project_id');
            $table->text('key_project_name')->nullable()->comment('ชื่อโครงการสำคัญ');
            $table->integer('strategy_id')->unsigned()->comment('รหัสกลยุทธ์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_key_projects');
    }
}
