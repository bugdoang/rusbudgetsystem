<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjectActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_project_activities',function (Blueprint $table){
            $table->increments('project_activity_id');
            $table->text('project_activity_name')->nullable()->comment('ชื่อกิจกรรม');
            $table->date('project_activity_date',180)->nullable()->comment('วันเดือนปี');
            $table->integer('project_id')->unsigned()->comment('รหัสโครงการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_project_activities');
    }
}
