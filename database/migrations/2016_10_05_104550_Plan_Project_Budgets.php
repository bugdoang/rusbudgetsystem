<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjectBudgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_project_budgets', function (Blueprint $table) {
            $table->increments('project_budget_id');
            $table->text('project_budget_name')->nullable()->comment('ชื่อรายการ');
            $table->decimal('project_budget_price')->nullable()->comment('จำนวนเงิน');
            $table->integer('project_budget_unit')->nullable()->comment('หน่วย');
            $table->decimal('project_budget_total')->nullable()->comment('ราคารวม');
            $table->integer('project_id')->unsigned()->comment('รหัสโครงการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_project_budgets');
    }
}
