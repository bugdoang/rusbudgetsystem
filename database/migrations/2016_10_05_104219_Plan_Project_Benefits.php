<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjectBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_project_benefits',function (Blueprint $table){
            $table->increments('project_benefit_id');
            $table->text('project_benefit_name')->nullable()->comment('รายการประโยชน์');
            $table->integer('project_id')->unsigned()->comment('รหัสโครงการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_project_benefits');
    }
}
