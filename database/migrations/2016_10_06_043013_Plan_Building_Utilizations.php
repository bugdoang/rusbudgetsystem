<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanBuildingUtilizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_building_utilizations', function (Blueprint $table) {
            $table->increments('building_utilization_id');
            $table->text('building_utilization_name')->nullable()->comment('รายการการใช้ประโยชน์ของอาคาร');
            $table->integer('building_utilization_size')->nullable()->comment('ขนาด');
            $table->integer('building_utilization_number')->nullable()->comment('จำนวนห้อง');
            $table->integer('building_id')->unsigned()->comment('รหัสที่ดินและสิ่งก่อนสร้าง');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_building_utilizations');
    }
}
