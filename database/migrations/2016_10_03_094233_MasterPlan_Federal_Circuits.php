<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanFederalCircuits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_federal_circuits',function (Blueprint $table){
            $table->increments('federal_circuit_id');
            $table->text('federal_circuit_name')->nullable()->comment('ชื่อพันธกิจ');
            $table->integer('vision_id')->unsigned()->comment('รหัสวิสัยทัศน์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_federal_circuits');
    }
}
