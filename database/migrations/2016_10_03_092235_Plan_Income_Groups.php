<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanIncomeGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_income_groups',function (Blueprint $table){
            $table->increments('income_group_id');
            $table->text('income_group_name')->nullable()->comment('ชื่อหมวดแหล่งเงินได้');
            $table->integer('income_type_id')->unsigned()->comment('รหัสประเภทแหล่งเงินได้');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_income_groups');
    }
}
