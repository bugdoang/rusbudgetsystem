<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanBuildings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_buildings',function (Blueprint $table){
            $table->increments('building_id');
            $table->text('building_name')->nullable()->comment('ชื่อที่ดินและสิ่งก่อสร้าง');
            $table->integer('budget_plan_id')->unsigned()->comment('รหัสจัดแผนเสนอของบประมาณ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_buildings');
    }
}
