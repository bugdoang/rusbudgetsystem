<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_projects',function (Blueprint $table){
            $table->increments('project_id');
            $table->text('project_name')->nullable()->comment('ชื่อโครงการ');
            $table->string('project_status',100)->nullable()->comment('สภานภาพโครงการ');
            $table->string('nature_event',100)->nullable()->comment('ลักษณะของกิจกรรมที่ดำเนินการ');
            $table->string('place',100)->nullable()->comment('สถานที่ดำเนินการ');
            $table->string('ready',100)->nullable()->comment('ความพร้อมของโครงการ');
            $table->string('project_goal',100)->nullable()->comment('เป้าหมายในการดำเนินโครงการ');
            $table->integer('budget_plan_id')->unsigned()->comment('รหัสจัดแผนเสนอของบประมาณ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_projects');
    }
}
