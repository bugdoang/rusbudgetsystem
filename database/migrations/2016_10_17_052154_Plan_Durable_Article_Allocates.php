<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanDurableArticleAllocates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_durable_articles_allocates',function (Blueprint $table){
            $table->increments('durable_articles_allocate_id');
            $table->text('durable_articles_allocate_name')->nullable()->comment('รายการหากไม่ได้รับการจัดสรรจะเกิดผลเสียอย่างไร');
            $table->integer('durable_article_id')->unsigned()->comment('รหัสครุภัณฑ์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
