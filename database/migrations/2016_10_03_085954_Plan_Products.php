<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_products',function (Blueprint $table){
            $table->increments('product_id');
            $table->text('product_name')->nullable()->comment('ชื่อผลผลิต');
            $table->integer('plan_id')->unsigned()->comment('รหัสแผนงาน');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_products');
    }
}
