<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanIncomeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_income_details',function (Blueprint $table){
            $table->increments('income_detail_id');
            $table->integer('income_id')->unsigned()->comment('รหัสแหล่งเงินได้');
            $table->integer('income_type_id')->unsigned()->comment('รหัสประเภทแหล่งเงินได้');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_income_details');
    }
}
