<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Positions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions',function (Blueprint $table){
            $table->increments('position_id');
            $table->string('position_name',100)->nullable()->comment('ชื่อตำแหน่งทางวิชาการ');
            $table->string('position_abbreviation',20)->nullable()->comment('คำย่อทางวิชาการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('positions');
    }
}
