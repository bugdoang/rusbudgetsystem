<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanObjectiveIndicators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_objective_indicators',function (Blueprint $table){
            $table->increments('objective_indicator_id');
            $table->text('objective_indicator_name')->nullable()->comment('ชื่อตัวชี้วัดเป้าประสงค์');
            $table->integer('goal_id')->unsigned()->comment('รหัสเป้าประสงค์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_objective_indicators');
    }
}
