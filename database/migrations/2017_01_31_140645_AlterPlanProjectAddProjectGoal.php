<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectAddProjectGoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->dropColumn('project_goal');
        });

        Schema::table('plan_projects',function (Blueprint $table){
            $table->text('project_goal')->nullable()->comment('เป้าหมายในการดำเนินโครงการ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->dropColumn('project_goal');
        });   
        
        Schema::table('plan_projects',function (Blueprint $table){
            $table->string('project_goal')->nullable()->comment('เป้าหมายในการดำเนินโครงการ');
        });   
    }
}
