<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectAddUploadFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->string('upload_file',180)->comment('ไฟล์แนบเอกสารที่ได้รับการอนุมัติ');
            $table->string('upload_file_name',255)->comment('ชื่อไฟล์แนบเอกสารที่ได้รับการอนุมัติ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->dropColumn('upload_file');
            $table->dropColumn('upload_file_name');
        });
    }
}
