<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetPlanAddApprovalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_plans',function (Blueprint $table){
            $table->enum('approval_status',['','รอการตรวจสอบ','ไม่อนุมัติ','อนุมัติ'])->default('')->comment('สถานะการขออนุมัติใช้งบประมาณ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_plans',function (Blueprint $table){
            $table->dropColumn('approval_status');
        });
    }
}
