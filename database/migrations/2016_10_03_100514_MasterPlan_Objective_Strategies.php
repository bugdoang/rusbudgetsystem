<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanObjectiveStrategies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_objective_strategies',function (Blueprint $table){
            $table->increments('objective_strategy_id');
            $table->text('objective_strategy_name')->nullable()->comment('ชื่อตัวชี้วัดกลยุทธ์');
            $table->integer('strategy_id')->unsigned()->comment('รหัสกลยุทธ์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_objective_strategies');
    }
}
