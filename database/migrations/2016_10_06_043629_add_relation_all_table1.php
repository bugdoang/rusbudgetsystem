<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_project_activities',function ($table){
            $table->foreign('project_id')->references('project_id')->on('plan_projects');
        });
        Schema::table('plan_project_benefits',function ($table){
            $table->foreign('project_id')->references('project_id')->on('plan_projects');
        });
        Schema::table('plan_project_periods',function ($table){
            $table->foreign('project_id')->references('project_id')->on('plan_projects');
        });
        Schema::table('plan_project_budgets',function ($table){
            $table->foreign('project_id')->references('project_id')->on('plan_projects');
        });
        Schema::table('plan_project_suggestions',function ($table){
            $table->foreign('project_id')->references('project_id')->on('plan_projects');
        });
        Schema::table('plan_durable_article_items',function ($table){
            $table->foreign('durable_article_id')->references('durable_article_id')->on('plan_durable_articles');
        });
        Schema::table('plan_building_utilizations',function ($table){
            $table->foreign('building_id')->references('building_id')->on('plan_buildings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_project_activities',function ($table){
            $table->dropForeign('plan_project_activities_project_id_foreign');
        });
        Schema::table('plan_project_benefits',function ($table){
            $table->dropForeign('plan_project_benefits_project_id_foreign');
        });
        Schema::table('plan_project_periods',function ($table){
            $table->dropForeign('plan_project_periods_project_id_foreign');
        });
        Schema::table('plan_project_budgets',function ($table){
            $table->dropForeign('plan_project_budgets_project_id_foreign');
        });
        Schema::table('plan_project_suggestions',function ($table){
            $table->dropForeign('plan_project_suggestions_project_id_foreign');
        });
        Schema::table('plan_durable_article_items',function ($table){
            $table->dropForeign('plan_durable_article_items_durable_article_id_foreign');
        });
        Schema::table('plan_building_utilizations',function ($table){
            $table->dropForeign('plan_building_utilizations_building_id_foreign');
        });
    }
}
