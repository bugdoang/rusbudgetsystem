<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanObjectives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_objectives',function (Blueprint $table){
            $table->increments('objective_id');
            $table->text('objective_name')->nullable()->comment('ชื่อวัตถุประสงค์');
            $table->integer('budget_plan_id')->unsigned()->comment('รหัสจัดแผนเสนอของบประมาณ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_objectives');
    }
}
