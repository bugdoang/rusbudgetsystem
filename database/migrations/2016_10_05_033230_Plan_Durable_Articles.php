<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanDurableArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_durable_articles',function (Blueprint $table){
            $table->increments('durable_article_id');
            $table->text('durable_article_name')->nullable()->comment('ชื่อครุภัณฑ์');
            $table->string('durable_article_place',255)->nullable()->comment('สถานที่นำไปใช้');
            $table->string('durable_article_frequency',500)->nullable()->comment('ความถี่ในการใช้งาน');
            $table->string('durable_article_purchase',255)->nullable()->comment('แผนจัดซื้อ');
            $table->string('durable_article_purchase_month',2)->nullable()->comment('แผนจัดซื้อ-ในเดือน');
            $table->integer('durable_article_purchase_year')->nullable()->comment('แผนจัดซื้อ-ในปี พ.ศ.');
            $table->string('durable_article_install_month',2)->nullable()->comment('แผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง-ในเดือน');
            $table->integer('durable_article_install_year')->nullable()->comment('แผนจัดซื้อ-ส่งมอบพร้อมติดตั้ง-ในปี พ.ศ.');
            $table->string('durable_article_spend_month',2)->nullable()->comment('แผนการใช้จ่าย-ในเดือน');
            $table->integer('durable_article_spend_year')->nullable()->comment('แผนการใช้จ่าย-ในปี พ.ศ.');
            $table->string('durable_article_list',255)->nullable()->comment('แบบรายการครุภัณฑ์');
            $table->decimal('durable_article_budget_total',10,2)->nullable()->comment('ราคารวม');
            $table->decimal('durable_article_budget_vat',10,2)->nullable()->comment('ภาษี');
            $table->decimal('durable_article_budget_net',10,2)->nullable()->comment('ราคาสุทธิ');
            $table->integer('budget_plan_id')->unsigned()->comment('รหัสจัดแผนเสนอของบประมาณ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_durable_articles');
    }
}
