<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetPlanAddCampus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_plans',function (Blueprint $table){
            $table->string('campus',100)->nullable()->comment('ศูนย์พื้นที่');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_plans',function (Blueprint $table){
            $table->dropColumn('campus');
        });    }
}
