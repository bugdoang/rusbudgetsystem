<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanArticleAddMoneyApprove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->decimal('money_approve',10,2)->nullable()->comment('จำนวนเงินที่เสนอขอ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->dropColumn('money_approve');
        });
    }
}
