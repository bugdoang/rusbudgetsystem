<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanIncomeTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_income_types',function (Blueprint $table){
            $table->increments('income_type_id');
            $table->text('income_type_name')->nullable()->comment('ชื่อประเภทแหล่งเงินได้');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_income_types');
    }
}
