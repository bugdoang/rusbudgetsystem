<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanArticleAddUploadFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->string('upload_file',180)->comment('ไฟล์แนบเอกสารที่ได้รับการอนุมัติ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->dropColumn('upload_file');
        });
    }
}
