<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectAddProjectMoneyApprove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->decimal('project_money_approve',10,2)->nullable()->comment('จำนวนเงินที่เสนอขอ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->dropColumn('project_money_approve');
        });
    }
}
