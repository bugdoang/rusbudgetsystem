<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Personnels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels',function (Blueprint $table){
            $table->increments('personnel_id');
            $table->string('personnel_picture',180)->comment('รูปภาพบุคลากร');
            $table->string('title_name',30)->nullable()->comment('คำนำหน้าชื่อ');
            $table->string('first_name',100)->nullable()->comment('ชื่อ-นามสกุลบุคลากร');
            $table->string('mobile',50)->nullable()->comment('เบอร์โทรศัพท์');
            $table->string('username',180)->comment('ชื่อผู้ใช้ ใช้ในการล็อกอินเข้าสู่ระบบ');
            $table->string('password',200)->comment('รหัสผ่านที่เข้ารหัสเรียบร้อยแล้ว');
            $table->integer('position_id')->unsigned()->comment('รหัสตำแหน่งทางวิชาการ');
            $table->integer('branch_id')->unsigned()->comment('รหัสสาขา');
            $table->integer('group_id')->unsigned()->comment('รหัสกลุ่มการใช้งาน');
            $table->integer('campus_id')->unsigned()->comment('รหัสศูนย์พื้นที่');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personnels');
    }
}
