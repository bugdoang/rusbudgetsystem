<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BudgetPlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_plans',function (Blueprint $table){
            $table->increments('budget_plan_id');
            $table->string('budget_plan_category',50)->nullable()->comment('ประเภทแผน');
            $table->text('reason')->nullable()->comment('เหตุผลความจำเป็น');
            $table->integer('fiscal_year')->unsigned()->comment('ปีงบประมาณ');
            $table->integer('faculty')->unsigned()->comment('คณะ/หน่วยงาน');
            $table->string('semester',50)->nullable()->comment('รอบภาคการศึกษา');
            $table->integer('personnel_id')->unsigned()->comment('รหัสบุคลากร(เขียนแผนเสนอของบ)');
            $table->integer('product_id')->unsigned()->comment('รหัสผลผลิต');
            $table->integer('plan_id')->unsigned()->comment('รหัสแผนงาน');
            $table->integer('income_id')->unsigned()->comment('รหัสแหล่งเงินได้');
            $table->integer('income_type_id')->unsigned()->comment('รหัสประเภทแหล่งเงินได้');
            $table->integer('income_group_id')->nullable()->unsigned()->comment('รหัสหมวดแหล่งเงินได้');
            $table->integer('key_project_id')->unsigned()->comment('รหัสโครงการสำคัญ');
            $table->integer('objective_strategy_id')->unsigned()->comment('รหัสตัวชี้วัดกลยุทธ์');
            $table->integer('strategy_id')->unsigned()->comment('รหัสกลยุทธ์');
            $table->integer('goal_id')->unsigned()->comment('รหัสเป้าประสงค์');
            $table->integer('objective_indicator_id')->unsigned()->comment('รหัสตัวชี้วัดเป้าประสงค์');
            $table->integer('issue_id')->unsigned()->comment('รหัสประเด็นยุทธศาสตร์');
            $table->integer('federal_circuit_id')->unsigned()->comment('รหัสพันธกิจ');
            $table->integer('vision_id')->unsigned()->comment('รหัสวิสัยทัศน์');
            $table->dateTime('date_approve',180)->nullable()->comment('วันที่เสนอขอ');
            $table->integer('amount')->unsigned()->comment('จำนวนเงิน');
            $table->enum('status',['ไม่ได้รับการอนุมัติ','รอการตรวจสอบ','อนุมัติแล้ว'])->default('รอการตรวจสอบ')->comment('สถานะจัดแผนเสนอของบประมาณ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_plans');
    }
}
