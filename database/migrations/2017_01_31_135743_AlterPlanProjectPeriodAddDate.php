<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectPeriodAddDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_project_periods',function (Blueprint $table){
            $table->dropColumn('project_period_start');
            $table->dropColumn('project_period_stop');
            $table->integer('month_start')->nullable()->comment('เดือนที่เริ่ม');
            $table->integer('year_start')->nullable()->comment('ปีที่เริ่ม');
            $table->integer('month_end')->nullable()->comment('เดือนที่สิ้นสุด');
            $table->integer('year_end')->nullable()->comment('เดือนที่สิ้นสุด');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_project_periods',function (Blueprint $table){
            $table->dropColumn('month_start');
            $table->dropColumn('year_start');
            $table->dropColumn('month_end');
            $table->dropColumn('year_end');
            $table->date('project_period_start')->nullable()->comment('เดือนที่เริ่ม');
            $table->date('project_period_stop')->nullable()->comment('ปีที่เริ่ม');
        });    }
}
