<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Licenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses',function (Blueprint $table){
            $table->increments('license_id');
            $table->integer('module_id')->unsigned()->comment('รหัสโมดูล');
            $table->integer('group_id')->unsigned()->comment('รหัสกลุ่ม');
            $table->enum('view',['Y','N'])->comment('เรียกดู');
            $table->enum('edit',['Y','N'])->comment('แก้ไข');
            $table->enum('add',['Y','N'])->comment('เพิ่ม');
            $table->enum('delete',['Y','N'])->comment('ลบ');
            $table->enum('export',['Y','N'])->comment('รายงาน');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('licenses');
    }
}
