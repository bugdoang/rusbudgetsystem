<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_income_details',function ($table){
            $table->foreign('income_id')->references('income_id')->on('plan_incomes');
            $table->foreign('income_type_id')->references('income_type_id')->on('plan_income_types');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_income_details',function ($table){
            $table->dropForeign('plan_income_details_income_id_foreign');
            $table->dropForeign('plan_income_details_income_type_id_foreign');
        });

    }
}
