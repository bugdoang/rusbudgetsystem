<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanProjectPeriods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_project_periods', function (Blueprint $table) {
            $table->increments('project_period_id');
            $table->date('project_period_start', 180)->nullable()->comment('ด./พ.ศ. ที่เริ่ม');
            $table->date('project_period_stop', 180)->nullable()->comment('ด./พ.ศ. ที่สิ้นสุด');
            $table->integer('project_id')->unsigned()->comment('รหัสโครงการ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_project_periods');
    }
}
