<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanCommittees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_committees',function (Blueprint $table){
            $table->increments('committee_id');
            $table->string('committee_name',100)->nullable()->comment('ชื่อ-นามสกุลผู้รับผิดชอบ');
            $table->enum('committee_status',['ผู้รับผิดชอบหลัก','กรรมการ'])->comment('สถานะผู้รับผิดชอบ');
            $table->integer('budget_plan_id')->unsigned()->comment('รหัสจัดแผนเสนอของบประมาณ');
            $table->integer('personnel_id')->unsigned()->comment('รหัสบุคลากร');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_committees');
    }
}
