<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectAddMoneyReceivedFirst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->decimal('money_received_first',10,2)->nullable()->comment('ยอดเงินขออนุมัติใช้ครั้งแรก');
            $table->decimal('money_received_two',10,2)->nullable()->comment('ยอดเงินขออนุมัติใช้ครั้งที่สอง');
            $table->dateTime('date_received_first')->nullable()->comment('วันที่ขออนุมัติใช้ครั้งแรก');
            $table->dateTime('date_received_two')->nullable()->comment('วันที่ขออนุมัติใช้ครั้งที่สอง');
            $table->string('pecen_received_first')->nullable()->comment('เปอร์เซนต์ขออนุมัติใช้ครั้งแรก');
            $table->string('pecen_received_two')->nullable()->comment('เปอร์เซนต์ขออนุมัติใช้ครั้งที่สอง');
            $table->string('file_received_first',180)->comment('ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก');
            $table->string('file_received_two',180)->comment('ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งที่สอง');
            $table->string('filename_received_first',255)->comment('ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก');
            $table->string('filename_received_two',255)->comment('ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งที่สอง');
        });
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->decimal('money_received',10,2)->nullable()->comment('ยอดเงินขออนุมัติใช้ครั้งแรก');
            $table->dateTime('date_received')->nullable()->comment('วันที่ขออนุมัติใช้ครั้งแรก');
            $table->integer('pecen_received')->nullable()->comment('เปอร์เซนต์ขออนุมัติใช้ครั้งแรก');
            $table->string('file_received',180)->comment('ไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก');
            $table->string('filename_received',255)->comment('ชื่อไฟล์แนบเอกสารขออนุมัติใช้งบครั้งแรก');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_projects',function (Blueprint $table){
            $table->dropColumn('money_received_first');
            $table->dropColumn('money_received_two');
            $table->dropColumn('date_received_first');
            $table->dropColumn('date_received_two');
            $table->dropColumn('pecen_received_first');
            $table->dropColumn('pecen_received_two');
            $table->dropColumn('file_received_first');
            $table->dropColumn('file_received_two');
            $table->dropColumn('filename_received_first');
            $table->dropColumn('filename_received_two');
        });
        Schema::table('plan_durable_articles',function (Blueprint $table){
            $table->dropColumn('money_received');
            $table->dropColumn('date_received');
            $table->dropColumn('pecen_received');
            $table->dropColumn('file_received');
            $table->dropColumn('filename_received');
        });
    }
}
