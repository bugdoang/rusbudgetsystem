<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanDurableArticleItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_durable_article_items', function (Blueprint $table) {
            $table->increments('durable_article_item_id');
            $table->text('durable_article_item_name')->nullable()->comment('ชื่อรายการครุภัณฑ์');
            $table->string('durable_article_unit',100)->nullable()->comment('หน่วยนับ');
            $table->integer('durable_article_qty')->nullable()->comment('จำนวน');
            $table->decimal('durable_article_priceunit',10,2)->nullable()->comment('ราคา/หน่วย');
            $table->decimal('durable_article_num',10,2)->nullable()->comment('จำนวนเงิน');
            $table->enum('durable_article_status',['Y','N'])->default('N')->comment('สถานะครุภัณฑ์');
            $table->enum('durable_article_reason',['Y','N'])->default('N')->comment('เหตุผลสรุป');
            $table->text('durable_article_item_detail')->nullable()->comment('มาตรฐานและรายละเอียดคุณลักษณะ');
            $table->integer('durable_article_id')->unsigned()->comment('รหัสครุภัณฑ์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_durable_article_items');
    }
}
