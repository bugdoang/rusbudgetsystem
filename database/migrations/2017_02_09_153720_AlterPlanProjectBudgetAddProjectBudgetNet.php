<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanProjectBudgetAddProjectBudgetNet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_project_budgets',function (Blueprint $table){
            $table->decimal('project_budget_net',10,2)->nullable()->comment('ราคาสุทธิ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_project_budgets',function (Blueprint $table){
            $table->dropColumn('project_budget_net');
        });
    }
}
