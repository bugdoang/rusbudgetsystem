<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_durable_articles_allocates',function ($table){
            $table->foreign('durable_article_id')->references('durable_article_id')->on('plan_durable_articles');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_durable_articles_allocates',function ($table){
            $table->dropForeign('plan_durable_articles_allocates_durable_article_id_foreign');
        });

    }
}
