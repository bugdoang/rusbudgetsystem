<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('licenses',function ($table){
            $table->foreign('module_id')->references('module_id')->on('modules');
            $table->foreign('group_id')->references('group_id')->on('groups');
        });
        Schema::table('branches',function ($table){
            $table->foreign('faculty_id')->references('faculty_id')->on('faculties');
        });
        Schema::table('personnels',function ($table){
            $table->foreign('campus_id')->references('campus_id')->on('campuses');
            $table->foreign('group_id')->references('group_id')->on('groups');
            $table->foreign('branch_id')->references('branch_id')->on('branches');
        });
        Schema::table('plan_products',function ($table){
            $table->foreign('plan_id')->references('plan_id')->on('plan_plans');
        });
        Schema::table('plan_income_groups',function ($table){
            $table->foreign('income_type_id')->references('income_type_id')->on('plan_income_types');
        });
        Schema::table('masterplan_key_projects',function ($table){
            $table->foreign('strategy_id')->references('strategy_id')->on('masterplan_strategies');
        });
        Schema::table('masterplan_objective_strategies',function ($table){
            $table->foreign('strategy_id')->references('strategy_id')->on('masterplan_strategies');
        });
        Schema::table('masterplan_goals',function ($table){
            $table->foreign('issue_id')->references('issue_id')->on('masterplan_issues');
        });
        Schema::table('masterplan_objective_indicators',function ($table){
            $table->foreign('goal_id')->references('goal_id')->on('masterplan_goals');
        });
        Schema::table('masterplan_issues',function ($table){
            $table->foreign('federal_circuit_id')->references('federal_circuit_id')->on('masterplan_federal_circuits');
        });
        Schema::table('masterplan_federal_circuits',function ($table){
            $table->foreign('vision_id')->references('vision_id')->on('masterplan_visions');
        });
        Schema::table('budget_plans',function ($table){
            $table->foreign('personnel_id')->references('personnel_id')->on('personnels');
            $table->foreign('product_id')->references('product_id')->on('plan_products');
            $table->foreign('plan_id')->references('plan_id')->on('plan_plans');
            $table->foreign('income_id')->references('income_id')->on('plan_incomes');
            $table->foreign('income_type_id')->references('income_type_id')->on('plan_income_types');
            $table->foreign('income_group_id')->references('income_group_id')->on('plan_income_groups');
            $table->foreign('key_project_id')->references('key_project_id')->on('masterplan_key_projects');
            $table->foreign('objective_strategy_id')->references('objective_strategy_id')->on('masterplan_objective_strategies');
            $table->foreign('strategy_id')->references('strategy_id')->on('masterplan_strategies');
            $table->foreign('goal_id')->references('goal_id')->on('masterplan_goals');
            $table->foreign('objective_indicator_id')->references('objective_indicator_id')->on('masterplan_objective_indicators');
            $table->foreign('issue_id')->references('issue_id')->on('masterplan_issues');
            $table->foreign('federal_circuit_id')->references('federal_circuit_id')->on('masterplan_federal_circuits');
            $table->foreign('vision_id')->references('vision_id')->on('masterplan_visions');
        });
        Schema::table('plan_objectives', function ($table) {
            $table->foreign('budget_plan_id')->references('budget_plan_id')->on('budget_plans');
        });
        Schema::table('plan_committees', function ($table) {
            $table->foreign('budget_plan_id')->references('budget_plan_id')->on('budget_plans');
            $table->foreign('personnel_id')->references('personnel_id')->on('personnels');
        });
        Schema::table('plan_projects', function ($table) {
            $table->foreign('budget_plan_id')->references('budget_plan_id')->on('budget_plans');
        });
        Schema::table('plan_durable_articles', function ($table) {
            $table->foreign('budget_plan_id')->references('budget_plan_id')->on('budget_plans');
        });
        Schema::table('plan_buildings', function ($table) {
            $table->foreign('budget_plan_id')->references('budget_plan_id')->on('budget_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('licenses',function ($table){
            $table->dropForeign('licenses_module_id_foreign');
            $table->dropForeign('licenses_group_id_foreign');
        });
        Schema::table('branches',function ($table){
            $table->dropForeign('branches_faculty_id_foreign');
        });
        Schema::table('personnels',function ($table){
            $table->dropForeign('personnels_campus_id_foreign');
            $table->dropForeign('personnels_group_id_foreign');
            $table->dropForeign('personnels_branch_id_foreign');
        });
        Schema::table('plan_products',function ($table){
            $table->dropForeign('plan_products_plan_id_foreign');
        });
        Schema::table('plan_income_groups',function ($table){
            $table->dropForeign('plan_income_groups_income_type_id_foreign');
        });
        Schema::table('masterplan_key_projects',function ($table){
            $table->dropForeign('masterplan_key_projects_strategy_id_foreign');
        });
        Schema::table('masterplan_objective_strategies',function ($table){
            $table->dropForeign('masterplan_objective_strategies_strategy_id_foreign');
        });
        Schema::table('masterplan_goals',function ($table){
            $table->dropForeign('masterplan_goals_issue_id_foreign');
        });
        Schema::table('masterplan_objective_indicators',function ($table){
            $table->dropForeign('masterplan_objective_indicators_goal_id_foreign');
        });
        Schema::table('masterplan_issues',function ($table){
            $table->dropForeign('masterplan_issues_federal_circuit_id_foreign');
        });
        Schema::table('masterplan_federal_circuits',function ($table){
            $table->dropForeign('masterplan_federal_circuits_vision_id_foreign');
        });
        Schema::table('budget_plans',function ($table){
            $table->dropForeign('budget_plans_personnel_id_foreign');
            $table->dropForeign('budget_plans_product_id_foreign');
            $table->dropForeign('budget_plans_plan_id_foreign');
            $table->dropForeign('budget_plans_income_id_foreign');
            $table->dropForeign('budget_plans_income_type_id_foreign');
            $table->dropForeign('budget_plans_income_group_id_foreign');
            $table->dropForeign('budget_plans_key_project_id_foreign');
            $table->dropForeign('budget_plans_objective_strategy_id_foreign');
            $table->dropForeign('budget_plans_strategy_id_foreign');
            $table->dropForeign('budget_plans_goal_id_foreign');
            $table->dropForeign('budget_plans_objective_indicator_id_foreign');
            $table->dropForeign('budget_plans_issue_id_foreign');
            $table->dropForeign('budget_plans_federal_circuit_id_foreign');
            $table->dropForeign('budget_plans_vision_id_foreign');
        });
        Schema::table('plan_objectives', function ($table) {
            $table->dropForeign('plan_objectives_budget_plan_id_foreign');
        });
        Schema::table('plan_committees', function ($table) {
            $table->dropForeign('plan_committees_budget_plan_id_foreign');
            $table->dropForeign('plan_committees_personnel_id_foreign');
        });
        Schema::table('plan_projects', function ($table) {
            $table->dropForeign('plan_projects_budget_plan_id_foreign');
        });
        Schema::table('plan_durable_articles', function ($table) {
            $table->dropForeign('plan_durable_articles_budget_plan_id_foreign');
        });
        Schema::table('plan_buildings', function ($table) {
            $table->dropForeign('plan_buildings_budget_plan_id_foreign');
        });
    }
}
