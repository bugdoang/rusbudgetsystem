<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Branches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches',function (Blueprint $table){
            $table->increments('branch_id');
            $table->string('branch_name',100)->nullable()->comment('ชื่อสาขา');
            $table->integer('faculty_id')->unsigned()->comment('รหัสคณะ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branches');
    }
}
