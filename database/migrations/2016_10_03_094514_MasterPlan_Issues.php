<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterPlanIssues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterplan_issues',function (Blueprint $table){
            $table->increments('issue_id');
            $table->text('issue_name')->nullable()->comment('ชื่อประเด็นยุทธศาสตร์');
            $table->integer('federal_circuit_id')->unsigned()->comment('รหัสพันธกิจ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masterplan_issues');
    }
}
