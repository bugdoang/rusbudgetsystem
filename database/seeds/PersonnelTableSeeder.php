<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonnelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personnels = [];
        $personnels[] = [
            'personnel_id'=>1,
            'personnel_picture'=>'/uploads/default/profile-img.jpg',
            'title_name'=>'นางสาว',
            'first_name'=>'ภัสราภรณ์ คำนวนฤทธิ์',
            'mobile'=>'0904230818',
            'username'=>'chompoo',
            'password'=>\Illuminate\Support\Facades\Hash::make('1234'),
            'position_id'=>1,
            'branch_id'=>1,
            'group_id'=>1,
            'campus_id'=>1
        ];

        $personnels[] = [
            'personnel_id'=>2,
            'personnel_picture'=>'/uploads/default/profile-img2.jpg',
            'title_name'=>'นาย',
            'first_name'=>'วัชรชัย คูหาสวัสดิ์',
            'mobile'=>'0895697977',
            'username'=>'aoae',
            'password'=>\Illuminate\Support\Facades\Hash::make('1234'),
            'position_id'=>1,
            'branch_id'=>1,
            'group_id'=>3,
            'campus_id'=>1

        ];
        DB::table('personnels')->delete();
        DB::table('personnels')->insert($personnels);
    }
}
