<?php

use Illuminate\Database\Seeder;

class InitdataSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Config::set('excel.import.startRow', 1);
        \Config::set('excel.import.heading', 'slugged_with_count');


        //read vision
        $items = Excel::selectSheetsByIndex(0)->load(dirname(__FILE__).'/plan2.xlsx')->get();
        if(!empty($items)){
            $visions = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $visions[] = [
                        'vision_id' => $item['id'],
                        'vision_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($visions)){
                DB::table('masterplan_visions')
                    ->insert($visions);
            }
        }



        //read federal_circuits
        $items = Excel::selectSheetsByIndex(1)->load(dirname(__FILE__).'/plan2.xlsx')->get();
        if(!empty($items)){
            $federal_circuits = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $federal_circuits[] = [
                        'federal_circuit_id' => $item['id'],
                        'federal_circuit_name' => $item['name'],
                        'vision_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($federal_circuits)){
                DB::table('masterplan_federal_circuits')
                    ->insert($federal_circuits);
            }
        }



        //read issues
        $items = Excel::selectSheetsByIndex(2)->load(dirname(__FILE__).'/plan2.xlsx')->get();
        if(!empty($items)){
            $issues = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $issues[] = [
                        'issue_id' => $item['id'],
                        'issue_name' => $item['name'],
                        'federal_circuit_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($issues)){
                DB::table('masterplan_issues')
                    ->insert($issues);
            }
        }



        //read goals
        $items = Excel::selectSheetsByIndex(3)->load(dirname(__FILE__).'/plan2.xlsx')->get();
        if(!empty($items)){
            $goals = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $goals[] = [
                        'goal_id' => $item['id'],
                        'goal_name' => $item['name'],
                        'issue_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($goals)){
                DB::table('masterplan_goals')
                    ->insert($goals);
            }
        }
    }
}
