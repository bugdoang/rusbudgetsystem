<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '1024M');
        $this->call(InitdataSeeder::class);
        $this->call(LicenseTableSeeder::class);
        $this->call(PersonnelTableSeeder::class);
        $this->call(InitdataSeeder1::class);
        $this->call(InitdataSeeder2::class);
        $this->call(InitdataSeeder3::class);
        $this->call(InitdataSeeder4::class);

    }
}
