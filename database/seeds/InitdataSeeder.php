<?php

use Illuminate\Database\Seeder;

class InitdataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Config::set('excel.import.startRow', 1);
        \Config::set('excel.import.heading', 'slugged_with_count');

        //read faculty
        $items = Excel::selectSheetsByIndex(0)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $faculties = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $faculties[] = [
                        'faculty_id' => $item['id'],
                        'faculty_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($faculties)){
                DB::table('faculties')
                    ->insert($faculties);
            }
        }

        //read campus
        $items = Excel::selectSheetsByIndex(1)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $campuses = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $campuses[] = [
                        'campus_id' => $item['id'],
                        'campus_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($campuses)){
                DB::table('campuses')
                    ->insert($campuses);
            }
        }



        //read branch
        $items = Excel::selectSheetsByIndex(2)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $branches = [];
            foreach ($items as $item){
                if(!empty($item['id'])) {
                    $branches[] = [
                        'branch_id' => $item['id'],
                        'branch_name' => $item['name'],
                        'faculty_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($branches)){
                DB::table('branches')
                    ->insert($branches);
            }
        }



        //read position
        $items = Excel::selectSheetsByIndex(3)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $positions = [];
            foreach ($items as $item){
                if(!empty($item['id'])) {
                    $positions[] = [
                        'position_id' => $item['id'],
                        'position_name' => $item['name'],
                        'position_abbreviation' => $item['title'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($positions)){
                DB::table('positions')
                    ->insert($positions);
            }
        }



        //read group
        $items = Excel::selectSheetsByIndex(4)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $groups = [];
            foreach ($items as $item){
                if(!empty($item['id'])) {
                    $groups[] = [
                        'group_id' => $item['id'],
                        'group_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($groups)){
                DB::table('groups')
                    ->insert($groups);
            }
        }



        //read module
        $items = Excel::selectSheetsByIndex(5)->load(dirname(__FILE__).'/russheet.xlsx')->get();
        if(!empty($items)){
            $modules = [];
            foreach ($items as $item){
                if(!empty($item['id'])) {
                    $modules[] = [
                        'module_id' => $item['id'],
                        'module_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($modules)){
                DB::table('modules')
                    ->insert($modules);
            }
        }
    }
}
