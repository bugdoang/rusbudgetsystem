<?php

use Illuminate\Database\Seeder;

class InitdataSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Config::set('excel.import.startRow', 1);
        \Config::set('excel.import.heading', 'slugged_with_count');


        //read objective_indicators
        $items = Excel::selectSheetsByIndex(0)->load(dirname(__FILE__).'/plan3.xlsx')->get();
        if(!empty($items)){
            $objective_indicators = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $objective_indicators[] = [
                        'objective_indicator_id' => $item['id'],
                        'objective_indicator_name' => $item['name'],
                        'goal_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($objective_indicators)){
                DB::table('masterplan_objective_indicators')
                    ->insert($objective_indicators);
            }
        }



        //read strategies
        $items = Excel::selectSheetsByIndex(1)->load(dirname(__FILE__).'/plan3.xlsx')->get();
        if(!empty($items)){
            $strategies = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $strategies[] = [
                        'strategy_id' => $item['id'],
                        'strategy_name' => $item['name'],
                        'goal_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($strategies)){
                DB::table('masterplan_strategies')
                    ->insert($strategies);
            }
        }



        //read objective_strategies
        $items = Excel::selectSheetsByIndex(2)->load(dirname(__FILE__).'/plan3.xlsx')->get();
        if(!empty($items)){
            $objective_strategies = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $objective_strategies[] = [
                        'objective_strategy_id' => $item['id'],
                        'objective_strategy_name' => $item['name'],
                        'strategy_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($objective_strategies)){
                DB::table('masterplan_objective_strategies')
                    ->insert($objective_strategies);
            }
        }



        //read key_projects
        $items = Excel::selectSheetsByIndex(3)->load(dirname(__FILE__).'/plan3.xlsx')->get();
        if(!empty($items)){
            $key_projects = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $key_projects[] = [
                        'key_project_id' => $item['id'],
                        'key_project_name' => $item['name'],
                        'strategy_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($key_projects)){
                DB::table('masterplan_key_projects')
                    ->insert($key_projects);
            }
        }

    }
}
