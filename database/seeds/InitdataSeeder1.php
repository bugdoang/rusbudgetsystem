<?php

use Illuminate\Database\Seeder;

class InitdataSeeder1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Config::set('excel.import.startRow', 1);
        \Config::set('excel.import.heading', 'slugged_with_count');



        //read product
        $items = Excel::selectSheetsByIndex(0)->load(dirname(__FILE__).'/plan1.xlsx')->get();
        //print_r($items);exit;
        if(!empty($items))
        {
            $plans = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $plans[] = [
                        'plan_id' => $item['id'],
                        'plan_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($plans)){
                DB::table('plan_plans')
                    ->insert($plans);
            }
        }


        //read plan
        $items = Excel::selectSheetsByIndex(1)->load(dirname(__FILE__).'/plan1.xlsx')->get();
        if(!empty($items))

        {
            $products = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $products[] = [
                        'product_id' => $item['id'],
                        'product_name' => $item['name'],
                        'plan_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($products)){
                DB::table('plan_products')
                    ->insert($products);
            }
        }


    }
}
