<?php

use Illuminate\Database\Seeder;

class LicenseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $licenses = [];
        $licenses[] = [
            'license_id'=>1,
            'module_id'=>1,
            'group_id'=>1,
            'view'=>'Y',
            'edit'=>'Y',
            'add'=>'Y',
            'delete'=>'Y',
            'export'=>'Y'
        ];

        $licenses[] = [
            'license_id'=>2,
            'module_id'=>1,
            'group_id'=>2,
            'view'=>'Y',
            'edit'=>'N',
            'add'=>'Y',
            'delete'=>'N',
            'export'=>'Y'
        ];

        $licenses[] = [
            'license_id'=>3,
            'module_id'=>1,
            'group_id'=>2,
            'view'=>'Y',
            'edit'=>'Y',
            'add'=>'N',
            'delete'=>'Y',
            'export'=>'Y'
        ];


        DB::table('licenses')->delete();
        DB::table('licenses')->insert($licenses);
    }
}
