<?php

use Illuminate\Database\Seeder;

class InitdataSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Config::set('excel.import.startRow', 1);
        \Config::set('excel.import.heading', 'slugged_with_count');


        //read incomes
        $items = Excel::selectSheetsByIndex(0)->load(dirname(__FILE__).'/plan4.xlsx')->get();
        if(!empty($items)){
            $incomes = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $incomes[] = [
                        'income_id' => $item['id'],
                        'income_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($incomes)){
                DB::table('plan_incomes')
                    ->insert($incomes);
            }
        }

        //read income_types
        $items = Excel::selectSheetsByIndex(1)->load(dirname(__FILE__).'/plan4.xlsx')->get();
        if(!empty($items)){
            $income_types = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $income_types[] = [
                        'income_type_id' => $item['id'],
                        'income_type_name' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($income_types)){
                DB::table('plan_income_types')
                    ->insert($income_types);
            }
        }

        //read income_groups
        $items = Excel::selectSheetsByIndex(2)->load(dirname(__FILE__).'/plan4.xlsx')->get();
        if(!empty($items)){
            $income_groups = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $income_groups[] = [
                        'income_group_id' => $item['id'],
                        'income_group_name' => $item['name'],
                        'income_type_id' => $item['ref_id'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($income_groups)){
                DB::table('plan_income_groups')
                    ->insert($income_groups);
            }
        }



        //read income_details
        $items = Excel::selectSheetsByIndex(3)->load(dirname(__FILE__).'/plan4.xlsx')->get();
        if(!empty($items)){
            $income_details = [];
            foreach ($items as $item){ //**$items แทนชื่อ table , $item แทนชื่อใน Excel**
                if(!empty($item['id'])) {
                    $income_details[] = [
                        'income_detail_id'  => $item['id'],
                        'income_id'         => $item['id_income'],
                        'income_type_id'    => $item['id_income_type'],
                        'created_at'        => date('Y-m-d H:i:s')
                    ];
                }
            }
            if(!empty($income_details)){
                DB::table('plan_income_details')
                    ->insert($income_details);
            }
        }
    }
}
